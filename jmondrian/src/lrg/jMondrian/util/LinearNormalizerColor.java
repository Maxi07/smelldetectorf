/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.util;

import lrg.jMondrian.commands.AbstractNumericalCommand;

import java.util.List;
import java.awt.*;


public class LinearNormalizerColor extends AbstractNumericalCommand {

    private double min,max;
    private AbstractNumericalCommand command;

    public LinearNormalizerColor(List context, AbstractNumericalCommand cmd) {
        command = cmd;
        cmd.setReceiver(context.get(0));
        min = max = cmd.execute();
        for(int i = 1; i < context.size(); i++) {
            cmd.setReceiver(context.get(i));
            double tmp = cmd.execute();
            if(tmp > max) max = tmp;
            if(tmp < min) min = tmp;
        }
    }

    public double execute() {
        command.setReceiver(receiver);
        double aux = command.execute();
        double pas = max - min;
        int r,g,b;
        r = g = b =(int)(255 -  ((aux - min)*  255 / pas));
        return new Color(r,g,b).getRGB();
    }

    public String toString() {
        command.setReceiver(receiver);
        return command.toString();
    }
    
}
