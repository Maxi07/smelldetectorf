/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.figures;

import lrg.jMondrian.painters.*;
import lrg.jMondrian.view.ViewRendererInterface;
import lrg.jMondrian.commands.AbstractNumericalCommand;

public class Node extends AbstractFigure {

    private double absoluteX = 0, absoluteY = 0;
    protected AbstractNodePainter painter;

    public Node(Object entity, AbstractNodePainter painter){
        super(entity);
        this.painter = painter;
    }

    public double getWidth() {
        return painter.getWidth(entity);
    }

    public double getHeight() {
        return painter.getHeight(entity);
    }

    public double getRelativeX() {
        return painter.getX(entity);
    }

    public double getRelativeY() {
        return painter.getY(entity);
    }

    public double getAbsoluteX() {
        return absoluteX;
    }

    public double getAbsoluteY() {
        return absoluteY;
    }

    public void show(ViewRendererInterface renderer, double xBias, double yBias, boolean last) {
        absoluteX = this.getRelativeX() + xBias;
        absoluteY = this.getRelativeY() + yBias;
        painter.paint(renderer, entity, xBias, yBias,last);
    }

    public void translateTo(AbstractNumericalCommand xCmd, AbstractNumericalCommand yCmd) {
        painter.x(xCmd).y(yCmd);
    }
}