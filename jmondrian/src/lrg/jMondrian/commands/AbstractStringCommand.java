/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.commands;

public abstract class AbstractStringCommand {

    protected Object receiver;
    private String name;

    public AbstractStringCommand() {
        this("");
    }

    public AbstractStringCommand(String prop) {
        name = prop;
    }

    public final AbstractStringCommand setReceiver(Object receiver) {
        this.receiver = receiver;
        return this;
    }

    public String toString() {
        if(name.equals("")) {
            return "";
        } else {
            return name + ":" + execute();
        }
    }

    public abstract String execute();

}
