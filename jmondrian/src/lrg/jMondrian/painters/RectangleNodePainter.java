/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.painters;

import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.commands.AbstractStringCommand;
import lrg.jMondrian.util.CommandColor;
import lrg.jMondrian.view.ViewRendererInterface;

import java.awt.*;
import java.util.*;
import java.util.List;

public class RectangleNodePainter extends AbstractNodePainter {

    private static class Rectangle {
        int x1,y1,x2,y2;
        Rectangle(int x1, int y1, int x2, int y2) {
            this.x1 = x1; this.y1 = y1;
            this.x2 = x2; this.y2 = y2;
        }
    }

    private boolean withBorders;
    private boolean cumulativeOutline;
    private List<Rectangle> list = new ArrayList<Rectangle>();

    public RectangleNodePainter(boolean withBorders){
        this(0,0,withBorders);
    }

    public RectangleNodePainter(final double width, final double height,boolean withBorders, boolean cumulativeOutline) {
        this(width,height,withBorders);
        this.cumulativeOutline = cumulativeOutline;
    }

    public RectangleNodePainter(final double width, final double height, boolean withBorders) {

        this.withBorders = withBorders;
        this.cumulativeOutline = false;

        nameCommand = new AbstractStringCommand() {
            public String execute() {
                return "";
            }
        };

        widthCommand = new AbstractNumericalCommand() {
            public double execute() {
                return width;
            }
        };

        heightCommand = new AbstractNumericalCommand() {
            public double execute() {
                return height;
            }
        };

        xCommand = yCommand = new AbstractNumericalCommand() {
            public double execute() {
                return 0;
            }
        };

        textCommand = new AbstractStringCommand() {
            public String execute() {
                return "";
            }
        };

        frameColorCommand = new AbstractNumericalCommand() {
            public double execute() {
                return Color.BLACK.getRGB();
            }
        };

       colorCommand = CommandColor.WHITE;
    }

    public void paint(ViewRendererInterface window, Object entity, double x1Bias, double y1Bias, boolean last){

        double color, frameColor = 0;
        boolean invisibleBorder = false;

        try {
            colorCommand.setReceiver(entity);
            color = colorCommand.execute();
        } catch (CommandColor.InvisibleException e) {
            return;
        }

        try {
            frameColorCommand.setReceiver(entity);
            frameColor = frameColorCommand.execute();
        } catch (CommandColor.InvisibleException e) {
            invisibleBorder = true;
        }
        
        double x = x1Bias;
        double y = y1Bias;

        widthCommand.setReceiver(entity);
        double width = widthCommand.execute();

        heightCommand.setReceiver(entity);
        double height = heightCommand.execute();

        xCommand.setReceiver(entity);
        x += xCommand.execute();

        yCommand.setReceiver(entity);
        y += yCommand.execute();

        nameCommand.setReceiver(entity);
        
        if(withBorders) {
            if(!invisibleBorder) {
                window.getShapeFactory().addRectangle(entity,this.toString(),(int)x,(int)y,(int)width,(int)height,(int)color, (int)frameColor);
            } else {
                window.getShapeFactory().addRectangle(entity,this.toString(),(int)x,(int)y,(int)width,(int)height,(int)color, false);                
            }
        } else {
            window.getShapeFactory().addRectangle(entity,this.toString(),(int)x,(int)y,(int)width,(int)height,(int)color, false);
        }

        textCommand.setReceiver(entity);
        window.getShapeFactory().addText(entity,this.toString(),textCommand.execute(), (int)x + 5, (int)y + 12, (int)Color.BLACK.getRGB());

        if(cumulativeOutline) {
            list.add(new Rectangle((int)x,(int)y,(int)(x + width),(int)(y + height)));
        }

        if(cumulativeOutline && last) {
            List<Rectangle> tmpOrder = new ArrayList<Rectangle>();
            List<Integer> px = new LinkedList<Integer>();
            List<Integer> py = new LinkedList<Integer>();
            tmpOrder.addAll(list);
            Collections.sort(tmpOrder,new Comparator<Rectangle>() {
                public int compare(Rectangle a, Rectangle b) {
                    if(a.y1 < b.y1) return -1;
                    if(a.y1 > b.y1) return 1;
                    if(a.x1 < b.x1) return -1;
                    if(a.x1 > b.x1) return 1;
                    return 0;
                }
            });
            for(int i = 0; i < tmpOrder.size(); i++) {
                if(i == 0) {
                    px.add(tmpOrder.get(0).x1);
                    py.add(tmpOrder.get(0).y2+1);
                    px.add(tmpOrder.get(0).x1);
                    py.add(tmpOrder.get(0).y1);
                } else if(tmpOrder.get(i-1).y1 != tmpOrder.get(i).y1) {
                    if(tmpOrder.get(i-1).y2 + 1 != tmpOrder.get(i).y1) {
                        px.add(tmpOrder.get(i-1).x2);
                        py.add(py.get(py.size()-1));
                        px.add(tmpOrder.get(i-1).x2);
                        py.add(tmpOrder.get(i-1).y2+1);
                        window.getShapeFactory().addPolyLine(null,null,px,py);
                        px.clear();
                        py.clear();
                        px.add(tmpOrder.get(i).x1);
                        py.add(tmpOrder.get(i).y2+1);
                        px.add(tmpOrder.get(i).x1);
                        py.add(tmpOrder.get(i).y1);                        
                    } else {
                        int j = i + 1;
                        while(j < tmpOrder.size() && tmpOrder.get(j).y1 == tmpOrder.get(i).y1) j++;
                        if(j - 1 < tmpOrder.size() && (tmpOrder.get(j-1).x2 < px.get(0) || tmpOrder.get(i-1).x2 < tmpOrder.get(i).x1)) {
                            px.add(tmpOrder.get(i-1).x2);
                            py.add(py.get(py.size()-1));
                            px.add(tmpOrder.get(i-1).x2);
                            py.add(tmpOrder.get(i-1).y2+1);
                            window.getShapeFactory().addPolyLine(null,null,px,py);
                            px.clear();
                            py.clear();
                            px.add(tmpOrder.get(i).x1);
                            py.add(tmpOrder.get(i).y2+1);
                            px.add(tmpOrder.get(i).x1);
                            py.add(tmpOrder.get(i).y1);
                        } else {
                            px.add(tmpOrder.get(i-1).x2);
                            py.add(py.get(py.size()-1));
                            px.add(tmpOrder.get(i-1).x2);
                            py.add(tmpOrder.get(i-1).y2+1);
                            px.add(0,tmpOrder.get(i).x1);
                            py.add(0,tmpOrder.get(i).y1);
                            px.add(0,tmpOrder.get(i).x1);
                            py.add(0,tmpOrder.get(i).y2+1);
                        }
                    }
                }
                if(i == tmpOrder.size()-1) {
                    px.add(0,tmpOrder.get(i).x2);
                    py.add(0,tmpOrder.get(i).y2+1);
                    px.add(0,tmpOrder.get(i).x2);
                    py.add(0,tmpOrder.get(i).y1);
                }
            }
            window.getShapeFactory().addPolyLine(null,null,px,py);
            list.clear();
        }
    }

}
