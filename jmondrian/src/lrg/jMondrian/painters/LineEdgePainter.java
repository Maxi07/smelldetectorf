/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.painters;

import lrg.jMondrian.commands.AbstractEntityCommand;
import lrg.jMondrian.util.CommandColor;
import lrg.jMondrian.view.ViewRendererInterface;

public class LineEdgePainter extends AbstractEdgePainter {

    private boolean oriented;

    public LineEdgePainter(AbstractEntityCommand fromCommand, AbstractEntityCommand toCommand){
        this(fromCommand,toCommand,false);
    }

    public LineEdgePainter(AbstractEntityCommand fromCommand, AbstractEntityCommand toCommand, boolean oriented){
        super(fromCommand,toCommand);
        this.oriented = oriented;
    }

    public void paint(ViewRendererInterface window, Object entity, double x1Bias, double y1Bias, double x2Bias, double y2Bias) {
        try {
            colorCommand.setReceiver(entity);
            fromCommand.setReceiver(entity);
            toCommand.setReceiver(entity);
            nameCommand.setReceiver(entity);
            double color = colorCommand.execute();
            window.getShapeFactory().addLine(entity,this.toString(),(int)x1Bias,(int)y1Bias,(int)x2Bias,(int)y2Bias,(int)color, oriented);
        } catch(CommandColor.InvisibleException e) {
            return;
        }
    }

}
