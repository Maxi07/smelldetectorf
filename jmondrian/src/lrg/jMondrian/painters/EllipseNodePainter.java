/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.painters;

import lrg.jMondrian.view.ViewRendererInterface;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.commands.AbstractStringCommand;
import lrg.jMondrian.util.CommandColor;

import java.awt.Color;

public class EllipseNodePainter extends AbstractNodePainter {

    private boolean withBorders = true;

    public EllipseNodePainter(boolean withBorders){
        this(0, 0,withBorders);
    }

    public EllipseNodePainter(final double width, final double height, boolean withBorders){

        this.withBorders = withBorders;

        nameCommand = new AbstractStringCommand() {
            public String execute() {
                return "";
            }
        };

        widthCommand = new AbstractNumericalCommand() {
            public double execute() {
                return width;
            }
        };

        heightCommand = new AbstractNumericalCommand() {
            public double execute() {
                return height;
            }
        };

        xCommand = yCommand = new AbstractNumericalCommand() {
            public double execute() {
                return 0;
            }
        };

        textCommand = new AbstractStringCommand() {
            public String execute() {
                return "";
            }
        };

        frameColorCommand = new AbstractNumericalCommand() {
            public double execute() {
                return Color.BLACK.getRGB();
            }
        };

        colorCommand = CommandColor.WHITE;
    }

    public void paint(ViewRendererInterface window, Object entity, double x1Bias, double y1Bias, boolean last) {

        double color, frameColor = 0;
        boolean invisibleBorder = false;
        
        try {
            colorCommand.setReceiver(entity);
            color = colorCommand.execute();
        } catch (CommandColor.InvisibleException e) {
            return;
        }

        try {
            frameColorCommand.setReceiver(entity);
            frameColor = frameColorCommand.execute();
        } catch (CommandColor.InvisibleException e) {
            invisibleBorder = true;
        }

        double x = x1Bias;
        double y = y1Bias;

        widthCommand.setReceiver(entity);
        double width = widthCommand.execute();

        heightCommand.setReceiver(entity);
        double height = heightCommand.execute();

        xCommand.setReceiver(entity);
        x += xCommand.execute();

        yCommand.setReceiver(entity);
        y += yCommand.execute();

        nameCommand.setReceiver(entity);

        if(withBorders) {
            if(!invisibleBorder)
                window.getShapeFactory().addEllipse(entity, this.toString(),(int)x,(int)y,(int)width,(int)height,(int)color, (int)frameColor);
            else {
                window.getShapeFactory().addEllipse(entity, this.toString(),(int)x,(int)y,(int)width,(int)height,(int)color, false);                
            }
        } else {
            window.getShapeFactory().addEllipse(entity, this.toString(),(int)x,(int)y,(int)width,(int)height,(int)color, false);
        }

        textCommand.setReceiver(entity);
        window.getShapeFactory().addText(entity, this.toString(), textCommand.execute(), (int)x, (int)y - 5, Color.BLACK.getRGB());
    }

}
