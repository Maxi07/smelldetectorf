/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.layouts;

import java.util.List;
import java.util.HashMap;

import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.Node;
import lrg.jMondrian.figures.EdgeFigure;

public abstract class AbstractLayout {

    public final double[] applyLayout(List<Node> nodeList, List<EdgeFigure> edgeList) {
        return distributeNodes(nodeList,edgeList);
    }

    protected abstract double[] distributeNodes(List<Node> nodeList, List<EdgeFigure> edgeList);

    protected static class ControlXY extends AbstractNumericalCommand {
        private HashMap<Object,Double> m = new HashMap<Object,Double>();
        public double execute() {
            return m.get(receiver);
        }
        public void link(Node n, double pos) {
            m.put(n.getEntity(),pos);
        }
    }
}
