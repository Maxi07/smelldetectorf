/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.layouts;

import lrg.jMondrian.figures.Node;
import lrg.jMondrian.figures.EdgeFigure;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;


public class HTreeLayout extends AbstractLayout{

    private double xDist, yDist;
       private int maxX = 0, maxY = 0;

       public HTreeLayout(){
           this(5,5);
       }

       public HTreeLayout(double xDist, double yDist) {
           this.xDist = xDist;
           this.yDist = yDist;
       }

       private List<Node> getRootNodes(List<Node> nodeList, List<EdgeFigure> edgeList){
           List<Node> rootNodes = new ArrayList<Node>();
           Iterator<Node> itNode = nodeList.iterator();
           Iterator<EdgeFigure> itEdge;
           Node node;
           EdgeFigure edge;
           boolean isRoot;

           while(itNode.hasNext()){
               node = itNode.next();
               isRoot = true;
               itEdge = edgeList.iterator();
               while(itEdge.hasNext()){
                   edge = itEdge.next();
                   if(edge.getFrom().equals(node)){
                       isRoot = false;
                   }
               }
               if(isRoot){
                   rootNodes.add(node);
               }
           }

           return rootNodes;
       }

       private List<Node> getChildren(Node node, List<EdgeFigure> edgeList){
           List<Node> childrenNodes = new ArrayList<Node>();
           Iterator<EdgeFigure> itEdge= edgeList.iterator();
           EdgeFigure edge;

           while(itEdge.hasNext()){
               edge = itEdge.next();
               if(edge.getTo().equals(node)){
                  // edge.setConnectionStyle(EdgeFigure.DOWN_MIDDLE,true);
                   //edge.setConnectionStyle(EdgeFigure.UP_MIDDLE,false);
                   edge.setConnectionStyle(EdgeFigure.MIDDLE,true);
                   childrenNodes.add(edge.getFrom());
               }
           }

           return childrenNodes;
       }

       private double layoutLayer(List<Node> nodeList,List<EdgeFigure> edgeList, double xTo, double yTo, double betweenX, double betweenY, ControlXY xCmd, ControlXY yCmd) {

           Iterator<Node> itNode = nodeList.iterator();
           List<Node> children;
           Node node;
           double treeHeight = 0, childrenX = 0, xPoz, yPoz, childrenMiddle = 0, height = 0;

           xPoz = betweenX;
           yPoz = betweenY;

           while(itNode.hasNext()) {
               node = itNode.next();
               children = getChildren(node, edgeList);
               childrenX = xPoz + node.getWidth() +  xDist;
               if(childrenX >= maxX) maxX = (int) childrenX;
               treeHeight = layoutLayer(children,edgeList,xPoz + node.getWidth(),yPoz/2,childrenX,yPoz,xCmd,yCmd);
               if(node.getHeight() > treeHeight) treeHeight = node.getHeight();
               childrenMiddle = yPoz + (treeHeight / 2);

               xCmd.link(node,xPoz);
               yCmd.link(node,childrenMiddle - node.getHeight() /2);
               node.translateTo(xCmd,yCmd);

               yPoz += treeHeight +  yDist;
               if(yPoz >= maxY) maxY = (int)yPoz;
           }


           height =  yPoz - betweenY - yDist;
           if((height / 2) >= yTo) yTo += (height / 2) +  betweenY - yTo;
           else yTo += betweenY;

           return height;
       }

       public double[] distributeNodes(List<Node> nodeList, List<EdgeFigure> edgeList) {

           List<Node> rootNodes;
           maxX = 0;
           maxY = 0;
           ControlXY xCmd = new ControlXY();
           ControlXY yCmd = new ControlXY();

           rootNodes = getRootNodes(nodeList, edgeList);
           layoutLayer(rootNodes, edgeList, 0, 0, xDist, yDist,xCmd,yCmd);

           if(nodeList.size() > 0) {
               nodeList.get(0).translateTo(xCmd,yCmd);
           }

           double[] rez = new double[2];
           rez[0] = maxX;
           rez[1] = maxY;

           return rez;
       }

} 