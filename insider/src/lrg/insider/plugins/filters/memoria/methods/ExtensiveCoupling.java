package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.02.2005
 * Time: 13:47:26
 * To change this template use File | Settings | File Templates.
 */
public class ExtensiveCoupling extends FilteringRule {
    public ExtensiveCoupling() {
        super(new Descriptor("Extensive Coupling", "method"));
    }

    private boolean functionCallsManyFromManyUnrelatedClasses(AbstractEntityInterface aMethod) {
        FilteringRule highIntensity = new FilteringRule("CINT", ">", "method", Threshold.SMemCap);
        FilteringRule highDispersion = new FilteringRule("CDISP", ">", "method", Threshold.HALF);

        return new AndComposedFilteringRule(highIntensity, highDispersion).applyFilter(aMethod);

    }

    public boolean applyFilter(AbstractEntityInterface aMethod) {
        boolean deepNestingOfCalls = new FilteringRule("MAXNESTING", ">", "method", Threshold.SHALLOW).applyFilter(aMethod);

        return deepNestingOfCalls && functionCallsManyFromManyUnrelatedClasses(aMethod);
    }
}
