package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.memoria.core.Method;

public class ShortcircuitingMethod extends FilteringRule {
    public ShortcircuitingMethod() {
        super(new Descriptor("Shortcircuiting Method", "method"));
    }

    private boolean isOverridingConcreteMethod(AbstractEntityInterface anEntity) {
        FilteringRule notAbstract = new NotComposedFilteringRule(new IsAbstract());
        return anEntity.uses("methods overriden").applyFilter(notAbstract).size() > 0;
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof Method == false) return false;

        if (isOverridingConcreteMethod(anEntity) == false) return false;
        GroupEntity myCallers = (GroupEntity) anEntity.getGroup("operations calling me").
                belongsTo("class");

        AbstractEntity scopeClass = anEntity.belongsTo("class");
        GroupEntity allDescendants = scopeClass.getGroup("all descendants").union(scopeClass);
        
        return myCallers.exclude(allDescendants).size() > 0;
    }
}
