package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class ShouldBeProtected extends FilteringRule {
    public ShouldBeProtected() {
        super(new Descriptor("should be protected", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface aMethod) {
    	if(new IsPublicMethod().applyFilter(aMethod) == false) 
    		return false;
    	if(new ShouldBePrivate().applyFilter(aMethod)) 
    		return false;
    	
    	AbstractEntity containerClass = aMethod.belongsTo("class");

    	GroupEntity methodsFromMyClass = containerClass.contains("method group");
    	GroupEntity methodsFromMyDescendants = containerClass.getGroup("all descendants").contains("method group");
    	GroupEntity methodsFromMyAncestors = containerClass.getGroup("all ancestors").contains("method group");
    	GroupEntity allRelatedMethods = methodsFromMyClass.union(methodsFromMyAncestors).union(methodsFromMyDescendants);

    	
    	GroupEntity directExternalCallers = aMethod.getGroup("operations calling me").exclude(methodsFromMyClass).exclude(methodsFromMyDescendants);    	
    	GroupEntity indirectExternalCallers = aMethod.getGroup("methods overriden").getGroup("operations calling me").exclude(allRelatedMethods);
    	    	    	 	
    	
    	return (directExternalCallers.size() + indirectExternalCallers.size() == 0);
    	
    }
}
