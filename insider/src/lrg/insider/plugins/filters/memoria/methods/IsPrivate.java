package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class IsPrivate extends FilteringRule {

    public IsPrivate() {
        super(new Descriptor("is private", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {

        if (anEntity instanceof lrg.memoria.core.Method == false) return false;
        return ((lrg.memoria.core.Method) anEntity).isPrivate();

    }

}
