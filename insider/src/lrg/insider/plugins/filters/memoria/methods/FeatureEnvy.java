package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.11.2004
 * Time: 18:20:01
 * To change this template use File | Settings | File Templates.
 */
public class FeatureEnvy extends FilteringRule {
    public FeatureEnvy() {
        super(new Descriptor("Feature Envy", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        FilteringRule highAID = new FilteringRule("ATFD", ">", "method", Threshold.FEW);
        FilteringRule lowDispersion = new FilteringRule("FDP", "<=", "method", Threshold.FEW);
        FilteringRule lowLocality = new FilteringRule("LAA", "<", "method", Threshold.ONE_THIRD);

        return new AndComposedFilteringRule(lowLocality,
                new AndComposedFilteringRule(highAID, lowDispersion)).applyFilter(anEntity);

    }
}
