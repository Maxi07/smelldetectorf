package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class IsSpecialization extends FilteringRule {
    public IsSpecialization() {
        super(new Descriptor("is specialization", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Method == false) return false;

        GroupEntity methodsOverriden = anEntity.uses("methods overriden");

        double countMethodsOverriden = methodsOverriden.size();

        if (countMethodsOverriden == 0) return false;

        return anEntity.uses("operations called").intersect(methodsOverriden).size() > 0;
    }
}
