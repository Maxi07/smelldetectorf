package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 18.10.2004
 * Time: 16:03:24
 * To change this template use File | Settings | File Templates.
 */
public class IsProtected extends FilteringRule {
    public IsProtected() {
        super(new Descriptor("is protected", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Method == false) return false;

        return ((lrg.memoria.core.Method) anEntity).isProtected();
    }
}
