package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class ShouldBePrivate extends FilteringRule {
    public ShouldBePrivate() {
        super(new Descriptor("should be private", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface aMethod) {
    	if(new IsPrivate().applyFilter(aMethod)) return false;
    	AbstractEntity containerClass = aMethod.belongsTo("class");

    	GroupEntity methodsFromMyClass = containerClass.contains("method group");
    	
    	GroupEntity directExternalCallers = aMethod.getGroup("operations calling me").exclude(methodsFromMyClass);
    	GroupEntity indirectCallers = aMethod.getGroup("methods overriden").getGroup("operations calling me");
    	    	    	 	
    	
    	return (directExternalCallers.size() + indirectCallers.size() == 0);    	
    }
}
