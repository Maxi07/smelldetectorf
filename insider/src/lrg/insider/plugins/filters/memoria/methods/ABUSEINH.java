package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.classes.RefusedParentBequestInterface;
import lrg.insider.plugins.filters.memoria.classes.TraditionBreaker;

public class ABUSEINH extends PropertyComputer {
    public ABUSEINH() {
        super("ABUSEINH", "Abusive Inheritance degree", "class", "numerical");
    }


    public ResultEntity compute(AbstractEntityInterface anEntity) {
        FilteringRule refusedParentBequest = new RefusedParentBequestInterface();
        FilteringRule traditionBreaker = new TraditionBreaker();

        int counter = 0;


        counter += 100 * shortcircuitedAbstraction(anEntity);
        if (refusedParentBequest.applyFilter(anEntity)) counter += 10;
        if (traditionBreaker.applyFilter(anEntity)) counter += 1;
        return new ResultEntity(counter);
    }

    private int shortcircuitedAbstraction(AbstractEntityInterface anEntity) {
        return anEntity.contains("method group").applyFilter("Shortcircuiting Method").size();
    }

}

