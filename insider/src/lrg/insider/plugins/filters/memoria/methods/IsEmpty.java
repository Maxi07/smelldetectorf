package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.Body;
import lrg.memoria.core.Method;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 19.02.2005
 * Time: 12:14:33
 * To change this template use File | Settings | File Templates.
 */
public class IsEmpty extends FilteringRule {
    public IsEmpty() {
        super(new Descriptor("is empty", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof Method == false) return false;

        Body theBody = ((Method) anEntity).getBody();
        if (theBody == null) return false;

        String theCode = theBody.getSourceCode();
        if (theCode == null) return false;

        int startIndex = theCode.indexOf("{") + 1;
        int stopIndex = theCode.indexOf("}") - 1;

        if (stopIndex - startIndex <= 0) return true;

        String realCode = theCode.substring(startIndex, stopIndex);

        return (realCode.trim().length() == 0);
    }
}
