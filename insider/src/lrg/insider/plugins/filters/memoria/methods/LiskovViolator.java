package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 19.02.2005
 * Time: 12:09:42
 * To change this template use File | Settings | File Templates.
 */
public class LiskovViolator extends FilteringRule {
    public LiskovViolator() {
        super(new Descriptor("Liskov Violator", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface aMethod) {
        if (new IsEmpty().applyFilter(aMethod) == false) return false;
        if (new IsOverriden().applyFilter(aMethod) == false) return false;

        FilteringRule notAbstract = new NotComposedFilteringRule(new IsAbstract());
        FilteringRule notEmpty = new NotComposedFilteringRule(new IsEmpty());

        return aMethod.getGroup("methods overriden").applyFilter(notAbstract).applyFilter(notEmpty).size() > 0;
    }

}