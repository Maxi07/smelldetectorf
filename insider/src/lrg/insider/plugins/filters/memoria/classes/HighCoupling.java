package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.01.2005
 * Time: 12:51:37
 * To change this template use File | Settings | File Templates.
 */
class HighCoupling extends FilteringRule {
    public HighCoupling() {
        super(new Descriptor("High Coupling", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        double nom = ((Double) anEntity.getProperty("NOM").getValue()).doubleValue();
        double fanout = ((Double) anEntity.getProperty("FANOUT").getValue()).doubleValue();
        double cbo = ((Double) anEntity.getProperty("CBO").getValue()).doubleValue();
        double amw = ((Double) anEntity.getProperty("AMW").getValue()).doubleValue();

        if ((cbo == 0) || (nom == 0)) return false;


        if ((cbo / fanout) >= 0.67) return false; // this is the dispersion ratio

        double fanoutNomRatio = fanout / nom;
        FilteringRule highFanout = new FilteringRule("FANOUT", ">=", "class", 24);

        return ((fanoutNomRatio >= 7) || highFanout.applyFilter(anEntity));
    }
}