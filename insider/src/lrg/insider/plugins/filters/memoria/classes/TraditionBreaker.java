package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;

public class TraditionBreaker extends FilteringRule {
    public TraditionBreaker() {
        super(new Descriptor("Tradition Breaker", "class"));
    }

    public boolean excessiveIncreaseOfInterfaceSizeInChild(AbstractEntityInterface childClass) {
        boolean highNASValue = new FilteringRule("NAS", ">=", "class", Threshold.NOM_AVG).applyFilter(childClass);   // 12 / 2
        boolean highPNASRatio = new FilteringRule("PNAS", ">=", "class", Threshold.TWO_THIRDS).applyFilter(childClass); // 2 / 3

        return (highNASValue && highPNASRatio);
    }

    public boolean substantialComplexityAndSizeInChild(AbstractEntityInterface childClass) {
        boolean amwhigherAverage = new FilteringRule("AMW", ">=", "class", Threshold.AMW_AVG).applyFilter(childClass);
        boolean veryHighWMC = new FilteringRule("WMC", ">=", "class", Threshold.WMC_VERYHIGH).applyFilter(childClass);
        boolean highNOM = new FilteringRule("NOM", ">=", "class", Threshold.NOM_HIGH).applyFilter(childClass);

        return ((amwhigherAverage || veryHighWMC) && highNOM);

    }

    public boolean parentClassNeitherSmallNorDumb(AbstractEntityInterface childClass) {
        FilteringRule notInterface = new NotComposedFilteringRule(new IsInterface());
        GroupEntity modelBaseClasses = childClass.uses("base classes").applyFilter("model class").applyFilter(notInterface);
        if (modelBaseClasses.size() == 0) return false;

        double nomAncestors = ((Double) modelBaseClasses.getProperty("NOM").aggregate("sum").getValue()).doubleValue();
        double wmcAncestors = ((Double) modelBaseClasses.getProperty("WMC").aggregate("sum").getValue()).doubleValue();
        double amwAncestors = ((Double) modelBaseClasses.getProperty("AMW").aggregate("sum").getValue()).doubleValue();

        return (nomAncestors >= (Threshold.NOM_HIGH / 2)) &&
                (amwAncestors > Threshold.AMW_AVG) &&
                (wmcAncestors >= (Threshold.WMC_VERYHIGH / 2));
    }

    public boolean applyFilter(AbstractEntityInterface childClass) {
        return excessiveIncreaseOfInterfaceSizeInChild(childClass) &&
                substantialComplexityAndSizeInChild(childClass) &&
                parentClassNeitherSmallNorDumb(childClass);
    }
}
