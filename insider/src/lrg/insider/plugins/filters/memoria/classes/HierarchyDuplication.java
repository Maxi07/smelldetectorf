package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.insider.plugins.core.filters.memoria.ModelClassFilter;

public class HierarchyDuplication extends FilteringRule {
    public HierarchyDuplication() {
        super(new Descriptor("Hierarchy Duplication", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface aClass) {
        FilteringRule isModelClasswithHierarchyDuplication = new AndComposedFilteringRule(new ModelClassFilter(),
                new FilteringRule("HDUPCLS", ">", "class", 0));

        return isModelClasswithHierarchyDuplication.applyFilter(aClass);
    }
}

