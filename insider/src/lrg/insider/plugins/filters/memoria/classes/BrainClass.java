package lrg.insider.plugins.filters.memoria.classes;

import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.insider.plugins.filters.Threshold;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.02.2005
 * Time: 12:54:13
 * To change this template use File | Settings | File Templates.
 */
public class BrainClass extends FilteringRule {
    public BrainClass() {
        super(new Descriptor("Brain Class", "class"));
    }

    private double loc(AbstractEntityInterface anEntity) {
        double result = 0.0;
        Iterator it = anEntity.getProperty("LOC").getValueAsCollection().iterator();
        while (it.hasNext()) {
            Object tmp = it.next();
            if (tmp instanceof ResultEntity)
                result += ((Double) ((ResultEntity) tmp).getValue()).doubleValue();
        }
        return result;
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (new GodClass().applyFilter(anEntity)) return false;

        double totalLOC = loc(anEntity);
        double NrOfBrainMethods = ((Double) anEntity.getProperty("NrBM").getValue()).doubleValue();

        FilteringRule veryHighWMC =
                new FilteringRule("WMC", ">", "class", Threshold.WMC_VERYHIGH);
        if ((NrOfBrainMethods > 1) &&
                (totalLOC > Threshold.LOC_CLASS_VERYHIGH) &&
                (veryHighWMC.applyFilter(anEntity)))
            return true;

        FilteringRule excessiveHighWMC =
                new FilteringRule("WMC", ">", "class", 2 * Threshold.WMC_VERYHIGH);
        if ((NrOfBrainMethods == 1) &&
                (totalLOC > 2 * Threshold.LOC_CLASS_VERYHIGH) &&
                (excessiveHighWMC.applyFilter(anEntity)))
            return true;

        return false;
    }
}