package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class IsLeafClass extends FilteringRule {
    public IsLeafClass() {
        super(new Descriptor("is leaf-class", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false) return false;

        if (anEntity.uses("base classes").applyFilter("model class").size() == 0) return false;
        return anEntity.isUsed("derived classes").size() == 0;
    }
}
