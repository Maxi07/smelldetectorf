package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class IsAbstract extends FilteringRule {
    public IsAbstract() {
        super(new Descriptor("is abstract", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false) return false;

        return ((lrg.memoria.core.Class) anEntity).isAbstract();
    }

}
