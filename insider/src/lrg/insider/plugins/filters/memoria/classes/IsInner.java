package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class IsInner extends FilteringRule {
    public IsInner() {
        super(new Descriptor("is inner", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false) return false;

        String address = (String) anEntity.getProperty("Address").getValue();
        return address.contains("$");
    }

}
