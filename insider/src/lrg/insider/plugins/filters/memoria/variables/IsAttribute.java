package lrg.insider.plugins.filters.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 05.11.2004
 * Time: 09:12:54
 * To change this template use File | Settings | File Templates.
 */
public class IsAttribute extends FilteringRule {
    public IsAttribute() {
        super(new Descriptor("is attribute", new String[]{"global variable", "attribute", "local variable", "parameter"}));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        return (anEntity instanceof lrg.memoria.core.Attribute);
    }
}
