package lrg.insider.plugins.filters.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 09.11.2006
 * Time: 18:25:47
 * To change this template use File | Settings | File Templates.
 */
public class IsParameter extends FilteringRule {
    public IsParameter() {
        super(new Descriptor("is parameter", new String[]{"global variable", "attribute", "local variable", "parameter"}));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        return (anEntity instanceof lrg.memoria.core.Parameter);
    }
}
