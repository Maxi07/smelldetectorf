package lrg.insider.plugins.filters.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.Statute;


public class IsUserDefined extends FilteringRule {
    public IsUserDefined() {
        super(new Descriptor("is user defined", "attribute"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Variable == false)
            return false;
        lrg.memoria.core.Variable theVariable = (lrg.memoria.core.Variable) anEntity;

        return ((theVariable.hasPrimitiveType() == false) &&
                (theVariable.getStatute() == Statute.NORMAL));
    }
}
