package lrg.insider.plugins.filters.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 03.02.2005
 * Time: 19:49:11
 * To change this template use File | Settings | File Templates.
 */
public class IsConstant extends FilteringRule {
    public IsConstant() {
        // super(new Descriptor("is constant", new String[]{"global variable", "attribute", "local variable", "parameter"}));
        super(new Descriptor("is constant", "attribute"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Attribute == false) return false;

        return ((lrg.memoria.core.Attribute) anEntity).isStatic() && ((lrg.memoria.core.Attribute) anEntity).isFinal();
    }
}
