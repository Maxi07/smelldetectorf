package lrg.insider.plugins.filters.memoria.packages;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class CyclicSubsystemsDependency extends FilteringRule {
	public CyclicSubsystemsDependency() {
		super(new Descriptor("Cyclic Subsystems Dependency", "package"));
	}
	
	public boolean applyFilter(AbstractEntityInterface anEntity) {
		return GraphUtil.buildGraph( anEntity,"system","package group","efferent coupling").belongstoCycle(new Node(anEntity));
	}
}
