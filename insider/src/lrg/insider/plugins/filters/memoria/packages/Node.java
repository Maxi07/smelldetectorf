package lrg.insider.plugins.filters.memoria.packages;

import graph.AbstractNode;
import lrg.common.abstractions.entities.AbstractEntityInterface;

public class Node extends AbstractNode{

	final AbstractEntityInterface entity;
	
	
	public Node(AbstractEntityInterface wrapper){
		this.entity = wrapper;
	}
	public AbstractEntityInterface getWrapper(){
		return entity;
	}

	public int hashCode() {
		return entity.hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o instanceof Node)
			return entity.getName().equals(((Node)o).entity.getName());
		return false;
	}
	
	public String toString(){
		return entity.getName();
	}

}
