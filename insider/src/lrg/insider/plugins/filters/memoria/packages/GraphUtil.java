package lrg.insider.plugins.filters.memoria.packages;

import graph.AdjacencyList;
import graph.Graph;

import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;

public class GraphUtil {
	
	public static Graph buildGraph(AbstractEntityInterface entity,String belongsTo,String getGroup,String relationship){
		AdjacencyList list = new AdjacencyList();
		GroupEntity packages = entity.belongsTo(belongsTo).getGroup(getGroup); 
		Iterator<AbstractEntityInterface> iterator = packages.iterator();
		while (iterator.hasNext()){
			AbstractEntityInterface packageWrapper = iterator.next();
			list.addNode(new Node(packageWrapper));
			GroupEntity efferentPackages = packageWrapper.getGroup(relationship);
			Iterator<AbstractEntityInterface> efferentIterator = efferentPackages.iterator();
			while (efferentIterator.hasNext()){
				AbstractEntityInterface efferentPackage = efferentIterator.next();
				list.addEdge(new Node(packageWrapper), new Node(efferentPackage));
			}
		}
		Graph graph = new Graph(list);
		System.out.println("the graph->"+graph);
		return graph;
	}
}
