package lrg.insider.plugins.details;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.insider.plugins.core.details.HTMLDetail;
import lrg.insider.plugins.core.groups.memoria.containment.ClassHasAttributes;
import lrg.insider.plugins.core.groups.memoria.containment.ClassHasMethods;
import lrg.insider.plugins.filters.memoria.classes.DataClass;
import lrg.insider.plugins.filters.memoria.classes.GodClass;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;
import lrg.insider.plugins.filters.memoria.variables.IsConstant;
import lrg.memoria.core.Class;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.GenericClass;
import lrg.memoria.core.ModelElementList;

public class DesignFlawsClassDetail extends HTMLDetail {

	public DesignFlawsClassDetail() {
        super("Details of Class Design Flaws", "Detail with Full Information on Design Flaws", "class");
	}

public ResultEntity compute(AbstractEntityInterface anEntity) {
	
    if (anEntity instanceof GenericClass) {
        GenericClass aGenericClass = (GenericClass) anEntity;
        String text = "<big>" + "Generic Class Detail: " + linkTo(aGenericClass) + "</big><hr><br>";
        text += "Full Name: " + aGenericClass.getFullName() + "<br>";
        text += "Number of parameters: " + aGenericClass.getTemplateParameters().size() + "<br>";
        text += bulletedLinkList(aGenericClass.getTemplateParameters());
        text += "Number of instances: " + aGenericClass.getTemplateInstances().size() + "<br>";
        text += bulletedLinkList(aGenericClass.getTemplateInstances());
        return new ResultEntity(text);
    }

    if (anEntity instanceof Class) {
        Class aClass = (Class) anEntity;

        String amc = getAccessModeHTML(aClass.getAccessMode());

        String text = "<big>" + getKindOf(aClass) + " </big><b>" + amc + "</b><big> " + linkTo(aClass.getPackage()) + " . " + linkTo(aClass) + "</big>" +
                "<br>" + classFlawsDecorations(anEntity) + "" +
                "<hr><br>";
        text += buildInheritanceHierarchy(aClass);


        if(new DataClass().applyFilter(anEntity) == true ) text += "<hr>" + printDataClassDetails(anEntity);
        if(new GodClass().applyFilter(anEntity) == true ) text += "<hr>" + printGodClassDetails(anEntity);
//        if(new BrainClass().applyFilter(anEntity) == true ) text += "<hr>" + printBrainClassDetails(anEntity);
        
        text += "<hr>";
        text += "<table><tr><td valign=\"top\">";
        text += "<b>methods (" + linkToNumber(new ClassHasMethods().buildGroupEntity(aClass)) + ")</b><br>";
        text += bulletedLinkList(aClass.getMethodList(), new MethodDecorations());
        text += "</td><td valign=\"top\">";
        text += "<b>attributes (" + linkToNumber(new ClassHasAttributes().buildGroupEntity(aClass)) + ")</b><br>";
        text += bulletedLinkList(aClass.getAttributeList(), new AttributeDecorations());
        text += "</td></tr></table>";

        return new ResultEntity(text);
    } else
        return null;
}

private String writeAttributesList(AbstractEntityInterface anEntity, GroupEntity foreignData) {
	String tmp = "";
	ArrayList<AbstractEntityInterface> allMethods = anEntity.getGroup("method group").getElements();
    FilteringRule notConstant = new NotComposedFilteringRule(new IsConstant());

    
	
	for (AbstractEntityInterface crtMethod : allMethods) {
		GroupEntity allAccesses = crtMethod.uses("variables accessed").applyFilter("is attribute").applyFilter(notConstant);
	    allAccesses = allAccesses.union(crtMethod.uses("operations called").applyFilter("is accessor")).distinct();

		ArrayList<AbstractEntityInterface> accessedForeignData = allAccesses.intersect(foreignData).getElements();
		if(accessedForeignData.size() >0) {
			tmp += "<ul>";
			tmp += "<li>" + linkTo(crtMethod) + new MethodDecorations().getAfterDecoration(crtMethod);
			tmp += "<ul>";
			for (AbstractEntityInterface theAttribute : accessedForeignData) {
				tmp += "<li>" + classFlawsDecorations(theAttribute.belongsTo("class")) + linkTo(theAttribute.belongsTo("class"));
				tmp += " :: " + linkTo(theAttribute) + new AttributeDecorations().getBeforeDecoration(crtMethod);														
			} 
			tmp += "</ul>";
			tmp += "</ul>";
		}
	}
	return tmp;
}

private String printGodClassDetails(AbstractEntityInterface anEntity) {
	String tmp = "";
    DecimalFormat twoDecimals = new DecimalFormat("#0.00");
    GroupEntity foreignData = anEntity.getGroup("foreign data").distinct();
    GroupEntity foreignDataClasses = ((GroupEntity)foreignData.belongsTo("class")).distinct();
    ArrayList<AbstractEntityInterface> fDataLista = foreignData.getElements();
	tmp += "<br>This is a &nbsp; <i>God Class</i> &nbsp; because:";	
	tmp += "<ul>";
	tmp += "<li> some of its methods access directly (or via getter/setters) ";
	tmp += anEntity.getProperty("ATFD").getValue() + " attributes from  " + foreignDataClasses.size() + " external classes:";
 	tmp += writeAttributesList(anEntity, foreignData);
	tmp += "<li> the methods are very complex, i.e. have many branches (" + anEntity.getProperty("WMC").getValue() + " in total, and "+anEntity.getProperty("AMW") + " in average)</li>";
	tmp += "<li> it is non-cohesive (TCC="+ twoDecimals.format(anEntity.getProperty("TCC").getValue()) + ") with respect to the way methods use the attributes of the class. ";
	tmp += "</ul>";


	return tmp;
}

private String printDataClassDetails(AbstractEntityInterface anEntity) {
	String tmp = "";
    FilteringRule notConstructor = new NotComposedFilteringRule(new IsConstructor());
    GroupEntity allMethods = anEntity.contains("method group");
    GroupEntity publicMethods = allMethods.applyFilter("is public").applyFilter(notConstructor);
    GroupEntity publicAttr = anEntity.contains("attribute group").applyFilter("not encapsulated");
    GroupEntity accessorMethods = publicMethods.applyFilter("is accessor");

    int totalPublicMembers = publicMethods.size() + publicAttr.size();
    
	tmp += "<br>This is a &nbsp; <i>Data Class</i> because:";	
	tmp += "<ul>";
	tmp += "<li> only " + (publicMethods.size() - accessorMethods.size()) + " of all the "+ totalPublicMembers +" interface members are functional methods. ";
	tmp += "<li> The lack of encapsulation happens because of these ";
	if(publicAttr.size() > 0)
			tmp += "public attributes: " + bulletedLinkList(publicAttr.getElements(), new AttributeDecorations());
	if(accessorMethods.size() > 0) {
		if(publicAttr.size() > 0) tmp += " and ";
		tmp += " accessor methods (getter and setters): " +  bulletedLinkList(accessorMethods.getElements(), new MethodDecorations());
	}
	tmp += "</ul>";
	tmp += "<br> The non-encapsulated methods are used from OUTSIDE the class as follows: ";
	ArrayList<AbstractEntityInterface> publicAttrArray = publicAttr.getElements();
	ArrayList<AbstractEntityInterface> accessorMethodsArray = accessorMethods.getElements();
	tmp += "<ul>";
	for (AbstractEntityInterface crtAttr : publicAttrArray) {
		tmp += "<li> " + linkTo(crtAttr);
		tmp += "<ul>";
		ArrayList<AbstractEntityInterface> methodUsers = crtAttr.getGroup("methods accessing variable").exclude(allMethods).distinct().getElements();
		if(methodUsers.size() == 0) tmp += "MAKE PRIVATE! (not used from outside)";
		for (AbstractEntityInterface crtMethod : methodUsers) {
			tmp += "<li>" + classFlawsDecorations(crtMethod.belongsTo("class")) + linkTo(crtMethod.belongsTo("class"));
			tmp += " :: " + linkTo(crtMethod) + new MethodDecorations().getAfterDecoration(crtMethod);						
		}
		
		tmp += "</ul>";
	}

	for (AbstractEntityInterface crtAccessor : accessorMethodsArray) {
		tmp += "<li> " + linkTo(crtAccessor);
		ArrayList<AbstractEntityInterface> methodUsers = crtAccessor.getGroup("operations calling me").exclude(allMethods).distinct().getElements();
		
		if(methodUsers.size() == 0) tmp += "<b>MAKE PRIVATE! (not used from outside the class) </b>";
		
		tmp += "<ul>";
		for (AbstractEntityInterface crtMethod : methodUsers) {
			tmp += "<li>" + classFlawsDecorations(crtMethod.belongsTo("class")) + linkTo(crtMethod.belongsTo("class"));
			tmp += "." + linkTo(crtMethod) + new MethodDecorations().getAfterDecoration(crtMethod);						
		}	
		tmp += "</ul>";
		
		
	}
	tmp += "</ul>";
	return tmp;
}

private String printBrainClassDetails(AbstractEntityInterface anEntity) {
	String tmp = "";
    DecimalFormat twoDecimals = new DecimalFormat("#0.00");
	tmp += "<br>This is a &nbsp; <i>Brain Class</i> because:";
	tmp += "<ul>";
	tmp += "<li> it is long (" + anEntity.getProperty("LOCC").getValue() + " lines)</li>";
	tmp += "<li> it's methods are very complex, i.e. have many branches (" + anEntity.getProperty("WMC").getValue() + " in total, and "+anEntity.getProperty("AMW") + " in average)</li>";
	tmp += "<li> it contains following &nbsp; <i>Brain Methods</i>:";
	tmp += bulletedLinkList(anEntity.contains("method group").applyFilter("Brain Method").getElements(), new MethodDecorations()) + "</li>";		
	tmp += "<li> it is non-cohesive (TCC="+ twoDecimals.format(anEntity.getProperty("TCC").getValue()) + ") with respect to the way methods use the attributes of the class. ";
	tmp += "Following cohesive groups of methods were detected: <br>";
    HashMap<GroupEntity, GroupEntity> attribClusteredMethods =  (HashMap<GroupEntity, GroupEntity>) anEntity.getProperty("Attribute Usage Clusters").getValue();
    for(GroupEntity crtKey : attribClusteredMethods.keySet()) {
    	if(crtKey.size() > 0) tmp += commaLinkList(attribClusteredMethods.get(crtKey).distinct().getElements()) + " &nbsp; &nbsp;are accessed in common by [ " + commaLinkList(crtKey.getElements()) + " ] <br>";
    }
	
	tmp += "</ul>";

	return tmp;
}

private String getKindOf(Class aClass) {
    if (aClass.isStructure())
        return "Structure";
    if (aClass.isInterface())
        return "Interface";
    if (aClass.isUnion())
        return "Union";
    return "class";
}

private String buildInheritanceHierarchy(Class aClass) {
    String text = "";
    ArrayList ancestors = new ArrayList();
    DataAbstraction c = aClass;
    ModelElementList<DataAbstraction> ancestorsList;
    while (c != null) {
        ancestors.add(c);
        ancestorsList = c.getAncestorsList();
        if (ancestorsList.size() > 0)
            c = (DataAbstraction) ancestorsList.get(0);
        else
            break;
    }
    for (int i = 1; i <= ancestors.size(); i++) {
        text += linkTo((Class) ancestors.get(ancestors.size() - i)) + "<br>";
        for (int j = 0; j < i; j++) text += "&nbsp;&nbsp;&nbsp;&nbsp;";
//           if (i != ancestors.size()) text += "|<br>";
        for (int j = 0; j < i; j++) text += "&nbsp;&nbsp;&nbsp;&nbsp;";
    }
    return text;
}


	
}
