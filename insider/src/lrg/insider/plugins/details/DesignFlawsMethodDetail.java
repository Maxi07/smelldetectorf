package lrg.insider.plugins.details;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.details.HTMLDetail;
import lrg.insider.plugins.filters.memoria.methods.FeatureEnvy;
import lrg.insider.plugins.filters.memoria.methods.IntensiveCoupling;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.memoria.core.Method;
import lrg.memoria.core.Parameter;

public class DesignFlawsMethodDetail extends HTMLDetail {

	public DesignFlawsMethodDetail() {
        super("Details of Method Design Flaws", "Detail customized depending on design flaw", "method");
	}

    private String buildModifiers(Method aMethod) {
        String c = MethodDecorations.propertyColor;
        String d = MethodDecorations.endFont;
        String text = "";
        if (aMethod.isAbstract()) text += c + "abstract" + d;
        if (new IsAccessor().applyFilter(aMethod)) text += c + "accessor" + d;
        if (aMethod.isConstructor()) text += c + "constructor" + d;
        if (aMethod.isFinal()) text += c + "final" + d;
        if (aMethod.isPackage()) text += c + "package" + d;
        if (aMethod.isStatic()) text += c + "static" + d;
        return text;
    }


    public ResultEntity compute(AbstractEntityInterface anEntity) {
        lrg.memoria.core.Function aFunction = (lrg.memoria.core.Function) anEntity;
        String returnType = "";
        if (aFunction.getReturnType() != null)
            returnType = linkTo(aFunction.getReturnType().getFullName());

        String text = "<big></big>";
        if (anEntity instanceof Method)
            text += getAccessModeHTML(((Method) anEntity).getAccessMode());
        
    
        text += "<big> " + returnType + "  " + linkTo(aFunction.getScope().getFullName()) + " :: " + linkTo(aFunction) + " ( ";

        Iterator paramIt = aFunction.getParameterList().iterator();
        // ArrayList list = new ArrayList();
        while (paramIt.hasNext()) {
            Parameter p = (Parameter) paramIt.next();
            String theParameter = linkTo(p.getType().getFullName()) + " " + linkTo(p.getFullName());
            // list.add(theParameter);
            text += theParameter;
            if (paramIt.hasNext()) text += ", ";
        }
        text += " )</big><br>";

        if (aFunction instanceof lrg.memoria.core.Method)
            text += buildModifiers((lrg.memoria.core.Method) anEntity) + "<br>";

        text += "<b>" + new MethodDecorations().getAfterDecoration(anEntity) + "</b><br>";

//        if(new BrainMethod().applyFilter(anEntity) == true ) text += "<hr>" + printBrainMethodDetails(anEntity);
        if(new FeatureEnvy().applyFilter(anEntity) == true ) text += "<hr>" + printFeatureEnvyDetails(anEntity);
        if(new IntensiveCoupling().applyFilter(anEntity) == true ) text += "<hr>" + printIntensiveCouplingDetails(anEntity);
        
/*        
        GroupEntity gp = new MethodOverrides().buildGroupEntity(aFunction);
        text += "<b>Methods overridden: </b>" + commaLinkList(gp.getElements()) + "<hr><br>";


        GroupEntity accClasses = aFunction.getGroup("accessed model classes");
        GroupEntity accData = aFunction.getGroup("accessed model data");
        GroupEntity classAndAncestors = aFunction.getGroup("current class and ancestors");
        GroupEntity allMethodsAttributes = classAndAncestors.getGroup("method group").union(classAndAncestors.getGroup("attribute group"));


        text += "<b>Accesses to Foreign Data(ATFD):</b> " + commaLinkList(accData.exclude(allMethodsAttributes).getElements()) + " <br>";
        text += "<b> Foreign Data Providers(FDP):</b> " + commaLinkList(accClasses.exclude(classAndAncestors).distinct().getElements()) + "<hr><br>";


        gp = new ChangingMethods().buildGroupEntity(aFunction);
        text += "<b>External methods calling this method:</b> " + commaLinkList(gp.getElements()) + "<br>";

        gp = new ChangingClasses().buildGroupEntity(aFunction);
        text += "<b>External classes depending on this method:</b> " + commaLinkList(gp.getElements()) + "<br>";
*/
                
        return new ResultEntity(text);
    }

	private String printBrainMethodDetails(AbstractEntityInterface anEntity) {
		String tmp = "";
		tmp += "<br>This is a &nbsp; <i>Brain Method</i> &nbsp; because:";
		tmp += "<ul>";
		tmp += "<li> it is long (" + anEntity.getProperty("LOC").getValue() + " lines)</li>";
		tmp += "<li> it has many branches (" + anEntity.getProperty("CYCLO").getValue() + ") and a deep nesting level ("+ anEntity.getProperty("MAXNESTING").getValue() +")</li>";
		tmp += "<li> it uses many variables (" + anEntity.getProperty("NOAV").getValue() + ")</li>";		
		tmp += "</ul>";
		AbstractEntityInterface theClass = anEntity.belongsTo("class");
		GroupEntity attributesOfClass = theClass.contains("attribute group");
		int usedLocalAttributes = anEntity.getGroup("variables accessed").intersect(attributesOfClass).distinct().size();
		if(usedLocalAttributes == 0 && attributesOfClass.size() == 0)  tmp += "The method uses NO ATTRIBUTES ";
		else tmp += "The method uses " + usedLocalAttributes;
		tmp += " (out of " + attributesOfClass.size() + ") local attributes.";

        HashMap<GroupEntity, GroupEntity>  clusteredMethods =  (HashMap<GroupEntity, GroupEntity>) anEntity.belongsTo("class").getProperty("Attribute Usage Clusters").getValue();
        int counter = 0;
        String text = "";
        for(GroupEntity crtKey : clusteredMethods.keySet()) {
        	if((crtKey.size() > 0) && crtKey.isInGroup(anEntity)) {
        			counter++;
        			text += "[ " + commaLinkList(crtKey.getElements()) + " ] use attribute(s): &nbsp;" + commaLinkList(clusteredMethods.get(crtKey).getElements()) + "<br>";
        	}
        }        
		
		tmp += "<br>The method is involved in following groups of methods by common usage of attributes ("+ counter + " out of " + clusteredMethods.keySet().size() + "):<br>";
		tmp += text + "<br>";
		return tmp;
	}

	private String printFeatureEnvyDetails(AbstractEntityInterface anEntity) {
		String tmp = "";
		AbstractEntityInterface theClass = anEntity.belongsTo("class");		
		GroupEntity attributesOfClass = theClass.contains("attribute group");
		int usedLocalAttributes = anEntity.getGroup("variables accessed").intersect(attributesOfClass).distinct().size();
		
		tmp += "<br>This is a &nbsp; <i>Feature Envy</i> &nbsp; because:";
		tmp += "<ul>";
		if(usedLocalAttributes == 0 && attributesOfClass.size() == 0) 
			tmp += "<li>it uses NO ATTRIBUTES of the class, because the class has no attributes!</li>";
		else if(usedLocalAttributes == 0)
			tmp += "<li>it uses NO ATTRIBUTES of the class (out of "+ attributesOfClass.size() + ")" + "</li>";		
		else
			tmp += "<li>it uses few ("+ usedLocalAttributes +" out of "+ attributesOfClass.size() + ") local attributes </li>"; 
		tmp += "<li> it uses following non-encapsulated attributes:</li>";
			tmp += "<ul>";
	        GroupEntity accData = anEntity.getGroup("accessed model data");
	        GroupEntity classAndAncestors = anEntity.getGroup("current class and ancestors");
	        GroupEntity allMethodsAttributes = classAndAncestors.getGroup("method group").union(classAndAncestors.getGroup("attribute group"));

	        ArrayList<AbstractEntityInterface> accessedAttributes = accData.exclude(allMethodsAttributes).getElements();
	        for (AbstractEntityInterface anAttribute : accessedAttributes) {
				tmp += "<li>"+ classFlawsDecorations(anAttribute.belongsTo("class")) + linkTo(anAttribute.belongsTo("class")) + "." + linkTo(anAttribute);
				GroupEntity users = ((GroupEntity)anAttribute.isUsed("methods accessing variable").belongsTo("class")).distinct().exclude((AbstractEntity) theClass);
				if(users.size() > 0) {
					tmp += " The attribute is also used by: &nbsp;&nbsp;&nbsp;";
					tmp += commaLinkList(users.getElements()) + "</li>";	        	
				}
				else tmp += " The attribute is NOT USED by anyone else!";
			}
	        tmp += "</ul>";
		tmp += "</ul>";

		return tmp;
	}

	private String printIntensiveCouplingDetails(AbstractEntityInterface anEntity) {
		String tmp = "";
		GroupEntity classMethods = new GroupEntity("group", new ArrayList());
		
		if(anEntity.belongsTo("class") != null) classMethods = anEntity.belongsTo("class").contains("method group");
		GroupEntity calledMethods = anEntity.getGroup("operations called").applyFilter("model function").exclude(classMethods).distinct();
		GroupEntity calledClasses = ((GroupEntity)calledMethods.belongsTo("class")).distinct();
		
		tmp += "This method has &nbsp; <i>Intensive Coupling</i> &nbsp; because it calls many methods (";
		tmp += calledMethods.size() + ") from a few classes ("  + calledClasses.size() + ") namely: <ul>";
		ArrayList<AbstractEntityInterface> calledMethodsArray = calledMethods.getElements();
		for (AbstractEntityInterface crtMethod : calledMethodsArray) {
			tmp += "<li>" + classFlawsDecorations(crtMethod.belongsTo("class")) + linkTo(crtMethod.belongsTo("class"));
			tmp += " :: " + linkTo(crtMethod);			
		}
		tmp+= "</ul>";
		
		
		return tmp;
	}
	
}
