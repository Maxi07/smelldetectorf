package lrg.insider.plugins.visualizations;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.insider.plugins.filters.memoria.variables.IsAttribute;
import lrg.jMondrian.commands.AbstractNumericalCommand;

class WidthNumericalCommand extends AbstractNumericalCommand {
	double theValue = 0;	
	private AbstractEntityInterface entity;
	public WidthNumericalCommand(AbstractEntityInterface anEntity) { entity = anEntity; }
	
	public double execute(){
		AbstractEntityInterface receiverEntity = (AbstractEntityInterface) receiver;

		double offset = 5;
		
		if(new IsAttribute().applyFilter(receiverEntity) )
			theValue =  receiverEntity.getGroup("methods accessing field").exclude(entity.contains("method group")).size() + offset;
		else
			theValue = receiverEntity.getGroup("operations calling me").size() + offset;
		return theValue;	
	}
}