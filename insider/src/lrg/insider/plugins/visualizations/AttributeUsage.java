package lrg.insider.plugins.visualizations;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.EntityType;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.insider.plugins.filters.memoria.variables.IsAttribute;
import lrg.insider.plugins.groups.memoria.CallRelation;
import lrg.insider.plugins.visualizations.comparators.MethodLOCComparator;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.FlowLayout;
import lrg.jMondrian.layouts.TreeLayout;
import lrg.jMondrian.painters.LineEdgePainter;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.util.CommandColor;
import lrg.jMondrian.view.ViewRenderer;


public class AttributeUsage extends AbstractVisualization {
	private Figure f;

    public AttributeUsage() {
        super("Attribute Usage", "Attribute Usage", "attribute");
    }
	
	public void view(final AbstractEntityInterface theAttribute) {
		final GroupEntity clientClassesLayer = new GroupEntity("Client Classes", new EntityType("")); 
		final GroupEntity attributeClassLayer = new GroupEntity("Attribute's Class", new EntityType("")); 
		GroupEntity allLayers = new GroupEntity(theAttribute.getName() + " layer", new EntityType("")); 
		final AbstractEntityInterface containerClass = theAttribute.belongsTo("class");
		
		final GroupEntity clientMethods = theAttribute.getGroup("methods accessing variable").applyFilter("model function");
		
		clientClassesLayer.addAll(((GroupEntity)clientMethods.belongsTo("class")).distinct().exclude((AbstractEntity) containerClass));

        
 		attributeClassLayer.add(containerClass);

 		
		if(clientClassesLayer.size() > 0) allLayers.add(clientClassesLayer); 	
		allLayers.add(attributeClassLayer); 
		
		ArrayList<AbstractEntityInterface> globalLayer = new ArrayList<AbstractEntityInterface>();
		globalLayer.add(allLayers);
		
        f = new Figure();
        
        f.nodesUsingForEach(globalLayer, 
       		 	new RectangleNodePainter(false),
                      new AbstractFigureDescriptionCommand(){
                            public Figure describe(){
                                        Figure fig = new Figure();
                                        GroupEntity receiverLayer = (GroupEntity) receiver;
                                        fig.nodesUsingForEach(receiverLayer.getElements(), 
                                        		new RectangleNodePainter(false), 
                                        		new AbstractFigureDescriptionCommand() {
                                        			public Figure describe() {
                                        				Figure classFig = new Figure();
                                        				final GroupEntity classList = (GroupEntity) receiver;
                                        				classFig.nodesUsingForEach(classList.getElements(), 
                                        						new RectangleNodePainter(true)
                                        						.name(Visualization.stringCommand("Name"))
                                        						.frameColor(CommandColor.LIGHT_GRAY)
                                        						.color(new AbstractNumericalCommand() { 
                                        							public double execute() {
                                                        				AbstractEntityInterface aClass  = (AbstractEntityInterface) receiver;
                                        								if(aClass.equals(containerClass)) return OurColors.VERY_LIGHT_GRAY;
                                        								if(containerClass.getGroup("all descendants").intersect(aClass).size() > 0)
                                        									return OurColors.LIGHT_RED;
                                        								else if(containerClass.getGroup("all ancestors").intersect(aClass).size() > 0)	
                                        									return OurColors.LIGHT_LILA;
                                        								return Color.WHITE.getRGB(); 
                                        							} }), 
                                        						new AbstractFigureDescriptionCommand() {
                                        							public Figure describe() {
                                        								Figure methodFig = new Figure();
                                                        				AbstractEntityInterface aClass  = (AbstractEntityInterface) receiver;
                                                        				GroupEntity interestingElements = new GroupEntity("group", new ArrayList());
                                                        				interestingElements.addAll(aClass.getGroup("method group").intersect(clientMethods));
                                                        				Collections.sort(interestingElements.getElements(), new MethodLOCComparator());
                                                        				if(aClass.equals(containerClass)) interestingElements.add(theAttribute);	                                                        			
                                                        				methodFig.nodesUsing(interestingElements.getElements(), 
                                        							new RectangleNodePainter(false)
                                                        				.name(Visualization.stringCommand("Name"))
                                										.width(new AbstractNumericalCommand(){
                                											public double execute(){
                                												double value;
                                												double offset = 7;
                                												AbstractEntityInterface receiverEntity = (AbstractEntityInterface) receiver;
                                												if(theAttribute.equals(receiverEntity)) value = 400;
                                												else if(new IsAttribute().applyFilter(receiverEntity))  value = (Double) receiverEntity.getProperty("NMAV").getValue() + offset;
                                												else value = (Double) receiverEntity.getProperty("CM").getValue() + offset;
                                												return value;
                                											}
                                										})
                                										.height(new AbstractNumericalCommand(){
                                											public double execute(){
                                												double value;
                                												double offset = 7;
                                												AbstractEntityInterface receiverEntity = (AbstractEntityInterface) receiver;
                                												if(theAttribute.equals(receiverEntity)) value = 10;
                                												else if(new IsAttribute().applyFilter(receiverEntity))  value = (Double) receiverEntity.getProperty("NMAV").getValue() + offset;
                                												else if(new IsAccessor().applyFilter(receiverEntity)) value = (Double) receiverEntity.getProperty("CM").getValue() + offset;
                                												else value = (Double)receiverEntity.getProperty("LOC").getValue() + offset;
                                												return value;
                                											}
                                										})
                                										.color(new AbstractNumericalCommand(){ 
                                											public double execute(){ 
                                                                				AbstractEntityInterface receiverEntity  = (AbstractEntityInterface) receiver;
                                                                				if(new IsAttribute().applyFilter(receiverEntity)) return OurColors.VERY_LIGHT_GRAY; 
                                                                				if(clientMethods.intersect(receiverEntity).size() > 0) {
                                                                					if(new IsAccessor().applyFilter(receiverEntity)) 
                                                                							return OurColors.ACCESSOR_BLUE; 
                                                                				}
                                                                				return OurColors.RED;
                                											} }));
                                        								methodFig.layout(new FlowLayout(5,5,400));
                                        								return methodFig;
                                        							}
                                        						});
                                        				classFig.layout(new FlowLayout(20,5,400));
                                        				return classFig;
                                        			}
                                        		}
                                        );
                            			ArrayList<CallRelation> layerEdges = new ArrayList<CallRelation>();
                            			if(clientClassesLayer.size()>0) layerEdges.add(new CallRelation(clientClassesLayer, attributeClassLayer));
                            			fig.edgesUsing(layerEdges,
                            					new LineEdgePainter
                            					(Visualization.entityCommand("getIsCalledNode"),
                            					 Visualization.entityCommand("getCallsNode")
                            					).color(CommandColor.INVISIBLE));


                                        fig.layout(new TreeLayout(2, 45));
                                        return fig;

                            }
        			});
        
        ArrayList<CallRelation> inEdges = new ArrayList<CallRelation>();
		ArrayList<AbstractEntity> accessMeMethods = clientMethods.getElements();
		for (AbstractEntity userMth : accessMeMethods) {
			inEdges.add(new CallRelation((AbstractEntity) theAttribute, userMth));
		}

		f.edgesUsing(inEdges,
			new LineEdgePainter
			(Visualization.entityCommand("getCallsNode"),
			 Visualization.entityCommand("getIsCalledNode")				 
			).color(CommandColor.INVISIBLE));
      
		f.layout(new FlowLayout());

        ViewRenderer r = new ViewRenderer("Attribute Usage on " + theAttribute.getName() );
        f.renderOn(r);
        r.open();

	}
	

}
