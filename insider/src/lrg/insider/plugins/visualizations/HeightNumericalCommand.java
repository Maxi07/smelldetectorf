package lrg.insider.plugins.visualizations;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.insider.plugins.filters.memoria.variables.IsAttribute;
import lrg.jMondrian.commands.AbstractNumericalCommand;


class HeightNumericalCommand extends AbstractNumericalCommand {
	private AbstractEntityInterface entity;
	public HeightNumericalCommand(AbstractEntityInterface anEntity) { entity = anEntity; }

	public double execute(){
		double theValue = 0;
		AbstractEntityInterface receiverEntity = (AbstractEntityInterface) receiver;

		double offset = 5;
		
		if(new IsAttribute().applyFilter(receiverEntity) )
			theValue = receiverEntity.getGroup("methods accessing field").intersect(entity.contains("method group")).size() + offset; 
			//theValue = 5;
		else 
			theValue = (Double)receiverEntity.getProperty("LOC").getValue() + offset; 
 		return theValue;


	}
}
