package lrg.insider.plugins.visualizations;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.plugins.groups.memoria.InheritanceRelation;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.FlowLayout;
import lrg.jMondrian.layouts.TreeLayout;
import lrg.jMondrian.painters.LineEdgePainter;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.util.LinearNormalizerColor;
import lrg.jMondrian.view.ViewRenderer;

public class SystemComplexity extends AbstractVisualization {
	private Figure theFigure;
    public SystemComplexity() {
        super("System Complexity Overview", "System Complexity", new String[]{"system", "package"});
    }

	public void view( AbstractEntityInterface entity) {
		final GroupEntity allClasses = entity.getGroup("class group").applyFilter("model class").applyFilter(new NotComposedFilteringRule(new FilteringRule("is interface","is true","class")));

		GroupEntity packageGroup = new GroupEntity("package group", new ArrayList());
		if (entity.getEntityType().getName().compareTo("package")==0){
			packageGroup.add(entity);
		}
		else{
			packageGroup.addAll(entity.getGroup("package group"));
		}
		final GroupEntity allEdges = new GroupEntity("all inheritance relations", new ArrayList());
		Iterator<AbstractEntity> iterator = packageGroup.iterator();
		while (iterator.hasNext()){
			AbstractEntity packageEntity = iterator.next();
			allEdges.addAll(packageEntity.getGroup("all inheritance relations"));
			//allEdges = entity.getGroup("all inheritance relations");
		}



		ArrayList theLayers = new ArrayList();

		GroupEntity standaloneClasses = new GroupEntity("standalone", new ArrayList());
		ArrayList<AbstractEntityInterface> theRoots = allClasses.getElements();
		for (AbstractEntityInterface aRoot : theRoots) {
			if(aRoot.getGroup("all ancestors").intersect(allClasses).size() > 0) continue;
			GroupEntity descendants = aRoot.getGroup("all descendants").intersect(allClasses);
			if(descendants.size() == 0) standaloneClasses.add(aRoot);
			else {
				GroupEntity aTree = new GroupEntity("tree", new ArrayList());
				aTree.add(aRoot); aTree.addAll(descendants);
				theLayers.add(aTree);
			}		
		}

		Collections.sort(theLayers, new HierarchyComparator());			
		Collections.sort(standaloneClasses.getElements(), new ClassComparator());
		theLayers.add(standaloneClasses);			

		theFigure = new Figure();

		theFigure.nodesUsingForEach(theLayers, new RectangleNodePainter(false),  new AbstractFigureDescriptionCommand(){
			public Figure describe(){
				Figure f = new Figure();	
				GroupEntity aLayer = (GroupEntity) receiver;
				f.nodesUsing(aLayer.getElements(), new RectangleNodePainter(true)
				.width(Visualization.metricCommand("NOA", 5))
				.height(Visualization.metricCommand("NOM", 5))
				.name(Visualization.stringCommand("Name"))
				.color(new LinearNormalizerColor(allClasses.getElements(),Visualization.metricCommand("WMC"))));
				
				if(aLayer.getName().equals("tree")) {
					Iterator<InheritanceRelation> it = allEdges.iterator();
					ArrayList edges = new ArrayList();
					while(it.hasNext()) {
						InheritanceRelation rel = it.next();
						if(aLayer.isInGroup(rel.getSubClass()) && aLayer.isInGroup(rel.getSuperClass())) {
							edges.add(rel);
						}
					}
					f.edgesUsing(edges, new LineEdgePainter(
							Visualization.entityCommand("getSubClass"),
							Visualization.entityCommand("getSuperClass"))
					.color( new AbstractNumericalCommand(){ 
						public double execute(){return Color.GRAY.getRGB();} 
					}));                    							
					f.layout(new TreeLayout(5,10));
				}
				else f.layout(new FlowLayout(10,10,300));
				return f;
			}
		});
		ViewRenderer r = new ViewRenderer("System Complexity Overview on " + entity.getName());
		theFigure.layout(new FlowLayout(20,20, 500));
		theFigure.renderOn(r);
        r.open();
	}

}

//         	

