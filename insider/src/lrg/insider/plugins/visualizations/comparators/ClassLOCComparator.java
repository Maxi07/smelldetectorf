package lrg.insider.plugins.visualizations.comparators;

import java.util.Collections;
import java.util.Comparator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.GroupEntity;

public class ClassLOCComparator implements Comparator {
	private GroupEntity interestingMethods;
	
	public ClassLOCComparator(GroupEntity methods) {
		interestingMethods = methods;
	}
	
	public int compare(Object o1, Object o2) {
		AbstractEntity minElem1 = null;
		AbstractEntity minElem2 = null;
		
			
		GroupEntity methods1 = ((AbstractEntity)o1).getGroup("method group").intersect(interestingMethods);
		GroupEntity methods2 = ((AbstractEntity)o2).getGroup("method group").intersect(interestingMethods);
		
		if(methods1.size() > 0) minElem1 = (AbstractEntity) Collections.min(methods1.getElements(), new MethodLOCComparator());
		if(methods2.size() > 0) minElem2 = (AbstractEntity) Collections.min(methods2.getElements(), new MethodLOCComparator());
		

		Double value1 = (minElem1 != null) ? (Double) minElem1.getProperty("LOC").getValue() : 0;
		Double value2 = (minElem2 != null) ? (Double) minElem2.getProperty("LOC").getValue() : 0;
		
		return (int) (value2 - value1);
		
	}	
}
