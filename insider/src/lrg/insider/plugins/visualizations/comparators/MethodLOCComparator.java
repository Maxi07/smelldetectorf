package lrg.insider.plugins.visualizations.comparators;

import java.util.Comparator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;

public class MethodLOCComparator implements Comparator {
	public int compare(Object o1, Object o2) {
		AbstractEntity meth1 = (AbstractEntity)o1;
		AbstractEntity meth2 = (AbstractEntity)o2;
		Double value1 = null, value2 = null;
		
		if(new IsAccessor().applyFilter(meth1)) 
			value1 = (Double) meth1.getProperty("CM").getValue();
		else value1 = (Double) meth1.getProperty("LOC").getValue();
			
		if(new IsAccessor().applyFilter(meth2)) value2 = (Double) meth2.getProperty("CM").getValue();
		else value2 = (Double) meth2.getProperty("LOC").getValue();

		return (int) (value2 - value1);
	}	
}

