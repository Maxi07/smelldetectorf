package lrg.insider.plugins.visualizations;

import java.awt.Color;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.plugins.filters.memoria.methods.IsAbstract;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.insider.plugins.filters.memoria.methods.IsOverriden;
import lrg.insider.plugins.filters.memoria.methods.IsSpecialization;
import lrg.insider.plugins.filters.memoria.variables.IsAttribute;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.HTreeLayout;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.view.ViewRenderer;


class ColorNumericalCommand extends AbstractNumericalCommand {
	private AbstractEntityInterface entity;
	public ColorNumericalCommand(AbstractEntityInterface anEntity) { entity = anEntity; }

	public double execute(){
		AbstractEntityInterface receiverEntity = (AbstractEntityInterface) receiver;
		if(new IsAttribute().applyFilter(receiverEntity) ) return Color.blue.getRGB();
		if(new IsAbstract().applyFilter(receiverEntity)) return Color.cyan.getRGB();
		if(new IsSpecialization().applyFilter(receiverEntity)) return Color.orange.getRGB();
		if(new IsOverriden().applyFilter(receiverEntity)) return (new Color(150,100,75)).getRGB();
		if(new IsAccessor().applyFilter(receiverEntity)) 
			if(receiverEntity.getName().contains("get")) return Color.red.getRGB();
			else if(receiverEntity.getName().contains("set")) return Color.orange.getRGB();
		return Color.WHITE.getRGB();
	}
}


class BlueprintFigureDescriptionCommand extends AbstractFigureDescriptionCommand {
	AbstractEntityInterface entity;
	public BlueprintFigureDescriptionCommand(AbstractEntityInterface theEntity) { 
		entity = theEntity;
	}
    public Figure describe(){
        Figure fig = new Figure();
        fig.nodesUsing(((GroupEntity) receiver).getElements(), 
       		 	    new RectangleNodePainter(true)
        					.name(Visualization.stringCommand("Name"))
        					.width(new WidthNumericalCommand(entity))
        					.height(new HeightNumericalCommand(entity))
        					.color(new ColorNumericalCommand(entity)));

        fig.layout(new HTreeLayout(10,10));
        return fig;
    }
}

public class ClassBlueprint extends AbstractVisualization {
    public ClassBlueprint(){
    	super("Class Blueprint", "Class Blueprint", "class");
    	
    }

     public void view(AbstractEntityInterface entity){  
		ClassBlueprintOperations operations = new ClassBlueprintOperations();
		operations.buildFigure(entity);
		Figure f = operations.getFigure();

        ViewRenderer r = new ViewRenderer("Class Blueprint of " + entity.getName());
        f.renderOn(r);
        r.open();
	}

	
}





