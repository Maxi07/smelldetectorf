package lrg.insider.plugins.visualizations;

import java.util.ArrayList;
import java.util.List;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.ScatterPlotLayout;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.view.ViewRenderer;

public class AISubsystemPlot extends AbstractVisualization {

	    public AISubsystemPlot() {
	        super("AI Plot (subsystems)", "Abstractness Instability Plot", "system");
	    }

	    public void view(AbstractEntityInterface entity) {
	    	FilteringRule significantPackages = new FilteringRule("TOTALDEP", ">", "subsystem", 10);
	        List set = entity.getGroup("subsystem group").applyFilter(significantPackages).getElements();
	        ArrayList container = new ArrayList(); container.add(set);
	        
	        Figure bigFigure = new Figure();
	        bigFigure.nodesUsingForEach(container,
	       		 	new RectangleNodePainter(310,310,false)
	        			.color(new AbstractNumericalCommand() { public double execute() { return OurColors.VERY_LIGHT_GRAY; }}),
                    new AbstractFigureDescriptionCommand(){
                        public Figure describe(){
                        	Figure v = new Figure();
                        	List theNodes = (List)receiver;
                        	v.nodesUsing(theNodes, new RectangleNodePainter(3,3,true)
    						.name(Visualization.stringCommand("Name"))
        	        		.color(new AbstractNumericalCommand() { public double execute() {
        	        			AbstractEntityInterface aPackage = (AbstractEntityInterface) receiver;
        	        			double SAPDistance = (Double) aPackage.getProperty("SAP Distance").getValue();
        	        			if(SAPDistance < -0.7) return OurColors.RED;
        	        			else if((SAPDistance >= -0.7) && (SAPDistance <= -0.3)) return OurColors.BLACK;
        	        			else if((SAPDistance > -0.3) && (SAPDistance < 0.3))  return OurColors.GREEN; 
        	        			else if((SAPDistance >= 0.3) && (SAPDistance <= 0.7)) return OurColors.BLACK;
        	        			else return OurColors.BLUE; }})
        	        		.x(new AbstractNumericalCommand() { 
        	        			public double execute() {
        	        				return (Double)((AbstractEntityInterface)receiver).getProperty("IF").getValue() * 300;
        	        			}})
        		       		.y(new AbstractNumericalCommand() { 
        	        			public double execute() {
        	        				return 300 - (Double)((AbstractEntityInterface)receiver).getProperty("AR").getValue() * 300;
        	        			}}));
                	        v.layout(new ScatterPlotLayout());
                	        return v;
                        }
	        		});	
	        	        
	        ViewRenderer r = new ViewRenderer("Abstractness Instability Plot");
	        bigFigure.renderOn(r);
	        r.open();
	    }
}