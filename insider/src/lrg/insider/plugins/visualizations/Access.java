package lrg.insider.plugins.visualizations;

import lrg.common.abstractions.entities.AbstractEntityInterface;

public class Access{
    private AbstractEntityInterface method;
    private AbstractEntityInterface attribute;

    public Access(AbstractEntityInterface method, AbstractEntityInterface attribute){
        this.method = method;
        this.attribute = attribute;
    }


    public AbstractEntityInterface getMethod() {
        return method;
    }

    public AbstractEntityInterface getAttribute() {
        return attribute;
    }
}
