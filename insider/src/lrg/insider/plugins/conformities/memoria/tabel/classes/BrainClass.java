package lrg.insider.plugins.conformities.memoria.tabel.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.thresholds.Threshold;

/**
 * Created by Horia Radu. User: horia Date: 21.09.2010 Time: 18:07:08 To change
 * this template use File | Settings | File Templates.
 */
public class BrainClass extends ConformityRule {
	public BrainClass() {
		super(new Descriptor("_Brain Class_", "class"));
	}
	
	private double noBrainMethods(AbstractEntityInterface anEntity) {
		GroupEntity aGroup = anEntity.getGroup("method group");

        return (Double) (new ResultEntity(aGroup.applyFilter("Brain Method").size()).getValue());
	}

	public Double applyConformity(AbstractEntityInterface anEntity) {
		double noParThreshold = 10;
		ConformityRule wocRule = new ConformityRule("WOC", ">", "method", Threshold.TWO_THIRDS);

		return wocRule.applyConformity(anEntity) + noBrainMethods(anEntity);
	}
}