package lrg.insider.plugins.conformities.memoria.tabel.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.AddComposedConformityRule;
import lrg.common.abstractions.plugins.conformities.thresholds.Threshold;
import lrg.insider.plugins.filters.memoria.variables.IsPublic;

/**
 * Created by Horia Radu. User: horia Date: 21.09.2010 Time: 18:07:08 To change
 * this template use File | Settings | File Templates.
 */
public class BrainMethod extends ConformityRule {
	public BrainMethod() {
		super(new Descriptor("_Brain Method_", "method"));
	}

	public Double applyConformity(AbstractEntityInterface anEntity) {
		double noParThreshold = 10;
		ConformityRule locRule = new ConformityRule("LOC", ">", "method", Threshold.LOC_VERYHIGH);
		ConformityRule cycloRule = new ConformityRule("CYCLO", ">", "method", Threshold.MANY);
		ConformityRule nestingRule = new ConformityRule("MAXNESTING", ">", "method", Threshold.DEEP);
		ConformityRule noParRule = new ConformityRule("NOP", ">", "method", noParThreshold);
		// ConformityRule locommRule = new ConformityRule("MAXNESTING", ">",
		// "method", Threshold.DEEP);

		return new AddComposedConformityRule(
				new AddComposedConformityRule(
						new AddComposedConformityRule(locRule, cycloRule), nestingRule),
						noParRule).applyConformity(anEntity);
	}
}