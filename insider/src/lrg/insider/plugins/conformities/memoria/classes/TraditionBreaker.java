package lrg.insider.plugins.conformities.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.ValueBasedConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.AddComposedConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.MaxComposedConformityRule;
import lrg.common.abstractions.plugins.conformities.weights.Weight;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;
import lrg.insider.plugins.filters.memoria.classes.IsInterface;

public class TraditionBreaker extends ConformityRule {
    public TraditionBreaker() {
        super(new Descriptor("__Tradition Breaker", "class"));
    }

    public ConformityRule excessiveIncreaseOfInterfaceSizeInChild() {
        ConformityRule highNASValue = new ConformityRule("NAS", ">", "class", Threshold.NOM_AVG);   // 12 / 2
        highNASValue.setWeight(Weight.HIGH.value());
        ConformityRule highPNASRatio = new ConformityRule("PNAS", ">", "class", Threshold.TWO_THIRDS); // 2 / 3
        highPNASRatio.setWeight(Weight.HIGH.value());

        return new AddComposedConformityRule(highNASValue, highPNASRatio);
    }

    public ConformityRule substantialComplexityAndSizeInChild() {
    	ConformityRule amwhigherAverage = new ConformityRule("AMW", ">", "class", Threshold.AMW_AVG);
    	amwhigherAverage.setWeight(Weight.MEDIUM.value());
    	ConformityRule veryHighWMC = new ConformityRule("WMC", ">", "class", Threshold.WMC_VERYHIGH);
    	veryHighWMC.setWeight(Weight.MEDIUM.value());
    	ConformityRule highNOM = new ConformityRule("NOM", ">", "class", Threshold.NOM_HIGH);
    	highNOM.setWeight(Weight.MEDIUM.value());
    	
    	return new AddComposedConformityRule(new MaxComposedConformityRule(amwhigherAverage, veryHighWMC), highNOM);
    }

    public ConformityRule parentClassNeitherSmallNorDumb(AbstractEntityInterface childClass) {
        FilteringRule notInterface = new NotComposedFilteringRule(new IsInterface());
        GroupEntity modelBaseClasses = childClass.uses("base classes").applyFilter("model class").applyFilter(notInterface);
        if (modelBaseClasses.size() == 0) return null;

        double nomAncestors = ((Double) modelBaseClasses.getProperty("NOM").aggregate("sum").getValue()).doubleValue();
        double wmcAncestors = ((Double) modelBaseClasses.getProperty("WMC").aggregate("sum").getValue()).doubleValue();
        double amwAncestors = ((Double) modelBaseClasses.getProperty("AMW").aggregate("sum").getValue()).doubleValue();

        ConformityRule nomAncRule = new ValueBasedConformityRule(nomAncestors, ">", "class", Threshold.NOM_HIGH / 2);
        nomAncRule.setWeight(Weight.MEDIUM.value());
        ConformityRule amwAncRule = new ValueBasedConformityRule(amwAncestors, ">", "class", Threshold.AMW_AVG);
        amwAncRule.setWeight(Weight.MEDIUM.value());
        ConformityRule wmcAncRule = new ValueBasedConformityRule(wmcAncestors, ">", "class", Threshold.WMC_VERYHIGH / 2);
        wmcAncRule.setWeight(Weight.MEDIUM.value());
        
        return new AddComposedConformityRule(new AddComposedConformityRule(nomAncRule, amwAncRule), wmcAncRule);
    }

    public Double applyConformity(AbstractEntityInterface childClass) {
    	ConformityRule result = new AddComposedConformityRule(excessiveIncreaseOfInterfaceSizeInChild(), substantialComplexityAndSizeInChild());
    	ConformityRule x = parentClassNeitherSmallNorDumb((childClass));
    	if (x != null)
    			result = new AddComposedConformityRule(result, x);
    	return result.applyConformity(childClass) / result.getWeight();
    }
}
