package lrg.insider.plugins.conformities.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.ValueBasedConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.AddComposedConformityRule;
import lrg.common.abstractions.plugins.conformities.weights.Weight;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;
import lrg.insider.plugins.filters.memoria.classes.IsInterface;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;
import lrg.insider.plugins.filters.memoria.methods.IsPrivate;

/**
 * Created by IntelliJ IDEA. User: user Date: 05.11.2004 Time: 18:48:19 To
 * change this template use File | Settings | File Templates.
 */
public class RefusedParentBequest extends ConformityRule {
	public RefusedParentBequest() {
		super(new Descriptor("__Refused Parent Bequest", "class"));
	}

	private double NOM = 0.0;

	private double BOvR(AbstractEntityInterface derivedClass) {
		GroupEntity baseclassMethods = derivedClass.uses("base classes").applyFilter("model class").contains("method group");
		double nrOfOverridenMethods = derivedClass.uses("methods overriden").intersect(baseclassMethods).size();
		return (nrOfOverridenMethods / NOM);

	}

	private double NumberOfProtectedMembersinBaseClass(AbstractEntityInterface derivedClass) {
		GroupEntity baseClasses = derivedClass.uses("base classes").applyFilter("model class");
		baseClasses.applyFilter(new NotComposedFilteringRule(new IsInterface()));
		if (baseClasses.size() == 0)
			return 0;

		GroupEntity protectedMethods = baseClasses.contains("method group").applyFilter("is protected");
		protectedMethods.applyFilter(new NotComposedFilteringRule(new IsConstructor()));
		GroupEntity protectedAttributes = baseClasses.contains("attribute group").applyFilter("is protected");

		return protectedAttributes.size() + protectedMethods.size();
	}

	private ConformityRule childClassIgnoresBequest(AbstractEntityInterface derivedClass) {
		double NProtMInBase = NumberOfProtectedMembersinBaseClass(derivedClass);
		ConformityRule lowUsageRatioRule = new ConformityRule("BUR", "<", "class", Threshold.ONE_THIRD);
		lowUsageRatioRule.setWeight(Weight.HIGH.value());
		ConformityRule lowOverridingRatioRule = new ValueBasedConformityRule(BOvR(derivedClass), "<", "class", Threshold.ONE_THIRD);
		lowOverridingRatioRule.setWeight(Weight.HIGH.value());
		ConformityRule nrProtectedMembrsInBase = new ValueBasedConformityRule(NProtMInBase, ">", "class", Threshold.FEW);
		nrProtectedMembrsInBase.setWeight(Weight.HIGH.value());

		ConformityRule result = new AddComposedConformityRule(new AddComposedConformityRule(lowOverridingRatioRule, lowUsageRatioRule), nrProtectedMembrsInBase);
		return result;
	}

	private ConformityRule childClassIsNotDwarf() {
		ConformityRule amwRule = new ConformityRule("AMW", ">", "class", Threshold.AMW_AVG);
		amwRule.setWeight(Weight.LOW.value());
		ConformityRule wmcRule = new ConformityRule("WMC", ">", "class", Threshold.WMC_AVG);
		wmcRule.setWeight(Weight.LOW.value());
		ConformityRule nomRule = new ConformityRule("NOM", ">", "class", Threshold.NOM_AVG);
		nomRule.setWeight(Weight.LOW.value());
		ConformityRule result =  new AddComposedConformityRule(new AddComposedConformityRule(amwRule, wmcRule), nomRule);
		return result;
	}

	public Double applyConformity(AbstractEntityInterface derivedClass) {
		FilteringRule notConstructors = new NotComposedFilteringRule(new IsConstructor());
		FilteringRule notPrivate = new NotComposedFilteringRule(new IsPrivate());
		//TODO: HORIA: add filter: NOT PRIVATE
		NOM = derivedClass.contains("method group").applyFilter(notConstructors).applyFilter(notPrivate).size();
		if (NOM == 0.0)
			return 0.0;

		ConformityRule result = new AddComposedConformityRule(childClassIgnoresBequest(derivedClass), childClassIsNotDwarf());
		return result.applyConformity(derivedClass) / result.getWeight();
	}
}