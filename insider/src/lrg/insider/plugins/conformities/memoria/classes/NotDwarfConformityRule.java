package lrg.insider.plugins.conformities.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.AddComposedConformityRule;
import lrg.insider.plugins.filters.Threshold;

/**
 * Created by IntelliJ IDEA. User: user Date: 05.11.2004 Time: 18:48:19 To
 * change this template use File | Settings | File Templates.
 */
public class NotDwarfConformityRule extends ConformityRule {
	public NotDwarfConformityRule() {
		super(new Descriptor("__Not Dwarf Conformity", "class"));
	}

	public Double applyConformity(AbstractEntityInterface derivedClass) {
		ConformityRule amwRule = new ConformityRule("AMW", ">", "class", Threshold.AMW_AVG);
		ConformityRule wmcRule = new ConformityRule("WMC", ">", "class", Threshold.WMC_AVG);
		ConformityRule nomRule = new ConformityRule("NOM", ">", "class", Threshold.NOM_AVG);
		ConformityRule result = new AddComposedConformityRule(new AddComposedConformityRule(amwRule, wmcRule), nomRule);
		return result.applyConformity(derivedClass);
	}
}