package lrg.insider.plugins.conformities.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;

/**
 * Created by Horia Radu.
 * User: horia
 * Date: 21.09.2010
 * Time: 18:07:08
 * To change this template use File | Settings | File Templates.
 */
public class HighFanin extends ConformityRule {
    public HighFanin() {
        super(new Descriptor("High Fanin", "method"));
    }

    public Double applyConformity(AbstractEntityInterface anEntity) {
        ConformityRule highLOC = new ConformityRule("FANIN", ">", "method", 5);

        return (highLOC.applyConformity(anEntity));
    }
}