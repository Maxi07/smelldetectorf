package lrg.insider.plugins.conformities.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.MaxComposedConformityRule;

/**
 * Created by Horia Radu.
 * User: horia
 * Date: 21.09.2010
 * Time: 18:07:08
 * To change this template use File | Settings | File Templates.
 */
public class MyMaxConformity extends ConformityRule {
    public MyMaxConformity() {
        super(new Descriptor("MyMaxConformity", "method"));
    }

    public Double applyConformity(AbstractEntityInterface anEntity) {
        ConformityRule highLOC = new ConformityRule("LOC", ">", "method", 30);
        ConformityRule highCYCLO = new ConformityRule("CYCLO", ">", "method", 20);
        
        MaxComposedConformityRule m = new MaxComposedConformityRule(highLOC, highCYCLO);

        return (m.applyConformity(anEntity));
    }
}