package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.01.2005
 * Time: 11:03:44
 * To change this template use File | Settings | File Templates.
 */
public class HeavyCalledMethods extends GroupBuilder {
    public HeavyCalledMethods() {
        super("heavy called methods", "", "system");
    }

    public ArrayList buildGroup(AbstractEntityInterface aSystem) {
        GroupEntity heavyCoupledMethods = aSystem.getGroup("method group").applyFilter("model function").applyFilter("Heavy Coupling");

        return heavyCoupledMethods.getGroup("operations called").applyFilter("model function").distinct().getElements();
    }

}


