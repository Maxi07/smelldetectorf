package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.03.2005
 * Time: 17:46:56
 * To change this template use File | Settings | File Templates.
 */
public class IdentityDisharmony extends GroupBuilder {
    private GroupEntity brainClasses;

    public IdentityDisharmony() {
        super("Identity Disharmony", "", "system");
    }


    public ArrayList buildGroup(AbstractEntityInterface theSystem) {
        AbstractEntity crtClass;
        String header = "Name" + "\t" + "nrCriticMeth" + "\t" + "nrBM" + "\t" +
                "IDUPLINES" + "\t" + "ATFD" + "\t" + "nrFE" + "\t" + "Annotations";

        brainClasses = theSystem.contains("class group").applyFilter("model class").applyFilter("God Class");

        annotateClassWithOtherFlaws();

        System.out.println("GOD CLASSES");
        System.out.println("===========\n" + header);

        for (Iterator it = brainClasses.iterator(); it.hasNext();) {
            crtClass = (AbstractEntity) it.next();
            printClass(crtClass);
        }

        GroupEntity tmp = brainClasses;

        brainClasses = theSystem.contains("class group").applyFilter("model class").applyFilter("Brain Class");
        annotateClassWithOtherFlaws();

        System.out.println("BRAIN CLASSES");
        System.out.println("=============\n" + header);
        for (Iterator it = brainClasses.iterator(); it.hasNext();) {
            crtClass = (AbstractEntity) it.next();
            printClass(crtClass);
        }


        return brainClasses.union(tmp).getElements();
    }


    private void annotateClassWithOtherFlaws() {
        brainClasses.applyFilter("Tradition Breaker").putAnnotation("TB", "Tradition Breaker");
        brainClasses.applyFilter("Refused Parent Bequest").putAnnotation("RPB", "Refused Parent Bequest");

        annotateWithMethodFlaw("NrIC", "Intensive Coupling", "IC");
        annotateWithMethodFlaw("NrEC", "Extensive Coupling", "EC");
        annotateWithMethodFlaw("NrSS", "Shotgun Surgery", "SS");
        annotateWithMethodFlaw("EDUPLCLS", "External Duplication", "EDUP");
        annotateWithMethodFlaw("HDUPCLS", "Hierarchy Duplication", "HDUP");
    }


    private void annotateWithMethodFlaw(String metricName, String designFlaw, String annotationName) {
        FilteringRule aFilter = new FilteringRule(metricName, ">", "class", 0);
        GroupEntity thefilteredGroup = brainClasses.applyFilter(aFilter);
        AbstractEntity crtClass;

        for (Iterator it = thefilteredGroup.iterator(); it.hasNext();) {
            crtClass = (AbstractEntity) it.next();
            int metricValue = ((Double) crtClass.getProperty(metricName).getValue()).intValue();
            crtClass.putAnnotation(annotationName + metricValue, designFlaw);
        }
    }


    private void printClass(AbstractEntity crtClass) {
        double nrCriticalMethods = crtClass.getGroup("Identity Harmony (critical methods)").size();
        double duplication = ((Double) crtClass.getProperty("IDUPLINES").getValue()).doubleValue();
        double nrGM = ((Double) crtClass.getProperty("NrBM").getValue()).doubleValue();
        double nrFE = ((Double) crtClass.getProperty("NrFE").getValue()).doubleValue();
        double atfd = ((Double) crtClass.getProperty("ATFD").getValue()).doubleValue();
        double nom = ((Double) crtClass.getProperty("NOM").getValue()).doubleValue();
        double wmc = ((Double) crtClass.getProperty("WMC").getValue()).doubleValue();
        double tcc = ((Double) crtClass.getProperty("TCC").getValue()).doubleValue();

        String toPrint = crtClass.getName() + "\t" + nrCriticalMethods + "\t" + nrGM + "\t" +
                duplication + "\t" + atfd + "\t" + nrFE + "\t" + crtClass.annotationsToString() +
                "\t" + nom + "\t" + wmc + "\t" + tcc;
        System.out.println(toPrint);
    }
}
