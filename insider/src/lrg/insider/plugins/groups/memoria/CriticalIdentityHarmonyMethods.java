package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.OrComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 07.02.2005
 * Time: 16:40:17
 * To change this template use File | Settings | File Templates.
 */
public class CriticalIdentityHarmonyMethods extends GroupBuilder {
    public CriticalIdentityHarmonyMethods() {
        super("Identity Harmony (critical methods)", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface measuredClass) {
        FilteringRule isUsignForeignData = new FilteringRule("ATFD", ">", "method", 0);
        FilteringRule intraDuplication = new FilteringRule("IDUPLINES", ">", "method", 0);
        FilteringRule usesForeignDataOrDuplication = new OrComposedFilteringRule(isUsignForeignData, intraDuplication);
        GroupEntity resultGroup = measuredClass.getGroup("method group").applyFilter("Brain Method");

        return resultGroup.union(measuredClass.getGroup("method group").applyFilter(usesForeignDataOrDuplication)).distinct().getElements();
    }

}