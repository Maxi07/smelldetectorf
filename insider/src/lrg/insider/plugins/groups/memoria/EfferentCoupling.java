package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class EfferentCoupling extends GroupBuilder {
    public EfferentCoupling() {
        super("efferent coupling", "", "package");
    }

    public ArrayList buildGroup(AbstractEntityInterface aPackage) {
        GroupEntity callees = (GroupEntity) aPackage.uses("operations called").distinct().belongsTo("package");
        GroupEntity accessed = (GroupEntity) aPackage.uses("variables accessed").distinct().belongsTo("package");

        callees.addAll(accessed);
        callees = callees.applyFilter("model package");


        return callees.exclude((AbstractEntity) aPackage).getElements();
    }
}
