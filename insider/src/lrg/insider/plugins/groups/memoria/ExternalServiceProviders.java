package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;
import lrg.insider.plugins.filters.memoria.methods.IsProtected;
import lrg.insider.plugins.filters.memoria.methods.IsStatic;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.01.2005
 * Time: 10:23:19
 * To change this template use File | Settings | File Templates.
 */
public class ExternalServiceProviders extends GroupBuilder {
    public ExternalServiceProviders() {
        super("External Service Providers (methods)", "", "method");
    }

    public ArrayList buildGroup(AbstractEntityInterface aMethod) {
        FilteringRule notStatic = new NotComposedFilteringRule(new IsStatic());
        FilteringRule notConstructor = new NotComposedFilteringRule(new IsConstructor());
        FilteringRule notHidden = new NotComposedFilteringRule(new IsProtected());
        GroupEntity callees = (GroupEntity) aMethod.uses("operations called").distinct();

        callees = callees.applyFilter("model function").applyFilter(notHidden).applyFilter(notStatic).applyFilter(notConstructor);

        GroupEntity relatedClasses = aMethod.belongsTo("class").getGroup("all ancestors").union(aMethod.belongsTo("class"));

        return callees.exclude(relatedClasses.getGroup("method group")).getElements();
    }

}
