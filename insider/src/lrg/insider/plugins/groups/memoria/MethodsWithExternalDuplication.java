package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.dude.duplication.Duplication;
import lrg.dude.duplication.DuplicationList;
import lrg.dude.duplication.MethodEntity;
import lrg.insider.plugins.filters.memoria.classes.IsInterface;

public class MethodsWithExternalDuplication extends GroupBuilder {
    public MethodsWithExternalDuplication() {
        super("unrelated methods with duplication", "", "method");
    }

    public ArrayList buildGroup(AbstractEntityInterface aMethod) {
        HashSet resultUniqueList = new HashSet();


        ResultEntity aResult = aMethod.getProperty("#DUPLICATION#");
        if (aResult == null || !(aResult.getValue() instanceof DuplicationList)) return new ArrayList();

        ArrayList duplicators = getUnrelatedMethodsWithDuplication((DuplicationList) aResult.getValue());

        for (Iterator it = duplicators.iterator(); it.hasNext();) {
            MethodEntity duplicator = (MethodEntity) ((Duplication) it.next()).getDuplicateCode().getEntity();
            resultUniqueList.add(duplicator.getMethod());
        }
        return new ArrayList(resultUniqueList);
    }


    public static ArrayList getUnrelatedMethodsWithDuplication(DuplicationList aDuplicationList) {
        FilteringRule notInterface = new NotComposedFilteringRule(new IsInterface());
        ArrayList resultList = new ArrayList();

        if (aDuplicationList.size() == 0) return resultList;

        AbstractEntity referenceMethod = ((MethodEntity) aDuplicationList.get(0).getReferenceCode().getEntity()).getMethod();
        AbstractEntity referenceClass = referenceMethod.belongsTo("class");
        GroupEntity referenceClassAndAncestors = referenceClass.getGroup("all ancestors").applyFilter("model class").applyFilter(notInterface).union(referenceClass);

        GroupEntity duplicatorClassAndAncestors;
        AbstractEntity duplicatorMethod, duplicatorClass;
        for (int i = 0; i < aDuplicationList.size(); i++) {
            Duplication aDuplication = aDuplicationList.get(i);
            duplicatorMethod = ((MethodEntity) aDuplication.getDuplicateCode().getEntity()).getMethod();
            duplicatorClass = duplicatorMethod.belongsTo("class");

            if (duplicatorClass == referenceClass) continue;

            duplicatorClassAndAncestors = duplicatorClass.getGroup("all ancestors").applyFilter("model class").applyFilter(notInterface).union(duplicatorClass);

            if (referenceClassAndAncestors.intersect(duplicatorClassAndAncestors).size() == 0) resultList.add(aDuplication);
        }
        return resultList;
    }
}
