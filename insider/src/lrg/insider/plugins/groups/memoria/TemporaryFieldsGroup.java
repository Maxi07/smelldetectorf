package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.insider.plugins.filters.memoria.variables.IsTemporaryField;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.02.2005
 * Time: 19:10:08
 * To change this template use File | Settings | File Templates.
 */
public class TemporaryFieldsGroup extends GroupBuilder {
    public TemporaryFieldsGroup() {
        super("temporary fields", "", "system");
    }

    public ArrayList buildGroup(AbstractEntityInterface measuredClass) {
        return measuredClass.getGroup("attribute group").applyFilter(new IsTemporaryField()).getElements();
    }

}
