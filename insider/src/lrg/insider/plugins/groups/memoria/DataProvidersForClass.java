package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class DataProvidersForClass extends GroupBuilder {
    public DataProvidersForClass() {
        super("foreign data providers", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface measuredClass) {
        GroupEntity scopeClassOfAccesses = (GroupEntity) measuredClass.getGroup("foreign data").belongsTo("class");

        return scopeClassOfAccesses.distinct().getElements();
    }

}
