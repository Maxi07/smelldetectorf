package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 25.02.2005
 * Time: 10:04:35
 * To change this template use File | Settings | File Templates.
 */
public class CollaborationDisharmony extends GroupBuilder {
    private GroupEntity heavyCoupledMethods;
    private GroupEntity classesOfHeavyCoupledMth;

    public CollaborationDisharmony() {
        super("Collaboration Disharmony", "", "system");
    }


    public ArrayList buildGroup(AbstractEntityInterface theSystem) {
        AbstractEntity crtClass;
        GroupEntity crtHCMethods;

        FilteringRule extDup = new FilteringRule("EDUPCLS", ">", "class", 0);
        heavyCoupledMethods = theSystem.contains("method group").applyFilter("model function").applyFilter("Intensive Coupling");
        heavyCoupledMethods = heavyCoupledMethods.union(theSystem.contains("method group").applyFilter("model function").applyFilter("Extensive Coupling"));
        heavyCoupledMethods = heavyCoupledMethods.union(theSystem.contains("method group").applyFilter("model function").applyFilter("Shotgun Surgery"));

        classesOfHeavyCoupledMth = ((GroupEntity) heavyCoupledMethods.belongsTo("class")).distinct();
        classesOfHeavyCoupledMth = classesOfHeavyCoupledMth.union(theSystem.contains("class group").applyFilter(extDup)).distinct();

        annotateClassWithOtherFlaws();

        for (Iterator it = classesOfHeavyCoupledMth.iterator(); it.hasNext();) {
            crtClass = (AbstractEntity) it.next();
            printClass(crtClass);
            // printHCMethods(heavyCoupledMethods.intersect(crtClass.contains("method group")));
        }

        return classesOfHeavyCoupledMth.getElements();
    }

    private void printClass(AbstractEntity crtClass) {
        double ic = ((Double) crtClass.getProperty("NrIC").getValue()).doubleValue();
        double ec = ((Double) crtClass.getProperty("NrEC").getValue()).doubleValue();
        double ss = ((Double) crtClass.getProperty("NrSS").getValue()).doubleValue();
        double nom = ((Double) crtClass.getProperty("NOM").getValue()).doubleValue();
//        double extdup = ((Double)crtClass.getProperty("EDUPLINES").aggregate("sum").getValue()).doubleValue();

        String toString = crtClass.getName() + "\t" + ic + "\t" + ec + "\t" +
                ss + "\t" + nom + "\t" + "\t" + "\t" + crtClass.annotationsToString();

        System.out.println(toString);
    }

    private void annotateClassWithOtherFlaws() {
        classesOfHeavyCoupledMth.applyFilter("God Class").putAnnotation("GC", "God Class");
        classesOfHeavyCoupledMth.applyFilter("Brain Class").putAnnotation("BC", "Brain Class");
        classesOfHeavyCoupledMth.applyFilter("Data Class").putAnnotation("DC", "Data Class");
        classesOfHeavyCoupledMth.applyFilter("Tradition Breaker").putAnnotation("TB", "Tradition Breaker");
        classesOfHeavyCoupledMth.applyFilter("Refused Parent Bequest").putAnnotation("RPB", "Refused Parent Bequest");

        annotateWithMethodFlaw("NrBM", "Brain Method", "GM");
        annotateWithMethodFlaw("NrFE", "Feature Envy", "FE");
    }


    private void annotateWithMethodFlaw(String metricName, String designFlaw, String annotationName) {
        FilteringRule aFilter = new FilteringRule(metricName, ">", "class", 0);
        GroupEntity thefilteredGroup = classesOfHeavyCoupledMth.applyFilter(aFilter);
        AbstractEntity crtClass;

        for (Iterator it = thefilteredGroup.iterator(); it.hasNext();) {
            crtClass = (AbstractEntity) it.next();
            int metricValue = ((Double) crtClass.getProperty(metricName).getValue()).intValue();
            crtClass.putAnnotation(annotationName + metricValue, designFlaw);
        }
    }
/*
    private void annotateMethodsWithOtherFlaws(GroupEntity methodGroup) {
        methodGroup.applyFilter("God Method").putAnnotation("BM", "Brain Method");
        methodGroup.applyFilter("Feature Envy").putAnnotation("FE", "Feature Envy");
        methodGroup.applyFilter("Shotgun Surgery").putAnnotation("SS", "Shotgun Surgery");
        methodGroup.applyFilter(new FilteringRule("IsAccessor", "IsTrue", "method")).putAnnotation("ACCS", "accessor");
    }

    private void printHCMethods(GroupEntity hcMethods) {
        AbstractEntity crtMethod;
        FilteringRule hasEXTDUP = new FilteringRule("EDUPLINES", ">", "method", 0);
        double loc = 0.0, depExt = 0.0, depIntens = 0.0;

        annotateMethodsWithOtherFlaws(hcMethods);

        for (Iterator it = hcMethods.iterator(); it.hasNext();) {
            crtMethod = (AbstractEntity) it.next();
            if (hasEXTDUP.applyFilter(crtMethod)) {
                Double duplines = (Double) crtMethod.getProperty("EDUPLINES").getValue();
                crtMethod.putAnnotation("DUP" + duplines.doubleValue(), "External Duplication");
            }

            depExt = ((Double) crtMethod.getProperty("CEXT").getValue()).doubleValue();
            depIntens = ((Double) crtMethod.getProperty("CINT").getValue()).doubleValue();
            loc = ((Double) crtMethod.getProperty("LOC").getValue()).doubleValue();

            System.out.println("\t" + crtMethod.getName() + " [" + crtMethod.annotationsToString() + "]  " +
                    "\tINTENS:" + depIntens + " EXT:" + depExt + " LOC:" + loc);

            printCalledMethods(crtMethod);
            // writeBodyToFile(crtMethod);

        }
    }

    private void writeBodyToFile(AbstractEntity crtMethod) {
        String file = "c:\\temp\\collab\\" + crtMethod.belongsTo("class").getName() + "__" + crtMethod.getName() + ".java";
        try {
            PrintStream out_stream = new PrintStream(new FileOutputStream(file));
            out_stream.print(((Method) crtMethod).getBody().getSourceCode());
            out_stream.close();
        } catch (Exception ex) {
        }
    }

    private void printCalledMethods(AbstractEntity aMethod) {
        GroupEntity calledMethods = aMethod.getGroup("External Service Providers (methods)");
        annotateMethodsWithOtherFlaws(calledMethods);
        AbstractEntity crtMethod;

        for (Iterator it = calledMethods.iterator(); it.hasNext();) {
            crtMethod = (AbstractEntity) it.next();
            double loc = ((Double) crtMethod.getProperty("LOC").getValue()).doubleValue();
            double cyclo = ((Double) crtMethod.getProperty("CYCLO").getValue()).doubleValue();
            System.out.println("\t\t" + crtMethod.belongsTo("class").getName() + " :: " +
                    crtMethod.getName() +
                    " [" + crtMethod.annotationsToString() + "]" + "  " + loc + "/" + cyclo);
        }
    }
*/
}

