package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class SubclassesDependedOn extends GroupBuilder {
    public SubclassesDependedOn() {
        super("subclasses dependencies", "", "method");
    }

    public ArrayList buildGroup(AbstractEntityInterface aMethod) {
        GroupEntity allDescendants = aMethod.belongsTo("class").isUsed("all descendants");
        GroupEntity allUsages = aMethod.uses("operations called").union(aMethod.uses("variables accessed"));

        return ((GroupEntity) allUsages.belongsTo("class")).intersect(allDescendants).getElements();

    }

}
