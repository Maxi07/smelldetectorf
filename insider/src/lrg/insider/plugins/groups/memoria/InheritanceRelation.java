package lrg.insider.plugins.groups.memoria;

import lrg.common.abstractions.entities.AbstractEntity;

public class InheritanceRelation extends AbstractEntity {
	private AbstractEntity subClass;
	private AbstractEntity superClass;
	public InheritanceRelation(AbstractEntity superC, AbstractEntity subC) {
		subClass = subC; superClass = superC;
	}
	
	public AbstractEntity getSubClass() { return subClass;}
	public AbstractEntity getSuperClass() { return superClass; }
}
