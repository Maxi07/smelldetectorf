package lrg.insider.plugins.core.properties.memoria.types;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Name extends PropertyComputer
{
    public Name()
    {
        super("Name", "The name of the type", "type", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Type == false)
            return null;

        return new ResultEntity(((lrg.memoria.core.Type) anEntity).getName());
    }
}
