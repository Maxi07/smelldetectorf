package lrg.insider.plugins.core.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Scope extends PropertyComputer {
    public Scope() {
        super("scope", "The scope of the class", "class", "entity");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;

        return new ResultEntity(((lrg.memoria.core.Class) anEntity).getPackage());
    }
}
