package lrg.insider.plugins.core.properties.memoria.inheritance;

import java.util.StringTokenizer;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.memoria.core.InheritanceRelation;

public class Detail extends AbstractDetail {

    public Detail() {
        super("Detail", "The detailed information for an inheritance relation", "inheritance relation", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {

        if (anEntity instanceof InheritanceRelation) {

            String text = "<h1>" + "Inheritance relation </h1><hr><br>";
            text += "<table>";
            StringTokenizer strToken = new StringTokenizer(anEntity.toString()," \t",false);
            text += "<tr>";
            while(strToken.hasMoreTokens()) {
                text += "<td valign=\"top\">";
                text += strToken.nextToken().replaceAll("<","&lt;").replaceAll(">","&gt;");
            }
            text += "</tr>";
            text += "</table>";
            return new ResultEntity(text);

        } else {

            return null;

        }
    }
}
