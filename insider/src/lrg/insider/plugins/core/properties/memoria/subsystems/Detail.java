package lrg.insider.plugins.core.properties.memoria.subsystems;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.memoria.core.Subsystem;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 12:57:34
 * To change this template use File | Settings | File Templates.
 */
public class Detail extends AbstractDetail {
    public Detail() {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", "subsystem", "string");
    }

    public ResultEntity compute(AbstractEntityInterface aSubsystem) {
        String text = "";
        text += "<h1>" + "Subsystem " + aSubsystem.getName() + "</h1><hr><br>";

        text += "consists of following packages:" + bulletedLinkList(((Subsystem)aSubsystem).getPackages());

        return new ResultEntity(text);
    }

}
