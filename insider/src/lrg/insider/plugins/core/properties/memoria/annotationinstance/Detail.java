package lrg.insider.plugins.core.properties.memoria.annotationinstance;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.memoria.core.AnnotationInstance;
import lrg.memoria.core.AnnotationPropertyValuePair;
import lrg.memoria.core.ModelElementList;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 12:57:34
 * To change this template use File | Settings | File Templates.
 */
public class Detail extends AbstractDetail {
    public Detail() {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", "annotation instance", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anAnnotationInstance) {
    	String text = "";
    	
    	AnnotationInstance theInstance = (AnnotationInstance) anAnnotationInstance;
    	
    	ModelElementList<AnnotationPropertyValuePair> listOfProperties = theInstance.getPropertyValuePairs();

    	text += "Annotation instance " + theInstance.getName() + " for " + linkTo(theInstance.getAnnotatedElement());

    	ArrayList<String> thePropertyStrings = new ArrayList<String>();
    	for (AnnotationPropertyValuePair aPropertyPair : listOfProperties)
    		thePropertyStrings.add(aPropertyPair.getAp().getName() + " = " + aPropertyPair.getValue());
    	
    	text += bulletedList(thePropertyStrings);
    	
    	text += theInstance.getAnnotation().toString();
    	
    	return new ResultEntity(text);
    	
		/// return new ResultEntity("Annotation instance " + anAnnotationInstance.getName());
    }

}
