package lrg.insider.plugins.core.properties.memoria.types;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.memoria.core.ExplicitlyDefinedType;
import lrg.memoria.core.Primitive;
import lrg.memoria.core.TemplateInstance;
import lrg.memoria.core.TypeDecorator;

public class Detail extends AbstractDetail {
    public Detail() {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", "type", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Primitive) {
            Primitive aPrimitive = (Primitive) anEntity;
            String text = "<h1>" + "Primitive Detail: " + linkTo(aPrimitive) + "</h1><hr><br>";
            return new ResultEntity(text);
        }

        if (anEntity instanceof TypeDecorator) {
            TypeDecorator aTypeDecorator = (TypeDecorator) anEntity;
            String text = "<h1>" + "Type Decorator Detail: " + linkTo(aTypeDecorator) + "</h1><hr><br>";
            text += "Full Name: " + aTypeDecorator.getFullName() + "<br>";
            text += "Decorated Type: " + linkTo(aTypeDecorator.getDecoratedType()) + "<br>";
            if (anEntity instanceof ExplicitlyDefinedType)
                text += "Location: " + ((ExplicitlyDefinedType) anEntity).getLocation().getFile().getFullName() + "<br>";
            return new ResultEntity(text);
        }

        if (anEntity instanceof TemplateInstance) {
            TemplateInstance aTemplateInstance = (TemplateInstance) anEntity;
            String text = "<h1>" + "Template Instance Detail: " + linkTo(aTemplateInstance) + "</h1><hr><br>";
            text += "Full Name: " + aTemplateInstance.getFullName() + "<br>";
            text += "Template: " + linkTo(aTemplateInstance.getTemplateType()) + "<br>";
            text += "Type instantiations: " + aTemplateInstance.getTypeInstantiations().size() + "<br>";
            text += bulletedLinkList(aTemplateInstance.getTypeInstantiations());
            return new ResultEntity(text);
        }

        return null;
    }
}
