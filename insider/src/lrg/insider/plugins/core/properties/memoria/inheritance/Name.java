package lrg.insider.plugins.core.properties.memoria.inheritance;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.InheritanceRelation;

public class Name extends PropertyComputer {

    public Name() {
        super("Name", "The name of the inheritance relation", "inheritance relation", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (!(anEntity instanceof InheritanceRelation)) {
            return null;
        }
        InheritanceRelation rel = (InheritanceRelation)anEntity;
        return new ResultEntity(rel.getSuperClass().getProperty("Name")+"<->"+rel.getSubClass().getProperty("Name"));
    }

}
