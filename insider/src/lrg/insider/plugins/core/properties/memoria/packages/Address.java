package lrg.insider.plugins.core.properties.memoria.packages;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Address extends PropertyComputer {
    public Address() {
        super("Address", "The address of the package", "package", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Package == false)
            return null;

        return new ResultEntity(lrg.insider.metamodel.Address.buildFor((lrg.memoria.core.Package) anEntity));
    }
}
