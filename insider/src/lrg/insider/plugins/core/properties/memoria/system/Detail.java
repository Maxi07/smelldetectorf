package lrg.insider.plugins.core.properties.memoria.system;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.details.HTMLDetail;
import lrg.insider.plugins.details.OverviewPyramid;
import lrg.memoria.core.System;

public class Detail extends HTMLDetail {
    public Detail() {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", "system");
    }

    private String printDesignFlasws(AbstractEntityInterface theSystem) {
        String text = "";
        GroupEntity classes = theSystem.getGroup("class group");
        GroupEntity methods = theSystem.contains("method group").union(theSystem.contains("global function group"));

        classes = classes.applyFilter("model class");
        methods = methods.applyFilter("model function");
        text += "<font bgcolor=#FFE0E0>";

        GroupEntity gp;
        gp = classes.applyFilter("Brain Class");
        if (gp.size() > 0) text += "Brain Class: " + linkToNumber(gp) + "<br>";

        gp = methods.applyFilter("Brain Method");
        if (gp.size() > 0) text += "Brain Method: " + linkToNumber(gp) + " <br>";

        gp = classes.applyFilter("God Class");
        if (gp.size() > 0) text += "God Class: " + linkToNumber(gp) + "<br>";

        gp = classes.applyFilter("Data Class");
        if (gp.size() > 0) text += "Data Class: " + linkToNumber(gp) + "<br>";

        gp = classes.applyFilter("Tradition Breaker");
        if (gp.size() > 0) text += "Tradition Breaker: " + linkToNumber(gp) + " (classes)<br>";

        gp = classes.applyFilter("Refused Parent Bequest");
        if (gp.size() > 0) text += "Refusing Parent Bequest: " + linkToNumber(gp) + " (classes)<br>";

//        gp = classes.applyFilter("Futile Hierarchy");
//        if (gp.size() > 0) text += "Futile Hierarchy: " + linkToNumber(gp) + " (classes)<br>";



        gp = methods.applyFilter("Feature Envy");
        if (gp.size() > 0) text += "Feature Envy [" + linkToNumber(gp) + " methods]<br>";

        gp = methods.applyFilter("Intensive Coupling");
        if (gp.size() > 0) text += "Intensive Coupling [" + linkToNumber(gp) + " methods]<br>";

        gp = methods.applyFilter("Extensive Coupling");
        if (gp.size() > 0) text += "Extensive Coupling [" + linkToNumber(gp) + " methods]<br>";

        gp = methods.applyFilter("Shotgun Surgery");
        if (gp.size() > 0) text += "Shotgun Surgery [" + linkToNumber(gp) + " methods]<br>";

        text += "</font>";
        return text;
    }


    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof System) {
            System aSystem = (System) anEntity;
            String text = "<h1>" + "System Detail: " + linkTo(aSystem) + "</h1><hr><br>";
            
            text += "<table><tr><td valign=\"top\">";
            text += new OverviewPyramid().buildHTMLPyramid(aSystem);
            text += "</td><td valign=\"top\">";
          //  text += printDesignFlasws(anEntity);
            text += "</td></tr></table>";
            
            return new ResultEntity(text);
        } else
            return null;
    }
}

