package lrg.insider.plugins.core.properties.memoria.functions;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.GlobalFunction;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.10.2004
 * Time: 12:39:22
 * To change this template use File | Settings | File Templates.
 */
public class Scope extends PropertyComputer {
    public Scope() {
        super("scope", "The scope of the type", new String[]{"method", "global function"}, "entity");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Function == false)
            return null;

        if (anEntity instanceof GlobalFunction)
            return new ResultEntity(((GlobalFunction)anEntity).getPackage());

        
        return new ResultEntity(((lrg.memoria.core.Function) anEntity).getScope());
    }

}
