package lrg.insider.plugins.core.properties.memoria.packages;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Subsystem extends PropertyComputer {
    public Subsystem() {
        super("Subsystem", "The address of the package", "package", "entity");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
    	return new ResultEntity(((lrg.memoria.core.Package)anEntity).getSubsystem());
    }
}

