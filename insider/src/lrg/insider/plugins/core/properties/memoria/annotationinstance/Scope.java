package lrg.insider.plugins.core.properties.memoria.annotationinstance;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.AnnotationInstance;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 12:57:45
 * To change this template use File | Settings | File Templates.
 */
public class Scope extends PropertyComputer {
    public Scope() {
        super("scope", "The scope of the annotation", "annotation instance", "entity");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.AnnotationInstance == false)
            return null;

        lrg.memoria.core.AnnotationInstance annotationInstance = (AnnotationInstance) anEntity;
        
        return new ResultEntity(annotationInstance.getAnnotatedElement());
    }
}
