package lrg.insider.plugins.core.properties.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.Body;
import lrg.memoria.core.GlobalVariable;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.10.2004
 * Time: 12:30:57
 * To change this template use File | Settings | File Templates.
 */
public class Scope extends PropertyComputer {
    public Scope() {
        super("scope", "The scope of the variable", new String[]{"global variable", "attribute", "local variable", "parameter"}, "entity");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Variable == false) return null;

        if (anEntity instanceof GlobalVariable)
            return new ResultEntity(((GlobalVariable)anEntity).getPackage());
        
        lrg.memoria.core.Scope aScope = (lrg.memoria.core.Scope) ((lrg.memoria.core.Variable) anEntity).getScope();
        if (aScope instanceof Body) aScope = ((Body) aScope).getScope();
        return new ResultEntity(aScope);
    }
}
