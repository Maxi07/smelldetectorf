package lrg.insider.plugins.core.properties.memoria.classes;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.details.HTMLDetail;
import lrg.insider.plugins.core.groups.memoria.containment.ClassHasAttributes;
import lrg.insider.plugins.core.groups.memoria.containment.ClassHasMethods;
import lrg.memoria.core.Class;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.GenericClass;
import lrg.memoria.core.ModelElementList;

public class Detail extends HTMLDetail {
    public Detail() {
        super("Detail", "The detailed information for a class.", "class");
    }

 
    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof GenericClass) {
            GenericClass aGenericClass = (GenericClass) anEntity;
            String text = "<big>" + "Generic Class Detail: " + linkTo(aGenericClass) + "</big><hr><br>";
            text += "Full Name: " + aGenericClass.getFullName() + "<br>";
            text += "Number of parameters: " + aGenericClass.getTemplateParameters().size() + "<br>";
            text += bulletedLinkList(aGenericClass.getTemplateParameters());
            text += "Number of instances: " + aGenericClass.getTemplateInstances().size() + "<br>";
            text += bulletedLinkList(aGenericClass.getTemplateInstances());
            return new ResultEntity(text);
        }

        if (anEntity instanceof Class) {
            Class aClass = (Class) anEntity;
            
            String amc = getAccessModeHTML(aClass.getAccessMode());

            String sik = classFlawsDecorations(anEntity);
            String text = "<big>" + getKindOf(aClass) + " </big><b>" + amc + "</b><big> " + linkTo(aClass.getPackage()) + " . " + linkTo(aClass) + "</big>" +
                    "<br>" + sik + "" +
                    "<hr><br>";
            text += buildInheritanceHierarchy(aClass) + "<hr>";

            text += "<table><tr><td valign=\"top\">";
            text += "<b>methods (" + linkToNumber(new ClassHasMethods().buildGroupEntity(aClass)) + ")</b><br>";
            text += bulletedLinkList(aClass.getMethodList(), new MethodDecorations());
            text += "</td><td valign=\"top\">";
            text += "<b>attributes (" + linkToNumber(new ClassHasAttributes().buildGroupEntity(aClass)) + ")</b><br>";
            text += bulletedLinkList(aClass.getAttributeList(), new AttributeDecorations());
            text += "</td></tr></table>";

            text += "Entity Type " + aClass.getEntityType();
            text += "<br>    Statute: " + aClass.getStatute();
            text += "<br> Elem ID: " + aClass.getElementID();
            return new ResultEntity(text);
        } else
            return null;
    }

    private String getKindOf(DataAbstraction aClass) {
        if (aClass.isStructure())
            return "Structure";
        if (aClass.isInterface())
            return "Interface";
        if (aClass.isUnion())
            return "Union";
        return "class";
    }

    private String buildInheritanceHierarchy(DataAbstraction aClass) {
        String text = "";
        ArrayList ancestors = new ArrayList();
        DataAbstraction c = aClass;
        ModelElementList<DataAbstraction> ancestorsList;
        while (c != null) {
            ancestors.add(c);
            ancestorsList = c.getAncestorsList();
            if (ancestorsList.size() > 0)
                c = (DataAbstraction) ancestorsList.get(0);
            else
                break;
        }
        for (int i = 1; i <= ancestors.size(); i++) {
/*            if (i!=1)
                text += image("images/inherit.gif");
            else
                text += image("images/inherit_.gif");
*/
//            if (i != 1) text += "|__ ";
            text += linkTo((DataAbstraction) ancestors.get(ancestors.size() - i)) + "<br>";
            for (int j = 0; j < i; j++) text += "\t\t";
 //           if (i != ancestors.size()) text += "|<br>";
            for (int j = 0; j < i; j++) text += "\t\t";
        }
        return text;
    }

}

