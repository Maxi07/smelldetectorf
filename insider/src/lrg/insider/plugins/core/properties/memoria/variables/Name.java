package lrg.insider.plugins.core.properties.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Name extends PropertyComputer {
    public Name() {
        super("Name", "Name of the variable", new String[]{"global variable", "attribute", "local variable", "parameter"}, "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Variable == false)
            return new ResultEntity("");

        return new ResultEntity(((lrg.memoria.core.Variable) anEntity).getName());
    }
}
