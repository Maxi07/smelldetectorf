package lrg.insider.plugins.core.properties.memoria.system;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Name extends PropertyComputer {
    public Name() {
        super("Name", "The name of the entity", "system", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.System == false)
            return null;

        String fullname = ((lrg.memoria.core.System) anEntity).getName();
        return new ResultEntity(fullname.substring(fullname.lastIndexOf("\\") + 1));
    }
}
