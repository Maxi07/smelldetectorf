package lrg.insider.plugins.core.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Name extends PropertyComputer
{
    public Name()
    {
        super("Name", "The name of the class", "class", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;

        return new ResultEntity(((lrg.memoria.core.Class) anEntity).getName());
    }
}
