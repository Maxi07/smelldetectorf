package lrg.insider.plugins.core.properties.memoria.annotation;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 12:57:39
 * To change this template use File | Settings | File Templates.
 */
public class Name extends PropertyComputer
{
    public Name()
    {
        super("Name", "The name of the annotation", "annotation", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
    	   if(anEntity instanceof lrg.memoria.core.Annotation == false)	return null;

        return new ResultEntity(anEntity.getName());
    }
}