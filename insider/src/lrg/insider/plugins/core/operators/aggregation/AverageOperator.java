package lrg.insider.plugins.core.operators.aggregation;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.AggregationOperator;

public class AverageOperator extends AggregationOperator
{
    public AverageOperator()
    {
        super("avg");
    }

    public ResultEntity aggregate(ArrayList resultGroup)
    {
        int size = resultGroup.size();
        if (size == 0) return new ResultEntity(0);
        double sum = 0;


        Iterator it = resultGroup.iterator();

        while (it.hasNext())
            sum += ((Double) (((ResultEntity) it.next()).getValue())).doubleValue();

        double res = sum / size;

        return new ResultEntity(((float)Math.round(res * 100)) / 100);
    }
}
