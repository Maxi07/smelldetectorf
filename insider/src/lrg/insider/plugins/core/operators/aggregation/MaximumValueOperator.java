package lrg.insider.plugins.core.operators.aggregation;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.AggregationOperator;

public class MaximumValueOperator extends AggregationOperator
{
    public MaximumValueOperator()
    {
        super("max");
    }

    public ResultEntity aggregate(ArrayList resultGroup)
    {
        int size = resultGroup.size();
        if (size == 0) return new ResultEntity(0);
        double max = -999;


        Iterator it = resultGroup.iterator();

        while (it.hasNext()) {
            double currentValue = ((Double) (((ResultEntity) it.next()).getValue())).doubleValue();
            if(currentValue > max) max = currentValue;
         }
	
        return new ResultEntity(max);
    }
}
