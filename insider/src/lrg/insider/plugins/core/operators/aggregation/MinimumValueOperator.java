package lrg.insider.plugins.core.operators.aggregation;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.AggregationOperator;

public class MinimumValueOperator extends AggregationOperator
{
    public MinimumValueOperator()
    {
        super("min");
    }

    public ResultEntity aggregate(ArrayList resultGroup)
    {
        int size = resultGroup.size();
        if (size == 0) return new ResultEntity(0);
        double min = 99999;


        Iterator it = resultGroup.iterator();

        while (it.hasNext()) {
            double currentValue = ((Double) (((ResultEntity) it.next()).getValue())).doubleValue();
            if(currentValue < min) min = currentValue;
         }
	
        return new ResultEntity(min);
    }
}
