package lrg.insider.plugins.core.operators.aggregation;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.AggregationOperator;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.05.2004
 * Time: 20:32:27
 * To change this template use File | Settings | File Templates.
 */
public class SumOperator extends AggregationOperator
{
    public SumOperator()
    {
        super("sum");
    }

    public ResultEntity aggregate(ArrayList resultGroup)
    {
        double sum = 0;

        Iterator it = resultGroup.iterator();
        while (it.hasNext())
            sum += ((Double) (((ResultEntity) it.next()).getValue())).doubleValue();

        return new ResultEntity(sum);
    }
}
