package lrg.insider.plugins.core.operators.aggregation;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.AggregationOperator;

public class PercTrueOperator extends AggregationOperator {
    public PercTrueOperator()
    {
        super("percTrue", "boolean");
    }

    public ResultEntity aggregate(ArrayList resultGroup)
    {
        int size = resultGroup.size();
        if (size == 0) return new ResultEntity(0);
        double countTrue = 0;
	Iterator it = resultGroup.iterator();

        while (it.hasNext())     	
             if (((Boolean) (((ResultEntity) it.next()).getValue())).booleanValue()) countTrue++;

        double res = countTrue	 / size;
        return new ResultEntity(((float)Math.round(res * 100)) / 100); 
    }
}
