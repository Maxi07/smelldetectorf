package lrg.insider.plugins.core.operators.string;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.FilteringOperatorWithThresholds;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 08.06.2006
 * Time: 12:36:06
 * To change this template use File | Settings | File Templates.
 */
public class Equals extends FilteringOperatorWithThresholds {
    public Equals()
    {
        super("==", "string");
    }

    public boolean apply(ResultEntity theResult, Object threshold)
    {
        if (theResult.getValue() instanceof String == false) return false;
        if (threshold instanceof String == false) return false;

        return ((String)theResult.getValue()).compareTo((String)threshold) == 0;
    }
}
