package lrg.insider.plugins.core.operators.string;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.FilteringOperatorWithThresholds;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 17.05.2004
 * Time: 20:49:06
 * To change this template use File | Settings | File Templates.
 */
public class StartsWith extends FilteringOperatorWithThresholds {
    public StartsWith()
    {
        super("starts as", "string");
    }

    public boolean apply(ResultEntity theResult, Object threshold)
    {
        if (theResult.getValue() instanceof String == false) return false;
        if (threshold instanceof String == false) return false;

        return ((String) theResult.getValue()).toLowerCase().startsWith(((String)threshold).toLowerCase());
    }
}


