package lrg.insider.plugins.core.operators.bool;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.FilteringOperator;

public class IsTrue extends FilteringOperator {
    public IsTrue() {
        super("is true", "boolean");
    }

    public boolean apply(ResultEntity theResult) {
        if (theResult.getValue() instanceof Boolean) {
            return ((Boolean) theResult.getValue()).booleanValue();
        } else
            return false;
    }
}
