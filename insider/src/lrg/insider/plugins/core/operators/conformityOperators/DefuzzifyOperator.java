package lrg.insider.plugins.core.operators.conformityOperators;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.ConformityOperatorWithThresholds;

/**
 * Created by Horia Radu.
 * User: horia
 * Date: 03.05.2004
 * Time: 12:52:02
 * To change this template use File | Settings | File Templates.
 */
public class DefuzzifyOperator extends ConformityOperatorWithThresholds
{
    public DefuzzifyOperator()
    {
        super("*", "numerical");
    }

    public Double apply(ResultEntity theResult, Object threshold)
    {
        if (theResult.getValue() instanceof Double == false) return new Double(0);
        if (threshold instanceof Double == false) return new Double(0);

        if ((Double) theResult.getValue() > (Double)threshold) return new Double(100);
        return new Double(0);
    }
}
