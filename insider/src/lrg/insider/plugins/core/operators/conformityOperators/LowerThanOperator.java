package lrg.insider.plugins.core.operators.conformityOperators;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.ConformityOperatorWithThresholds;

/**
 * Created by Horia Radu.
 * User: horia
 * Date: 03.05.2004
 * Time: 17:13:33
 * To change this template use File | Settings | File Templates.
 */
public class LowerThanOperator extends ConformityOperatorWithThresholds
{
    public LowerThanOperator()
    {
        super("<", "numerical");
    }

    public Double apply(ResultEntity theResult, Object threshold)
    {
    	if (theResult.getValue() instanceof Double == false) return new Double(0);
        if (threshold instanceof Double == false) return new Double(0);
        
        if (valueIsZero((Double)theResult.getValue()))
        	return 100.00;

        return ((Double) threshold / ((Double) theResult.getValue())) * 100;
    }
    
    private boolean valueIsZero(Double value) {
//    	int x = (int)(value * 100);
//    	return x == 0;
    	if (value < 0.1)
    		return true;
    	return false;
//    	return value == 0;
    }
}
