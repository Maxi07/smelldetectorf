package lrg.insider.plugins.core.operators.numerical;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.FilteringOperatorWithThresholds;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 03.05.2004
 * Time: 17:13:33
 * To change this template use File | Settings | File Templates.
 */
public class LowerThanOperator extends FilteringOperatorWithThresholds
{
    public LowerThanOperator()
    {
        super("<", "numerical");
    }

    public boolean apply(ResultEntity theResult, Object threshold)
    {
        if (theResult.getValue() instanceof Double == false) return false;
        if (threshold instanceof Double == false) return false;

        return ((Double) theResult.getValue()).compareTo((Double) threshold) < 0;
    }
}
