package lrg.insider.plugins.core.operators.numerical;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.FilteringOperatorWithThresholds;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.02.2005
 * Time: 18:48:41
 * To change this template use File | Settings | File Templates.
 */
public class Equal extends FilteringOperatorWithThresholds {
    public Equal() {
        super("==", "numerical");
    }

    public boolean apply(ResultEntity theResult, Object threshold) {
        if (theResult.getValue() instanceof Double == false) return false;
        if (threshold instanceof Double == false) return false;

        return ((Double) theResult.getValue()).compareTo((Double) threshold) == 0;
    }
}