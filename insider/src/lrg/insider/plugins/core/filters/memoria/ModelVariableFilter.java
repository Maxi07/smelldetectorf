package lrg.insider.plugins.core.filters.memoria;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.Statute;

public class ModelVariableFilter extends FilteringRule {
    public ModelVariableFilter() {
        super(new Descriptor("model variable", "", new String[]{"attribute", "global variable"}));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Variable == false) return false;

        return ((lrg.memoria.core.Variable) anEntity).getStatute() == Statute.NORMAL;
    }
}

