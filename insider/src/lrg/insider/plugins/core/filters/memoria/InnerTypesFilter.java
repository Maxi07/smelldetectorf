package lrg.insider.plugins.core.filters.memoria;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.Namespace;

public class InnerTypesFilter extends FilteringRule
{
    public InnerTypesFilter()
    {
        super(new Descriptor("inner types", "type"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Type == false) return false;

        return !(((lrg.memoria.core.Type) anEntity).getScope() instanceof Namespace);
    }
}
