package lrg.insider.plugins.core.filters.memoria;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.UnnamedNamespace;

public class UnnamedNamespacesFilter extends FilteringRule {

    public UnnamedNamespacesFilter() {
        super(new Descriptor("unnamed namespaces", "namespace"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        return anEntity instanceof UnnamedNamespace;
    }
}
