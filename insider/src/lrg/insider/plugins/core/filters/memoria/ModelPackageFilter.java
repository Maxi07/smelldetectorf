package lrg.insider.plugins.core.filters.memoria;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.Statute;

public class ModelPackageFilter extends FilteringRule
{
    public ModelPackageFilter()
    {
        super(new Descriptor("model package", "package"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Package == false) return false;

        return ((lrg.memoria.core.Package) anEntity).getStatute() == Statute.NORMAL;
    }
}
