package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.GlobalVariable;
import lrg.memoria.core.ModelElementList;

public class PackageHasGlobalVariables extends GroupBuilder {
    public PackageHasGlobalVariables() {
        super("global variable group", "", new String[]{"package", "namespace"});
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        ArrayList onlyGlobalVariables = new ArrayList();
        if (anEntity instanceof lrg.memoria.core.Package == false)
            return onlyGlobalVariables;

        ModelElementList variablesList = ((lrg.memoria.core.Package) anEntity).getGlobalVariablesList();
        for(int i = 0; i < variablesList.size(); i++)
           if(variablesList.get(i) instanceof GlobalVariable)
               if(((GlobalVariable)variablesList.get(i)).getRefersTo() == null)
                onlyGlobalVariables.add(variablesList.get(i));
        return onlyGlobalVariables;
    }
}
