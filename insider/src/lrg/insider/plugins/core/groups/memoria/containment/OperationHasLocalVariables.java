package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class OperationHasLocalVariables extends GroupBuilder
{
    public OperationHasLocalVariables()
    {
        super("local variable group", "", new String[] {"method", "global function"});
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Function == false)
            return new ArrayList();
        ArrayList lv = ((lrg.memoria.core.Function) anEntity).getBody().getLocalVarList();
        return lv;
    }
}


