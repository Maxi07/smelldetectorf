package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.Method;
import lrg.memoria.core.ModelElementList;
import lrg.memoria.core.Parameter;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 05.11.2004
 * Time: 15:53:19
 * To change this template use File | Settings | File Templates.
 */
public class MethodOverrides extends GroupBuilder {
    public MethodOverrides() {
        super("methods overriden", "", new String[]{"global function", "method"});
    }

    public ArrayList buildGroup(AbstractEntityInterface aFunction) {
        ArrayList resultList = new ArrayList();

        if (aFunction instanceof lrg.memoria.core.Method == false) return resultList;

        lrg.memoria.core.Method aMethod = (lrg.memoria.core.Method) aFunction;

        if (aMethod.getBody() == null) return resultList;

        GroupEntity methodsOfAncestors = aMethod.belongsTo("class").uses("all ancestors").applyFilter("model class").contains("method group");

        Iterator it = methodsOfAncestors.iterator();
        lrg.memoria.core.Method ancestorMethod;

        while (it.hasNext()) {
            ancestorMethod = (lrg.memoria.core.Method) it.next();
            if (isOverriding(aMethod, ancestorMethod)) resultList.add(ancestorMethod);
        }
        return resultList;
    }

    private boolean isOverriding(Method aMethod, Method ancestorMethod) {
        if (aMethod.getName().compareTo(ancestorMethod.getName()) != 0) return false;

        ModelElementList<Parameter> methodParameters = aMethod.getParameterList();
        ModelElementList<Parameter> ancestorsParameters = ancestorMethod.getParameterList();

        if (methodParameters.size() != ancestorsParameters.size()) return false;

        ArrayList paramTypes = new ArrayList();
        ArrayList paramTypesOfAncestor = new ArrayList();

        for (Iterator it = methodParameters.iterator(); it.hasNext(); paramTypes.add(((Parameter) it.next()).getType())) ;
        for (Iterator it = ancestorsParameters.iterator(); it.hasNext(); paramTypesOfAncestor.add(((Parameter) it.next()).getType())) ;

        return paramTypes.containsAll(paramTypesOfAncestor);
    }

}

