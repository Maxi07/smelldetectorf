package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.DataAbstraction;

public class ClassHasAttributes extends GroupBuilder
{
    public ClassHasAttributes()
    {
        super("attribute group", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return new ArrayList();
        return ((DataAbstraction) anEntity).getAttributeList();
    }
}
