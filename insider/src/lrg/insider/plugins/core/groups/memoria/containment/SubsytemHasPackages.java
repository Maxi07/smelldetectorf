package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class SubsytemHasPackages extends GroupBuilder {
	    public SubsytemHasPackages() {
	        super("package group", "", "subsystem");
	    }

	    public ArrayList buildGroup(AbstractEntityInterface aSubsystem) {
	    	return ((lrg.memoria.core.Subsystem)aSubsystem).getPackages();
	    }
}