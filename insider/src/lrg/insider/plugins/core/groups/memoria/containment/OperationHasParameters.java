package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.10.2004
 * Time: 14:26:12
 * To change this template use File | Settings | File Templates.
 */
public class OperationHasParameters extends GroupBuilder {
    public OperationHasParameters() {
        super("parameter group", "", new String[]{"method", "global function"});
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Function == false)
            return new ArrayList();
        ArrayList lv = ((lrg.memoria.core.Function) anEntity).getParameterList();
        return lv;
    }
}
