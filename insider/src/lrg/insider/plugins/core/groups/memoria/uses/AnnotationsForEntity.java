package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.NamedModelElement;

public class AnnotationsForEntity extends GroupBuilder {
    public AnnotationsForEntity() {
        super("annotation instances (for all entities)", "", new String[]{"method", "class", "attribute", "local variable", "parameter"});
    }

	@Override
	public ArrayList buildGroup(AbstractEntityInterface anEntity) {
		ArrayList theAnnotations = new ArrayList();
		
		theAnnotations.addAll(((NamedModelElement)anEntity).getAnnotations());
		if(anEntity instanceof lrg.memoria.core.Class) {
			GroupEntity members = anEntity.contains("method group").union(anEntity.contains("attribute group"));
			theAnnotations.addAll(members.getGroup("annotation instances (for all entities)").getElements());
		}
		
		if(anEntity instanceof lrg.memoria.core.Method) {
			GroupEntity members = anEntity.contains("parameter group").union(anEntity.contains("local variable group"));
			theAnnotations.addAll(members.getGroup("annotation instances (for all entities)").getElements());
		}

		return theAnnotations;
	}

}
