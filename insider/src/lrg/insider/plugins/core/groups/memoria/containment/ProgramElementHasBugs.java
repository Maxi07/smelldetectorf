package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class ProgramElementHasBugs extends GroupBuilder
{
    public ProgramElementHasBugs()
    {
        super("bugs group", "", new String[] {"package", "class", "method", "global function"});
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.NamedModelElement == false) return new ArrayList();
        return ((lrg.memoria.core.NamedModelElement) anEntity).getBugs();
    }
}