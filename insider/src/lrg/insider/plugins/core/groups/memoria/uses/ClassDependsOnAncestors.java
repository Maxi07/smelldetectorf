package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;
import java.util.HashSet;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 15.10.2004
 * Time: 18:52:49
 * To change this template use File | Settings | File Templates.
 */
public class ClassDependsOnAncestors extends GroupBuilder {
    public ClassDependsOnAncestors() {
        super("all ancestors", "", "class");
    }

    private boolean noCycles(AbstractEntityInterface child, AbstractEntity parent) {
        return parent.getGroup("base classes").intersect(child).size() == 0;
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        HashSet<AbstractEntity> setOfBaseClasses = new HashSet<AbstractEntity>();
        HashSet<AbstractEntity> tmp = new HashSet<AbstractEntity>();

        if (anEntity instanceof lrg.memoria.core.Class == false)
            return new ArrayList();

        if(anEntity.getName().compareTo("<unknownClassType>") == 0)
           return new ArrayList();
        setOfBaseClasses.addAll(anEntity.getGroup("base classes").getElements());
        if (setOfBaseClasses.isEmpty()) return new ArrayList();

        tmp.addAll(setOfBaseClasses);
        for(AbstractEntity crtClass : setOfBaseClasses) {
            if(noCycles(anEntity, crtClass)) {
              GroupEntity allAncestors = crtClass.getGroup("all ancestors");
              tmp.addAll(allAncestors.getElements());
            }
            else System.out.println("CLASS-->ANCESTOR CYCLE: "+ anEntity.getName() + " --> " + crtClass.getName());
        }
        return new ArrayList(tmp);
    }

}
