package lrg.insider.plugins.core.groups.memoria.containment;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.ModelElementList;
import lrg.memoria.core.Namespace;

public class SystemHasNamespaces extends GroupBuilder {

    public SystemHasNamespaces()
    {
        super("namespace group", "", "system");
    }

    public ModelElementList<Namespace> buildGroup(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.System == false)
            return new ModelElementList<Namespace>();

        return ((lrg.memoria.core.System) anEntity).getNamespaces();
    }

}
