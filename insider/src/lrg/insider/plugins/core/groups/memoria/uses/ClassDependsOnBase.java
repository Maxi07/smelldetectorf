package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.DataAbstraction;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 15.10.2004
 * Time: 18:40:14
 * To change this template use File | Settings | File Templates.
 */
public class ClassDependsOnBase extends GroupBuilder {
    public ClassDependsOnBase() {
        super("base classes", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {

        DataAbstraction aClass;

        ArrayList resultList = new ArrayList();
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return resultList;

        ArrayList result = ((DataAbstraction) anEntity).getAncestorsList();
        result.remove(anEntity);
        return result;
    }
}
