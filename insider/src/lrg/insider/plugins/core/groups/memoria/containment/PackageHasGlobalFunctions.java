package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 03.06.2004
 * Time: 10:31:46
 * To change this template use File | Settings | File Templates.
 */
public class PackageHasGlobalFunctions extends GroupBuilder {
    public PackageHasGlobalFunctions() {
        super("global function group", "global functions", new String[]{"package", "namespace"});
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        ArrayList functionsList = new ArrayList();
        if (anEntity instanceof lrg.memoria.core.Package == false)
            return functionsList;
        functionsList = ((lrg.memoria.core.Package)anEntity).getGlobalFunctionsList();
        return functionsList;
    }
}
