package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 24.05.2004
 * Time: 16:05:18
 * To change this template use File | Settings | File Templates.
 */
public class OperationCalls extends GroupBuilder {
    public OperationCalls() {
        super("operations called", "", new String[]{"global function", "method"});
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        ArrayList resultList = new ArrayList();

         if (anEntity instanceof lrg.memoria.core.Function == false)
            return resultList;

        lrg.memoria.core.Function aMethod = (lrg.memoria.core.Function)anEntity;
        if(aMethod.getBody() == null) return resultList;

        if(aMethod.getBody().getCallList() == null) return resultList;

        Iterator it = aMethod.getBody().getCallList().iterator();
        while(it.hasNext()) {
            lrg.memoria.core.Call theCall = (lrg.memoria.core.Call) it.next();
            resultList.add(theCall.getFunction());
        }
        return resultList;
    }

}
