package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class NOA extends PropertyComputer {
    public NOA() {
        super("NOA", "Number of Attributes", "class", "numerical");
    }


    public ResultEntity compute(AbstractEntityInterface anEntity) {
        return new ResultEntity(anEntity.contains("attribute group").size());
    }
}
