package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.variables.IsTemporaryField;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.02.2005
 * Time: 20:32:18
 * To change this template use File | Settings | File Templates.
 */
public class NTempF extends PropertyComputer {
    public NTempF() {
        super("NTempF", "Number of Temporary Fields", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface aClass) {
        return new ResultEntity(aClass.contains("attribute group").applyFilter(new IsTemporaryField()).size());
    }
}
