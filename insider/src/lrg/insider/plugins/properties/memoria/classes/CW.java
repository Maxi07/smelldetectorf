package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;
import lrg.insider.plugins.filters.memoria.methods.IsEmpty;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 31.08.2004
 * Time: 20:54:44
 * To change this template use File | Settings | File Templates.
 */
public class CW extends PropertyComputer {
    public CW() {
        super("CW", "Class Weight", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;

        // NumericalResult aResult = (NumericalResult) new lrg.metrics.classes.WeightOfAClass().measure((lrg.memoria.core.Class) anEntity);

        FilteringRule notConstructor = new NotComposedFilteringRule(new IsConstructor());
        FilteringRule notEmpty = new NotComposedFilteringRule(new IsEmpty());
        FilteringRule notAccessor = new NotComposedFilteringRule(new IsAccessor());
        FilteringRule filter = new AndComposedFilteringRule(
        		new AndComposedFilteringRule(notConstructor, notEmpty), notAccessor);
        
        // FilteringRule isAccessor = new FilteringRule("IsAccessor", "IsTrue", "method");
        GroupEntity publicMethods = anEntity.contains("method group").applyFilter("is public").applyFilter(notConstructor);
        int interfaceWidth = publicMethods.size();
        int functionalMethods = publicMethods.applyFilter(filter).size();

        return new ResultEntity((double)functionalMethods / (double)interfaceWidth);
    }
}
