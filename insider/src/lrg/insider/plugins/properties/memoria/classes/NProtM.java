package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class NProtM extends PropertyComputer {
    public NProtM() {
        super("NProtM", "Number of Protected Methods", "class", "numerical");
    }


    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;
        return new ResultEntity(anEntity.getGroup("method group").applyFilter("is protected").size());
    }
}
