package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.classes.IsInterface;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 05.11.2004
 * Time: 18:46:10
 * To change this template use File | Settings | File Templates.
 */
public class BUR extends PropertyComputer {
    public BUR() {
        super("BUR", "Base Class Usage Ratio", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        double totalProtected = 0.0, usedProtected = 0.0;
        GroupEntity protectedMethods, protectedAttributes;
        GroupEntity usedprotectedMethods, usedprotectedAttributes;
        GroupEntity baseClasses = anEntity.uses("base classes").applyFilter("model class");
        baseClasses.applyFilter(new NotComposedFilteringRule(new IsInterface()));
        if (baseClasses.size() == 0) return new ResultEntity(1);

        protectedMethods = baseClasses.contains("method group").applyFilter("is protected");
        protectedMethods.applyFilter(new NotComposedFilteringRule(new IsConstructor()));
        protectedAttributes = baseClasses.contains("attribute group").applyFilter("is protected");

        totalProtected = protectedAttributes.size() + protectedMethods.size();

        if (totalProtected < 3) return new ResultEntity(1);

        usedprotectedMethods = anEntity.uses("operations called").distinct().applyFilter("is protected");
        usedprotectedAttributes = anEntity.uses("variables accessed").distinct().applyFilter("is protected");

        usedProtected = protectedAttributes.intersect(usedprotectedAttributes).size() +
                protectedMethods.intersect(usedprotectedMethods).size() +
                protectedMethods.intersect(anEntity.uses("methods overriden")).size();

        return new ResultEntity(usedProtected / totalProtected);
    }
}

