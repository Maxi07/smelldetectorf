package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.ClassExternalServiceProviders;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.01.2005
 * Time: 12:12:37
 * To change this template use File | Settings | File Templates.
 */
public class FANOUT extends PropertyComputer {
    public FANOUT() {
        super("FANOUT", "FANOUT (Dependency Intensity) aggregated at class level", "class", "numerical");
        basedOnGroup(new ClassExternalServiceProviders());
    }
}
