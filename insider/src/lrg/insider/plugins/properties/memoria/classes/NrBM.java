package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 31.01.2005
 * Time: 18:25:33
 * To change this template use File | Settings | File Templates.
 */
public class NrBM extends PropertyComputer {
    public NrBM() {
        super("NrBM", "Number of methods detected as Brain Method", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        GroupEntity aGroup = measuredClass.getGroup("method group");

        return new ResultEntity(aGroup.applyFilter("Brain Method").size());
    }
}