package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class NOD extends PropertyComputer {
    public NOD() {
        super("NOD", "Number of Descendants", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        return new ResultEntity(measuredClass.getGroup("all descendants").size());
    }
}
