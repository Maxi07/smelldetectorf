package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.Class;
import lrg.metrics.NumericalResult;
import lrg.metrics.classes.DepthOfInheritanceTree;

public class DIT extends PropertyComputer {
    public DIT() {
        super("DIT", "Depth of Inheritance Tree", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;

        NumericalResult aResult = (NumericalResult) new DepthOfInheritanceTree().measure((Class) anEntity);
        return new ResultEntity(aResult.getValue());
    }
}
