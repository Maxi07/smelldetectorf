package lrg.insider.plugins.properties.memoria.classes;

import java.util.ArrayList;
import java.util.HashMap;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.tools.ClusteringAlgorithm;

public class AttributeUsageClusters extends PropertyComputer {
	
    public AttributeUsageClusters() {
        super("Attribute Usage Clusters", "Attribute Usage Clusters", "class", "numerical");
    }

    private HashMap<AbstractEntity, GroupEntity> createInitialMap(AbstractEntityInterface measuredClass) {
		HashMap<AbstractEntity, GroupEntity> entity2Partners = new HashMap<AbstractEntity, GroupEntity>();
		GroupEntity attributesAccessed; 
		ArrayList<AbstractEntity> methods = measuredClass.contains("method group").getElements();
    	for(AbstractEntity crtMethod : methods) {
    		attributesAccessed = crtMethod.getGroup("variables accessed").applyFilter("is attribute").distinct().intersect(measuredClass.contains("attribute group"));
    		if(attributesAccessed.size() > 0) 
    			entity2Partners.put(crtMethod, attributesAccessed);
    	}
		
		return entity2Partners;
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
		HashMap<AbstractEntity, GroupEntity> initialMapping = createInitialMap(measuredClass);
		return new ResultEntity(new ClusteringAlgorithm(initialMapping).cluster());
    }        
	
 }

