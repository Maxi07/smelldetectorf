package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.DataAbstraction;

public class CodeReuseIntention extends PropertyComputer {

    public CodeReuseIntention() {
        super("CodeReuseIntention", "Code Reuse Intention", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface aClass) {

        DataAbstraction theClass = (DataAbstraction)aClass;
        if((Double)theClass.getProperty("PCR").getValue() < 0 || (Double)theClass.getProperty("SCR").getValue() < 0) {
            return new ResultEntity(-1);
        }
        return new ResultEntity((Double)aClass.getProperty("PCR").getValue() + (Double)aClass.getProperty("SCR").getValue());
    }

}
