package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.DataAbstraction;

public class LOC extends PropertyComputer {

    public LOC() {
           super("LOCC", "Lines of Code in Class", "class", "numeric");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        DataAbstraction aClass = (DataAbstraction) anEntity;
        return new ResultEntity(aClass.getLocation().getEndLine() - aClass.getLocation().getStartLine() + 1);
    }

}

