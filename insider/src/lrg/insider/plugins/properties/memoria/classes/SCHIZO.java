package lrg.insider.plugins.properties.memoria.classes;

import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.Threshold;
import lrg.insider.plugins.filters.memoria.classes.TemporaryFieldHost;
import lrg.insider.plugins.filters.memoria.classes.TraditionBreaker;

public class SCHIZO extends PropertyComputer {
    public SCHIZO() {
        super("SCHIZO", "Schizofrenia degree of a class", "class", "numerical");
    }


    private double loc(AbstractEntityInterface anEntity) {
        double result = 0.0;
        Iterator it = anEntity.getProperty("LOC").getValueAsCollection().iterator();
        while (it.hasNext()) {
            Object tmp = it.next();
            if (tmp instanceof ResultEntity)
                result += ((Double) ((ResultEntity) tmp).getValue()).doubleValue();
        }
        return result;
    }

    private boolean refusedParentBequestInChildren(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false) return false;

        return anEntity.getGroup("derived classes").applyFilter("Refused Parent Bequest").size() > 0;
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        FilteringRule lowTCC = new FilteringRule("TCC", "<", "class", Threshold.ONE_THIRD);
        FilteringRule noTemporaryFields = new TemporaryFieldHost();
        FilteringRule veryHighWMC = new FilteringRule("WMC", ">", "class", Threshold.WMC_VERYHIGH);
        FilteringRule manyAttributes = new FilteringRule("NOA", ">", "class", Threshold.SMemCap);
        FilteringRule extensiveCoupling = new FilteringRule("NrEC", ">", "class", Threshold.NONE);
        FilteringRule traditionBreaker = new TraditionBreaker();
        boolean largeAndComplex = (loc(anEntity) > Threshold.LOC_CLASS_VERYHIGH) &&
                (veryHighWMC.applyFilter(anEntity));
        int counter = 0;


        if (lowTCC.applyFilter(anEntity) && manyAttributes.applyFilter(anEntity) == false)
            return new ResultEntity(counter);

        if (largeAndComplex) counter += 10000;
        if (extensiveCoupling.applyFilter(anEntity)) counter += 1000;
        if (traditionBreaker.applyFilter(anEntity)) counter += 100;
        if (refusedParentBequestInChildren(anEntity)) counter += 10;
        if (noTemporaryFields.applyFilter(anEntity)) counter++;

        return new ResultEntity(counter);
    }

}
