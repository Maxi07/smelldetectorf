package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.ClassWithOverwritingMethods;

public class BOvM extends PropertyComputer {
    public BOvM() {
        super("BOvM", "Baseclass Overwriting Methods", "class", "numerical");
        basedOnGroup(new ClassWithOverwritingMethods());
    }
}
