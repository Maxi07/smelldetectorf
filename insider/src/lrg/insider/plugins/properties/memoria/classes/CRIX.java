package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class CRIX extends PropertyComputer {
    public CRIX() {
        super("CRIX", "Criticality Index", "class", "numerical");
    }
     
    public ResultEntity compute(AbstractEntityInterface measuredClass) {
    	double crix = 0;
    	GroupEntity methods = measuredClass.contains("method group");
        Boolean BrainClass = (Boolean) measuredClass.getProperty("Brain Class").getValue();

    	Boolean GodClass = (Boolean) measuredClass.getProperty("God Class").getValue();
        Boolean DataClass = (Boolean) measuredClass.getProperty("Data Class").getValue();
        Boolean TraditionBreaker = (Boolean) measuredClass.getProperty("Tradition Breaker").getValue();
        Boolean RefusedParentBequest = (Boolean) measuredClass.getProperty("Refused Parent Bequest").getValue();
        
        
        int FeatureEnvy = methods.applyFilter("Feature Envy").size();
        int IntensiveCoupling = methods.applyFilter("Intensive Coupling").size();
        int BrainMethod = methods.applyFilter("Brain Method").size();
        
        if(GodClass) crix += 2;
        if(DataClass) crix +=2;
        if(BrainClass) crix +=2;
        if(TraditionBreaker) crix += 1;
        if(RefusedParentBequest) crix += 1;
        
        crix += IntensiveCoupling*0.5;
        crix += FeatureEnvy*0.5;
        crix += BrainMethod*0.5;
        
        return new ResultEntity(crix);
    }
}
