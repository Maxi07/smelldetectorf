package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class NAbsM extends PropertyComputer {
    public NAbsM() {
        super("NAbsM", "Number of Abstract Methods", "class", "numerical");
    }


    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return new ResultEntity(0);

        return new ResultEntity(anEntity.contains("method group").applyFilter("is abstract").size());
    }
}