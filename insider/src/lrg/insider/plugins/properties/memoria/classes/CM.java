package lrg.insider.plugins.properties.memoria.classes;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupEntityBuilder;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;
import lrg.insider.plugins.filters.memoria.methods.IsStatic;

class ChangingMethods extends GroupEntityBuilder {
    public ChangingMethods() {
        super("group of changing methods",
                "methods that would be potentiall affected by a change in analyzed class", "class");
    }


    public GroupEntity buildGroupEntity(AbstractEntityInterface measuredClass) {
        FilteringRule notStatic = new NotComposedFilteringRule(new IsStatic());
        FilteringRule notConstructor = new NotComposedFilteringRule(new IsConstructor());

        GroupEntity interestingMethods = measuredClass.getGroup("method group").applyFilter(notConstructor).applyFilter(notStatic);
        GroupEntity clientMethods = interestingMethods.isUsed("operations calling me");

        GroupEntity relatedMethods = measuredClass.contains("method group");
        relatedMethods = relatedMethods.union(measuredClass.getGroup("all ancestors").applyFilter("model class").getGroup("method group"));
        relatedMethods = relatedMethods.union(measuredClass.getGroup("all descendants").applyFilter("model class").getGroup("method group"));
        return clientMethods.exclude(relatedMethods);
    }

    public ArrayList buildGroup(AbstractEntityInterface measuredClass) {
        return buildGroupEntity(measuredClass).getElements();
    }
}

public class CM extends PropertyComputer {
    public CM() {
        super("CM", "Changing Methods", "class", "numerical");
        basedOnDistinctGroup(new ChangingMethods());
    }
}
