package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class NDU extends PropertyComputer {
    public NDU() {
        super("NDU", "Number of Descendants Used", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        double resultValue = 0.0;
        if (anEntity instanceof lrg.memoria.core.Class == false) return new ResultEntity(resultValue);

        resultValue = anEntity.contains("method group").getGroup("subclasses dependencies").distinct().size();

        return new ResultEntity(resultValue);
    }
}
