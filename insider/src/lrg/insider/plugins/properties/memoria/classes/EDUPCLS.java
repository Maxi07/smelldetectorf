package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class EDUPCLS extends PropertyComputer {
    public EDUPCLS() {
        super("EDUPCLS", "Number of other (unrelated) classes with sharing duplicated code", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        GroupEntity siblingClasses = (GroupEntity) anEntity.contains("method group").getGroup("unrelated methods with duplication").belongsTo("class");
        return new ResultEntity(siblingClasses.distinct().size());
    }

}
