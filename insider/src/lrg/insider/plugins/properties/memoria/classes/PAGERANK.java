package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.tools.PRStructure;
import lrg.memoria.core.DataAbstraction;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 09.11.2006
 * Time: 17:42:41
 * To change this template use File | Settings | File Templates.
 */
public class PAGERANK extends PropertyComputer {
    public PAGERANK() {
        super("PAGERANK", "Importance of class computed using PAGERANK", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        ResultEntity aResult = measuredClass.getProperty("#PAGERANK#");
        DataAbstraction aClass;

        if (aResult == null) return new ResultEntity(-1);

        PRStructure classessPRStruct = (PRStructure) aResult.getValue();
        return new ResultEntity(classessPRStruct.getPR());

    }
}
