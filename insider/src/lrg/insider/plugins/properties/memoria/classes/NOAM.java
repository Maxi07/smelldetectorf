package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 31.08.2004
 * Time: 22:11:15
 * To change this template use File | Settings | File Templates.
 */
public class NOAM extends PropertyComputer {
    public NOAM() {
        super("NOAM", "Number of Accessor Methods", "class", "numerical");
    }


    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;

        double noam = anEntity.contains("method group").applyFilter("is accessor").size();

        return new ResultEntity(noam);
    }
}
