package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.NewlyAddedServices;

public class NAS extends PropertyComputer {
    public NAS() {
        super("NAS", "Number of Added Services", "class", "numerical");
        basedOnGroup(new NewlyAddedServices());
    }
}
