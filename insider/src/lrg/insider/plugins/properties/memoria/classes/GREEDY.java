package lrg.insider.plugins.properties.memoria.classes;

import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.Threshold;

public class GREEDY extends PropertyComputer {
    public GREEDY() {
        super("GREEDY", "GREEDY degree of a class", "class", "numerical");
    }


    private double loc(AbstractEntityInterface anEntity) {
        double result = 0.0;
        Iterator it = anEntity.getProperty("LOC").getValueAsCollection().iterator();
        while (it.hasNext()) {
            Object tmp = it.next();
            if (tmp instanceof ResultEntity)
                result += ((Double) ((ResultEntity) tmp).getValue()).doubleValue();
        }
        return result;
    }

    private int dataClassesAmongDataProvider(AbstractEntityInterface anEntity) {
        return anEntity.getGroup("foreign data providers").applyFilter("Data Class").size();
    }

    public boolean featureEnvy(AbstractEntityInterface anEntity) {
        FilteringRule featureEnvy = new FilteringRule("NrFE", ">", "class", Threshold.NONE);
        FilteringRule highATFD = new FilteringRule("ATFD", ">", "class", 2 * Threshold.FEW);

        return featureEnvy.applyFilter(anEntity) || highATFD.applyFilter(anEntity);
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        FilteringRule veryHighWMC = new FilteringRule("WMC", ">", "class", Threshold.WMC_VERYHIGH);
        boolean largeAndComplex = (loc(anEntity) > Threshold.LOC_CLASS_VERYHIGH) &&
                (veryHighWMC.applyFilter(anEntity));

        FilteringRule intensiveCoupling = new FilteringRule("NrIC", ">", "class", Threshold.NONE);
        FilteringRule extensiveCoupling = new FilteringRule("NrEC", ">", "class", Threshold.NONE);
        int counter = 0;

        if (featureEnvy(anEntity) == false) return new ResultEntity(0);

        counter += 1000 * dataClassesAmongDataProvider(anEntity);

        if (largeAndComplex) counter += 100;
        if (intensiveCoupling.applyFilter(anEntity)) counter += 10;
        if (extensiveCoupling.applyFilter(anEntity)) counter++;


        return new ResultEntity(counter);
    }

}
