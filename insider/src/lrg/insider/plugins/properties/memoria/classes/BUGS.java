package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.NamedModelElement;

public class BUGS extends PropertyComputer {

    public BUGS() {
           super("BUGS", "Number of Bug Reports for the Class", new String[] {"package", "class", "method", "global function"}, "numeric");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        NamedModelElement modelElement = (NamedModelElement) anEntity;
        return new ResultEntity(modelElement.getBugs().size());
    }

}