package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.variables.IsConstant;

public class NOPA extends PropertyComputer {
    public NOPA() {
        super("NOPA", "Number of Public Attributes", "class", "numerical");
    }


    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;

        FilteringRule notConstant = new NotComposedFilteringRule(new IsConstant());

        return new ResultEntity(anEntity.getGroup("attribute group").applyFilter(notConstant).applyFilter("not encapsulated").size());
    }
}
