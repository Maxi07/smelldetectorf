package lrg.insider.plugins.properties.memoria.classes;

import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 20.10.2004
 * Time: 16:45:57
 * To change this template use File | Settings | File Templates.
 */
public class TCC extends PropertyComputer {
    public TCC() {
        super("TCC", "Tight Class Cohesion", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        FilteringRule notConstructors = new NotComposedFilteringRule(new IsConstructor());
        GroupEntity methods = anEntity.contains("method group").applyFilter(notConstructors);
        double cohesivePairs = 0.0, nrOfMethods = methods.size();

        if ((nrOfMethods == 0) || (nrOfMethods == 1)) return new ResultEntity(nrOfMethods);

        Iterator it = methods.iterator();
        while (it.hasNext()) {
            AbstractEntity crtMethod = (AbstractEntity) it.next();
            GroupEntity accessingMethods = crtMethod.uses("variables accessed").isUsed("methods accessing variable").exclude(crtMethod);
            cohesivePairs += accessingMethods.intersect(methods).distinct().size();
        }

        return new ResultEntity(cohesivePairs / (nrOfMethods * (nrOfMethods - 1)));
    }
}
