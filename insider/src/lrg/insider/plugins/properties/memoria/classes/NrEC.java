package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 25.01.2005
 * Time: 19:36:14
 * To change this template use File | Settings | File Templates.
 */
public class NrEC extends PropertyComputer {
    public NrEC() {
        super("NrEC", "Number of Highly Coupled Methods", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        GroupEntity aGroup = measuredClass.getGroup("method group");

        return new ResultEntity(aGroup.applyFilter("Extensive Coupling").size());
    }
}
