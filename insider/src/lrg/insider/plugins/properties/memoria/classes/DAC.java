package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class DAC extends PropertyComputer {
    public DAC() {
        super("DAC", "Data Abstraction Coupling", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface aClass) {
        GroupEntity resultGroup = aClass.contains("attribute group").applyFilter("is user defined");

        resultGroup = resultGroup.uses("type of variable").applyFilter("model class");
        return new ResultEntity(resultGroup.distinct().size());
    }
}