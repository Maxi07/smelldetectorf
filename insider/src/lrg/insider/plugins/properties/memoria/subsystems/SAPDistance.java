package lrg.insider.plugins.properties.memoria.subsystems;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class SAPDistance extends PropertyComputer {
    public SAPDistance() {
        super("SAP Distance", "SAP Distance", "subsystem", "numerical");
    }
    
    public ResultEntity compute(AbstractEntityInterface aPackage) {
    	double IF = (Double) aPackage.getProperty("IF").getValue();
    	double AR = (Double) aPackage.getProperty("AR").getValue();

    	return new ResultEntity(AR+IF-1);
    }
}
