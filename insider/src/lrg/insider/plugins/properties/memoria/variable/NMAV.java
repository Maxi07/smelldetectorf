package lrg.insider.plugins.properties.memoria.variable;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.core.groups.memoria.uses.VariableIsAccessed;

public class NMAV extends PropertyComputer {
    public NMAV() {
        super("NMAV", "Number of Methods Accessing a variable", new String[]{"global variable", "attribute", "local variable", "parameter"}, "numerical");
        basedOnDistinctGroup(new VariableIsAccessed());
    }
}
