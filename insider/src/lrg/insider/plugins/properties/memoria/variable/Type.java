package lrg.insider.plugins.properties.memoria.variable;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 23.08.2006
 * Time: 12:23:13
 * To change this template use File | Settings | File Templates.
 */
public class Type extends PropertyComputer {
    public Type() {
        super("Type", "Type of the variable", new String[]{"global variable", "attribute", "local variable", "parameter"}, "string");
    }
    public ResultEntity compute(AbstractEntityInterface anEntity) {
            return new ResultEntity(anEntity.getGroup("type of variable").getElementAt(0).getName());
    }
}
