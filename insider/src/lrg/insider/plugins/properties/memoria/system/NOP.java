package lrg.insider.plugins.properties.memoria.system;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class NOP extends PropertyComputer
{
    public NOP()
    {
        super("NOP", "Nr of packages", "system", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.System)
            return new ResultEntity(((lrg.memoria.core.System)anEntity).getPackages().size());
        else
            return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
