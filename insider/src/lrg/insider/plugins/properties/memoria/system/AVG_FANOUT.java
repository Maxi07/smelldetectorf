package lrg.insider.plugins.properties.memoria.system;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.01.2005
 * Time: 12:38:00
 * To change this template use File | Settings | File Templates.
 */
public class AVG_FANOUT extends PropertyComputer {
    public AVG_FANOUT() {
        super("AVG_FANOUT", "Average Fan-Out", "system", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        return anEntity.getGroup("method group").applyFilter("model function").getProperty("FANOUT").aggregate("avg");
    }
}

