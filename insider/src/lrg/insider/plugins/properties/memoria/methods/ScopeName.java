package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.01.2005
 * Time: 14:37:27
 * To change this template use File | Settings | File Templates.
 */
public class ScopeName extends PropertyComputer {
    public ScopeName() {
        super("scope name", "", new String[]{"method", "global function"}, "string");
    }

    public lrg.common.abstractions.entities.ResultEntity compute(AbstractEntityInterface anEntity) {
        return new ResultEntity(anEntity.belongsTo("class").getName());
    }
}
