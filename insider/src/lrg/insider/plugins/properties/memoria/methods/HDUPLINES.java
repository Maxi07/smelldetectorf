package lrg.insider.plugins.properties.memoria.methods;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.dude.duplication.Duplication;
import lrg.dude.duplication.DuplicationList;
import lrg.insider.plugins.groups.memoria.MethodsWithHierarchyDuplication;


public class HDUPLINES extends PropertyComputer {
    public HDUPLINES() {
        super("HDUPLINES", "Total Lines of Duplication with other classes in hierarchy", "method", "numerical");
    }


    public ResultEntity compute(AbstractEntityInterface aMethod) {
        double result = 0.0;
        ResultEntity aResult = aMethod.getProperty("#DUPLICATION#");
        if (aResult == null) return new ResultEntity(result);

        // @see class EDUPLINES for the reason of this instanceof
        if (aResult.getValue() instanceof DuplicationList) {
            ArrayList duplicators = MethodsWithHierarchyDuplication.getMethodsWithHierarchyDuplication((DuplicationList) aResult.getValue());

            for (Iterator it = duplicators.iterator(); it.hasNext();)
                result += ((Duplication) it.next()).copiedLength();
        }

        return new ResultEntity(result);
    }
}
