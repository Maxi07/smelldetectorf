package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.core.groups.memoria.containment.OperationHasLocalVariables;

public class NOLV extends PropertyComputer {
    public NOLV() {
        super("NOLV", "Number of Local Variables", new String[]{"method", "global function"}, "numerical");
        basedOnGroup(new OperationHasLocalVariables());
    }
}
