package lrg.insider.plugins.properties.memoria.methods;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupEntityBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 23.02.2005
 * Time: 19:51:48
 * To change this template use File | Settings | File Templates.
 */

public class ChangingMethods extends GroupEntityBuilder {
    public ChangingMethods() {
        super("group of changing methods",
                "methods that would be potentiall affected by a change in analyzed method", "method");
    }


    public GroupEntity buildGroupEntity(AbstractEntityInterface measuredMethod) {
        GroupEntity clientMethods = measuredMethod.isUsed("operations calling me");
        AbstractEntity measuredClass = measuredMethod.belongsTo("class");

        GroupEntity relatedMethods = measuredClass.contains("method group");
        relatedMethods = relatedMethods.union(measuredClass.getGroup("all ancestors").getGroup("method group"));
        relatedMethods = relatedMethods.union(measuredClass.getGroup("all descendants").getGroup("method group"));
        return clientMethods.exclude(relatedMethods);
    }

    public ArrayList buildGroup(AbstractEntityInterface measuredClass) {
        return buildGroupEntity(measuredClass).getElements();
    }
}
