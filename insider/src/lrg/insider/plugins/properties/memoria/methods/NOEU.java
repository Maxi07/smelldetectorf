package lrg.insider.plugins.properties.memoria.methods;


import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class NOEU extends PropertyComputer {
    public NOEU() {
        super("NOEU", "Number of External Usages", "method", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredMethod) {
    	GroupEntity callers = measuredMethod.uses("operations calling me");
    	
    	int noOfInternalCallers = 
    		callers.intersect(measuredMethod.belongsTo("class").contains("method group")).size();
    	
    	return new ResultEntity(callers.size() - noOfInternalCallers);

    }
}

