package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class FDP extends PropertyComputer {
    public FDP() {
        super("FDP", "Number of Import Classes", "method", "numerical");
        basedOnGroup(new AccessedModelClasses());
        basedOnGroup(new CurrentClassAndAncestors());
    }

    public lrg.common.abstractions.entities.ResultEntity compute(AbstractEntityInterface aMethod) {
        GroupEntity allAccessedClasses = getGroup("accessed model classes", aMethod);
        GroupEntity currentClassAndAncestors = getGroup("current class and ancestors", aMethod);

        return new ResultEntity(allAccessedClasses.exclude(currentClassAndAncestors).distinct().size());
    }
}
