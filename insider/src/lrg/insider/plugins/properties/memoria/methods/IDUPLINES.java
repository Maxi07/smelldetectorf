package lrg.insider.plugins.properties.memoria.methods;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.dude.duplication.Duplication;
import lrg.dude.duplication.DuplicationList;
import lrg.dude.duplication.MethodEntity;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 07.02.2005
 * Time: 16:14:22
 * To change this template use File | Settings | File Templates.
 */
public class IDUPLINES extends PropertyComputer {
    public IDUPLINES() {
        super("IDUPLINES", "Duplication Level Inside Class", "method", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface aMethod) {
        int result = 0;
        if (aMethod instanceof lrg.memoria.core.Function == false) return null;

        ResultEntity aResult = aMethod.getProperty("#DUPLICATION#");
        if (aResult == null) return new ResultEntity(0);

        // @see class EDUPLINES for the reason of this instanceof
        if (aResult.getValue() instanceof DuplicationList) {
            DuplicationList aDuplicationList = (DuplicationList) aResult.getValue();
            if (aDuplicationList.size() > 0)
                result += countDuplicationLength(aDuplicationList);
        }
        
        return new ResultEntity(result);
    }

    
    private static boolean isFromSameClass(MethodEntity reference, MethodEntity duplicator) {
        String referenceAddress = (String) reference.getMethod().belongsTo("class").getProperty("Address").getValue();
        String duplicatorAddress = (String) duplicator.getMethod().belongsTo("class").getProperty("Address").getValue();

        return referenceAddress.compareTo(duplicatorAddress) == 0;
    }
    
    public static ArrayList getMethodsWithIntraClassDuplication(DuplicationList aDuplicationList) {
        ArrayList resultsList = new ArrayList();
        if (aDuplicationList.size() == 0) return resultsList;
        MethodEntity reference = (MethodEntity) aDuplicationList.get(0).getReferenceCode().getEntity();


        for (int i = 0; i < aDuplicationList.size(); i++) {
            Duplication aDuplication = aDuplicationList.get(i);
            MethodEntity duplicator = (MethodEntity) aDuplication.getDuplicateCode().getEntity();

            if (isFromSameClass(reference, duplicator)) resultsList.add(aDuplication);
        }
        return resultsList;
    }

    private int countDuplicationLength(DuplicationList aDuplicationList) {
        int result = 0;
        MethodEntity reference = (MethodEntity) aDuplicationList.get(0).getReferenceCode().getEntity();

        for (int i = 0; i < aDuplicationList.size(); i++) {
            Duplication aDuplication = aDuplicationList.get(i);
            MethodEntity duplicator = (MethodEntity) aDuplication.getDuplicateCode().getEntity();

            if (isFromSameClass(reference, duplicator)) result += aDuplication.isSelfDuplication() ? aDuplication.copiedLength() : (aDuplication.copiedLength() / 2);
        }
        return result;
    }
}
