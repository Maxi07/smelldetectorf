package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.ExternalServiceProviders;

public class CINT extends PropertyComputer {
    public CINT() {
        super("CINT", "Coupling Intensity", new String[]{"method", "global function"}, "numerical");
        basedOnGroup(new ExternalServiceProviders());
    }
}
