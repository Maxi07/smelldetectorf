package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;


public class CC extends PropertyComputer {
    public CC() {
        super("CC", "Changing Classes", "method", "numerical");
        basedOnDistinctGroup(new ChangingClasses());
    }
}
