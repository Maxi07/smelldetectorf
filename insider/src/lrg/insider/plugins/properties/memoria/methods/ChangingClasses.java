package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupEntityBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 23.02.2005
 * Time: 19:56:40
 * To change this template use File | Settings | File Templates.
 */


public class ChangingClasses extends GroupEntityBuilder {
    public ChangingClasses() {
        super("group of changing classes",
                "classes that would be potentiall affected by a change in analyzed class", "method");
    }

    public GroupEntity buildGroupEntity(AbstractEntityInterface measuredClass) {
        GroupEntity clientMethods = new ChangingMethods().buildGroupEntity(measuredClass);
        return ((GroupEntity) clientMethods.belongsTo("class"));
    }
}
