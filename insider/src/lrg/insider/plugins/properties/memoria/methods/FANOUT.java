package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.core.groups.memoria.uses.OperationCalls;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.01.2005
 * Time: 12:29:59
 * To change this template use File | Settings | File Templates.
 */
public class FANOUT extends PropertyComputer {
    public FANOUT() {
        super("FANOUT", "Import Coupling", new String[]{"method", "global function"}, "numerical");
        basedOnDistinctGroup(new OperationCalls());
    }
}
