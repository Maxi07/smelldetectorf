package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.Body;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 07.02.2005
 * Time: 10:47:42
 * To change this template use File | Settings | File Templates.
 */
public class MAXNESTING extends PropertyComputer {
    public MAXNESTING() {
        super("MAXNESTING", "MAXNESTING (complexity)", "method", "numerical");
    }

    public lrg.common.abstractions.entities.ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Function == false) return null;

        Body theBody = ((lrg.memoria.core.Function) anEntity).getBody();

        if (theBody == null) return new ResultEntity(0);

        return new ResultEntity(theBody.getMaxNestingLevel());
    }
}
