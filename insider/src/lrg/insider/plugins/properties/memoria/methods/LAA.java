package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.11.2004
 * Time: 18:23:39
 * To change this template use File | Settings | File Templates.
 */
public class LAA extends PropertyComputer {
    public LAA() {
        super("LAA", "Locality of Attributes Accesses", "method", "numerical");
    }

    public lrg.common.abstractions.entities.ResultEntity compute(AbstractEntityInterface anEntity) {
        double aid = ((Double) anEntity.getProperty("ATFD").getValue()).doubleValue();
        double ald = ((Double) anEntity.getProperty("ALD").getValue()).doubleValue();

        if (aid + ald == 0) return new ResultEntity(1);
        return new ResultEntity(ald / (aid + ald));
    }
}
