package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;


public class ATFD extends PropertyComputer {
    public ATFD() {
        super("ATFD", "Access of foreign Data", "method", "numerical");
        basedOnGroup(new AccessedModelClasses());
        basedOnGroup(new CurrentClassAndAncestors());
    }


    public ResultEntity compute(AbstractEntityInterface aMethod) {
        GroupEntity allAccessedClasses = getGroup("accessed model classes", aMethod);
        GroupEntity currentClassAndAncestors = getGroup("current class and ancestors", aMethod);

        return new ResultEntity(allAccessedClasses.exclude(currentClassAndAncestors).size());
    }
}
