package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.core.groups.memoria.containment.OperationHasParameters;

public class NOP extends PropertyComputer {
    public NOP() {
        super("NOP", "Number of Parameters", new String[]{"method", "global function"}, "numerical");
        basedOnGroup(new OperationHasParameters());
    }
}
