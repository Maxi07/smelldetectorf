package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.ExternalServiceProviders;

public class CEXT extends PropertyComputer {
    public CEXT() {
        super("CEXT", "Coupling Extent", new String[]{"method", "global function"}, "numerical");
        basedOnDistinctGroup(new ExternalServiceProviders());
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        GroupEntity aGroup = (GroupEntity) anEntity.getGroup("External Service Providers (methods)").belongsTo("class");
        return new ResultEntity(aGroup.distinct().applyFilter("model class").size());
    }

}
