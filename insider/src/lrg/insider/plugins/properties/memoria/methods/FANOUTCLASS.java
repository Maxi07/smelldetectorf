package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.01.2005
 * Time: 12:46:12
 * To change this template use File | Settings | File Templates.
 */
public class FANOUTCLASS extends PropertyComputer {
    public FANOUTCLASS() {
        super("FANOUTCLASS", "Number of classes from which methods are called", new String[]{"method", "global function"}, "numerical");
    }

    public lrg.common.abstractions.entities.ResultEntity compute(AbstractEntityInterface anEntity) {
        return new ResultEntity(((GroupEntity) anEntity.uses("operations called").belongsTo("class")).distinct().size());
    }
}

