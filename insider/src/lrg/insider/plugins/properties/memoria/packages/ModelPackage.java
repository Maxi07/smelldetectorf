package lrg.insider.plugins.properties.memoria.packages;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.Statute;

public class ModelPackage extends PropertyComputer
{
    public ModelPackage()
    {
        super("Model Package", "Is package part of the model", "package", "boolean");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Namespace == false)
            return new ResultEntity(false);
        lrg.memoria.core.Namespace aPackage = (lrg.memoria.core.Namespace) anEntity;
        return new ResultEntity(aPackage.getStatute()==Statute.NORMAL);
    }
}
