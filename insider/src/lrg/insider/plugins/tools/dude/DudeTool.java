package lrg.insider.plugins.tools.dude;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.dude.duplication.MethodEntity;
import lrg.dude.duplication.Observer;
import lrg.dude.duplication.Parameters;
import lrg.dude.duplication.Processor;
import lrg.dude.duplication.Subject;
import lrg.dude.duplication.SuffixTreeProcessor;
import lrg.insider.gui.ui.utils.ProgressBar;

public class DudeTool extends AbstractEntityTool implements Observer {
    private ProgressBar progress;

    public DudeTool() {
        super("Dude", "Duplication detector", "system");
    }

    public void run(AbstractEntityInterface abstractEntityInterface, Object o) {
        if (abstractEntityInterface instanceof lrg.memoria.core.System == false) return;

        final lrg.memoria.core.System currentSystem = (lrg.memoria.core.System) abstractEntityInterface;
        ArrayList<String> params = (ArrayList<String>) o;

        int par1 = Integer.parseInt(params.get(0));
        int par2 = Integer.parseInt(params.get(1));
        int par3 = Integer.parseInt(params.get(2));
        Parameters dudeParameters = new Parameters(par1, par2, par3, false);

        runDude(currentSystem, dudeParameters, true);
    }

    public void runDude(lrg.memoria.core.System currentSystem) {
        runDude(currentSystem, new Parameters(10,2,3,false), false);
    }
    private void runDude(lrg.memoria.core.System currentSystem,  Parameters dudeParameters, boolean displayProgressBar) {
        MethodEntity[] arrayOfMethodEntities;
        Iterator it = null;
        GroupEntity methodGroup = currentSystem.getGroup("method group").applyFilter("model function").distinct();

        it = methodGroup.iterator();
        arrayOfMethodEntities = new MethodEntity[methodGroup.size()];
        int i = 0;
        lrg.memoria.core.Method currentMethod;
        while (it.hasNext()) {
            currentMethod = (lrg.memoria.core.Method) it.next();
            arrayOfMethodEntities[i++] = new MethodEntity(currentMethod);
        }

        if(displayProgressBar) progress = new ProgressBar("Computing the duplications ...");
        else progress = null;
        final Processor aDudeProcessor = new SuffixTreeProcessor(arrayOfMethodEntities, progress);
        //final Processor aDudeProcessor = new Processor(arrayOfMethodEntities, progress);
        aDudeProcessor.setParams(dudeParameters);
        aDudeProcessor.attach(this);
        if(displayProgressBar) aDudeProcessor.start();
        else aDudeProcessor.run();
    }

    public void getDuplication(Subject source) {
        ((Processor) source).attachResultsToMethods();
        if(progress != null) progress.close();
        System.out.println("Duplications have entered the building");
    }

    public String getToolName() {
        return "Dude";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Min length ");
        parList.add("Max line bias ");
        parList.add("Min exact chunk ");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Minimum accepted length for duplication chains (LOC)");
        parList.add("The maximum size of the line bias between two exact chunks");
        parList.add("Minimum accepted size of the exact chunks within a chain");
        return parList;
    }
}





