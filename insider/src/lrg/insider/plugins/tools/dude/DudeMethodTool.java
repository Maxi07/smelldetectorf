package lrg.insider.plugins.tools.dude;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.dude.duplication.MethodEntity;
import lrg.dude.duplication.Observer;
import lrg.dude.duplication.Parameters;
import lrg.dude.duplication.Processor;
import lrg.dude.duplication.Subject;
import lrg.dude.duplication.SuffixTreeProcessor;
import lrg.insider.gui.ui.utils.ProgressBar;

public class DudeMethodTool extends AbstractEntityTool implements Observer {
    private ProgressBar progress;

    public DudeMethodTool() {
        super("Dude Method", "Duplication detector", "method");
    }

    public void run(AbstractEntityInterface theMeth, Object o) {
        if (theMeth instanceof lrg.memoria.core.Method == false) return;

        final lrg.memoria.core.Method currentMethod = (lrg.memoria.core.Method) theMeth;
        ArrayList<String> params = (ArrayList<String>) o;

        int par1 = Integer.parseInt(params.get(0));
        int par2 = Integer.parseInt(params.get(1));
        int par3 = Integer.parseInt(params.get(2));
        Parameters dudeParameters = new Parameters(par1, par2, par3, false);

        runDude(currentMethod, dudeParameters, true);
    }

    public void runDude(lrg.memoria.core.Method currentMethod) {
        runDude(currentMethod, new Parameters(10,2,3,false), false);
    }
    
    private void runDude(lrg.memoria.core.Method theMethod,  Parameters dudeParameters, boolean displayProgressBar) {
        MethodEntity[] arrayOfMethodEntities;
        Iterator it = null;
        
        GroupEntity methodGroup = theMethod.belongsTo("system").
        							getGroup("method group").applyFilter("model function").distinct();

        it = methodGroup.iterator();
        arrayOfMethodEntities = new MethodEntity[methodGroup.size()];
        int i = 0;
        lrg.memoria.core.Method crtMeth;
        while (it.hasNext()) {
            crtMeth = (lrg.memoria.core.Method) it.next();
            arrayOfMethodEntities[i++] = new MethodEntity(crtMeth);
        }
        System.out.println(i);

        if(displayProgressBar) progress = new ProgressBar("Computing the duplications ...");
        else progress = null;
        final Processor aDudeProcessor = new SuffixTreeProcessor(arrayOfMethodEntities, new MethodEntity(theMethod), progress);
        //final Processor aDudeProcessor = new Processor(arrayOfMethodEntities, new MethodEntity(theMethod), progress);
        aDudeProcessor.setParams(dudeParameters);
        aDudeProcessor.attach(this);
        if(displayProgressBar) aDudeProcessor.start();
        else aDudeProcessor.run();
    }

    public void getDuplication(Subject source) {
        ((Processor) source).attachResultsToReferenceMethod();
        if(progress != null) progress.close();
    }

    public String getToolName() {
        return "Dude";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Min length ");
        parList.add("Max line bias ");
        parList.add("Min exact chunk ");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Minimum accepted length for duplication chains (LOC)");
        parList.add("The maximum size of the line bias between two exact chunks");
        parList.add("Minimum accepted size of the exact chunks within a chain");
        return parList;
    }
}





