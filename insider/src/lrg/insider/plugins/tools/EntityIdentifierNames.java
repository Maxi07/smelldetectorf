package lrg.insider.plugins.tools;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.insider.gui.ui.utils.ProgressBar;
import lrg.insider.plugins.filters.memoria.classes.IsInner;
import lrg.memoria.core.Class;
import lrg.memoria.core.File;
import lrg.memoria.core.GlobalFunction;
import lrg.memoria.core.Method;
import lrg.memoria.core.Package;


public class EntityIdentifierNames extends AbstractEntityTool {
	    private ProgressBar progress;
	    private lrg.memoria.core.System currentSystem;

	    private ArrayList<Method> methodGroup;
	    private ArrayList<GlobalFunction> globalFunctionGroup;
		private ArrayList<Package> packageGroup;
		private ArrayList<Class> classGroup;
		private ArrayList<File> fileGroup;

	    public EntityIdentifierNames() {
	        super("Print Entity Identifier Names", "Put in a list names of packages, classes and methods", "system");
	    }

		@Override
		public String getToolName() {
	        return "Entity Identifier Names";
		}

		private void initMembers(AbstractEntityInterface theSystem) {
			currentSystem = (lrg.memoria.core.System) theSystem;
			 FilteringRule notInnerClass = new NotComposedFilteringRule(new IsInner());
		        

			packageGroup = currentSystem.getGroup("package group").applyFilter("model package").getElements();
			fileGroup = currentSystem.getGroup("package group").applyFilter("model package").getGroup("file group").applyFilter("model file").getElements();

			classGroup = currentSystem.getGroup("class group").applyFilter("model class").applyFilter(notInnerClass).getElements();

			methodGroup = currentSystem.getGroup("method group").applyFilter("model function").getElements();
			globalFunctionGroup = currentSystem.getGroup("global function group").applyFilter("model function").getElements();
		}

		@Override
		public void run(AbstractEntityInterface anEntity, Object theToolParameters) {
	        if (anEntity instanceof lrg.memoria.core.System == false) return;
	        initMembers(anEntity);
	        ArrayList<String> params = (ArrayList<String>) theToolParameters;
	        if (params.get(0).equals(""))
	            System.out.println(exportNames());
	        else {
	            try {
	                PrintStream out_stream = new PrintStream(new FileOutputStream(params.get(0)));
	                out_stream.print(exportNames());
	                out_stream.close();
	            } catch (Exception ex) {
	            }
	        }
		}

	    private String truncateFilename(String fullFilename) {
	    	if(fullFilename.indexOf("FILE:") < 0) return fullFilename;

	    	String prefix = "FILE:" + currentSystem.getFullName() + java.io.File.separator;
	    	
	    	String newName = fullFilename.substring(prefix.length());
    		    	
	    	return  "." + java.io.File.separator + newName;    	
	    }

	    private String exportNames() {
	    	String temp = "";
	    	
	    	temp += "*** PACKAGES\n";
	    	for(Package aPackage : packageGroup) temp += aPackage.getFullName() + "\n";

	    	temp += "*** FILES\n";
	    	for(File aFile : fileGroup) temp += truncateFilename(aFile.getFullName()) + "\n";

	    	temp += "*** CLASSES\n";
	    	for(Class aClass : classGroup) temp += aClass.getFullName() + "\n";

	    	temp += "*** FUNCTIONS\n";
	    	for(Method aMethod : methodGroup) temp += aMethod.getScope().getFullName() + "." + aMethod.getName() + "\n";
	    	for(GlobalFunction aMethod : globalFunctionGroup) temp += aMethod.getScope().getFullName() + "." + aMethod.getName() + "\n";

	    	
	    	return temp;	    
	    }

		public ArrayList<String> getParameterList() {
	        ArrayList<String> parList = new ArrayList<String>();
	        parList.add("Complete File name ");
	        return parList;
	    }

	    public ArrayList<String> getParameterExplanations() {
	        ArrayList<String> parList = new ArrayList<String>();
	        parList.add("Name of the file which will store the names of all packages, classes, methods");
	        return parList;
	    }

}
