package lrg.insider.plugins.tools.bugreports;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import lrg.common.abstractions.managers.EntityTypeManager;
import lrg.common.metamodel.MetaModel;
import lrg.memoria.core.Bug;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

//cand se mai adauga un format, se mai introduce o metoda cu urmatoarea denumire:
//<"identificator format">_<numeproiect>
//aceasta metoda va fi scrisa in maniera lui SF_filterBugList()

/**
 * Pentru rapoarte de pe SourceForge s-au folosit xml-uri de forma:
 * SELECT a.artifact_id, g.group_name, ast.status_name, a.open_date, a.close_date, a.details
FROM sf0709.artifact a, sf0709.artifact_group_list ag, sf0709.groups g, sf0709.artifact_status ast
WHERE a.group_artifact_id=ag.group_artifact_id AND ag.group_id=g.group_id AND g.group_name='JHotDraw' AND a.status_id=ast.id
 
 * 
 * Pentru rapoarte de pe IssueZilla, formatul trebuie ordonat in ordine cronologica a comentariilor.
 */
class BugProcessor {
	private String xmlfile=null;
	private String projectName=null;
	private StringAnalyzer analyzer=null;
	private HashMap<Bug,ArrayList<HashSet<String>>> output=new HashMap<Bug,ArrayList<HashSet<String>>>();
	private Element root; //elementul radacina din xml
	
	public BugProcessor(String xmlfile, StringAnalyzer analyzer) throws DocumentException, SAXException {
		this.analyzer=analyzer;
	        SAXReader reader;
	        this.xmlfile=xmlfile.trim();
			reader = new SAXReader();
			Document inDoc = reader.read(xmlfile);	
			root=inDoc.getRootElement();
	    }
	
	//a se adauga la aceasta metoda apelurile la alte formate
	public HashMap<Bug, ArrayList<HashSet<String>>> route() throws InvalidBugFileNameException {
		System.out.println("Processing bug list");
		
		String xmlFileTrim=getTrimmedFile(this.xmlfile);
		System.out.println("File name: "+xmlFileTrim);
		
		if (xmlFileTrim.startsWith("SF_"))
			return SF_filterBugList();
		if (xmlFileTrim.startsWith("IS_"))
			return IS_filterBugList();
		throw new InvalidBugFileNameException();
	}
	
	


	private String getTrimmedFile(String xmlfile2) {
		int lastIndexOfPathSeparator = xmlfile2.lastIndexOf(File.separator);
		return xmlfile2.substring(lastIndexOfPathSeparator+1);
	}

	private HashMap<Bug, ArrayList<HashSet<String>>> IS_filterBugList() {
		System.out.println("Issuezilla file detected\nProcessing...");
		ArrayList<HashSet<String>> out = null;
		this.projectName=root.attributeValue("project_name"); //getProjectName
		try{
			ArrayList<Element> bugs = (ArrayList<Element>) root.elements("issue");
			String bugDetails="";
			for (Element bug:bugs){
				//extrage bug textul
				bugDetails=((Element) bug.elements("long_desc").get(0)).element("thetext").getText();
				
				//primeste analiza lui bugDetails
				//se schumba usor daca apar mai multe bug texts, gen bugZilla report
				//0 pozitie -- lista pachete gasite
				//1 pozitie -- lista clase gasite
				//2 pozitie -- lista metode gasite
				//3 pozitie -- lista fisiere gasite 
				out=analyzer.analyze(bugDetails);						
				
				if (out!=null){
					try{
						
					String id=bug.element("issue_id").getText();
					String status=bug.element("issue_status").getText();
					Date date= readISDate(bug.element("creation_ts").getText()); 
					String subComponent = bug.element("subcomponent").getText();;
					
					addToOutput(id,status,date,date,subComponent,bugDetails,out);
					}
					catch(NullPointerException e){
						System.out.println("Please check xml file format! It may have changed (+-new fields)");
					}
				}
			}
			
			return output;
	}
	catch (ParameterException e) {
		System.out.println(e.getMessage());
		return null;
	}
		
	}
	
	



	private Date readISDate(String textDate) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd H:m:s");
		Date date=null;
		try {
			date=df.parse(textDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	private Date readSFDate(long seconds) {
		Calendar cal=Calendar.getInstance();
		try {
			
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date epoch = df.parse("1970-01-01");
		cal.setTime(epoch);
		cal.add(Calendar.SECOND, (int) seconds);
		
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return cal.getTime();
	}

	private HashMap<Bug, ArrayList<HashSet<String>>> SF_filterBugList(){
		ArrayList<HashSet<String>> out = null;
		System.out.println("SourceForge file detected\nProcessing...");
		this.projectName=root.element("row").element("group_name").getText();
		
		try {
			
			ArrayList<Element> bugs = (ArrayList<Element>) root.elements("row");
			String bugDetails="";
				for (Element bug:bugs){
						//extrage bug textul
						bugDetails=(String) bug.element("details").getData();
						//il introduce intr-un arraylist pentru a fi compatibil
						//cu XmlWriter.addbug(ArraList<String>,ArrayList<HashSet<String>>);
						//pentru cazul in care sunt mai multe bug texts per bug
						ArrayList<String> bugTexts=new ArrayList<String>();
						bugTexts.add(bugDetails);
						
						//System.out.println(bugDetails);
						//primeste analiza lui bugDetails
						//se schumba usor daca apar mai multe bug texts, gen bugZilla report
						//0 pozitie -- lista pachete gasite
						//1 pozitie -- lista clase gasite
						//2 pozitie -- lista metode gasite
						//3 pozitie -- lista fisiere gasite 
						out =analyzer.analyze(bugDetails);
						//TODO implementeaza data mai jos !!!
						if (out!=null){
							try{
								String id=bug.element("artifact_id").getText();
								String status=bug.element("status_name").getText();
								Date startDate = readSFDate(Long.parseLong(bug.element("open_date").getText()));
								Date closeDate = readSFDate(Long.parseLong(bug.element("close_date").getText()));
								addToOutput(id,status,startDate,closeDate,"",bugDetails,out);
							}
							catch(NullPointerException e){
								System.out.println("Please check xml file format! It may have changed (+-new fields)");
							}
						}
				}
	
				return output; 
				
		}
		catch (ParameterException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}	
	


	private void addToOutput(String id, String status, Date startDate,
			Date closeDate, String subComponent, String bugDetails, ArrayList<HashSet<String>> out) {
		Bug bug=new Bug(id,status,startDate,closeDate,subComponent,bugDetails);
		bug.setEntityType(EntityTypeManager.getEntityTypeForName("bug"));
		MetaModel.instance().addEntityToAddressMap(bug);
		output.put(bug, out);
	}
}


