package lrg.insider.plugins.tools.bugreports;

	 class ParameterException extends Exception {
		public ParameterException(){
			super("Method has been called with an invalid parameter ");
		}
}
