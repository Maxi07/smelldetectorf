package lrg.insider.plugins.tools.bugreports;

public class InvalidBugFileNameException extends Exception {
	public InvalidBugFileNameException(){
		super ("The bug file used has an invalid name (should start with SF_ for sourceforge, IS_ for issuezilla and so on\n Please refer to BugReportsImporter and BugProcessor classes for more details)");
	}
}
