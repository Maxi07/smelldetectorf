package lrg.insider.plugins.tools.bugreports;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


//TODO lipeste stacktraceurile si cauta si in ele
//TODO implementeaza chestia precedenta (clasa anonima...)

class StringAnalyzer {
	private Entity entity;
	
	public StringAnalyzer(Entity entity){
		this.entity=entity;
	}
	
	//sterge toate stacktraceurile dintr-un string
	private String deleteStackTraces(String s){
			//vechiul model "\\n.+(\\n\\s*(\\[.+\\])?\\s*at\\s*\\n?(.+))+"
			Pattern p = Pattern.compile("\\n.+(\\n\\s*(\\[.+\\])?\\s*at\\s*\\n?(.+))+");
			Matcher m=p.matcher(s);
			StringBuffer s2 = new StringBuffer();
			//ArrayList<String> stacks=new ArrayList<String>();
			while (m.find()){
				int startindex=m.start();
				int endindex=m.end();
				String sub=s.substring(startindex, endindex);
				//stacks.add(sub);
				m.appendReplacement(s2,"");
				//System.out.println(m.group(2));
			}
			m.appendTail(s2);
			
		return s2.toString();
	}

	//primeste un bugdescription si:
	//returneaza null daca nu are referinte la cod
	//returneaza un Result daca are referinte la cod
	//bText2 = bug description fara stacktrace+
	//bText3 = fara nume de clase in nume complet+
	//btext4 = fara pachete+
	//btext5 = fara nume de clase in nume simplu
	public ArrayList<HashSet<String>> analyze(String bugText) throws ParameterException {
		Object[] prelucrare=new Object[2];
		boolean hasCode=false;
		//sterge stacktraces
		String btext2=deleteStackTraces(bugText);
		
		
		//cauta clase in nume complet si le scoate afara din text
		prelucrare=entity.getHashMap(btext2, 4);
		String btext3=(String) prelucrare[1];
		HashMap<String,ArrayList<StringLimits>> classesFullName=(HashMap<String, ArrayList<StringLimits>>) prelucrare[0];
		
		//cauta numele de pachete si le scoate afara din text
		prelucrare=entity.getHashMap(btext3, 3);
		String btext4=(String) prelucrare[1];
		HashMap<String,ArrayList<StringLimits>> packages=(HashMap<String, ArrayList<StringLimits>>) prelucrare[0];
		
		
		//verifica daca exista cod pasteuit;
		hasCode=entity.hasJavaCode(btext4);
		
		
		//cauta clase in nume simplu si le scoate afara din text
		prelucrare=entity.getHashMap(btext4, 1);
		String btext5=(String) prelucrare[1];
		HashMap<String,ArrayList<StringLimits>> classesSimpleName=(HashMap<String, ArrayList<StringLimits>>) prelucrare[0];
		
		//cauta metode in nume simplu si le scoate afara din text
		prelucrare=entity.getHashMap(btext5, 2);
		String btext6=(String) prelucrare[1];
		HashMap<String,ArrayList<StringLimits>> methodsSimpleName=(HashMap<String, ArrayList<StringLimits>>) prelucrare[0];
		
		//cauta fisiere si le scoate afara din text
		prelucrare=entity.getHashMap(btext5, 5);
		String btext7=(String) prelucrare[1];
		HashMap<String,ArrayList<StringLimits>> fisiereExtrase=(HashMap<String, ArrayList<StringLimits>>) prelucrare[0];
		
//		System.out.println("*******************\nIn stringanalyzer");
//		System.out.println(classesFullName.keySet());
//		System.out.println(packages.keySet());
//		System.out.println(classesSimpleName.keySet());
//		System.out.println(methodsSimpleName.keySet());
//		System.out.println(fisiereExtrase.keySet());
		
//----------------------------------------------------------------------------------------------------
//de aici incepe filtrarea datelor
		//listele ce vor contine entitatile finale
		HashSet<String> claseFinal = new HashSet<String>();
		HashSet<String> pacheteFinal = new HashSet<String>(packages.keySet());
		HashSet<String> metodeFinal = new HashSet<String>();
		HashSet<String> fisiereFinal = new HashSet<String>(fisiereExtrase.keySet());
		
		if (/*classesFullName.isEmpty()&&*/!(classesSimpleName.isEmpty()))
			claseFinal.add(getEarliestElement(classesSimpleName));
		
		if (!hasCode){
			if (!(classesSimpleName.isEmpty())){
				claseFinal.add(getMostUsedElement(classesSimpleName));
			}
			//conditie de adaugat metode:
			//am metode SI (exista claseFinal SAU(NU exista clase SI(exista " method " SAU " function " in string)))
			if ((!methodsSimpleName.isEmpty())&&((!claseFinal.isEmpty())||((btext7.indexOf("method")!=-1) || (btext7.indexOf("function")!=-1))))
					metodeFinal.add(getEarliestElement(methodsSimpleName));
		}
		
		
		
		//daca s-au detectat si pachete, si clase din ele, se sterg pachetele respective
		pacheteFinal=deleteHigher(claseFinal, pacheteFinal);
		
		
		
		
		//(1)
		//TODO itereaza prin clasele cu nume simplu, si daca inainte de o clasa s-a detectat un pachet, baga clasa in pachetul ala
		//(2)
		//TODO itereaza prin metode, si daca inainte de o metoda s-a detectat o clasa, baga metoda in clasa respectiva
		
		
		
		//(1') daca au ramas clase nebagate in pachete, se adauga toate clasele in nume complet care au numele simplu egal cu cel din claseFinal
		claseFinal=finalSolution(claseFinal,"clasa");

		//(2') la fel ca pentru (1') dar pentru metode
		metodeFinal=finalSolution(metodeFinal,"metoda");

		//la sfarsit, la lista cu clase in nume simplu se adauga lista cu clase gasite in nume complet ce corespund acesteia
		if (classesFullName!=null)
			claseFinal.addAll(classesFullName.keySet());
		
		if (claseFinal.isEmpty()&&pacheteFinal.isEmpty()&&metodeFinal.isEmpty()&&fisiereFinal.isEmpty())
			return null;
		
		
		ArrayList<HashSet<String>> output = new ArrayList<HashSet<String>>();
		output.addAll(Arrays.asList(pacheteFinal, claseFinal, metodeFinal,fisiereFinal));
			
		return output;
	}
	
	private HashSet<String> finalSolution(HashSet<String> entitatiFinal,String target) {
		try {
		//-------------------------------------------------	
		HashSet<String> deSters=new HashSet<String>();
		HashSet<String> deAdaugat=new HashSet<String>();
		for (String entitate:entitatiFinal)
			if (entity.isSimpleName(entitate)){
				deAdaugat.addAll(entity.getSpecifiedCLFUs(entitate, target));
				deSters.add(entitate);
			}
		entitatiFinal.addAll(deAdaugat);
		entitatiFinal.removeAll(deSters);
	
		return entitatiFinal;
		//-------------------------------------------------
		}
		 catch (ParameterException e) {
				return entitatiFinal;
			}
	}

	//daca un element din interior este cuprins si in exterior, atunci se sterge elementul din exterior
	private HashSet<String> deleteHigher(HashSet<String> interior,
			HashSet<String> exterior) {
		ArrayList<String> deSters=new ArrayList<String>();
		for (String x:interior)
			for (String y:exterior)
				if ((entity.hasClass(x,y)))
					deSters.add(y);
		for (String y:deSters)
			exterior.remove(y);	
		return exterior;
	}

	//retine elementul entitate care apare de cele mai multe ori
	private String getMostUsedElement(
			HashMap<String, ArrayList<StringLimits>> clfuSimpleName) {
		String mostCommon="";
		int aparitii=Integer.MIN_VALUE;
		for (String x:clfuSimpleName.keySet()){
			if(clfuSimpleName.get(x).size()>aparitii){
				aparitii=clfuSimpleName.get(x).size();
				mostCommon=x;
			}
		}
		return mostCommon;
	}

	//retine elementul entitate din hashmap, care apare cel mai devreme in string
	private String getEarliestElement(
			HashMap<String, ArrayList<StringLimits>> clfuSimpleName) {
		StringLimits minWord=new StringLimits("initial",Integer.MAX_VALUE,Integer.MAX_VALUE);
		for (String x:clfuSimpleName.keySet())
			for (StringLimits word:clfuSimpleName.get(x))
				if (minWord.compareTo(word)<0)
					minWord=word;
		return minWord.getName();
	}
}
