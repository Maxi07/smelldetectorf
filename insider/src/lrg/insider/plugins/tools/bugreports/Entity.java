package lrg.insider.plugins.tools.bugreports;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


 class Entity {
	//listele cu entitati, in nume complet.Exista metode pentru extragerea numelui simplu
	
	private List<String> packages=new ArrayList<String>();
	private List<String> files=new ArrayList<String>();
	private List<String> classes=new ArrayList<String>();
	private List<String> functions=new ArrayList<String>();
	
	private List<String> simplePackages=new ArrayList<String>();
	private List<String> simpleFiles=new ArrayList<String>();
	private List<String> simpleClasses=new ArrayList<String>();
	private List<String> simpleFunctions=new ArrayList<String>();
	
	public Entity(Set<String> packages, Set<String> files, Set<String> classes, Set<String> methods) {
		this.packages.addAll(packages);
		this.files.addAll(files);
		
		this.classes.addAll(classes);
		this.simpleClasses = this.getSimpleNameList(this.classes);
		
		//elimina constructorii din lista de metode
		this.functions.addAll(methods);
		
		save();
		
		this.functions=purgeConstructors(this.functions,this.simpleClasses);
		
		this.simpleFiles = this.getSimpleNameList(this.files);
		this.simpleFunctions = this.getSimpleNameList(this.functions);
		this.simplePackages = this.getSimpleNameList(this.packages);
		
		
	}
	
	private void save(){
		ArrayList<List<String>> save = new ArrayList<List<String>>();
		save.add(0, this.packages);
		save.add(1, this.files);
		save.add(2, this.classes);
		save.add(3, this.functions);
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("E:\\argouml"));
			oos.writeObject(save);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.exit(0);
	}

	//get class/function simple name from complete name
	 private String getLastName(String x){
		String[] parts=x.split("\\.");
		return parts[parts.length-1];
	}
	 
	//get the list of simple classes/functions from the list of fullname classes/functions.
	 private List<String> getSimpleNameList(List<String> clfu){
		List<String> sclfu=new LinkedList<String>();
		for (String x:clfu){
			sclfu.add(getLastName(x));
		}
		return sclfu;
	}
	 
	 public List<String> getSimpleClasses(){
		 return this.simpleClasses;
	 }
	 
	 public List<String> getSimpleFunctions(){
		 return this.simpleFunctions;
	 }
	
	//elimina ".\" de la inceputul fiecarui fisier
	//inlocuieste toate "\" cu "\\"
	 private List<String> processFileNames(List<String> files){
			List<String> c=new ArrayList<String>();
			for (String x:files)
				c.add(x.replaceAll("\\.\\\\", "").replaceAll("\\\\", "\\\\\\\\"));
			return c;
	 }
	
	//elimina constructorii din lista de metode
	private List<String> purgeConstructors(List<String> methods,
			List<String> simpleClasses) {
		
		ArrayList<String> armeth=new ArrayList<String>();
		Collections.addAll(armeth, methods.toArray(new String[0]));
		ArrayList<String> construct=new ArrayList<String>();
		
		Iterator<String> iter=armeth.iterator();
		while (iter.hasNext()){
			String x=iter.next();
			if (simpleClasses.contains(getLastName(x)))
				construct.add(x);
		}
		
		for (String x:construct)
			armeth.remove(x);
		
		return armeth;
	}
	
	//creeaza un hashmap cu:
	//cheia - nume entitate gasita
	//field - lista cu indecsi din string unde a fost gasit
	//parametri (String s (din care cauta), int entityCode) entityCode:
	//1 - clase nume simplu, 
	//2 - metode nume simplu, 
	//3 - pachete in nume complet
	//4 - clase in nume complet
	//5 - fisiere
	public Object[] getHashMap(String s, int entityCode) throws ParameterException {
		String methodRegexStart="(\\.|\\s+)";
		String methodRegexStop="(\\(|\\s+)";
		
		String classRegexStart="(\\s+)";
		String classRegexStop="(\\.|\\s+|\\()"; //am pus si punct pentru cazul in care singurele referiri la clase sunt din cod pasteuit
		
		String packageRegexStart="";
		String packageRegexStop="";
		
		String classFullRegexStart="";
		String classFullRegexStop="\\.?";
		
		String filesRegexStart="";
		String filesRegexStop="";
		
		
		switch(entityCode){
		case 1:
//			System.out.println("*********************clase simple");
			return getPreciseHashMap(s, this.simpleClasses, classRegexStop, classRegexStop);
			
		case 2:
//			System.out.println("***********************metode simple");
			return getPreciseHashMap(s, this.simpleFunctions,methodRegexStart,methodRegexStop);
			
		case 3: 
//			System.out.println("***********************pachete");
			return getPreciseHashMap(s, packages, packageRegexStart,packageRegexStop);
		
		case 4:
//			System.out.println("***********************clase full");
			return getPreciseHashMap(s, classes, classFullRegexStart,classFullRegexStop);
		
		case 5:
//			System.out.println("***********************fisiere full");
			return getPreciseHashMap(s, files, filesRegexStart,filesRegexStop);
		}
			
		throw new ParameterException();
	}
	
	private Object[] getPreciseHashMap(String bugText,
			List<String> entityList, String regexStart, String regexStop) {
		
		HashMap<String, ArrayList<StringLimits>> tab= new HashMap<String, ArrayList<StringLimits>>();
		String bufffinal="";
//		System.out.println("Se analizeaza :\n"+s);
		//pentru fiecare entitate x cauta toate matchurile sale in s,
		//si salveaza la fiecare indexul de inceput.
		for(String entity:entityList){
			if (okString(entity) && bugText.contains(entity)){	
				Pattern p=Pattern.compile("("+regexStart+"("+entity+")"+regexStop+")");
				Matcher m=p.matcher(bugText);
				ArrayList<StringLimits> indexList = new ArrayList<StringLimits>();
				
				while(m.find()){
					indexList.add(new StringLimits(entity,m.start(),m.end()));
				}
				
				if (!indexList.isEmpty())
					tab.put(entity, indexList);
				else
					indexList = null;
			}
		}
//		System.out.println("Se sterge"+tab.keySet());
		bufffinal=sterge(bugText,tab.keySet());
//		System.out.println("din care s-a sters:\n"+bufffinal);		
		return new Object[]{tab,bufffinal.toString()};
	}
	
	
	//valideaza unele entitati
	//momentan returneaza false pentru entitatile care sunt formate din cifre, deoarece doar adauga zgomot
	private boolean okString(String x) {
		if (x.matches("\\d+"))
			return false;
		return true;
	}
	//sterge din stringul s toate stringurile din keySet<string>
	private String sterge(String s, Set<String> keySet) {
		for (String x:keySet){
			s=s.replaceAll(x, "");
		}
		return s;
	}
	
	//verifica daca in string exista cod pasteuit
	//TODO extinde si pentru c++
	public boolean hasJavaCode(String s) {
		String regexStart="(\\.)";
		String regexStop="(\\()";
		boolean b=false;
		
		for(String x:this.simpleFunctions){
			if (s.contains(x)){
				Pattern p=Pattern.compile(regexStart+x+regexStop);
				Matcher m=p.matcher(s);
				while (m.find()){ 
	//				System.out.println("\n"+s.substring(m.start(),m.end()));
					b=true;
					break; //modif
				}
			}
		}
		return b;
	}
	
	//returneaza true daca "clasa" este in "pachet"
	public boolean hasClass(String clasa, String pachet) {
		
		for (String oclasa:classes)
			if (getLastName(oclasa).equals(clasa)&&(oclasa.startsWith(pachet)))
				return true;
		return false;
	}

	//returneaza true daca clasa/metoda gasita este in nume simplu
	public boolean isSimpleName(String entitate) {
		Pattern p=Pattern.compile("(\\..+)+");
		Matcher m=p.matcher(entitate);
		return !m.find();
	}
	
	//returneaza toate "targeturile"("clasa"|"metoda") pentru care getSimpleCLFU().equals(entitate)
	public HashSet<String> getSpecifiedCLFUs(String entitate,
			String target) throws ParameterException {
		List<String> entitati;
		HashSet<String> deAdaugat=new HashSet<String>();
		if (target.equals("clasa")) 
			entitati=classes;
		else
			if (target.equals("metoda"))
				entitati=functions;
			else throw new ParameterException();
		
		for (String x:entitati)
			if (this.getLastName(x).equals(entitate))
				deAdaugat.add(x);
		
		return deAdaugat;
	}
}
