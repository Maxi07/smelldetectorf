package lrg.insider.plugins.tools.memoria;

import java.io.File;
import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.insider.gui.ui.utils.ToolsStarter;
import lrg.memoria.utils.ModelLighter;

class ModelLighterTool extends AbstractEntityTool {
    public ModelLighterTool() {
        super("Model Lighter", "Lightens the model", "system");
    }

    public void run(AbstractEntityInterface abstractEntityInterface, Object o) {
        if (abstractEntityInterface instanceof lrg.memoria.core.System == false) return;

        lrg.memoria.core.System aSystem = (lrg.memoria.core.System) abstractEntityInterface;

        ArrayList<String> params = (ArrayList<String>) o;
        new ModelLighter(aSystem).lightenModel(Integer.parseInt(params.get(0)));
        lrg.memoria.core.System.serializeToFile(new File(params.get(1)), aSystem);
    }

    public String getToolName() {
        return "Model Lighter";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Level ");
        parList.add("Output File ");
        ToolsStarter.kindOfButtons = new ArrayList();
        ToolsStarter.kindOfButtons.add(new Integer(0));
        ToolsStarter.kindOfButtons.add(new Integer(1));
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("1 = dispose_up_to_types, 2 = dispose_up_to_methods");
        parList.add("The file where the light model will be saved");
        return parList;
    }
}
