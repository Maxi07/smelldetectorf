package lrg.insider.plugins.tools.memoria.graphgen.strategies;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;

public class UsersOfAllMembers extends InterestingMembersProvider {
    public UsersOfAllMembers(AbstractEntityInterface aei) {
        super(aei);
    }

    public GroupEntity methodRelated() {
        return theCurrentEntity.isUsed("operations calling me");
    }

    public GroupEntity attributeRelated() {
        return theCurrentEntity.isUsed("methods accessing variable");
    }
}
