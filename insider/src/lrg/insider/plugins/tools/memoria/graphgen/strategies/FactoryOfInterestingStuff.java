package lrg.insider.plugins.tools.memoria.graphgen.strategies;

import lrg.common.abstractions.entities.AbstractEntityInterface;

public class FactoryOfInterestingStuff {
    public static InterestingMembersProvider create(String strategyClassName, AbstractEntityInterface theEntity) {
            try {
                java.lang.Class aClass = java.lang.Class.forName(strategyClassName);
                return (InterestingMembersProvider) aClass.getConstructor(java.lang.Class.forName("lrg.common.abstractions.entities.AbstractEntityInterface")).newInstance(theEntity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        return null;
        }


    public static InterestingMembersProvider create(String strategyClassName, AbstractEntityInterface theEntity, String strategyParameters) {
        try {
            java.lang.Class aClass = java.lang.Class.forName(strategyClassName);
            return (InterestingMembersProvider) aClass.getConstructor(java.lang.Class.forName("lrg.common.abstractions.entities.AbstractEntityInterface"), java.lang.Class.forName("java.lang.String")).newInstance(theEntity, strategyParameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}