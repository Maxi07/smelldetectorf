package lrg.insider.plugins.tools.memoria.graphgen.utils;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import lrg.insider.plugins.tools.memoria.graphgen.AnnotationConstants;
import lrg.insider.plugins.tools.memoria.graphgen.SystemGraph;
import lrg.insider.plugins.tools.memoria.graphgen.SystemSubgraph;
import lrg.memoria.core.Component;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.ModelElementList;
import cdc.clusters.ExtendedWeightedEdge;
import cdc.clusters.Node;

import com.cloudgarden.layout.AnchorConstraint;
import com.cloudgarden.layout.AnchorLayout;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class ReviewComponentsJFrame extends javax.swing.JFrame {
	private JPanel jPanel1;
	private JPanel jPanel9;
	private JLabel jLabel1;
	private JButton jButtonRemove;
	private JButton jButtonAdd;
	private JPanel jPanel8;
	private JButton jButtonPrev;
	private JList jListAvailableClasses;
	private JScrollPane jScrollPane1;
	private JPanel jPanel12;
	private JScrollPane jScrollPane2;
	private JButton jButtonOK;
	private JLabel jLabel3;
	private JList jListComponentClasses;
	private JPanel jPanel11;
	private JButton jButtonNext;
	private JLabel jLabelComponentName;
	private JLabel jLabel2;
	private JPanel jPanel10;
	private JPanel jPanel7;
	private JPanel jPanel6;
	private JPanel jPanel5;
	private JPanel jPanel4;
	private JSplitPane jSplitPane1;
	private JPanel jPanel3;
	private JPanel jPanel2;

	private ArrayList<Node> availableList;
	private List<SystemSubgraph<Node, ExtendedWeightedEdge>> componentList;
	private int crtComponentIndex;
	
	/**
	* Auto-generated main method to display this JFrame
	*/
	public static void main(String[] args) {
		ArrayList<String> l1 = new ArrayList<String>();
		ArrayList<String> l2 = new ArrayList<String>();
		for(int i=0; i<30; i++)
		{
			l1.add("Available"+i);
			l2.add("ComponentClass"+i);
		}
	//	ReviewComponentsJFrame inst = new ReviewComponentsJFrame(l1, l2);
	//	inst.setVisible(true);
	}
	
	private void initLists(SystemGraph<Node, ExtendedWeightedEdge> graph, List<SystemSubgraph<Node, ExtendedWeightedEdge>> componentList)
	{
		this.componentList = componentList;
		
		ArrayList<Node> notInComponents = new ArrayList<Node>(graph.vertexSet());
		for(SystemSubgraph<Node, ExtendedWeightedEdge> subgraph : componentList)
			for(Node node : subgraph.vertexSet())
				notInComponents.remove(node);
		this.availableList = notInComponents;
		
		DefaultListModel availableModel = (DefaultListModel)jListAvailableClasses.getModel();
		
		for(Node n : availableList)
			availableModel.addElement(n);
		
		this.crtComponentIndex = 0;
		fillCurrentComponentListModel();
	}

	private void fillCurrentComponentListModel()
	{
		DefaultListModel componentModel = (DefaultListModel)jListComponentClasses.getModel();
		SystemSubgraph<Node, ExtendedWeightedEdge> crtComponent = this.componentList.get(this.crtComponentIndex);
		jLabelComponentName.setText(crtComponent.getCorrespondingComponent().getName());
		for(Node n : crtComponent.vertexSet())
			componentModel.addElement(n);
	}
	
	private void switchToComponent(int newComponentIndex)
	{
		DefaultListModel componentModel = (DefaultListModel)jListComponentClasses.getModel();
		
		componentModel.removeAllElements();
		this.crtComponentIndex = newComponentIndex;
		fillCurrentComponentListModel();
	}
	
	public ReviewComponentsJFrame(lrg.memoria.core.System system) {
		super();
		initGUI();
		
		SystemGraph<Node, ExtendedWeightedEdge> graph = (SystemGraph<Node, ExtendedWeightedEdge>)system.getAnnotation(AnnotationConstants.SYSTEM_JGRAPHTGRAPH);
		ModelElementList<Component> listOfComponents = (ModelElementList<Component>)system.getAnnotation(AnnotationConstants.SYSTEM_ALLCOMPONENTS);
		ArrayList<SystemSubgraph<Node, ExtendedWeightedEdge>> subgraphList = new ArrayList<SystemSubgraph<Node,ExtendedWeightedEdge>>();
		
		for(Component c : listOfComponents)
			subgraphList.add((SystemSubgraph<Node, ExtendedWeightedEdge>)c.getAnnotation(AnnotationConstants.COMPONENT_JGRAPHTSUBGRAPH));
		initLists(graph, subgraphList);
		
	}
	
	public void startUI(ArrayList<String> availableList, ArrayList<String> initialComponentList)
	{
		
	}
	
	private void initGUI() {
		try {
			BorderLayout thisLayout = new BorderLayout();
			getContentPane().setLayout(thisLayout);
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			this.setTitle("Review components");
			{
				jPanel1 = new JPanel();
				getContentPane().add(jPanel1, BorderLayout.NORTH);
				jPanel1.setPreferredSize(new java.awt.Dimension(583, 32));
				{
					jLabel3 = new JLabel();
					jPanel1.add(jLabel3);
					jLabel3.setText("Review the component content by moving classes at will. Note: all changes in the model are made on the fly.");
					jLabel3.setFont(new java.awt.Font("Tahoma",2,14));
					jLabel3.setPreferredSize(new java.awt.Dimension(696, 17));
					jLabel3.setHorizontalAlignment(SwingConstants.CENTER);
				}
			}
			{
				jPanel2 = new JPanel();
				BorderLayout jPanel2Layout = new BorderLayout();
				jPanel2.setLayout(jPanel2Layout);
				getContentPane().add(jPanel2, BorderLayout.CENTER);
				{
					jSplitPane1 = new JSplitPane();
					jPanel2.add(jSplitPane1, BorderLayout.CENTER);
					jSplitPane1.setPreferredSize(new java.awt.Dimension(185, 92));
					{
						jPanel4 = new JPanel();
						jSplitPane1.add(jPanel4, JSplitPane.LEFT);
						BorderLayout jPanel4Layout = new BorderLayout();
						jPanel4.setLayout(jPanel4Layout);
						jPanel4.setPreferredSize(new java.awt.Dimension(400, 533));
						{
							jPanel5 = new JPanel();
							BorderLayout jPanel5Layout = new BorderLayout();
							jPanel5.setLayout(jPanel5Layout);
							jPanel4.add(jPanel5, BorderLayout.NORTH);
							jPanel5.setPreferredSize(new java.awt.Dimension(183, 47));
							{
								jLabel1 = new JLabel();
								jPanel5.add(jLabel1, BorderLayout.CENTER);
								jLabel1.setText("Available classes");
								jLabel1.setPreferredSize(new java.awt.Dimension(316, 14));
								jLabel1.setFont(new java.awt.Font("Tahoma",0,16));
								jLabel1.setHorizontalAlignment(SwingConstants.CENTER);
							}
						}
						{
							jPanel6 = new JPanel();
							BorderLayout jPanel6Layout = new BorderLayout();
							jPanel6.setLayout(jPanel6Layout);
							jPanel4.add(jPanel6, BorderLayout.CENTER);
							jPanel6.setPreferredSize(new java.awt.Dimension(
								296,
								344));
							{
								jScrollPane1 = new JScrollPane();
								jPanel6.add(jScrollPane1, BorderLayout.CENTER);
								{
									ListModel jListAvailableClassesModel = new DefaultListModel();
									jListAvailableClasses = new JList();
									jScrollPane1.setViewportView(jListAvailableClasses);
									jListAvailableClasses
										.setModel(jListAvailableClassesModel);
									/*jListAvailableClasses
										.setPreferredSize(new java.awt.Dimension(
											251,
											484));*/
								}
							}
						}
						{
							jPanel8 = new JPanel();
							BorderLayout jPanel8Layout = new BorderLayout();
							jPanel4.add(jPanel8, BorderLayout.EAST);
							jPanel8.setLayout(jPanel8Layout);
							jPanel8.setFocusable(false);
							jPanel8.setAutoscrolls(true);
							jPanel8.setPreferredSize(new java.awt.Dimension(98, 483));
							{
								jPanel12 = new JPanel();
								AnchorLayout jPanel12Layout = new AnchorLayout();
								jPanel8.add(jPanel12, BorderLayout.SOUTH);
								jPanel12.setPreferredSize(new java.awt.Dimension(98, 133));
								jPanel12.setLayout(jPanel12Layout);
								{
									jButtonAdd = new JButton();
									jPanel12.add(jButtonAdd, new AnchorConstraint(53, 1005, 35, 5, AnchorConstraint.ANCHOR_NONE, AnchorConstraint.ANCHOR_NONE, AnchorConstraint.ANCHOR_ABS, AnchorConstraint.ANCHOR_NONE));
									jButtonAdd.setText("Add   >>");
									jButtonAdd
										.setHorizontalAlignment(SwingConstants.LEADING);
									jButtonAdd
										.setHorizontalTextPosition(SwingConstants.LEADING);
									jButtonAdd.setPreferredSize(new java.awt.Dimension(98, 21));
									jButtonAdd
										.addActionListener(new ActionListener() {
											public void actionPerformed(
												ActionEvent evt) {
												jButtonAddActionPerformed(evt);
											}
										});
								}
								{
									jButtonRemove = new JButton();
									jPanel12.add(jButtonRemove, new AnchorConstraint(255, 1005, 7, 5, AnchorConstraint.ANCHOR_NONE, AnchorConstraint.ANCHOR_NONE, AnchorConstraint.ANCHOR_ABS, AnchorConstraint.ANCHOR_NONE));
									jButtonRemove.setText("<< Remove");
									jButtonRemove
										.setHorizontalTextPosition(SwingConstants.LEADING);
									jButtonRemove
										.setHorizontalAlignment(SwingConstants.LEADING);
									jButtonRemove.setPreferredSize(new java.awt.Dimension(98, 21));
									jButtonRemove
										.addActionListener(new ActionListener() {
											public void actionPerformed(
												ActionEvent evt) {
												jButtonRemoveActionPerformed(evt);
											}
										});
								}
							}
						}
					}
					{
						jPanel7 = new JPanel();
						BorderLayout jPanel7Layout = new BorderLayout();
						jPanel7.setLayout(jPanel7Layout);
						jSplitPane1.add(jPanel7, JSplitPane.RIGHT);
						jPanel7.setPreferredSize(new java.awt.Dimension(145, 533));
						{
							jPanel9 = new JPanel();
							BorderLayout jPanel9Layout = new BorderLayout();
							jPanel9.setLayout(jPanel9Layout);
							jPanel7.add(jPanel9, BorderLayout.NORTH);
							jPanel9.setPreferredSize(new java.awt.Dimension(394, 78));
							{
								jLabel2 = new JLabel();
								jPanel9.add(jLabel2, BorderLayout.NORTH);
								jLabel2.setText("Component:");
								jLabel2.setFont(new java.awt.Font("Tahoma",0,16));
							}
							{
								jLabelComponentName = new JLabel();
								jPanel9.add(jLabelComponentName, BorderLayout.CENTER);
								jLabelComponentName.setText("Component_1");
								jLabelComponentName.setFont(new java.awt.Font("Tahoma",1,14));
								jLabelComponentName.setHorizontalAlignment(SwingConstants.CENTER);
								jLabelComponentName.setPreferredSize(new java.awt.Dimension(394, 16));
							}
							{
								jPanel11 = new JPanel();
								jPanel9.add(jPanel11, BorderLayout.SOUTH);
								{
									jButtonPrev = new JButton();
									jPanel11.add(jButtonPrev);
									jButtonPrev.setText("Previous");
									jButtonPrev
										.addActionListener(new ActionListener() {
										public void actionPerformed(
											ActionEvent evt) {
											jButtonPrevActionPerformed(evt);
										}
										});
								}
								{
									jButtonNext = new JButton();
									jPanel11.add(jButtonNext);
									jButtonNext.setText("Next");
									jButtonNext
										.addActionListener(new ActionListener() {
										public void actionPerformed(
											ActionEvent evt) {
											jButtonNextActionPerformed(evt);
										}
										});
								}
							}
						}
						{
							jPanel10 = new JPanel();
							BorderLayout jPanel10Layout = new BorderLayout();
							jPanel10.setLayout(jPanel10Layout);
							jPanel7.add(jPanel10, BorderLayout.CENTER);
							{
								jScrollPane2 = new JScrollPane();
								jPanel10.add(jScrollPane2, BorderLayout.CENTER);
								{
									ListModel jListComponentClassesModel = new DefaultListModel();
									jListComponentClasses = new JList();
									jScrollPane2.setViewportView(jListComponentClasses);
									jListComponentClasses
										.setModel(jListComponentClassesModel);
								}
							}
						}
					}
				}
			}
			{
				jPanel3 = new JPanel();
				getContentPane().add(jPanel3, BorderLayout.SOUTH);
				{
					jButtonOK = new JButton();
					jPanel3.add(jButtonOK);
					jButtonOK.setText("Close");
					jButtonOK.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							jButtonOKActionPerformed(evt);
						}
					});
				}
			}

			pack();
			this.setSize(734, 633);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void jButtonAddActionPerformed(ActionEvent evt) {
		DefaultListModel componentModel = (DefaultListModel)jListComponentClasses.getModel();
		DefaultListModel availableModel = (DefaultListModel)jListAvailableClasses.getModel();

		//modify the GUI list models
		int selectedIndex = jListAvailableClasses.getSelectedIndex();
		if(selectedIndex < 0)
			return;
		
		Node nodeToAdd = (Node)availableModel.getElementAt(selectedIndex);
		componentModel.addElement(nodeToAdd);
		availableModel.removeElementAt(selectedIndex);
		
		//modify the system model and graph
		DataAbstraction classToAdd = (DataAbstraction)nodeToAdd.getObject();
		SystemSubgraph<Node, ExtendedWeightedEdge> newSubgraph = this.componentList.get(this.crtComponentIndex);
		newSubgraph.addVertex(nodeToAdd);
		Component newComponent = newSubgraph.getCorrespondingComponent();
		newComponent.addScopedElement(classToAdd);
		classToAdd.putAnnotation(AnnotationConstants.CLASS_COMPONENT, newComponent);
	}
	
	private void jButtonRemoveActionPerformed(ActionEvent evt) {
		DefaultListModel componentModel = (DefaultListModel)jListComponentClasses.getModel();
		DefaultListModel availableModel = (DefaultListModel)jListAvailableClasses.getModel();

		//modify the GUI list models		
		int selectedIndex = jListComponentClasses.getSelectedIndex();
		if(selectedIndex < 0)
			return;
		
		Node nodeToRemove = (Node)componentModel.getElementAt(selectedIndex);
		availableModel.addElement(nodeToRemove);
		componentModel.removeElementAt(selectedIndex);
		
		//modify the system model and graph
		DataAbstraction classToRemove = (DataAbstraction)nodeToRemove.getObject();
		Component oldComponent = (Component)classToRemove.getAnnotation(AnnotationConstants.CLASS_COMPONENT);
		//classToRemove.putAnnotation(AnnotationConstants.CLASS_COMPONENT, null);
		classToRemove.removeAnnotation(AnnotationConstants.CLASS_COMPONENT);
		oldComponent.removeScopedElement(classToRemove);
		this.componentList.get(this.crtComponentIndex).removeVertex(nodeToRemove);
	}
	
	private void jButtonPrevActionPerformed(ActionEvent evt) {
		if(this.crtComponentIndex == 0)
			return;
		switchToComponent(--this.crtComponentIndex);
	}
	
	private void jButtonNextActionPerformed(ActionEvent evt) {
		if(this.crtComponentIndex == (this.componentList.size()-1))
			return;
		switchToComponent(++this.crtComponentIndex);
	}
	
	private void jButtonOKActionPerformed(ActionEvent evt) {
		this.setVisible(false);
	}
	
}

