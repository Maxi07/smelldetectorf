package lrg.insider.plugins.tools.memoria.graphgen.rules;


public class StrategyDefinition {
	private Class initialStrategy;
	private String initialStrategyParameter;
	private Class levelTwoStrategy;
	private String edgeColor; //graphviz edge color
	private boolean edgeDirectedOutward; //the direction of the arrow
	
	public StrategyDefinition(String edgeColor, boolean edgeDirectedOutward, 
			Class initialStrategy, String initialStrategyParameter, 
			Class levelTwoStrategy, String levelTwoStrategyParameter)
	{
		super();
		this.edgeColor = edgeColor;
		this.initialStrategy = initialStrategy;
		this.initialStrategyParameter = initialStrategyParameter;
		this.levelTwoStrategy = levelTwoStrategy;
		this.edgeDirectedOutward = edgeDirectedOutward;
	}
	public Class getInitialStrategy()
	{
		return initialStrategy;
	}
	public String getInitialStrategyParameter()
	{
		return initialStrategyParameter;
	}
	public Class getLevelTwoStrategy()
	{
		return levelTwoStrategy;
	}
	public String getEdgeColor()
	{
		return edgeColor;
	}
	public boolean isEdgeDirectedOutward()
	{
		return edgeDirectedOutward;
	}

}
