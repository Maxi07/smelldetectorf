package lrg.insider.plugins.tools.memoria.graphgen;

import java.io.FileWriter;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.insider.plugins.core.properties.AbstractDetail;

import org.jgrapht.Graph;

import cdc.clusters.ExtendedWeightedEdge;
import cdc.clusters.Node;
import cdc.clusters.NodeFactory;



class Element {
    protected AbstractEntityInterface theEntity;
    protected int depth;
    protected Node graphNode;

    public String HTMLExport() {
        return AbstractDetail.linkTo(theEntity)+"\n";
    }

    /*
     * Export in graphwiz dot format for later graph visualisation.
     */
    public void dotFormatExport(FileWriter writer, String edgeColor, boolean isEdgeDirectedOutward) 
    {
    	return; 
    }
    
    public void jgraphtExport(Graph<Node, ExtendedWeightedEdge> graph, boolean isEdgeDirectedOutward)
    {
    	return;
    }

    public Element(AbstractEntityInterface anEntity, int depth, Graph<Node, ExtendedWeightedEdge> graph) {
        theEntity = anEntity;
        this.depth = depth;
        this.graphNode = NodeFactory.getInstance().getOrCreateNode(theEntity.getProperty("Address").toString(), theEntity);
        graph.addVertex(this.graphNode);
    }
}