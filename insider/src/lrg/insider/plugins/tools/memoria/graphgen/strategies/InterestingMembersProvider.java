package lrg.insider.plugins.tools.memoria.graphgen.strategies;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;

public abstract class InterestingMembersProvider {

	   protected AbstractEntityInterface theCurrentEntity;

	   public InterestingMembersProvider(AbstractEntityInterface aei) {
	        theCurrentEntity = aei;
	   }

	   public abstract GroupEntity methodRelated();
	   public abstract GroupEntity attributeRelated();
	}

