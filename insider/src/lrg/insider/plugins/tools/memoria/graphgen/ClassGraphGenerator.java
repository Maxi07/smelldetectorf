package lrg.insider.plugins.tools.memoria.graphgen;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.insider.plugins.tools.memoria.graphgen.rules.AllBuildRule;
import lrg.insider.plugins.tools.memoria.graphgen.rules.BalancedFieldTypeBuildRule;
import lrg.insider.plugins.tools.memoria.graphgen.rules.FieldTypeBuildRule;

public class ClassGraphGenerator extends AbstractGraphGenerator {

	public ClassGraphGenerator()
	{
		super("GraphGenerator", "Creates class dependency graphs as GraphViz dot format files", "class");
	}

	@Override
	protected GroupEntity getGroupOfRootClasses(AbstractEntityInterface entity, String attributeType)
	{
		// we are in the generator for Class, therefore the entity must be a class
		//lrg.memoria.core.Class theClass = (lrg.memoria.core.Class) entity;
		ArrayList<AbstractEntityInterface> list = new ArrayList<AbstractEntityInterface>();
		list.add(entity); 
		return new GroupEntity("oneclass group", list);
	}

	@Override
	protected boolean useCommonFiles()
	{
		return false;
	}

	@Override
	protected void defineBuildStrategies(Object toolParameters)
	{
		ArrayList<String> params = (ArrayList<String>)toolParameters;
		
		addRule(new AllBuildRule());
		addRule(new FieldTypeBuildRule(params.get(0)));// the type name is the first param to the tool
		addRule(new BalancedFieldTypeBuildRule(params.get(0)));// the type name is the first param to the tool
		
	}

}
