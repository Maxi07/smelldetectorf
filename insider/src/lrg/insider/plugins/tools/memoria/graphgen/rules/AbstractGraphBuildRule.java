package lrg.insider.plugins.tools.memoria.graphgen.rules;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public abstract class AbstractGraphBuildRule {
	private FileWriter fileWriter;
	protected ArrayList<StrategyDefinition> strategies;
	
	public AbstractGraphBuildRule()
	{
		this.fileWriter = null;
		this.strategies = new ArrayList<StrategyDefinition>();
	}
	
	protected abstract String fileNameSuffix();

	/**
	 * @return the strategy definitions for the strategies used by this rule
	 * The strategy definition contains:
	 * - the class that will handle the first step of the dependency 
	 * detection, e.g. UsersOfAllMembers.class
	 * - its parameter (null if none)
	 * - the class that will handle the next steps of the
	 * dependency detection, e.g. UsersOfMembersLevelTwo.class
	 * - its parameter (null if none)
	 */
	public ArrayList<StrategyDefinition> getStrategies()
	{
		return this.strategies;
	}
	
	public FileWriter getWriter()
	{
		return this.fileWriter; 
	}
	
	public void createFile(String directory, String fileNameRoot)
	{
		try
		{
			this.fileWriter = new FileWriter(directory + "/" + fileNameRoot + fileNameSuffix() + ".dot");
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void writeToFile(String text)
	{
		try
		{
			fileWriter.write(text);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void closeFile()
	{
		try
		{
			fileWriter.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
