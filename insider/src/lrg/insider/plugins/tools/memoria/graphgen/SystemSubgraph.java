package lrg.insider.plugins.tools.memoria.graphgen;

import java.util.Set;

import lrg.memoria.core.Component;

import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.DirectedWeightedSubgraph;

import cdc.clusters.ExtendedWeightedEdge;
import cdc.clusters.Node;

public class SystemSubgraph<V, E> extends DirectedWeightedSubgraph<Node, ExtendedWeightedEdge> {
	
	private Component correspondingComponent;
	private String name;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5751731533337902378L;

	public SystemSubgraph(WeightedGraph<Node, ExtendedWeightedEdge> arg0, Set<Node> arg1, Set<ExtendedWeightedEdge> arg2)
	{
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

	public void exportAsGraphviz(String graphName, String dirName, String fileName)
	{
		GraphvizExporter.exportGraph(this, graphName, dirName, fileName);
	}

	public Component getCorrespondingComponent()
	{
		return correspondingComponent;
	}

	public void setCorrespondingComponent(Component correspondingComponent)
	{
		this.correspondingComponent = correspondingComponent;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
