package lrg.insider.plugins.tools.memoria.graphgen;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.insider.plugins.tools.memoria.graphgen.rules.RMIBuildRule;
import lrg.insider.plugins.tools.memoria.graphgen.rules.RMICalledHierarcyRule;

public class RMISystemGraphGenerator extends AbstractGraphGenerator {

	public RMISystemGraphGenerator()
	{
		super("RMIGraphGenerator", "Creates class dependency graphs as Graphviz dot format files", "system");
	}

	public RMISystemGraphGenerator(String name, String description, String entity)
	{
		super(name, description, entity);
	}

	@Override
	protected GroupEntity getGroupOfRootClasses(AbstractEntityInterface entity, String ancestorName)
	{
		// we are in the generator for System, therefore the entity must be the system
		lrg.memoria.core.System system = (lrg.memoria.core.System) entity;
		
		FilteringRule derivedOf = new FilteringRule("ancestor name", "==", "class", ancestorName);
        return system.contains("class group").applyFilter("model class").applyFilter(derivedOf);
	}

	@Override
	protected boolean useCommonFiles()
	{
		return false;
	}

	@Override
	protected void defineBuildStrategies(Object toolParameters)
	{
		addRule(new RMIBuildRule());
		addRule(new RMICalledHierarcyRule());
	}
	
	protected String toolParameter0Name()
	{
		return "Name of the ancestor: ";
	}
	
	protected String toolParameter0Default()
	{
		return "Remote";
	}
	protected String toolParameter0Description()
	{
		return "The exact name of the ancestor class or interface";
	}
}
