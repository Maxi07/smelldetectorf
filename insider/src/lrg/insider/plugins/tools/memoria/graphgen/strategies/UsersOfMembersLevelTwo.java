package lrg.insider.plugins.tools.memoria.graphgen.strategies;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;

public class UsersOfMembersLevelTwo extends InterestingMembersProvider {
    public UsersOfMembersLevelTwo(AbstractEntityInterface aei) {
        super(aei);
    }

    public GroupEntity methodRelated() {
        return theCurrentEntity.isUsed("operations calling me"); // entity is now a GROUP of methods (those found interesting at the previous step)
    }

    public GroupEntity attributeRelated() {
        return new GroupEntity("void group", new ArrayList());
    }
}
