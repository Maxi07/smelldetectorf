package lrg.insider.plugins.tools.memoria.graphgen.rules;

import lrg.insider.plugins.tools.memoria.graphgen.strategies.AllUsedMembers;
import lrg.insider.plugins.tools.memoria.graphgen.strategies.UsedMembersLevelTwo;
import lrg.insider.plugins.tools.memoria.graphgen.strategies.UsersOfMembersByFieldType;
import lrg.insider.plugins.tools.memoria.graphgen.strategies.UsersOfMembersLevelTwo;

public class FieldTypeBuildRule extends AbstractGraphBuildRule {

	public FieldTypeBuildRule(String helperClassParameter)
	{
		super();
		strategies.add(new StrategyDefinition("blue", false, UsersOfMembersByFieldType.class, helperClassParameter, 
				  UsersOfMembersLevelTwo.class, null));
		strategies.add(new StrategyDefinition("orange", true, AllUsedMembers.class, null, 
				UsedMembersLevelTwo.class, null));
	}
	
	@Override
	protected String fileNameSuffix()
	{
		return "-byfield";
	}

}
