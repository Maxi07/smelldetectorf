package lrg.insider.metamodel;

import lrg.common.utils.ProgressObserver;
import lrg.memoria.importer.CacheModelLoader;

public class MemoriaCacheModelBuilder extends MemoriaModelBuilder {

    public MemoriaCacheModelBuilder(String cachePath, ProgressObserver observer) {
        super(null, cachePath, observer);
    }

    protected void loadModel() throws Exception {
        currentSystem = new CacheModelLoader().loadModel(cachePath);
    }
}
