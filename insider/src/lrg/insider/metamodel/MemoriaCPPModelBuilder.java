package lrg.insider.metamodel;

import lrg.common.utils.ProgressObserver;
import lrg.memoria.importer.mcc.TablesLoader;

public class MemoriaCPPModelBuilder extends MemoriaModelBuilder {

    public MemoriaCPPModelBuilder(String sourcePath, String cachePath, ProgressObserver observer) {
        super(sourcePath, cachePath, observer, "C++");
    }

    protected void loadModel() throws Exception {
        currentSystem = new TablesLoader(progressObserver, sourcePath, cachePath).getSystem();
    }
}
