package lrg.insider.metamodel;

import lrg.common.abstractions.managers.EntityTypeManager;
import lrg.membrain.representation.BasicBlock;
import lrg.membrain.representation.ControlFlowGraph;
import lrg.membrain.representation.instructionSet.Abstractions.Instruction;
import lrg.membrain.representation.instructionSet.Abstractions.MCall;
import lrg.memoria.core.*;
import lrg.memoria.core.Package;
import lrg.memoria.importer.recoder.XJavaModelLoader;
import lrg.common.utils.ProgressObserver;

public class XMemoriaJavaModelBuilder extends MemoriaModelBuilder {

    public XMemoriaJavaModelBuilder(String sourcePath, String cachePath, ProgressObserver observer) {
        super(sourcePath, cachePath, observer);
    }

    protected void loadModel() throws Exception {
        currentSystem = new XJavaModelLoader(sourcePath, cachePath, progressObserver).getSystem();
    }

    public void buildModel() throws Exception {

        super.buildModel();

        for (int i = 0; i < currentSystem.getNamespaces().size(); i++) {

            Namespace currentNamespace = (Namespace) currentSystem.getNamespaces().get(i);

            Function aFunction;
            for (int j = 0; j < currentNamespace.getGlobalFunctionsList().size(); j++) {
                aFunction = (Function) currentNamespace.getGlobalFunctionsList().get(j);
                if (aFunction instanceof GlobalFunction) {
                    XFunctionBody aFunctionBody = (XFunctionBody) aFunction.getBody();
                    ControlFlowGraph cfg = aFunctionBody.getControlFlowGraph();
                    if (cfg == null) continue;
                    BasicBlock list[] = cfg.getBasicBlocks();
                    for (int k = 0; k < list.length; k++) {
                        list[k].setEntityType(EntityTypeManager.getEntityTypeForName("basic block"));
                        lrg.insider.plugins.core.properties.membrain.basicblock.Scope.registerScopeProperty(list[k],aFunction);
                        addressMap.put(Address.buildFor(list[k]), list[k]);
                    }
                }
            }
        }

        for (Package currentPackage : currentSystem.getPackages()) {
            for (Type currentType : currentPackage.getAllTypesList()) {
                if (currentType instanceof DataAbstraction) {
                    DataAbstraction currentDataAbstraction = (DataAbstraction) currentType;                    
                    for (int k = 0; k < currentDataAbstraction.getMethodList().size(); k++) {
                        Method currentMethod = (Method) currentDataAbstraction.getMethodList().get(k);
                        XFunctionBody aFunctionBody = (XFunctionBody) currentMethod.getBody();
                        ControlFlowGraph cfg = aFunctionBody.getControlFlowGraph();
                        if (cfg == null) continue;
                        BasicBlock list[] = cfg.getBasicBlocks();
                        for (int z = 0; z < list.length; z++) {
                            list[z].setEntityType(EntityTypeManager.getEntityTypeForName("basic block"));
                            lrg.insider.plugins.core.properties.membrain.basicblock.Scope.registerScopeProperty(list[z],currentMethod);
                            addressMap.put(Address.buildFor(list[z]), list[z]);
                            BasicBlock.InstructionsIterator instrIt = list[z].getForwardInstructionsIterator();
                            while(instrIt.hasNext()) {
                                Instruction instr = instrIt.getNext();
                                if(instr instanceof MCall) {
                                    instr.setEntityType(EntityTypeManager.getEntityTypeForName("membrain call"));
                                    addressMap.put(Address.buildFor((MCall)instr),instr);
                                }
                            }
                        }
                    }
                }
            }
        }

        java.lang.System.out.println("Runtime Exception in translation during building: " + ControlFlowGraph.getExceptionCount());
    }

    protected void registerEntityTypes() {
        super.registerEntityTypes();
        EntityTypeManager.registerEntityType("basic block", "method");        
        EntityTypeManager.registerEntityType("membrain call", "basic block");
    }

}
