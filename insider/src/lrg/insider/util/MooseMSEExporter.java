package lrg.insider.util;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.classes.BrainClass;
import lrg.insider.plugins.filters.memoria.classes.DataClass;
import lrg.insider.plugins.filters.memoria.classes.GodClass;
import lrg.insider.plugins.filters.memoria.classes.RefusedParentBequest;
import lrg.insider.plugins.filters.memoria.classes.TraditionBreaker;
import lrg.insider.plugins.filters.memoria.methods.BrainMethod;
import lrg.insider.plugins.filters.memoria.methods.ExtensiveCoupling;
import lrg.insider.plugins.filters.memoria.methods.FeatureEnvy;
import lrg.insider.plugins.filters.memoria.methods.IntensiveCoupling;
import lrg.insider.plugins.properties.memoria.classes.AMW;
import lrg.insider.plugins.properties.memoria.classes.ATFD;
import lrg.insider.plugins.properties.memoria.classes.BUR;
import lrg.insider.plugins.properties.memoria.classes.CRIX;
import lrg.insider.plugins.properties.memoria.classes.NAS;
import lrg.insider.plugins.properties.memoria.classes.NOAM;
import lrg.insider.plugins.properties.memoria.classes.NOM;
import lrg.insider.plugins.properties.memoria.classes.NOPA;
import lrg.insider.plugins.properties.memoria.classes.PNAS;
import lrg.insider.plugins.properties.memoria.classes.TCC;
import lrg.insider.plugins.properties.memoria.classes.WMC;
import lrg.insider.plugins.properties.memoria.classes.WOC;
import lrg.insider.plugins.properties.memoria.methods.LAA;
import lrg.insider.plugins.properties.memoria.methods.MAXNESTING;
import lrg.insider.plugins.properties.memoria.methods.NOAV;
import lrg.memoria.core.Access;
import lrg.memoria.core.Annotation;
import lrg.memoria.core.AnnotationInstance;
import lrg.memoria.core.AnnotationProperty;
import lrg.memoria.core.AnnotationPropertyValuePair;
import lrg.memoria.core.Attribute;
import lrg.memoria.core.Body;
import lrg.memoria.core.Call;
import lrg.memoria.core.Class;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.ExplicitlyDefinedType;
import lrg.memoria.core.Function;
import lrg.memoria.core.FunctionBody;
import lrg.memoria.core.GlobalFunction;
import lrg.memoria.core.GlobalVariable;
import lrg.memoria.core.InitializerBody;
import lrg.memoria.core.LocalVariable;
import lrg.memoria.core.Location;
import lrg.memoria.core.Method;
import lrg.memoria.core.ModelElement;
import lrg.memoria.core.ModelElementList;
import lrg.memoria.core.ModelElementsRepository;
import lrg.memoria.core.ModelVisitor;
import lrg.memoria.core.Namespace;
import lrg.memoria.core.Package;
import lrg.memoria.core.Parameter;
import lrg.memoria.core.Statute;
import lrg.memoria.core.Type;
import lrg.memoria.utils.Logger;
import lrg.memoria.utils.MEMORIABreadthIterator;

public class MooseMSEExporter extends ModelVisitor {
    private lrg.memoria.core.System system;
    private PrintStream os;
    private long counter;
    private ArrayList additionalPackages;
    private HashSet allIDs = new HashSet();
	private HashMap<String, Long> classMap = new HashMap<String, Long>();
    
    private static long packageCounter = 0;
    private static long classCounter = 0;
    private static long functionCounter = 0;
    private static long attributeCounter = 0;
    private static long accessCounter = 0;
    private static long callCounter = 0;
	private static int annotationCounter = 0;
	private static int annotationInstanceCounter = 0;
	private static int parameterCounter = 0;
	private static int localVariableCounter = 0;
    

	private static void printStatistics() {
		System.out.println("Packages: " + packageCounter);
		System.out.println("Classes: " + classCounter);
		System.out.println("Methods: " + functionCounter);
		System.out.println("Attributes: " + attributeCounter);
		System.out.println("Parameters: " + parameterCounter);
		System.out.println("Local Var: " + localVariableCounter);
		System.out.println("Annotations: " + annotationCounter);
		System.out.println("Annotation References: " + annotationInstanceCounter);
		System.out.println("Calls: " + callCounter);
		System.out.println("Accesses: " + accessCounter);
		
	}
	
    private String twoDecimalsConvert(Double DaDoubleNumberoubleNumber) {
       DecimalFormat twoDecimals = new DecimalFormat("#0.00");
       return twoDecimals.format(DaDoubleNumberoubleNumber).replaceAll(",",".");        
    }
    public MooseMSEExporter(lrg.memoria.core.System sys) {
        system = sys;
        counter = ModelElementsRepository.getCurrentModelElementsRepository().getElementCount();
        additionalPackages = new ArrayList();
    }

    public void exportToStream(PrintStream os) {
        this.os = os;
        computeDuplication();
        printHeader();
        Iterator it = new MEMORIABreadthIterator(system);
        for (; it.hasNext();)
            ((ModelElement) it.next()).accept(this);
        printFooter();
        printStatistics();
    }

    private void computeDuplication() {
        // java.lang.System.out.println("Compute Duplication...");

        // new DudeTool().runDude(system);
    }

    private void printHeader() {
        System.out.println("Printing header ...");
        os.println("(Moose.Model (sourceLanguage '" + system.programmingLanguage + "') (entity ");
        os.println("");
    }

    private void printFooter() {
        System.out.println("Printing footer ...");
        os.println("))");
    }

    private void printBelongsTo(Long idref) {
        os.println("\t(belongsTo " + "(idref: " + idref + "))");
    }

    private String printScope(String packageName, ModelElementList allPackages) {
        String containerPackageName = "";
        int indexOfLastScopeSeparator = packageName.lastIndexOf("::");
        if (indexOfLastScopeSeparator <= 0) return containerPackageName;

        containerPackageName = packageName.substring(0, indexOfLastScopeSeparator);
        String containedNameInModel = containerPackageName.replaceAll("::", ".");

        for (Iterator it = allPackages.iterator(); it.hasNext();)
            if (((ModelElement) it.next()).getName().compareTo(containedNameInModel) == 0)
                return containerPackageName;

        for (Iterator it = additionalPackages.iterator(); it.hasNext();)
            if (((String) it.next()).compareTo(containerPackageName) == 0)
                return containerPackageName;

        additionalPackages.add(containerPackageName);
        String furtherContainerName = printScope(containerPackageName, allPackages);

        return containerPackageName;
    }

    private String getSimplePackageName(String fullname) {
        int index = fullname.lastIndexOf("::");
        if (index <= 0) return fullname;
        return fullname.substring(index + 2);
    }

    private boolean isDuplicated(Long id) {
         boolean isDuplicated = allIDs.contains(id);
        if(isDuplicated == false) allIDs.add(id);
//        if(isDuplicated) System.err.println("DUPLICATED: "+id);
        return isDuplicated;
    }

    public void visitPackage(Package pack) {
         if(isDuplicated(pack.getElementID())) return;

        String packageName = pack.getName().replaceAll("\\Q.\\E", "::");
        ModelElementList allPackages = pack.getSystem().getPackages();
        String containerName = printScope(packageName, allPackages);
        packageCounter++;
        os.println("(FAMIX.Namespace ");
        os.println("\t(id: " + pack.getElementID() + ")");
        os.println("\t(name '" + packageName + "')");
        os.println(")");
        os.println();

    }

    private String printMetricValue(PropertyComputer pc, Class c) {
    	if(c.getStatute() != Statute.NORMAL) return twoDecimalsConvert(0.00);
    	else return twoDecimalsConvert((Double) pc.compute(c).getValue());
    }

    private String printFilterValue(FilteringRule fr, Class c) {
    	if(c.getStatute() != Statute.NORMAL) return "false";
    	else return fr.applyFilter(c)+"";
    }
 
    private String truncateFilename(String fullFilename) {
    	String prefix = "FILE:" + system.getName()+"/";
    	
    	return fullFilename.replaceFirst(prefix, "");    	
    }
    
    public void visitClass(Class c) {
        if(isDuplicated(c.getElementID())) return;

        Body currentBody;
        Package tp = c.getPackage();
        int len = tp.getFullName().length();
        classCounter++;
        
        classMap.put(c.getFullName(), c.getElementID());
        os.println("(FAMIX.Class ");
        os.println("\t(id: " + c.getElementID() + ")");
        os.println("\t(name '" + c.getName() + "')");
        this.printBelongsTo(tp.getElementID());
        os.println("\t(isAbstract " + Boolean.toString(c.isAbstract()) + ")");
        os.println("\t(isInterface " + Boolean.toString(c.isInterface()) + ")");
        os.println("\t(fileName '" + truncateFilename(c.getLocation().getFile().getFullName()) + "')");
        os.println("\t(startLine " + c.getLocation().getStartLine() + " )");
        os.println("\t(endLine " + c.getLocation().getEndLine() + " )");
        if (c.getStatute() != Statute.NORMAL) os.println("\t(stub true)");

        os.println("\t(GodClass " + printFilterValue(new GodClass(),c) + ")");
        os.println("\t(DataClass " + printFilterValue(new DataClass(), c) + ")");
        os.println("\t(BrainClass " + printFilterValue(new BrainClass(), c) + ")");

        os.println("\t(RefusedParentBequest " + printFilterValue(new RefusedParentBequest(), c) + ")");
        os.println("\t(TraditionBreaker " + printFilterValue(new TraditionBreaker(), c) + ")");


/*        ArrayList<String> propertComputers = c.getEntityType().nameAllPropertyComputers();
        for (String propertyName : propertComputers) {
            Object result = c.getProperty(propertyName).getValue();
            if (result instanceof Double) {
                double doubleResult = ((Double) result).doubleValue();
                printMeasurement(propertyName, doubleResult);
            }
        }
*/
        printMeasurement("WLOC", compute(c, "WLOC"));
        printMeasurement("WNOS", compute(c, "WNOS"));
        printMeasurement("WNOCond", compute(c, "WNOCond"));
        printMeasurement("WNOCmts", compute(c, "WNOCmts"));

        printMeasurement("WOC",  printMetricValue(new WOC(), c));
        printMeasurement("ATFD", printMetricValue(new lrg.insider.plugins.properties.memoria.classes.ATFD(), c));
        printMeasurement("WMC",  printMetricValue(new WMC(), c));
        printMeasurement("TCC",  printMetricValue(new TCC(), c));
        printMeasurement("CRIX",  printMetricValue(new CRIX(), c));
        printMeasurement("NOAM", printMetricValue(new NOAM(), c));
        printMeasurement("NOPA", printMetricValue(new NOPA(), c));

        printMeasurement("BUR",  printMetricValue(new BUR(), c));
        printMeasurement("BOvR", printMetricValue(new lrg.insider.plugins.properties.memoria.classes.BOvM(), c));
        printMeasurement("AMW",  printMetricValue(new AMW(), c));
        printMeasurement("NOM",  printMetricValue(new NOM(), c));

        printMeasurement("NAS",  printMetricValue(new NAS(), c));
        printMeasurement("PNAS", printMetricValue(new PNAS(), c));

        printMeasurement("LOC",   printMetricValue(new lrg.insider.plugins.properties.memoria.classes.LOC(), c));
        printMeasurement("NProtM", printMetricValue(new lrg.insider.plugins.properties.memoria.classes.NProtM(), c));
        os.println(")");
        os.println();

        for (DataAbstraction superclass : c.getAncestorsList()) {
            if (superclass instanceof Class == false) continue;

            os.println("(FAMIX.InheritanceDefinition ");
            os.println("\t(id: " + (counter++) + ")");
            os.println("\t(subclass " + "(idref: " + c.getElementID() + "))");

            
            os.println("\t(superclass " + "(idref: " + findCorrectSuperClassID(superclass) + "))");
            os.println(")");
            os.println();
            visitClass((Class) superclass);
        }
    }

    private Long findCorrectSuperClassID(DataAbstraction theSuperClass) {
    	Long classID = classMap.get(theSuperClass.getFullName());
    	if(classID != null) {
    		theSuperClass.setElementID(classID);
    		return classID;
    	}
    	
    	return theSuperClass.getElementID();
    }

	private double compute(DataAbstraction c, String metricName) {
        ModelElementList<Method> ml = c.getMethodList();
        int temp = 0;
        Body currentBody;
        for (Method currentMethod : ml) {
            currentBody = currentMethod.getBody();
            if (currentBody != null)
                temp += returnMetricValue(currentBody, metricName);
        }
        return temp;
    }

    private int returnMetricValue(Body b, String metricName) {
        if (metricName.equals("WLOC"))
            return b.getNumberOfLines();
        if (metricName.equals("WMC"))
            return b.getCyclomaticNumber();
        if (metricName.equals("WNOS"))
            return b.getNumberOfStatements();
        if (metricName.equals("WNOCond"))
            return b.getNumberOfDecisions();
        if (metricName.equals("WNOCmts"))
            return b.getNumberOfComments();
        return 0;
    }

    private void printMeasurement(String measurementName, double value) {
        os.println("\t(" + measurementName + " " + value + ")");
    }

    private void printMeasurement(String measurementName, String value) {
    	if(measurementName.compareTo("(default value)") == 0) measurementName = "defaultValue";    	
    	os.println("\t(" + measurementName + " " + value + ")");
    }

    public void visitMethod(Method m) {
        if(isDuplicated(m.getElementID())) return;
        if(!(m.getScope() instanceof Class)) {
        	return;
        }

        functionCounter++;
        os.println("(FAMIX.Method");
        os.println("\t(id: " + m.getElementID() + ")");

        Location loc = m.getLocation();
        if (loc != null) os.println("\t(fileName '" + truncateFilename(loc.getFile().getFullName()) + "')");
        else os.println("\t(fileName " + "library" + ")");
        os.println("\t(startLine " + m.getLocation().getStartLine() + " )");
        os.println("\t(endLine " + m.getLocation().getEndLine() + " )");
        os.println("\t(name '" + m.getName() + "')");
        String fullName = m.getFullName();
        os.println("\t(accessControlQualifier " + accessQualifierToString(m) + ")");
        int index = fullName.substring(0, fullName.indexOf("(")).lastIndexOf(".");
        os.println("\t(signature '" + fullName.substring(index + 1).replaceAll("\\Q.\\E", "::") + "')");
           this.printBelongsTo(((DataAbstraction) m.getScope()).getElementID());
        os.println("\t(hasClassScope " + Boolean.valueOf(m.isStatic()).toString() + ")");
        os.println("\t(isAbstract " + Boolean.valueOf(m.isAbstract()).toString() + ")");
        os.println("\t(isConstructor " + Boolean.valueOf(m.isConstructor()).toString() + ")");
        os.println("\t(isPureAccessor " + Boolean.toString(isAccessor(m)) + ")");

//        ArrayList<String> propertComputers = m.getEntityType().nameAllPropertyComputers();
//        for (String propertyName : propertComputers) {
//            try{                             
//                Object result = m.getProperty(propertyName).getValue();
//                if (result instanceof Double) {
//                    double doubleResult = ((Double) result).doubleValue();
//                    printMeasurement(convertFullMethodNameToCDIF(m.getFullName()), propertyName, doubleResult);
//                }
//            }
//            catch(Exception e){}
//        }

        if (m.getBody() != null) {
            os.println("\t(FeatureEnvy " + new FeatureEnvy().applyFilter(m) + ")");
            os.println("\t(BrainMethod " + new BrainMethod().applyFilter(m) + ")");
            os.println("\t(IntensiveCoupling " + new IntensiveCoupling().applyFilter(m) + ")");
            os.println("\t(DispersedCoupling " + new ExtensiveCoupling().applyFilter(m) + ")");
            os.println("\t(ShotgunSurgery " + new lrg.insider.plugins.filters.memoria.methods.ShotgunSurgery().applyFilter(m) + ")");


            printMeasurement("NOS", m.getBody().getNumberOfStatements());
            printMeasurement("NOCond", m.getBody().getNumberOfDecisions());
            printMeasurement("NMAA", m.getBody().getAccessList().size());
            printMeasurement("NI", m.getBody().getCallList().size());
            printMeasurement("NOCmts", m.getBody().getNumberOfComments());
            printMeasurement("CYCLO", m.getBody().getCyclomaticNumber());

            printMeasurement("CINT", twoDecimalsConvert((Double) new lrg.insider.plugins.properties.memoria.methods.CINT().compute(m).getValue()));
            printMeasurement("CDISP", twoDecimalsConvert((Double) new lrg.insider.plugins.properties.memoria.methods.CDISP().compute(m).getValue()));
            printMeasurement("CM", twoDecimalsConvert((Double) new lrg.insider.plugins.properties.memoria.methods.CM().compute(m).getValue()));
            printMeasurement("CC", twoDecimalsConvert((Double) new lrg.insider.plugins.properties.memoria.methods.CC().compute(m).getValue()));
            printMeasurement("ATFD", twoDecimalsConvert((Double) new ATFD().compute(m).getValue()));
            printMeasurement("LAA", twoDecimalsConvert((Double) new LAA().compute(m).getValue()));
            printMeasurement("FDP", twoDecimalsConvert((Double) new lrg.insider.plugins.properties.memoria.methods.FDP().compute(m).getValue()));
            printMeasurement("LOC", twoDecimalsConvert((Double) new lrg.insider.plugins.properties.memoria.methods.LOC().compute(m).getValue()));
            printMeasurement("MAXNESTING", twoDecimalsConvert((Double) new MAXNESTING().compute(m).getValue()));
            printMeasurement("NOAV", twoDecimalsConvert((Double) new NOAV().compute(m).getValue()));

        }

        os.println(")");
        os.println();
    }

    public void visitAttribute(Attribute a) {
        if(isDuplicated(a.getElementID())) return;
        
        attributeCounter++;
        os.println("(FAMIX.Attribute");
        os.println("\t(id: " + a.getElementID() + ")");
        os.println("\t(name '" + a.getName() + "')");
        this.printBelongsTo(a.getScope().getElementID());
//        os.println("\t(declaredType (idref: " + ((Class) a.getType()).getElementID() + "))");
//        os.println("\t(declaredClass (idref: " + ((Class) a.getType()).getElementID() + "))");
        os.println("\t(hasClassScope " + Boolean.valueOf(a.isStatic()).toString() + ")");
        os.println("\t(accessControlQualifier " + attributeAccessQualifierToString(a) + ")");

        ArrayList<String> propertComputers = a.getEntityType().nameAllPropertyComputers();
        for (String propertyName : propertComputers) {
            Object result = a.getProperty(propertyName).getValue();
            if (result instanceof Double) {
                double doubleResult = ((Double) result).doubleValue();
                printMeasurement(propertyName, doubleResult);
            }
        }
        os.println(")");
        os.println();
    }

    public void visitParameter(Parameter p) {
        if(isDuplicated(p.getElementID())) return;

        parameterCounter++;
        os.println("(FAMIX.FormalParameter");
        os.println("\t(id: " + p.getElementID() + ")");
        os.println("\t(name " + p.getName() + ")");
        this.printBelongsTo(p.getScope().getElementID());
//        os.println("\t(declaredType (idref:" + ((Class) p.getType()).getElementID() + "))");
//        os.println("\t(declaredClass (idref: " + ((Class) p.getType()).getElementID() + "))");
        os.println("\t(position " + getParameterPosition(p) + ")");
        os.println(")");
        os.println();
    }

    public void visitGlobalFunction(GlobalFunction f) {
        if(isDuplicated(f.getElementID())) return;

        
        os.println("(FAMIX.Function");
        os.println("\t(id: " + f.getElementID() + ")");
        Location loc = f.getLocation();
        os.println("\t(name '" + f.getName() + "')");
        String fullName = f.getFullName();
        int index = fullName.substring(0, fullName.indexOf("(")).lastIndexOf(".");
        os.println("\t(signature '" + fullName.substring(index + 1).replaceAll("\\Q.\\E", "::") + "')");
        if (loc != null)
            os.println("\t(sourceAnchor '" + loc.getFile().getFullName() + "')");
        else
            os.println("\t(sourceAnchor '" + "library" + "')");
        this.printBelongsTo(((Namespace) f.getScope()).getElementID());
//        os.println("\t(declaredType (idref:" + ((Class) f.getReturnType()).getElementID() + "))");
//        os.println("\t(declaredClass (idref: " + ((Class) f.getReturnType()).getElementID() + "))");
        os.println("\t(accessControlQualifier " + "public" + ")");

        if (f.getBody() != null) {
            printMeasurement("LOC", f.getBody().getNumberOfLines());
            printMeasurement("CYCLO", f.getBody().getCyclomaticNumber());
            printMeasurement("NOS", f.getBody().getNumberOfStatements());
            printMeasurement("NOCond", f.getBody().getNumberOfDecisions());
            printMeasurement("NMAA", f.getBody().getAccessList().size());
            printMeasurement("NI", f.getBody().getCallList().size());
            printMeasurement("NOCmts", f.getBody().getNumberOfComments());
        }
        printMeasurement("NOP", f.getParameterList().size());
        os.println(")");
        os.println();

    }

    public void visitLocalVar(LocalVariable l) {
        if(isDuplicated(l.getElementID())) return;

        localVariableCounter++;
        os.println("(FAMIX.LocalVariable");
        os.println("\t(id: " + l.getElementID() + ")");
        os.println("\t(name '" + l.getName() + "')");
//        os.println("\t(declaredType (idref:" + ((Class) l.getType()).getElementID() + "))");
//        os.println("\t(declaredClass (idref: " + ((Class) l.getType()).getElementID() + "))");
        if (((Method) l.belongsTo("method")) != null) {
            this.printBelongsTo(((Method) l.belongsTo("method")).getElementID());
        } else {
            AbstractEntity tmp = l.belongsTo("global function");
            if(tmp != null)
                this.printBelongsTo(((GlobalFunction) tmp).getElementID());
        }
        os.println(")");
        os.println();
    }

    public void visitGlobalVar(GlobalVariable l) {
        if(isDuplicated(l.getElementID())) return;

        os.println("(FAMIX.GlobalVariable");
        os.println("\t(id: " + l.getElementID() + ")");
        os.println("\t(name " + l.getName() + ")");
//        os.println("\t(declaredType (idref:" + ((Class) l.getType()).getElementID() + "))");
//        os.println("\t(declaredClass (idref: " + ((Class) l.getType()).getElementID() + "))");
        AbstractEntity tmp =  l.belongsTo("namespace");
        if(tmp != null)
            this.printBelongsTo(((Namespace) tmp).getElementID());
        os.println(")");
        os.println();
    }

    public void visitAccess(Access a) {
        if(isDuplicated(a.getElementID())) return;

        accessCounter++;
        os.println("(FAMIX.Access");
        os.println("\t(id: " + a.getElementID() + ")");
        os.println("\t(accesses " + "(idref: " + a.getVariable().getElementID() + "))");
        Body body = a.getScope();
        Long accessedIn;
        if (body instanceof FunctionBody)
            accessedIn = ((FunctionBody) body).getScope().getElementID();
        else
            accessedIn = ((InitializerBody) body).getScope().getElementID();
        os.println("\t(accessedIn " + "(idref: " + accessedIn + "))");
        os.println(")");
        os.println();
    }

    public void visitCall(Call c) {
        if(isDuplicated(c.getElementID())) return;

        if (c.getScope() instanceof FunctionBody && c.getFunction().getScope() != null) {
            Function scope = (Function) ((FunctionBody) c.getScope()).getScope();
            callCounter++;
            os.println("(FAMIX.Invocation");
            os.println("\t(id: " + c.getElementID() + ")");
            os.println("\t(invokedBy (idref: " + scope.getElementID() + "))");
            os.println("\t(candidate (idref: " + c.getFunction().getElementID() + "))");
            String methodFullName = convertFullMethodNameToCDIF(c.getFunction().getFullName());
            String methodName = methodFullName.substring(methodFullName.lastIndexOf(".") + 1);
            os.println("\t(invokes '" + methodName + "')");
            os.println(")");
            os.println();
        }
    }

    
    public void visitAnnotation(Annotation annotation) {
        if(isDuplicated(annotation.getElementID())) return;
        
        annotationCounter++;
        os.println("(FAMIX.Annotation");
        os.println("\t(id: " + annotation.getElementID() + ")");
        os.println("\t(name '" + annotation.getName() + "')");
        printBelongsTo(annotation.getPackage().getElementID());
    	ModelElementList<AnnotationProperty> listOfProperties = annotation.getAnnotationProperties();

    	for (AnnotationProperty aProperty : listOfProperties) {
    		Type propertyType = aProperty.getType(); 
    		if(propertyType instanceof ExplicitlyDefinedType)    			
    			os.println("\t(" + aProperty.getName() +   " (idref: " + ((ExplicitlyDefinedType)propertyType).getElementID() + "))");    
    		else
    			os.println("\t(" + aProperty.getName() + " " +  propertyType.getFullName()  +")");    

    	}

        os.println(")");
        os.println();
    }
	

    public void visitAnnotationInstance(AnnotationInstance annotInstance) {    	
        if(isDuplicated(annotInstance.getElementID())) return;
        
        annotationInstanceCounter++;
        os.println("(FAMIX.AnnotationInstance");
        os.println("\t(id: " + annotInstance.getElementID() + ")");
        os.println("\t(name '" + annotInstance.getName() + "')");
        printBelongsTo(annotInstance.getAnnotatedElement().getElementID());   
    	ModelElementList<AnnotationPropertyValuePair> listOfProperties = annotInstance.getPropertyValuePairs();

    	for (AnnotationPropertyValuePair aPropertyPair : listOfProperties)    	
    		printMeasurement(aPropertyPair.getAp().getName(), aPropertyPair.getValue().replaceAll("\"", "'"));
        
        os.println(")");
        os.println();
    }

    
    
    private String convertFullClassNameToCDIF(String fullName) {
        return fullName.replaceAll("\\Q.\\E", "::");
    }

    private String convertFullMethodNameToCDIF(String fullName) {
        int index = fullName.substring(0, fullName.indexOf("(")).lastIndexOf(".");
        String signature = fullName.substring(index + 1).replaceAll("\\Q.\\E", "::");
        return convertFullClassNameToCDIF(fullName.substring(0, index)) + "." + signature;
    }

    private int getParameterPosition(Parameter p) {
        Function m = p.getScope();
        ArrayList pl = m.getParameterList();
        int pos = 0;
        for (int i = 0; i < pl.size(); i++)
            if (pl.get(i) != p)
                pos++;
            else
                break;
        return pos;
    }

    private String accessQualifierToString(Method m) {
        if (m.isPublic())
            return "public";
        if (m.isPrivate())
            return "private";
        if (m.isProtected())
            return "protected";
        return "package";
    }

    private String attributeAccessQualifierToString(Attribute a) {
        if (a.isPublic())
            return "public";
        if (a.isPrivate())
            return "private";
        if (a.isProtected())
            return "protected";
        return "package";
    }

    public static void main(String args[]) {
        if (args.length != 3) {
            java.lang.System.err.println("Usage: MooseMSEExporter source_path cache_path mse_file_name");
            java.lang.System.err.println(args.length);
            java.lang.System.exit(1);
        }

        String source_path = args[0];
        String cache_path = args[1];
        String additional_library_path = "";//args[2];
        String cdif_file_name = args[2];
        java.lang.System.setOut(java.lang.System.err);
        String error_file = "errorfile"; //args[4];
        java.io.File err = new java.io.File(error_file);
        try {
            err.createNewFile();
            Logger errorLogger = new Logger(new FileOutputStream(err));
            java.lang.System.setOut(errorLogger);
            java.lang.System.setErr(errorLogger);
            java.lang.System.err.println("Building: JavaModelLoader for source_path = " + source_path);
            lrg.memoria.importer.recoder.JavaModelLoader model = new lrg.memoria.importer.recoder.JavaModelLoader(source_path, cache_path, additional_library_path, null);
            lrg.memoria.core.System mySystem;
            mySystem = model.getSystem();
            MooseMSEExporter exporter = new MooseMSEExporter(mySystem);
            java.io.File file = new java.io.File(cdif_file_name);
            java.lang.System.out.println("Writing the AL file for the path: " + source_path);
            exporter.exportToStream(new PrintStream(new FileOutputStream(file)));
            errorLogger.close();
        } catch (Exception pex) {
            java.lang.System.out.println("Error !!!\nAL file generation aborted !!!");
            pex.printStackTrace();
            java.lang.System.exit(6);
        }
    }

    private static String readLine(BufferedReader br) {
        String line = null;
        try {
            line = br.readLine();
            while (line != null && line.startsWith("-"))
                line = br.readLine();
        } catch (IOException e) {
            java.lang.System.out.println(e);
        }
        return line;
    }

    private boolean isAccessor(Method aMethod) {
        if ((!aMethod.getName().startsWith("get")) && (!aMethod.getName().startsWith("Get")) &&
                (!aMethod.getName().startsWith("set")) && (!aMethod.getName().startsWith("Set")))
            return false;

        if (aMethod.getBody() == null) return false;

        return (aMethod.getBody().getCyclomaticNumber() < 2);
    }

}
