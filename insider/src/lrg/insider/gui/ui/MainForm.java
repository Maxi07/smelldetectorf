package lrg.insider.gui.ui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.filechooser.FileFilter;

import lrg.common.abstractions.managers.CacheManager;
import lrg.common.abstractions.managers.EntityTypeManager;
import lrg.common.metamodel.MetaModel;
import lrg.insider.gui.InsiderGUIMain;
import lrg.insider.gui.ui.browser.BrowserUI;
import lrg.insider.gui.ui.loader.ModelLoaderUI;
import lrg.insider.gui.ui.stories.StoryTreeUI;
import lrg.insider.gui.ui.utils.ProgressBar;
import lrg.insider.metamodel.MemoriaCPPModelBuilder;
import lrg.insider.metamodel.MemoriaCacheModelBuilder;
import lrg.insider.metamodel.MemoriaJavaModelBuilder;
import recoder.ParserException;


// import lrg.memoria.importer.mcc.TablesLoader;

public class MainForm implements ActionListener {

    public static MainForm instance() {
        if (theMainForm == null)
            theMainForm = new MainForm();

        return theMainForm;
    }

    private static MainForm theMainForm;
    public JMenu groupBuildersMenu = new JMenu("Group");
    public JMenu propertiesMenu = new JMenu("Property");
    public JMenu filtersMenu = new JMenu("Filter");
    public JMenu conformityMenu = new JMenu("Conformity");

    private MainForm() {
        JMenu loadMenu, saveMenu;
        //statusBar = new JLabel();
        setStatusBarText("Ready to go.");

        menuBar = new JMenuBar();

        loadMenu = new JMenu("Load");
        openJavaSources = new JMenuItem("Java sources");
        openJavaSources.addActionListener(this);
        loadMenu.add(openJavaSources);
/*
        openExtendedJavaSources = new JMenuItem("Java sources - MemBrain Extension");
        openExtendedJavaSources.addActionListener(this);
        loadMenu.add(openExtendedJavaSources);
 */       
        openCppSources = new JMenuItem("C++ (from McC tables)");
        openCppSources.addActionListener(this);
        loadMenu.add(openCppSources);

        openCachedModel = new JMenuItem("Memoria cached model");
        openCachedModel.addActionListener(this);
        loadMenu.add(openCachedModel);

        loadMenu.addSeparator();
        closeMetamodel = new JMenuItem("Close meta-model");
        closeMetamodel.addActionListener(this);
        loadMenu.add(closeMetamodel);

        loadMenu.addSeparator();
        exitItem = new JMenuItem("Exit");
        exitItem.addActionListener(this);
        loadMenu.add(exitItem);

        saveMenu = new JMenu("Cache");
        staticEntityTypeCache = new JMenuItem("Save Entity Types Cache");
        staticEntityTypeCache.addActionListener(this);
        saveMenu.add(staticEntityTypeCache);

        clearEntityTypeCache = new JMenuItem("Delete EntityTypes Cache");
        clearEntityTypeCache.addActionListener(this);
        saveMenu.add(clearEntityTypeCache);


        menuBar.add(loadMenu);
        // menuBar.add(saveMenu);

        groupBuildersMenu.getPopupMenu().setLayout(new GridLayout(0, 2));
        propertiesMenu.getPopupMenu().setLayout(new GridLayout(0, 2));
        filtersMenu.getPopupMenu().setLayout(new GridLayout(0, 2));
        conformityMenu.getPopupMenu().setLayout(new GridLayout(0, 2));

        groupBuildersMenu.setEnabled(false);
        propertiesMenu.setEnabled(false);
        filtersMenu.setEnabled(false);
        conformityMenu.setEnabled(false);

        menuBar.add(groupBuildersMenu);
        menuBar.add(propertiesMenu);
        menuBar.add(filtersMenu);
        menuBar.add(conformityMenu);

        //splitPane = new JSplitPane();
        splitPane.setTopComponent(StoryTreeUI.instance().getTopComponent());
        splitPane.setBottomComponent(BrowserUI.instance().getTopComponent());
        splitPane.setDividerLocation(200);
    }


    public void actionPerformed(final ActionEvent e) {
        if ((e.getSource() == openCppSources) || (e.getSource() == openJavaSources) || (e.getSource() == openExtendedJavaSources)) {
            filtersMenu.setEnabled(true);
            conformityMenu.setEnabled(true);
            if (ModelLoaderUI.show()) {
                new Thread() {
                    public void run() {
                        ProgressBar progress = new ProgressBar("Loading the model ...");
                        try {
                            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
                            if (e.getSource() == openCppSources)
                                MetaModel.createFrom(
                                    new MemoriaCPPModelBuilder(ModelLoaderUI.getSourcePath(),
                                        ModelLoaderUI.getCachePath(), progress),
                                    ModelLoaderUI.getSourcePath()
                                );
                                //MetaModel.createFrom(ModelLoaderUI.getSourcePath(), ModelLoaderUI.getCachePath(), MetaModel.CPP, progress);
                            if (e.getSource() == openJavaSources)
                                MetaModel.createFrom(
                                    new MemoriaJavaModelBuilder(ModelLoaderUI.getSourcePath(),
                                        ModelLoaderUI.getCachePath(), InsiderGUIMain.getAdditioanClassPath(), progress),
                                    ModelLoaderUI.getSourcePath()
                                );
                                //MetaModel.createFrom(ModelLoaderUI.getSourcePath(), ModelLoaderUI.getCachePath(), MetaModel.JAVA, progress);
/*
                            if (e.getSource() == openExtendedJavaSources)
                                                            MetaModel.createFrom(
                                   new XMemoriaJavaModelBuilder(ModelLoaderUI.getSourcePath(), ModelLoaderUI.getCachePath(), progress),
                                    ModelLoaderUI.getSourcePath()
                                );
*/                               
                                //MetaModel.createFrom(ModelLoaderUI.getSourcePath(), ModelLoaderUI.getCachePath(), MetaModel.EXTENDED_JAVA, progress);
                        } catch (ParserException e1) {
                            e1.printStackTrace();
                            JOptionPane.showMessageDialog(topComponent, e1.toString(), "EXCEPTION: ParserException", JOptionPane.WARNING_MESSAGE);
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            JOptionPane.showMessageDialog(topComponent, "The model could not be loaded !", "ERROR", JOptionPane.WARNING_MESSAGE);
                            return;
                        } finally {
                            progress.close();
                        }

                        BrowserUI.instance().newMetaModelLoaded();
                        StoryTreeUI.instance().setMetaModel();
                        setStatusBarText("Model succesfully loaded from: " + ModelLoaderUI.getSourcePath());
                    }
                }.start();
            }
        }

        if (e.getSource() == openCachedModel || e.getSource() == openHisMo) {
            final JFileChooser fc = new JFileChooser();
            if (e.getSource() == openCachedModel) {
                fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                fc.setFileFilter(new FileFilter() {
                    public boolean accept(File f) {
                        return f.isDirectory() || f.getPath().endsWith(".dat");
                    }

                    public String getDescription() {
                        return "Memoria cache files";
                    }
                });
            }
            if (e.getSource() == openHisMo)
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            if (fc.showOpenDialog(topComponent) == JFileChooser.APPROVE_OPTION) {
                new Thread() {
                    public void run() {
                        ProgressBar progress = new ProgressBar("Loading the model ...");
                        try {
                            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
                            if (e.getSource() == openCachedModel)
                                MetaModel.createFrom(
                                    new MemoriaCacheModelBuilder(fc.getSelectedFile().getAbsolutePath(), progress),
                                    fc.getSelectedFile().getAbsolutePath()
                                );
                                //MetaModel.createFrom(fc.getSelectedFile().getAbsolutePath(), null, MetaModel.CACHE, progress);
                            /* if (e.getSource() == openHisMo)
                                MetaModel.createFrom(
                                    new HismoModelBuilder(fc.getSelectedFile().getAbsolutePath(),"hismo", progress),
                                    fc.getSelectedFile().getAbsolutePath()
                                ); */
                                //MetaModel.createFrom(fc.getSelectedFile().getAbsolutePath(), null, MetaModel.HISMO, progress);
                        } catch (ParserException e1) {
                            e1.printStackTrace();
                            JOptionPane.showMessageDialog(topComponent, e1.toString(), "EXCEPTION: ParserException", JOptionPane.WARNING_MESSAGE);
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            JOptionPane.showMessageDialog(topComponent, "The model could not be loaded !", "ERROR", JOptionPane.WARNING_MESSAGE);
                            return;
                        } finally {
                            progress.close();
                        }

                        BrowserUI.instance().newMetaModelLoaded();
                        StoryTreeUI.instance().setMetaModel();
                        setStatusBarText("Model succesfully loaded from: " + fc.getSelectedFile().getAbsolutePath());
                    }
                }.start();
            }
        }

        if (e.getSource() == closeMetamodel) {
            MetaModel.closeMetaModel();
            BrowserUI.instance().metaModelUnloaded();
            StoryTreeUI.instance().unloadMetaModel();
            System.gc();
            setStatusBarText("Model was succesfully unloaded ! ");
        }

        if (e.getSource() == exitItem) {
            System.exit(0);
        }

        if (e.getSource() == staticEntityTypeCache) {
            EntityTypeManager.writeStaticEntityTypesToCache();
            setStatusBarText("EntityTypes cache saved.");
        }

        if (e.getSource() == clearEntityTypeCache) {
            CacheManager.getStaticETCache().delete();
            setStatusBarText("EntityTypes cache deleted!");
        }
    }

    public Container getTopComponent() {
        return topComponent;
    }

    public JMenuBar getMenuBar() {
        return menuBar;
    }

    public void setStatusBarText(String text) {
        if (text.length() > 100) text = text.substring(0, 100) + " [...]";
        statusBar.setText(text);
    }

    private JLabel statusBar;
    private JPanel topComponent;
    private JSplitPane splitPane;   // this splits the screen in two for the browser and for the StoryView

    JMenuBar menuBar;

    //
    JMenuItem openJavaSources, openCppSources, exitItem, openCachedModel, openExtendedJavaSources, closeMetamodel;
    JMenuItem staticEntityTypeCache, dynamicEntityTypeCache, clearEntityTypeCache, openHisMo;

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     */
    private void $$$setupUI$$$() {
        final JPanel _1;
        _1 = new JPanel();
        topComponent = _1;
        _1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        final JLabel _2;
        _2 = new JLabel();
        statusBar = _2;
        _2.setText("Label");
        _1.add(_2, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, 8, 1, 0, 0, null, null, null));
        final JSplitPane _3;
        _3 = new JSplitPane();
        splitPane = _3;
        _3.setOneTouchExpandable(true);
        _3.setDividerLocation(35);
        _3.setOrientation(0);
        _1.add(_3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 3, 3, 3, null, null, null));
    }

}
