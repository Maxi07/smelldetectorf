package lrg.insider.gui.ui.views;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.insider.custom.Custom;
import lrg.insider.gui.ui.EntityMouseAction;
import lrg.insider.gui.ui.MainForm;
import lrg.insider.gui.ui.stories.StoryTreeUI;
import lrg.insider.gui.ui.stories.StoryUI;

/**
 * Created by IntelliJ IDEA.
 * User: cristic
 * Date: Apr 10, 2004
 * Time: 12:39:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewUI extends MouseAdapter implements ListSelectionListener
{
    public ViewUI(GroupEntity aGroupEntity)
    {
        model = new InsiderViewTableModel();
        table = new InsiderViewTable(model, this);
        topComponent = new JScrollPane(table);

        JTableHeader th = table.getTableHeader();
        th.addMouseListener(this);
//        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.getSelectionModel().addListSelectionListener(this);
        table.addMouseListener(this);

        columnNamesToBeDisplayed = new ArrayList();
        columnNamesToBeDisplayed.add("Name");

        this.setGroupEntity(aGroupEntity);
        ascending = true;
        oldColor = table.getSelectionBackground();
        lastHighlightFilterName = null;
    }

    public GroupEntity getGroupEntity()
    {
        return myGroupEntity;
    }

    public void highlightFilter(FilteringRule highlightRule)
    {
        lastHighlightFilterName = highlightRule;
        table.getSelectionModel().removeListSelectionListener(this);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table.setSelectionBackground(new Color(1.0f, 0.75f, 0.75f));
        for (int i = 0; i < myGroupEntity.size(); i++)
        {
            AbstractEntityInterface currentEntity = myGroupEntity.getElementAt(model.mapToGroupElementIndex(i));
            if (highlightRule.applyFilter(currentEntity))
            {
                table.getSelectionModel().addSelectionInterval(i, i);
            }
        }
        table.getSelectionModel().addListSelectionListener(this);
    }

    public void unHighlight()
    {
        table.getSelectionModel().clearSelection();
        table.setSelectionBackground(oldColor);
//        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public void applyFilter(FilteringRule aFilteringRule)
    {
    	Custom custom = new Custom(myGroupEntity);
    	
    	
        GroupEntity filteredGroupEntity = myGroupEntity.applyFilter(aFilteringRule);
        if (filteredGroupEntity.size() < 1) {
            JOptionPane.showMessageDialog(getTopComponent(), "There is no entity matching the filter criteria\n" +
                   "Filter was not applied", "Filter to restrictive", JOptionPane.YES_NO_OPTION);
        }
        else {
            StoryTreeUI.instance().addStoryUI(new StoryUI(new ViewUI(filteredGroupEntity)),
                    StoryTreeUI.CHILD);
        }
    }


    public Collection getAllFilteringRules()
    {
        if((myGroupEntity == null) || (myGroupEntity.getEntityTypeOfElements() == null))
                return new ArrayList();
        return myGroupEntity.getEntityTypeOfElements().nameAllFilteringRules();
    }

    public Component getTopComponent()
    {
        return topComponent;
    }

    public void valueChanged(ListSelectionEvent e)
    {
        //Ignore extra messages.
        if (e.getValueIsAdjusting()) return;

        ListSelectionModel lsm = (ListSelectionModel) e.getSource();
        if (lsm.isSelectionEmpty()) {
            selectedEntity = null;
            MainForm.instance().groupBuildersMenu.setEnabled(false);
        } else {
            int selectedRow = lsm.getMinSelectionIndex();
            table.setSelectionBackground(oldColor);
            table.getSelectionModel().removeListSelectionListener(this);
//            table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            table.getSelectionModel().setLeadSelectionIndex(selectedRow);
            table.getSelectionModel().addListSelectionListener(this);
            selectedEntity = myGroupEntity.getElementAt(model.mapToGroupElementIndex(selectedRow));
        }
    }

    public void updateGroupMenu() {
        // fake mouse click ;) - very bad!
        if (selectedEntity!=null) {
            MainForm.instance().groupBuildersMenu.setEnabled(true);
            EntityMouseAction.instance().buildFor(selectedEntity,
                new MouseEvent(table,0,0,0,0,0,1,false,MouseEvent.BUTTON1));
        } else {
            MainForm.instance().groupBuildersMenu.setEnabled(false);
        }
    }

    public void mouseClicked(MouseEvent e)
    {
        if (e.getSource() == table.getTableHeader())
        {
            TableColumnModel columnModel = table.getColumnModel();
            int viewColumn = columnModel.getColumnIndexAtX(e.getX());
            int column = table.convertColumnIndexToModel(viewColumn);
            if (e.getClickCount() == 1 && column != -1)
            {
                //System.out.println("Sorting ...");
                ascending = !ascending;
                model.sortByColumn(column, ascending);
                table.clearSelection();
                if (table.getSelectionBackground() != oldColor)
                {
                    unHighlight();
                    highlightFilter(lastHighlightFilterName);
                }
            }
        }
        if (e.getSource() == table)
        {
            // these 2 line are here because when right-clicking on a table it doesn't automatically change it's selection
            Point p = new Point(e.getX(), e.getY());
            table.getSelectionModel().setLeadSelectionIndex(table.rowAtPoint(p));

            if (selectedEntity != null) {
                EntityMouseAction.instance().buildFor(selectedEntity, e);
            }
        }
    }

    public void setStoryUI(StoryUI storyUI)
    {
        myStoryUI = storyUI;
    }

    private void setGroupEntity(GroupEntity aGroupEntity)
    {
    	
        model.feedWithData(columnNamesToBeDisplayed, aGroupEntity.iterator());
        myGroupEntity = aGroupEntity;
        String name=aGroupEntity.getName();
        if (name.length()>80) name=name.substring(0,80)+" (...) ";
        topComponent.setName(name + " [" + aGroupEntity.size() + "]");
        if (myStoryUI != null)
        {
            JTabbedPane myStorysTabbedPane = (JTabbedPane) myStoryUI.getTopComponent();
            myStorysTabbedPane.setTitleAt(myStorysTabbedPane.getSelectedIndex(), topComponent.getName());
        }
    }

    public boolean hasPropertyCalled(String propertyName)
    {
        for (int i = 0; i < model.getColumnCount(); i++)
            if (model.getColumnName(i).compareTo(propertyName) == 0)
                return true;

        return false;
    }

    public void addColumnForPropertyCalled(String propertyName)
    {
        if (this.hasPropertyCalled(propertyName))
            return;

        columnNamesToBeDisplayed.add(propertyName);
        model.feedWithData(columnNamesToBeDisplayed, myGroupEntity.iterator());
    }

    public void removeColumnForPropertyCalled(String propertyName)
    {
        if (this.hasPropertyCalled(propertyName))
        {
            columnNamesToBeDisplayed.remove(propertyName);
            model.feedWithData(columnNamesToBeDisplayed, myGroupEntity.iterator());
        }
    }

    public String toString() {
        StringBuffer temp = new StringBuffer("");
        DecimalFormat twoDecimals = new DecimalFormat("#0.00");
        for(Object crtColumn : columnNamesToBeDisplayed) {
            temp.append(crtColumn + "\t");
        }
        temp.append("\n");
        for(int i = 0; i < model.getRowCount(); i++) {
          for(int j = 0; j < model.getColumnCount(); j++) {
              temp.append(model.getValueAt(i,j));
              if(j < model.getColumnCount()-1) temp.append("\t");
          }
          temp.append("\n");
        }

        return temp.toString();
    }

    private boolean ascending;
    private FilteringRule lastHighlightFilterName;
    private Color oldColor;

    private JScrollPane topComponent;
    private JTable table;
    private InsiderViewTableModel model;

    private AbstractEntityInterface selectedEntity;
    private GroupEntity myGroupEntity;

    private StoryUI myStoryUI;
    private ArrayList columnNamesToBeDisplayed;
}
