package lrg.insider.gui.ui.views.sort;

import java.util.ArrayList;

import lrg.common.abstractions.entities.ResultEntity;

public class BubbleSortStrategy extends SortStrategy
{
    public void sort(ArrayList unsortedColumnValues, int[] indexMap, boolean ascending)
    {
        for (int i=0; i < unsortedColumnValues.size(); i++)
        {
            for (int j=0 ; j < unsortedColumnValues.size(); j++)
            {
                ResultEntity ri = (ResultEntity) unsortedColumnValues.get(indexMap[i]);
                ResultEntity rj = (ResultEntity) unsortedColumnValues.get(indexMap[j]);
                // if(ri == null || rj == null) continue;
                if (ascending)
                {
                    if (ri.compareTo(rj) > 0)
                    {
                        int temp = indexMap[i];
                        indexMap[i] = indexMap[j];
                        indexMap[j] = temp;
                    }
                }
                else
                {
                    if (ri.compareTo(rj) < 0)
                    {
                        int temp = indexMap[i];
                        indexMap[i] = indexMap[j];
                        indexMap[j] = temp;
                    }
                }
            }
        }
    }
}
