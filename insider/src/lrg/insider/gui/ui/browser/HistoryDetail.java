package lrg.insider.gui.ui.browser;

public class HistoryDetail {
    private String address;
    private String detailName;

    public HistoryDetail(String address, String detailName) {
        this.address = address;
        this.detailName = detailName;
    }

    public String getAddress() {
        return this.address;
    }

    public String getDetailName() {
        return this.detailName;
    }
}
