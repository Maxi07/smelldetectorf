package lrg.insider.gui.ui.browser;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.metamodel.MetaModel;
import lrg.insider.metamodel.Address;

public class BrowserUI extends MouseAdapter implements ActionListener, KeyListener, ChangeListener {
    private JTabbedPane tabs;

    public static BrowserUI instance() {
        if (theBrowserUI == null)
            theBrowserUI = new BrowserUI();

        return theBrowserUI;
    }

    private static BrowserUI theBrowserUI;

    private BrowserUI() {
        // BrowserUI.addIconToButton(backButton, "images/Back24.gif", "Back");
        // BrowserUI.addIconToButton(forwardButton, "images/Forward24.gif", "Forward");
        // BrowserUI.addIconToButton(rootButton, "images/Home24.gif", "Root");
        //SwingUI.addIconToButton(searchButton, "general/Search", "Search");

        rootButton.addActionListener(this);
        backButton.addActionListener(this);
        forwardButton.addActionListener(this);

        rootButton.setEnabled(false);
        backButton.setEnabled(false);
        forwardButton.setEnabled(false);
        searchButton.setEnabled(false);

        addressComboBox.setEnabled(false);
        addressComboBox.addActionListener(this);
        addressComboBox.getEditor().getEditorComponent().addKeyListener(this);

        Dimension d=addressComboBox.getSize();
        addressComboBox.setMaximumSize(d);
        addressComboBox.getEditor().getEditorComponent().setMaximumSize(d);

        addressComboBoxModel = new DefaultComboBoxModel();
        addressComboBox.setModel(addressComboBoxModel);

        browsers = new ArrayList();

        popup = new JPopupMenu();

        closeTabMenuItem = new JMenuItem("Close This Tab");
        closeTabMenuItem.addActionListener(this);
        popup.add(closeTabMenuItem);

        tabs.addMouseListener(this);
        tabs.addChangeListener(this);
    }

    public void newMetaModelLoaded() {
        rootButton.setEnabled(true);
        backButton.setEnabled(false);
        forwardButton.setEnabled(false);
        searchButton.setEnabled(false);
        addressComboBox.setEnabled(true);

        this.pointToInNewTab(Address.buildForRoot(), "Detail");
    }

    public void metaModelUnloaded() {
        rootButton.setEnabled(false);
        backButton.setEnabled(false);
        forwardButton.setEnabled(false);
        searchButton.setEnabled(false);
        addressComboBox.setEnabled(false);

        tabs.removeChangeListener(this);
        tabs.removeAll();
        browsers.clear();
        tabs.addChangeListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        // TODO: how to filter unwanted notifications from the combobox ?! is this the right way ?
        if (e.getSource() == addressComboBox && e.getActionCommand().compareTo("comboBoxEdited") == 0) {
            String tempAddress = (String) addressComboBox.getSelectedItem();

            if (MetaModel.instance().findEntityByAddress(tempAddress) != null)
                getSelectedBrowser().goTo(tempAddress, "Detail");
            else
                JOptionPane.showMessageDialog(topComponent, "The address you entered could not be found.", "Invalid Address!", JOptionPane.WARNING_MESSAGE);
        }

        if (e.getSource() == backButton)
            getSelectedBrowser().goBack();

        if (e.getSource() == forwardButton)
            getSelectedBrowser().goForward();

        if (e.getSource() == rootButton) {
        	String filename = "../../results/";
        	if(getSelectedBrowser().getCurrentEntity() instanceof lrg.memoria.core.System)
        		filename += "~root";
        	else filename += getSelectedBrowser().getCurrentEntity().getName();
        	filename += "_"+getSelectedBrowser().getDetailName()+".html";
        	System.out.println(filename);
            try {
            	PrintStream out_stream = new PrintStream(new FileOutputStream(filename));
            	out_stream.print(getSelectedBrowser().getCurrentDetailText());
            	out_stream.close();
            } catch (Exception ex) { }					
        }
        
        if (e.getSource() == closeTabMenuItem) {
            BrowserPageUI toBeRemoved = getSelectedBrowser();
            tabs.remove(toBeRemoved.getTopComponent());
            browsers.remove(toBeRemoved);
        }
    }

    public void update(BrowserPageUI browser) {
        AbstractEntityInterface currentEntity = browser.getCurrentEntity();
        if (currentEntity == null) return;

        if (tabs.getSelectedIndex() >= 0) {
            String title=currentEntity.getProperty("Address").toString();
            if (title.length()>80) title=title.substring(0,80)+"(...)";
            tabs.setTitleAt(tabs.indexOfComponent(browser.getTopComponent()), title);
        }


        JTextField jtf = (JTextField) addressComboBox.getEditor().getEditorComponent();
        jtf.setText(currentEntity.getProperty("Address").toString());

        backButton.setEnabled(browser.history().numberOfBackLocations() > 0);
        forwardButton.setEnabled(browser.history().numberOfForwardLocations() > 0);
    }

    public void keyPressed(KeyEvent e) {
        if (!addressComboBox.isPopupVisible())
            addressComboBox.setPopupVisible(true);
        switch (e.getKeyCode()) {
            case KeyEvent.VK_DOWN: {
                JTextField jtf = (JTextField) addressComboBox.getEditor().getEditorComponent();
                int selectedIndex = addressComboBox.getSelectedIndex();
                if (selectedIndex < addressComboBox.getItemCount() - 1)
                    if (jtf.getText().compareTo(addressComboBox.getSelectedItem().toString()) == 0)
                        selectedIndex++;

                addressComboBox.setSelectedIndex(selectedIndex);
                jtf.setText((String) addressComboBox.getSelectedItem());
                e.consume();
            } break;
            case KeyEvent.VK_UP: {
                JTextField jtf = (JTextField) addressComboBox.getEditor().getEditorComponent();
                int selectedIndex = addressComboBox.getSelectedIndex();
                if (selectedIndex > 0)
                    if (jtf.getText().compareTo(addressComboBox.getSelectedItem().toString()) == 0)
                        selectedIndex--;

                addressComboBox.setSelectedIndex(selectedIndex);
                jtf.setText((String) addressComboBox.getSelectedItem());
                e.consume();
            } break;
        }
    }


    public void keyReleased(KeyEvent e) {
        // nop
    }

    public void keyTyped(KeyEvent e) {
        JTextField jtf = (JTextField) addressComboBox.getEditor().getEditorComponent();
        String prefix = jtf.getText().substring(0, jtf.getSelectionStart());

        switch (e.getKeyChar()) {
            case KeyEvent.VK_BACK_SPACE:
                if (prefix.length() > 2)
                    prefix = jtf.getText().substring(0, jtf.getSelectionStart() - 1);
                else
                    prefix = "";
                break;
            case KeyEvent.VK_ENTER:
                break;                      // this is here to stop adding '\n' to the address string
            default:
                prefix = prefix + e.getKeyChar();
                break;
        }

        addressComboBoxModel.removeAllElements();
        ArrayList matches = MetaModel.instance().findAddressesThatStartWith(prefix);
        for (int i = 0; i < matches.size(); i++)
            addressComboBoxModel.addElement(matches.get(i));

        if (matches.size() > 0 && prefix.compareTo(matches.get(0).toString()) != 0) {
            jtf.setText(matches.get(0).toString());
            jtf.setCaretPosition(matches.get(0).toString().length());
            jtf.moveCaretPosition(prefix.length());
        } else
            jtf.setText(prefix);

        e.consume();
    }

    public void pointTo(String externalAddressAsString, String detailName) {
        getSelectedBrowser().goTo(externalAddressAsString, detailName);
    }

    public void pointToInNewTab(String address, String detailName) {
        // this call will also register the newly created browserpage with this object (browserUI)
        new BrowserPageUI(this, address, detailName);
    }
    
    public Component getTopComponent() {
        return topComponent;
    }

    public void stateChanged(ChangeEvent e) {
        this.update(this.getSelectedBrowser());
    }

    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger() && browsers.size() > 1)
            popup.show(e.getComponent(), e.getX(), e.getY());
    }

    void addBrowser(BrowserPageUI b) {
        browsers.add(b);
        tabs.add(b.getTopComponent());
        tabs.setSelectedComponent(b.getTopComponent());
    }

    private BrowserPageUI getSelectedBrowser() {
        return (BrowserPageUI) browsers.get(tabs.getSelectedIndex());
    }

    private JButton backButton;
    private JButton forwardButton;
    private JButton rootButton;
    private JButton searchButton;
    private JComboBox addressComboBox;
    private DefaultComboBoxModel addressComboBoxModel;

    private JPanel topComponent;

    private JPopupMenu popup;
    private JMenuItem closeTabMenuItem;

    private ArrayList browsers;

    private static void addIconToButton(JButton button, String imageName, String altText) {
        //Look for the image.
        String imgLocation = "file://" + java.lang.System.getProperty("user.dir") + java.lang.System.getProperty("file.separator") + imageName;
        //URL imageURL = BrowserUI.class.getResource(imgLocation);
        URL imageURL = null;

        try {
            imageURL = new URL(imgLocation);
        } catch (MalformedURLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        // beautify the button.
        if (imageURL != null) {                      //image found
            button.setIcon(new ImageIcon(imageURL, altText));
            button.setText(altText);
        } else {                                     //no image found
            button.setText(altText);
            System.err.println("Resource not found: " + imgLocation);
        }
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     */
    private void $$$setupUI$$$() {
        final JPanel _1;
        _1 = new JPanel();
        topComponent = _1;
        _1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        final JPanel _2;
        _2 = new JPanel();
        _2.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        _1.add(_2, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 8, 1, 3, 0, null, null, null));
        final JPanel _3;
        _3 = new JPanel();
        _3.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 6, new Insets(0, 0, 0, 0), -1, -1));
        _2.add(_3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 8, 1, 0, 0, null, null, null));
        final JButton _4;
        _4 = new JButton();
        backButton = _4;
        _4.setToolTipText("Go back one location");
        _4.setText("Back");
        _4.setHorizontalAlignment(0);
        _4.setHorizontalTextPosition(11);
        _3.add(_4, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, 0, 0, 3, 0, null, null, null));
        final com.intellij.uiDesigner.core.Spacer _5;
        _5 = new com.intellij.uiDesigner.core.Spacer();
        _3.add(_5, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 1, 0, 1, new Dimension(10, -1), null, null));
        final JButton _6;
        _6 = new JButton();
        forwardButton = _6;
        _6.setToolTipText("Go forward one location");
        _6.setText("Forward");
        _6.setHorizontalAlignment(0);
        _6.setHorizontalTextPosition(11);
        _3.add(_6, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, 8, 0, 3, 0, null, null, null));
        final JButton _7;
        _7 = new JButton();
        rootButton = _7;
        _7.setToolTipText("Save Detail as HTML");
        _7.setText("Save");
        _3.add(_7, new com.intellij.uiDesigner.core.GridConstraints(0, 3, 1, 1, 0, 0, 3, 0, null, null, null));
        final JComboBox _8;
        _8 = new JComboBox();
        addressComboBox = _8;
        _8.setToolTipText("for simplicity the valid syntax for addresses is like this: package.package::class.class::member");
        _8.setEditable(true);
        _3.add(_8, new com.intellij.uiDesigner.core.GridConstraints(0, 4, 1, 1, 8, 1, 7, 0, null, null, null));
        final JButton _9;
        _9 = new JButton();
        searchButton = _9;
        _9.setToolTipText("search not implemented yet! :(");
        _9.setText("Search");
        _3.add(_9, new com.intellij.uiDesigner.core.GridConstraints(0, 5, 1, 1, 4, 0, 3, 0, null, null, null));
        final JTabbedPane _10;
        _10 = new JTabbedPane();
        tabs = _10;
        _1.add(_10, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, 0, 3, 3, 7, null, new Dimension(200, 200), null));
    }

}
