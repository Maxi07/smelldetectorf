package lrg.insider.gui.ui.filter;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import lrg.common.abstractions.entities.EntityType;
import lrg.common.abstractions.managers.EntityTypeManager;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.filters.composed.OrComposedFilteringRule;
import lrg.insider.gui.ui.views.ViewUI;

public class CustomFilterUI extends MouseAdapter implements ActionListener, TreeSelectionListener {
    public CustomFilterUI(ViewUI aViewUI) {
        andButton.addActionListener(this);
        orButton.addActionListener(this);
        notButton.addActionListener(this);

        filterTree.addTreeSelectionListener(this);
        filterTree.addMouseListener(this);
        myViewUI = aViewUI;

        // TODO: make apply button enabled only if the filtering is completed and valid (ie no wannabes instances are present in it)
        applyButton.addActionListener(this);
        highlightButton.addActionListener(this);
        unHighlightButton.addActionListener(this);
        this.validPropertiesTabs.setTabPlacement(JTabbedPane.TOP);

        populateValidFilterBuilders();
        splitPane.setDividerLocation(400);

        DefaultMutableTreeNode filterTreeRoot;
        filterTreeRoot = new DefaultMutableTreeNode(WannabeFilteringRule.instance());
        filterTreeModel = new DefaultTreeModel(filterTreeRoot);
        filterTree.setModel(filterTreeModel);
        filterTree.setSelectionPath(new TreePath(filterTreeRoot.getPath()));

        selectedNode = filterTreeRoot;

        popup = new JPopupMenu();

        deleteFilteringRule = new JMenuItem("Delete");
        deleteFilteringRule.addActionListener(this);
        popup.add(deleteFilteringRule);
    }

    public Container getTopComponent() {
        return topComponent;
    }

    private void addValidFilteringRuleTabs(EntityType entityType) {
        Iterator it = EntityTypeManager.getAllSubtypesForName(entityType.getName()).iterator();
        while (it.hasNext()) {
            EntityType currentEntityType = (EntityType) it.next();

            buildersMap.put(currentEntityType.getName(), new FilteringRuleBuilderUI(currentEntityType, this));
            validPropertiesTabs.add(((FilteringRuleBuilderUI) (buildersMap.get(currentEntityType.getName()))).getTopComponent());
            addValidFilteringRuleTabs(currentEntityType);
        }
    }

    private void populateValidFilterBuilders() {
        validPropertiesTabs.removeAll();
        EntityType eType = myViewUI.getGroupEntity().getEntityTypeOfElements();
        buildersMap = new HashMap();
        buildersMap.put(eType.getName(), new FilteringRuleBuilderUI(eType, this));
        validPropertiesTabs.add(((FilteringRuleBuilderUI) (buildersMap.get(eType.getName()))).getTopComponent());
        addValidFilteringRuleTabs(eType);

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == applyButton) {
            myViewUI.unHighlight();
            FilteringRule aRule = (FilteringRule) ((DefaultMutableTreeNode) filterTreeModel.getRoot()).getUserObject();
            if (EntityTypeManager.getEntityTypeForName(aRule.getDescriptorObject().getEntityTypeName()).findFilteringRule(aRule.getDescriptorObject().getName()) == null) {
                String userSelection = JOptionPane.showInputDialog(getTopComponent(), "Do you want to save this filter ?", aRule.getDescriptorObject().getName());
                if (userSelection != null) {
                    aRule.getDescriptorObject().setName(userSelection);
                    EntityTypeManager.attach(aRule);
                    // CacheManager.writeDynamicETStream(aRule);
                    // this.splitPane.getParent().disable();
                }
            }
            // this is called a HACK !!!
            JDialog x = (JDialog) topComponent.getParent().getParent().getParent();
            x.dispose();
            myViewUI.applyFilter(aRule);
            populateValidFilterBuilders();
        }
        if (e.getSource() == highlightButton) {
            FilteringRule aRule = (FilteringRule) ((DefaultMutableTreeNode) filterTreeModel.getRoot()).getUserObject();
            myViewUI.highlightFilter(aRule);
        }
        if (e.getSource() == unHighlightButton) {
            myViewUI.unHighlight();
        }
        if (e.getSource() == deleteFilteringRule) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) filterTree.getLastSelectedPathComponent();
            if (node == null || node == filterTreeModel.getRoot())
                return;

            filterTree.setSelectionPath(new TreePath(((DefaultMutableTreeNode) (filterTreeModel.getRoot())).getPath()));
            DefaultMutableTreeNode parent = (DefaultMutableTreeNode) node.getParent();
            filterTreeModel.removeNodeFromParent(node);
            parent.add(new DefaultMutableTreeNode(WannabeFilteringRule.instance()));
            maybeUpdateParentOf(parent);
        }

        if (e.getSource() == orButton) {
            DefaultMutableTreeNode parentOfSelectedNode = (DefaultMutableTreeNode) selectedNode.getParent();
            DefaultMutableTreeNode orNode = null;
            if (selectedNode.getChildCount() == 2) {
                FilteringRule fr0 = (FilteringRule) ((DefaultMutableTreeNode) selectedNode.getChildAt(0)).getUserObject();
                FilteringRule fr1 = (FilteringRule) ((DefaultMutableTreeNode) selectedNode.getChildAt(1)).getUserObject();
                FilteringRule orRule = new OrComposedFilteringRule(fr0, fr1);
                orNode = new DefaultMutableTreeNode(orRule);
                orNode.add((DefaultMutableTreeNode) selectedNode.getChildAt(0));
                orNode.add((DefaultMutableTreeNode) selectedNode.getChildAt(0));
                selectedNode.removeFromParent();

            } else {
                FilteringRule orRule = new OrComposedFilteringRule((FilteringRule) selectedNode.getUserObject(), WannabeFilteringRule.instance());
                orNode = new DefaultMutableTreeNode(orRule);
                orNode.add(selectedNode);
                orNode.add(new DefaultMutableTreeNode(WannabeFilteringRule.instance()));
            }
            if (parentOfSelectedNode == null)
                filterTreeModel.setRoot(orNode);
            else
                parentOfSelectedNode.add(orNode);
        }

        if (e.getSource() == andButton) {
            DefaultMutableTreeNode parentOfSelectedNode = (DefaultMutableTreeNode) selectedNode.getParent();
            DefaultMutableTreeNode andNode = null;
            if (selectedNode.getChildCount() == 2) {
                FilteringRule fr0 = (FilteringRule) ((DefaultMutableTreeNode) selectedNode.getChildAt(0)).getUserObject();
                FilteringRule fr1 = (FilteringRule) ((DefaultMutableTreeNode) selectedNode.getChildAt(1)).getUserObject();
                FilteringRule andRule = new AndComposedFilteringRule(fr0, fr1);
                andNode = new DefaultMutableTreeNode(andRule);
                andNode.add((DefaultMutableTreeNode) selectedNode.getChildAt(0));
                andNode.add((DefaultMutableTreeNode) selectedNode.getChildAt(0));
                selectedNode.removeFromParent();
            } else {
                FilteringRule andRule = new AndComposedFilteringRule((FilteringRule) selectedNode.getUserObject(), WannabeFilteringRule.instance());
                andNode = new DefaultMutableTreeNode(andRule);
                andNode.add(selectedNode);
                andNode.add(new DefaultMutableTreeNode(WannabeFilteringRule.instance()));
            }
            if (parentOfSelectedNode == null)
                filterTreeModel.setRoot(andNode);
            else
                parentOfSelectedNode.add(andNode);
        }

        if (e.getSource() == notButton) {
            DefaultMutableTreeNode parentOfSelectedNode = (DefaultMutableTreeNode) selectedNode.getParent();
            FilteringRule notRule = new NotComposedFilteringRule((FilteringRule) selectedNode.getUserObject());
            DefaultMutableTreeNode notNode = new DefaultMutableTreeNode(notRule);
            notNode.add(selectedNode);
            if (parentOfSelectedNode == null)
                filterTreeModel.setRoot(notNode);
            else
                parentOfSelectedNode.add(notNode);
        }

        filterTreeModel.reload();
        filterTree.setSelectionPath(new TreePath(selectedNode.getPath()));
        filterTree.expandPath(new TreePath(selectedNode.getPath()));

    }

    public void setFilter(FilteringRule aRule) {
        selectedNode.removeAllChildren();
        selectedNode.setUserObject(aRule);
        maybeUpdateParentOf(selectedNode);
        filterTreeModel.reload();
        filterTree.setSelectionPath(new TreePath(selectedNode.getPath()));
    }

    private void maybeUpdateParentOf(DefaultMutableTreeNode node) {
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) node.getParent();

        if (parent != null) {
            if (parent.getChildCount() == 2) {
                FilteringRule fr0 = (FilteringRule) ((DefaultMutableTreeNode) parent.getChildAt(0)).getUserObject();
                FilteringRule fr1 = (FilteringRule) ((DefaultMutableTreeNode) parent.getChildAt(1)).getUserObject();
                if (parent.getUserObject() instanceof AndComposedFilteringRule)
                    parent.setUserObject(new AndComposedFilteringRule(fr0, fr1));
                if (parent.getUserObject() instanceof OrComposedFilteringRule)
                    parent.setUserObject(new OrComposedFilteringRule(fr0, fr1));
            }
            if (parent.getChildCount() == 1) {
                FilteringRule fr = (FilteringRule) ((DefaultMutableTreeNode) parent.getChildAt(0)).getUserObject();
                if (parent.getUserObject() instanceof NotComposedFilteringRule)
                    parent.setUserObject(new NotComposedFilteringRule(fr));
            }

            maybeUpdateParentOf(parent);
        }
    }

    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) filterTree.getLastSelectedPathComponent();

        if (node == null)
            return;

        selectedNode = node;

        FilteringRule selectedRule = (FilteringRule) node.getUserObject();
        if (selectedRule instanceof WannabeFilteringRule) return;
        String title = selectedRule.getDescriptorObject().getEntityTypeName();
        FilteringRuleBuilderUI x = (FilteringRuleBuilderUI) buildersMap.get(title);
        if (x == null) return;
        validPropertiesTabs.setSelectedComponent(x.getTopComponent());
    }

    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) filterTree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent();
            if (node == filterTreeModel.getRoot()) return;
            popup.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    private JTabbedPane validPropertiesTabs;
    private HashMap buildersMap;

    private JPanel topComponent;
    private JButton andButton;
    private JButton notButton;
    private JButton orButton;

    private JButton applyButton;
    private JButton highlightButton;
    private JButton unHighlightButton;

    private JSplitPane splitPane;

    private JTree filterTree;

    private DefaultTreeModel filterTreeModel;
    private ViewUI myViewUI;


    private DefaultMutableTreeNode selectedNode;

    private JPopupMenu popup;
    private JMenuItem deleteFilteringRule;

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     */
    private void $$$setupUI$$$() {
        final JPanel _1;
        _1 = new JPanel();
        topComponent = _1;
        _1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        final JSplitPane _2;
        _2 = new JSplitPane();
        splitPane = _2;
        _1.add(_2, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 3, 3, 3, null, new Dimension(200, 200), null));
        final JTabbedPane _3;
        _3 = new JTabbedPane();
        validPropertiesTabs = _3;
        _2.setRightComponent(_3);
        final JPanel _4;
        _4 = new JPanel();
        _4.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        _2.setLeftComponent(_4);
        final JScrollPane _5;
        _5 = new JScrollPane();
        _4.add(_5, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 3, 7, 3, null, null, null));
        final JTree _6;
        _6 = new JTree();
        filterTree = _6;
        _6.setShowsRootHandles(true);
        _6.setRootVisible(true);
        _5.setViewportView(_6);
        final JPanel _7;
        _7 = new JPanel();
        _7.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        _4.add(_7, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, 0, 1, 3, 1, null, null, null));
        final JButton _8;
        _8 = new JButton();
        andButton = _8;
        _8.setText("And");
        _7.add(_8, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        final JButton _9;
        _9 = new JButton();
        orButton = _9;
        _9.setText("Or");
        _7.add(_9, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        final JButton _10;
        _10 = new JButton();
        notButton = _10;
        _10.setText("Not");
        _7.add(_10, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, 0, 1, 3, 0, null, null, null));
        final JButton _11;
        _11 = new JButton();
        applyButton = _11;
        _11.setText("Apply");
        _1.add(_11, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        final JPanel _12;
        _12 = new JPanel();
        _12.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        _1.add(_12, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, 0, 3, 3, 1, null, null, null));
        final JButton _13;
        _13 = new JButton();
        unHighlightButton = _13;
        _13.setText("Unhighlight");
        _12.add(_13, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        final JButton _14;
        _14 = new JButton();
        highlightButton = _14;
        _14.setText("Highlight");
        _12.add(_14, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
    }

}
