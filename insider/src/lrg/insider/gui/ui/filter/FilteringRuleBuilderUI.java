package lrg.insider.gui.ui.filter;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import lrg.common.abstractions.entities.EntityType;
import lrg.common.abstractions.managers.EntityTypeManager;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.operators.FilteringOperatorWithThresholds;

public class FilteringRuleBuilderUI implements ListSelectionListener, ActionListener {


    public FilteringRuleBuilderUI(EntityType entityType, CustomFilterUI aCustomFilterUI) {
        myCustomFilterUI = aCustomFilterUI;

        propertyNameListModel = new DefaultListModel();
        propertyNameList.setModel(propertyNameListModel);
        propertyNameList.addListSelectionListener(this);
        operatorNameListModel = new DefaultListModel();
        operatorNameList.setModel(operatorNameListModel);
        operatorNameList.addListSelectionListener(this);

        deleteButton.addActionListener(this);
        deleteButton.setEnabled(false);

        topComponent.setName(entityType.getName());
        Iterator propertyIterator = entityType.nameAllPropertyComputers().iterator();
        while (propertyIterator.hasNext()) {
            String currentPCName = (String) propertyIterator.next();
            propertyNameListModel.addElement(currentPCName);
        }

        Iterator filterIterator = entityType.nameAllFilteringRules().iterator();
        while (filterIterator.hasNext()) {
            String currentFilterName = (String) filterIterator.next();
            propertyNameListModel.addElement(currentFilterName);
        }

        myEntityType = entityType;
    }

    public Container getTopComponent() {
        return topComponent;
    }

    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == propertyNameList) {
            if (e.getValueIsAdjusting() == false) {
                if (propertyNameList.getSelectedIndex() != -1) {
                    // btw, PC stands for PropertyComputer
                    selectedPCName = (String) propertyNameList.getSelectedValue();
                    if (myEntityType.findPropertyComputer(selectedPCName) != null) {
                        String entityTypeNameOfResultEntity = myEntityType.findPropertyComputer(selectedPCName).getResultEntityTypeName();
                        EntityType eType = EntityTypeManager.getEntityTypeForName(entityTypeNameOfResultEntity);
                        Iterator it = eType.nameAllFilteringOperators().iterator();
                        operatorNameListModel.clear();
                        while (it.hasNext())
                            operatorNameListModel.addElement(it.next().toString());

                        deleteButton.setEnabled(false);
                    } else {
                        if (myEntityType.findFilteringRule(selectedPCName) != null) {
                            FilteringRule aRule = (FilteringRule) myEntityType.findFilteringRule(selectedPCName);
                            operatorNameListModel.clear();
                            myCustomFilterUI.setFilter(aRule);
                            deleteButton.setEnabled(true);
                        }
                    }
                }
            }
        }
        if (e.getSource() == operatorNameList) {
            if (e.getValueIsAdjusting() == false) {
                if (operatorNameList.getSelectedIndex() != -1) {
                    FilteringRule aRule;
                    if (myEntityType.findPropertyComputer(selectedPCName) != null) {
                        String entityTypeNameOfResultEntity = myEntityType.findPropertyComputer(selectedPCName).getResultEntityTypeName();
                        String selectedOperatorName = (String) operatorNameList.getSelectedValue();
                        if (EntityTypeManager.getEntityTypeForName(entityTypeNameOfResultEntity).findFilteringOperator(selectedOperatorName) instanceof FilteringOperatorWithThresholds) {
                            aRule = new FilteringRule(selectedPCName, selectedOperatorName, myEntityType.getName(), getThresholdFromUser());
                        } else {
                            aRule = new FilteringRule(selectedPCName, selectedOperatorName, myEntityType.getName());
                        }
                    } else {
                        aRule = (FilteringRule) myEntityType.findFilteringRule(selectedPCName);
                    }
                    myCustomFilterUI.setFilter(aRule);
                }
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == deleteButton) {
            FilteringRule aRule = (FilteringRule) myEntityType.findFilteringRule(selectedPCName);
            propertyNameListModel.removeElement(selectedPCName);
            EntityTypeManager.unAttach(aRule);
        }
    }

    private Object getThresholdFromUser() {
        boolean userInputOK = false;
        Double threshold = new Double(0.0);
        while (!userInputOK) {
            String userInput = JOptionPane.showInputDialog(getTopComponent(), "What's the threshold ?");
            if (userInput == null)      // null means user pressed Cancel
            {
                threshold = new Double(0.0);
                userInputOK = true;
            } else {
                try {
                    threshold = new Double(userInput);
                    userInputOK = true;
                } catch (NumberFormatException nfe) {
                    // userInputOK = false;
                    return userInput;
                }
            }
        }
        return threshold;
    }

    private CustomFilterUI myCustomFilterUI;

    private JPanel topComponent;
    private JList propertyNameList;
    private DefaultListModel propertyNameListModel;

    private JList operatorNameList;
    private DefaultListModel operatorNameListModel;
    private EntityType myEntityType;
    private String selectedPCName;

    private JButton deleteButton;

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     */
    private void $$$setupUI$$$() {
        final JPanel _1;
        _1 = new JPanel();
        topComponent = _1;
        _1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
        final JTextField _2;
        _2 = new JTextField();
        _1.add(_2, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, 8, 1, 6, 0, null, new Dimension(150, -1), null));
        final JScrollPane _3;
        _3 = new JScrollPane();
        _1.add(_3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 3, 7, 7, null, null, null));
        _3.setBorder(BorderFactory.createTitledBorder("Available Properties"));
        final JList _4;
        _4 = new JList();
        propertyNameList = _4;
        _4.setSelectionMode(0);
        _3.setViewportView(_4);
        final JScrollPane _5;
        _5 = new JScrollPane();
        _1.add(_5, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, 0, 3, 7, 7, null, null, null));
        _5.setBorder(BorderFactory.createTitledBorder("Available Operators"));
        final JList _6;
        _6 = new JList();
        operatorNameList = _6;
        _6.setSelectionMode(0);
        _5.setViewportView(_6);
        final JButton _7;
        _7 = new JButton();
        deleteButton = _7;
        _7.setText("Delete");
        _1.add(_7, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, 0, 1, 3, 0, null, null, null));
    }

}
