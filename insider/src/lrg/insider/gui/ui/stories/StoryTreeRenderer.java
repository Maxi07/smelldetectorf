package lrg.insider.gui.ui.stories;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * Created by IntelliJ IDEA.
 * User: Vio
 * Date: Apr 8, 2005
 * Time: 4:28:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class StoryTreeRenderer extends DefaultTreeCellRenderer {
    public Component getTreeCellRendererComponent(
                        JTree tree,
                        Object value,
                        boolean sel,
                        boolean expanded,
                        boolean leaf,
                        int row,
                        boolean hasFocus) {
        String nodeName;
        int pos;

        super.getTreeCellRendererComponent(
                        tree, value, sel,
                        expanded, leaf, row,
                        hasFocus);

        nodeName = getText();

        if (nodeName != null) {
            setToolTipText(nodeName);

            if ((pos = nodeName.indexOf(" filter on (")) != -1) {
                setText(nodeName.substring(0, pos + 7));
            }
        }

        return this;
    }
}
