package lrg.insider.gui.ui.stories;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import lrg.common.abstractions.entities.EntityType;
import lrg.common.abstractions.managers.EntityTypeManager;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.gui.InsiderGUIMain;
import lrg.insider.gui.ui.views.ViewUI;

/**
 * Created by IntelliJ IDEA.
 * User: cristic
 * Date: May 4, 2004
 * Time: 2:37:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class SelectColumnsUI extends JDialog implements ActionListener, ListSelectionListener, KeyListener {

    public SelectColumnsUI(ViewUI aViewUI) {
        super(InsiderGUIMain.getFrame(), true);        // true means: create a modal dialogbox
        myViewUI = aViewUI;


        this.setContentPane(topComponent);
//        this.setLocationRelativeTo(InsiderGUIMain.getFrame());
        okButton.addActionListener(this);
        searchEditField.addKeyListener(this);

        availableDirectModel = new DefaultListModel();
        currentDirectModel = new DefaultListModel();

        currentAggregatePropertiesModel = new DefaultListModel();
        aggregatesModel = new DefaultListModel();
        availableAggregatePropertiesModel = new DefaultListModel();

        currentAggregatePropertiesList.setModel(currentAggregatePropertiesModel);
        aggregatesList.setModel(aggregatesModel);
        availableAggregatePropertiesList.setModel(availableAggregatePropertiesModel);
        availableDirectList.setModel(availableDirectModel);
        currentDirectList.setModel(currentDirectModel);

        addDirectButton.addActionListener(this);
        removeDirectButton.addActionListener(this);
        addAggregateButton.addActionListener(this);
        removeAggregateButton.addActionListener(this);

        availableAggregatePropertiesList.addListSelectionListener(this);

        addDirectPropertyComputers("");

        pack();
        this.setLocationRelativeTo(null);

    }

    private boolean nameIsValid(String name, String info, String filter) {
        return (name.toLowerCase().indexOf(filter.toLowerCase()) != -1) || (info.toLowerCase().indexOf(filter.toLowerCase()) != -1);
    }

    private void addDirectPropertyComputers(String filter) {
        currentDirectModel.clear();
        availableDirectModel.clear();
        ArrayList availableProperties = myViewUI.getGroupEntity().getEntityTypeOfElements().nameAllPropertyComputers();

        Iterator availablePropertiesIterator = availableProperties.iterator();
        while (availablePropertiesIterator.hasNext()) {
            PropertyComputer aPC = myViewUI.getGroupEntity().getEntityTypeOfElements().findPropertyComputer(availablePropertiesIterator.next().toString());
            String propertyName = aPC.getDescriptorObject().getName();
            String propertyInfo = aPC.getDescriptorObject().getInfo();
            if (nameIsValid(propertyName, propertyInfo, filter)) {
                if (myViewUI.hasPropertyCalled(propertyName)) {
                    currentDirectModel.addElement(propertyName);
                } else {
                    availableDirectModel.addElement(propertyName);
                }
            }
        }

        availableAggregatePropertiesModel.clear();
        addPropertyComputersFromSubTypesOf(myViewUI.getGroupEntity().getEntityTypeOfElements(), filter);
    }

    private void addPropertyComputersFromSubTypesOf(EntityType entityType, String filter) {
        Iterator subTypesIt = EntityTypeManager.getAllSubtypesForName(entityType.getName()).iterator();
        while (subTypesIt.hasNext()) {
            EntityType subType = (EntityType) subTypesIt.next();
            Iterator subTypePropertyIterator = subType.nameAllPropertyComputers().iterator();

            while (subTypePropertyIterator.hasNext()) {
                PropertyComputer aPC = subType.findPropertyComputer(subTypePropertyIterator.next().toString());
                String propertyName = subType.getName() + "_" + aPC.getDescriptorObject().getName();
                String propertyInfo = aPC.getDescriptorObject().getInfo();

                if (nameIsValid(propertyName, propertyInfo, filter))
                    availableAggregatePropertiesModel.addElement(propertyName);
            }
            addPropertyComputersFromSubTypesOf(subType, filter);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == okButton) {
            dispose();
            return;
        }

        if (e.getSource() == addDirectButton) {
            Object selectedPropertyNames[] = availableDirectList.getSelectedValues();
            for (int i = 0; i < selectedPropertyNames.length; i++) {
                myViewUI.addColumnForPropertyCalled((String) selectedPropertyNames[i]);
                availableDirectModel.removeElement(selectedPropertyNames[i]);
                currentDirectModel.addElement(selectedPropertyNames[i]);
            }
            return;
        }

        if (e.getSource() == removeDirectButton) {
            Object selectedPropertyNames[] = currentDirectList.getSelectedValues();
            for (int i = 0; i < selectedPropertyNames.length; i++) {
                myViewUI.removeColumnForPropertyCalled((String) selectedPropertyNames[i]);
                currentDirectModel.removeElement(selectedPropertyNames[i]);
                availableDirectModel.addElement(selectedPropertyNames[i]);
            }
            return;
        }

        if (e.getSource() == addAggregateButton) {
            if (availableAggregatePropertiesList.getSelectedIndex() != -1) {
                if (aggregatesList.getSelectedIndex() != -1) {
                    String selectedValue = ((String) availableAggregatePropertiesList.getSelectedValue());
                    String propertyComputerName = selectedValue.substring(selectedValue.indexOf("_") + 1);
                    String groupName = selectedValue.substring(0,selectedValue.indexOf("_"));
                    String aggregatorName = (String) aggregatesList.getSelectedValue();
                    String aggregatedPropertyName = groupName + "." + aggregatorName + "_" + propertyComputerName;
                    //String aggregatedPropertyName = aggregatorName + "_" + propertyComputerName;
                    currentAggregatePropertiesModel.addElement(aggregatedPropertyName);
                    myViewUI.addColumnForPropertyCalled(aggregatedPropertyName);

                } else
                    JOptionPane.showMessageDialog(topComponent, "No AggregationOperator selected.");
            } else
                JOptionPane.showMessageDialog(topComponent, "No subtype PropertyComputer selected.");
            return;
        }

        if (e.getSource() == removeAggregateButton) {
            Object selectedPropertyNames[] = currentAggregatePropertiesList.getSelectedValues();
            for (int i = 0; i < selectedPropertyNames.length; i++) {
                myViewUI.removeColumnForPropertyCalled((String) selectedPropertyNames[i]);
                currentAggregatePropertiesModel.removeElement(selectedPropertyNames[i]);
            }
            return;

        }
    }

    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == availableAggregatePropertiesList) {
            if (e.getValueIsAdjusting() == false) {
                if (availableAggregatePropertiesList.getSelectedIndex() != -1) {
                    String selectedValue = (String) availableAggregatePropertiesList.getSelectedValue();
                    String subEntityTypeName = selectedValue.substring(0, selectedValue.indexOf("_"));
                    String propertyComputerName = selectedValue.substring(selectedValue.indexOf("_") + 1);
                    aggregatesModel.clear();
                    EntityTypeManager.getEntityTypeForName(subEntityTypeName).findPropertyComputer(propertyComputerName).getResultEntityTypeName();
                    Iterator it = EntityTypeManager.getAllFiltersForName(EntityTypeManager.getEntityTypeForName(subEntityTypeName).findPropertyComputer(propertyComputerName).getResultEntityTypeName()).iterator();
                    while (it.hasNext()) {
                        aggregatesModel.addElement(it.next().toString());
                    }
                }
            }
        }
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.

        //if (java.lang.Character.isLetterOrDigit(e.getKeyChar()))
        {
            addDirectPropertyComputers(searchEditField.getText());
            //JOptionPane.showMessageDialog(topComponent, searchEditField.getText());
        }

    }

    public void keyTyped(KeyEvent e) {

        //To change body of implemented methods use File | Settings | File Templates.
    }

    private ViewUI myViewUI;
    private JPanel topComponent;
    private JButton okButton;

    private JButton addDirectButton;
    private JButton removeDirectButton;

    private JList availableDirectList;
    private DefaultListModel availableDirectModel;

    private JList currentDirectList;
    private DefaultListModel currentDirectModel;

    private JList currentAggregatePropertiesList;
    private DefaultListModel currentAggregatePropertiesModel;

    private JButton removeAggregateButton;
    private JButton addAggregateButton;

    private JList aggregatesList;
    private DefaultListModel aggregatesModel;

    private JList availableAggregatePropertiesList;
    private DefaultListModel availableAggregatePropertiesModel;

    // TODO: implement search feature
    private JTextField searchEditField;

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     */
    private void $$$setupUI$$$() {
        final JPanel _1;
        _1 = new JPanel();
        topComponent = _1;
        _1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 1, new java.awt.Insets(0, 0, 0, 0), -1, -1));
        final JPanel _2;
        _2 = new JPanel();
        _2.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 4, new java.awt.Insets(0, 0, 0, 0), -1, -1));
        _1.add(_2, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, 0, 3, 0, 0, null, null, null));
        _2.setBorder(BorderFactory.createTitledBorder("Aggregate Properties"));
        final JScrollPane _3;
        _3 = new JScrollPane();
        _2.add(_3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 3, 6, 0, null, null, null));
        final JList _4;
        _4 = new JList();
        availableAggregatePropertiesList = _4;
        _4.setSelectionMode(0);
        _3.setViewportView(_4);
        final JScrollPane _5;
        _5 = new JScrollPane();
        _2.add(_5, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, 0, 3, 6, 0, null, null, null));
        final JList _6;
        _6 = new JList();
        aggregatesList = _6;
        _6.setSelectionMode(0);
        _5.setViewportView(_6);
        final JPanel _7;
        _7 = new JPanel();
        _7.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new java.awt.Insets(0, 0, 0, 0), -1, -1));
        _2.add(_7, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, 0, 0, 0, 0, null, null, null));
        final JButton _8;
        _8 = new JButton();
        addAggregateButton = _8;
        _8.setText(">>");
        _7.add(_8, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        final JButton _9;
        _9 = new JButton();
        removeAggregateButton = _9;
        _9.setText("<<");
        _7.add(_9, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        final JScrollPane _10;
        _10 = new JScrollPane();
        _2.add(_10, new com.intellij.uiDesigner.core.GridConstraints(0, 3, 1, 1, 0, 3, 6, 0, null, null, null));
        final JList _11;
        _11 = new JList();
        currentAggregatePropertiesList = _11;
        _10.setViewportView(_11);
        final JButton _12;
        _12 = new JButton();
        okButton = _12;
        _12.setText("OK");
        _1.add(_12, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, 0, 3, 3, 0, null, null, null));
        final JPanel _13;
        _13 = new JPanel();
        _13.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 3, new java.awt.Insets(0, 0, 0, 0), -1, -1));
        _1.add(_13, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 3, 0, 0, null, null, null));
        _13.setBorder(BorderFactory.createTitledBorder("Direct Properties"));
        final JScrollPane _14;
        _14 = new JScrollPane();
        _13.add(_14, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 3, 7, 7, null, null, null));
        final JList _15;
        _15 = new JList();
        availableDirectList = _15;
        _14.setViewportView(_15);
        final JPanel _16;
        _16 = new JPanel();
        _16.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new java.awt.Insets(0, 0, 0, 0), -1, -1));
        _13.add(_16, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, 0, 0, 0, 3, null, null, null));
        final JButton _17;
        _17 = new JButton();
        addDirectButton = _17;
        _17.setText(">>");
        _16.add(_17, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        final JButton _18;
        _18 = new JButton();
        removeDirectButton = _18;
        _18.setText("<<");
        _16.add(_18, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        final JScrollPane _19;
        _19 = new JScrollPane();
        _13.add(_19, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, 0, 3, 7, 7, null, null, null));
        final JList _20;
        _20 = new JList();
        currentDirectList = _20;
        _19.setViewportView(_20);
        final JPanel _21;
        _21 = new JPanel();
        _21.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 2, new java.awt.Insets(0, 0, 0, 0), -1, -1));
        _1.add(_21, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, 0, 3, 3, 3, null, null, null));
        final JLabel _22;
        _22 = new JLabel();
        _22.setText("Search:");
        _21.add(_22, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        final JTextField _23;
        _23 = new JTextField();
        searchEditField = _23;
        _23.setEnabled(true);
        _21.add(_23, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, 0, 3, 6, 0, null, new java.awt.Dimension(150, -1), null));
    }

}
