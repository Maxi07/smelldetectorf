package lrg.insider.gui.ui.codeviewer;


import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

import lrg.insider.gui.InsiderGUIMain;
import lrg.memoria.core.Method;
import de.java2html.Java2Html;
import de.java2html.options.Java2HtmlConversionOptions;
import de.java2html.options.JavaSourceStyleTable;

/**
 * Created by IntelliJ IDEA.
 * User: cristic
 * Date: Jun 21, 2004
 * Time: 10:56:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class CodeViewerUI extends JDialog implements MouseListener {
    public CodeViewerUI(Method aMethod) {
        super(InsiderGUIMain.getFrame(), aMethod.getProperty("Address").getValue().toString(), false);
        this.setLocationRelativeTo(InsiderGUIMain.getFrame());

        sourceEditorPane = new JEditorPane();
        sourceEditorPane.setEditable(false);
        sourceEditorPane.addMouseListener(this);
        sourceEditorPane.setContentType("text/html");

        JScrollPane scrollPane = new JScrollPane(sourceEditorPane);
        this.setContentPane(scrollPane);

        styles = new ArrayList(3);
        Java2HtmlConversionOptions x = Java2HtmlConversionOptions.getDefault();
        x.setStyleTable(JavaSourceStyleTable.getDefaultKawaStyleTable());
        styles.add(x);
        x = Java2HtmlConversionOptions.getDefault();
        x.setStyleTable(JavaSourceStyleTable.getDefaultEclipseStyleTable());
        styles.add(x);
        x = Java2HtmlConversionOptions.getDefault();
        x.setStyleTable(JavaSourceStyleTable.getDefaultMonochromeStyleTable());
        styles.add(x);

        stylesIt = styles.iterator();

        if ((aMethod.getBody() != null) && (aMethod.getBody().getSourceCode() != null))
            sourceCode = aMethod.getBody().getSourceCode().trim();
        else
            sourceCode = "(oops!) No body found.";

        syntaxHighlight(sourceCode);

    }

    public void setVisible(boolean b) {
        if (b==true) {
            int sx=this.getWidth(),sy=this.getHeight();
            if (sx>900) { sx=900; sy+=40; }
            if (sy>500) { sy=500; sx+=40; }
            this.setSize(sx,sy);
            sx/=2; sy/=2;
            this.setLocation((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2 - sx, (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2 - sy);
        }
        super.setVisible(b);
    }

    public void mouseReleased(MouseEvent e) {
        if (e.getSource() == sourceEditorPane) {
            if (e.getButton() == MouseEvent.BUTTON2)
                syntaxHighlight(sourceCode);
        }
    }

    private void syntaxHighlight(String sourceCode) {
        int caretPos = sourceEditorPane.getCaretPosition();
        //sourceEditorPane.setCaretPosition(0);
        if (!(stylesIt.hasNext())) stylesIt = styles.iterator();
        String htmlified = Java2Html.convertToHtml(sourceCode, (Java2HtmlConversionOptions) stylesIt.next());
        sourceEditorPane.setText(htmlified.replaceAll("/>", ">"));
        sourceEditorPane.setCaretPosition(caretPos);
    }

    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private JEditorPane sourceEditorPane;
    private ArrayList styles;
    private Iterator stylesIt;
    private String sourceCode;

}
