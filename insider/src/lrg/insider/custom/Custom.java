package lrg.insider.custom;

import java.util.ArrayList;

import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class Custom {

	private GroupEntity group;
	
	public Custom(GroupEntity group){
		this.group = group;
		
		FilteringRule aRule = (FilteringRule) this.group.getEntityTypeOfElements().findFilteringRule("Data Class");
        this.applyFilter(aRule);
	}
	
	public void applyFilter(FilteringRule aFilteringRule)
    {
		if (aFilteringRule != null){
			GroupEntity filteredGroupEntity = this.group.applyFilter(aFilteringRule);
			if (filteredGroupEntity.size() < 1) {
				System.out.println("There is no entity matching the filter criteria\n Filter was not applied");
			}
			else {
				ArrayList<lrg.memoria.core.Class> elements = filteredGroupEntity.getElements();
				
				String g = elements.get(0).getClass().toString(); 
				for (lrg.memoria.core.Class element : elements){
					String b = element.getClass().toString();
					String a = element.toString();
					System.out.println(element.toString());
				}
				System.out.println("LA AGREGAMOS");
			}
		}
    }
	
	
}
