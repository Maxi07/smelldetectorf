package graph;

public class MaxCyclesNumberException extends Exception {
	public MaxCyclesNumberException() {
		super("Max number of cycles exceeded");
	}
}
