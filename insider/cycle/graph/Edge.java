package graph;

public class Edge{

	private AbstractNode from, to;

	Edge(AbstractNode f, AbstractNode t){
		this.from = f;
		this.to = t;
	}

	public AbstractNode getTo() {
		return to;
	}

	public AbstractNode getFrom() {
		return from;
	}

}
