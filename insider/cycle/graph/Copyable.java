package graph;

public interface Copyable {

	public AdjacencyList copy();

}
