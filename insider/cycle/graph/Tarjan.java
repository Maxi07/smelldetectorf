package graph;

import java.util.ArrayList;

public class Tarjan {

   private int index = 0;
   private ArrayList<AbstractNode> stack = new ArrayList<AbstractNode>();
   private ArrayList<ArrayList<AbstractNode>> SCC = new ArrayList<ArrayList<AbstractNode>>();

   public ArrayList<ArrayList<AbstractNode>> tarjan(AbstractNode v, AdjacencyList list){
       v.index = index;
       v.lowlink = index;
       index++;
       stack.add(0, v);
       for(Edge e : list.getAdjacent(v)){
    	   AbstractNode n = e.getTo();
           if(n.index == -1){
               tarjan(n, list);
               v.lowlink = Math.min(v.lowlink, n.lowlink);
           }else if(stack.contains(n)){
               v.lowlink = Math.min(v.lowlink, n.index);
           }
       }
       if(v.lowlink == v.index){
    	   AbstractNode n;
           ArrayList<AbstractNode> component = new ArrayList<AbstractNode>();
           do{
               n = stack.remove(0);
               if (!component.contains(n))
            	   component.add(n);
           }
           while(n != v);
           SCC.add(component);
       }
       return SCC;
   }
}
