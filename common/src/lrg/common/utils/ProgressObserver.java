package lrg.common.utils;

public interface ProgressObserver {
    public void setMaxValue(int max);
    public void increment();
}
