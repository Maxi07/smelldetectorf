package lrg.common.abstractions.plugins.conformities.creation.parsing.reading;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileReader implements LineReader {
	private BufferedReader source;
	private boolean empty = false;
	private String info = null;

	public FileReader(String filename) throws FileNotFoundException {
		source = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
		
		try {
			info = source.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (info == null)
			empty = true;
	}

	@Override
	public String readLine() {
		String result = info;
		try {
			do {
				info = source.readLine();
			} while (info != null && info.length() == 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (info == null)
			empty = true;
		return result;
	}

	@Override
	public boolean isEmpty() {
		return empty;
	}

}