package lrg.common.abstractions.plugins.conformities.creation.wrappers.operators;

import lrg.common.abstractions.plugins.conformities.StackItem;
import lrg.common.abstractions.plugins.conformities.creation.parsing.ParsingItemWrapper;
import lrg.common.abstractions.plugins.conformities.operators.AddOperator;

public class AndOperator extends ComparableOperator implements ParsingItemWrapper {
	{
		precedence = 2;
	}
	
	public String toString() {
		return "&";
	}

	@Override
	public StackItem getItemInside() {
		return new AddOperator();
	}
}