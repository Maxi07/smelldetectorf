package lrg.common.abstractions.plugins.conformities.creation.parsing;

import lrg.common.abstractions.plugins.conformities.StackItem;

public interface ParsingItemWrapper {
	public StackItem getItemInside();
}