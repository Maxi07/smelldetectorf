package lrg.common.abstractions.plugins.conformities.creation;

import java.util.LinkedList;
import java.util.Stack;

import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.StackItem;
import lrg.common.abstractions.plugins.conformities.composed.ConformityRuleWrapper;
import lrg.common.abstractions.plugins.conformities.creation.parsing.ParsingItemWrapper;
import lrg.common.abstractions.plugins.conformities.creation.parsing.RuleParser;
import lrg.common.abstractions.plugins.conformities.creation.wrappers.Filter;
import lrg.common.abstractions.plugins.conformities.creation.wrappers.Rule;
import lrg.common.abstractions.plugins.conformities.weights.Weight;

public class ConformityRuleCreator {
	private RuleParser ruleParser = new RuleParser();
	public ConformityRule createConformityRule (String rawRule) {
		Stack<ParsingItemWrapper> stack = null;
		try {
			stack = ruleParser.startParse(rawRule);
		} catch (InvalidExpressionException e) {
			e.printStackTrace();
			return null;
		}
		if (stack == null)
			return null;
		LinkedList<StackItem> expression = new LinkedList<StackItem>();
		LinkedList<Filter> filters = new LinkedList<Filter>();
		Weight currentWeight = null;
		for (ParsingItemWrapper s : stack) {
			if (s instanceof Weight) {
				currentWeight = (Weight) s;
				continue;
			}
			if (s instanceof Rule) {
				ConformityRule rule = (ConformityRule) s.getItemInside();
				if (currentWeight == null)
					rule.setWeight(Weight.MEDIUM.value());
				else
					rule.setWeight(currentWeight.value());
				expression.add(rule);
				continue;
			}
			if (s instanceof Filter) {
				filters.add((Filter) s.getItemInside());
			}
			expression.add(s.getItemInside());
		}
		
		System.out.println("--------------------RULE:" + expression);
		
		if (filters.isEmpty())
			return new ConformityRuleWrapper(ruleParser.getRuleName(), ruleParser.getTargetEntityType(), expression);
		return new ConformityRuleWrapper(ruleParser.getRuleName(), ruleParser.getTargetEntityType(), expression, filters);
	}
}