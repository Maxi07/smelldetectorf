package lrg.common.abstractions.plugins.conformities.creation.parsing.reading;


public class MainTest {
	public static void main(String[] args) {
		try {
			LineReader r = new FileReader("input.txt");
			while (r.isEmpty() == false)
				System.out.println(r.readLine());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}