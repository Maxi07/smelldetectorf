package lrg.common.abstractions.plugins.conformities.weights;

import lrg.common.abstractions.plugins.conformities.StackItem;
import lrg.common.abstractions.plugins.conformities.creation.parsing.ParsingItemWrapper;

/**
 * Created by Horia Radu.
 * User: horia
 * Date: 20.04.2005
 * Time: 13:31:43
 * To change this template use File | Settings | File Templates.
 */
public enum Weight implements StackItem, ParsingItemWrapper {
    VERY_LOW (1),
    LOW (3),
    MEDIUM (5),
    HIGH (7),
    VERY_HIGH (9);
    
    private double value;
    private Weight(double value) {
    	this.value = value;
    }
    public double value() {
    	return value;
    }
	@Override
	public StackItem getItemInside() {
		return this;
	}
}

