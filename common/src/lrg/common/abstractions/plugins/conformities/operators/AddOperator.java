package lrg.common.abstractions.plugins.conformities.operators;

import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.AddComposedConformityRule;

public class AddOperator extends Operator {
	public Double apply(Double d1, Double d2) {
		return d1 + d2;
	}
	
	public String toString() {
		return "+";
	}

	@Override
	public ConformityRule apply(ConformityRule rule1, ConformityRule rule2) {
		return new AddComposedConformityRule(rule1, rule2);
	}
}