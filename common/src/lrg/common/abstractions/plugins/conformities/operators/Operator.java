package lrg.common.abstractions.plugins.conformities.operators;

import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.StackItem;

public abstract class Operator implements StackItem {
	public abstract ConformityRule apply(ConformityRule rule1, ConformityRule rule2);
}