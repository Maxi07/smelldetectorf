package lrg.metrics.attributes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;
import lrg.memoria.core.Access;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Number of Attribute Accesses.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOAA
 * <br>
 * <b>Description:</b> It is defined as the number of methods that access a given attribute.
 * <br>
 * Accesses of constructors and destructors are not counted.
 * <br>
 * The metric is defined only for private and protected attribures.
 * <br>
 * <b>Source:</b>
 */

public class NumberOfAttributeAccesses extends AttributeMeasure {

    public NumberOfAttributeAccesses() {
        m_name = "NumberOfAttributeAccesses";
        m_fullName = "Number of Attribute Accesses";
    }

    /**
     * It is defined as the number of methods that access a given attribute.
     * <br>
     * Accesses of constructors and destructors are not counted.
     * <br>
     * The metric is defined only for private and protected attribures.
     */

    public Result measure(lrg.memoria.core.Attribute att) {
        lrg.memoria.core.Class cls = (lrg.memoria.core.Class) att.getScope();
        int temp = 0;
        ArrayList accesses = att.getAccessList();
        lrg.memoria.core.Method crtmeth;
        HashSet methl = new HashSet();

        if (att.isPrivate() || att.isProtected()) {
            for (int i = 0; i < accesses.size(); i++) {
                try {
                    crtmeth = (lrg.memoria.core.Method) ((lrg.memoria.core.Access)
                              accesses.get(i)).getScope().getScope();
                } catch(ClassCastException e) {
                    continue;
                }
                if (!crtmeth.isConstructor() && (!methl.contains(crtmeth))) {
                    methl.add(crtmeth);
                    temp++;
                }
            }
        }

        return new NumericalResult(cls, temp);
    }

}
