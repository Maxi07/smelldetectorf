package lrg.metrics.attributes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;
import lrg.memoria.core.Access;

import java.util.ArrayList;

/**
 * <b>Name:</b> Attribute Access Ratio.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> AAR
 * <br>
 * <b>Description:</b> It is the relative number of methods
 * from which an attribute of a class is used. <br>
 * (total number of accesses from own methods) / (total number of methods).
 * <br>
 * <b>Source:</b>
 */

public class AttributeAccessRatio extends AttributeMeasure {

    public AttributeAccessRatio() {
        m_name = "AttributeAccessRatio";
        m_fullName = "Attribute Access Ratio";
    }

    /**
     * It is the relative number of methods
     * from which an attribute of a class is used. <br>
     * (total number of accesses from own methods) / (total number of methods)
     * <br> Accesses of constructors and destructors are not counted.
     * They are not counted as methods.
     * The metric is defined only for private and protected attribures.
     */

    public Result measure(lrg.memoria.core.Attribute att) {
        double method_number = 0, no_of_accessors = 0;
        double temp;

        lrg.memoria.core.Class cls = (lrg.memoria.core.Class) (att.getScope());

        if (!(att.isPrivate()) && !(att.isProtected()))
            temp = 0;
        else {
            ArrayList method_list = att.getAccessList();
            lrg.memoria.core.Method crt_method;
            Access crt_access;

            method_number = cls.getMethodList().size();

            for (int i = 0; i < method_list.size(); i++) {
                crt_access = (lrg.memoria.core.Access) method_list.get(i);
                try {
                    crt_method = (lrg.memoria.core.Method) crt_access.getScope().getScope();
                } catch(ClassCastException e) {
                    continue;
                }
                if (crt_method.getScope().equals(cls))
                    if (crt_method.isConstructor())
                        method_number--;
                    else
                        no_of_accessors++;
            }

            if (method_number != 0)
                temp = no_of_accessors / method_number;
            else if (no_of_accessors == 0)
                temp = 0;
            else
                temp = Integer.MAX_VALUE;   //if we have no methods, we return an Integer.MAX_VALUE
        }
        return new NumericalResult(cls, temp);
    }

}
