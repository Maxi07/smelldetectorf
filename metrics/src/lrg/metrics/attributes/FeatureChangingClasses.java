package lrg.metrics.attributes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Feature Changing Classes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> FCC
 * <br>
 * <b>Description:</b> Measures the dispersion of the changes, i.e. in how many classes are spread
 * the methods that are potentially affected by a change of the given feature.
 * <br>
 * A feature might be an attribute or a method.
 * A method depends on a feature if:
 * - it invokes that feature, or
 * - it redefines that feature (a method) from a superclass
 * Constructors and destructors are not considered as methods.
 * <br>
 * <b>Source:</b>
 */

public class FeatureChangingClasses extends AttributeMeasure {

    public FeatureChangingClasses() {
        m_name = "FeatureChangingClasses";
        m_fullName = "Feature Changing Classes";
    }

    /**
     * Measures the dispersion of the changes, i.e. in how many classes are spread
     * the methods that are potentially affected by a change of the given feature.
     * <br>
     * A feature might be an attribute or a method.
     * A method depends on a feature if:
     * - it invokes that feature, or
     * - it redefines that feature (a method) from a superclass
     * Constructors and destructors are not considered as methods.
     */

    public Result measure(lrg.memoria.core.Attribute att) {
        int count = 0, i, j;
        lrg.memoria.core.Class cls = (lrg.memoria.core.Class) att.getScope();
        ArrayList accesses, ml = new ArrayList();
        lrg.memoria.core.Class crtcls;
        lrg.memoria.core.Method crtmeth;
        HashSet cl = new HashSet();

        accesses = att.getAccessList();
        for (j = 0; j < accesses.size(); j++) {
            try {
                crtmeth = (lrg.memoria.core.Method)
                          ((lrg.memoria.core.Access) accesses.get(j)).getScope().getScope();
            } catch(ClassCastException e) {
                continue;
            }
            ml.add(crtmeth);
        }

        for (i = 0; i < ml.size(); i++) {
            if (!((lrg.memoria.core.Method) ml.get(i)).isConstructor()) {
                try {
                    crtcls = (lrg.memoria.core.Class) ((lrg.memoria.core.Method) ml.get(i)).getScope();
                } catch(ClassCastException e) {
                    continue;
                }
                if (!cl.contains(crtcls) && (!crtcls.isAbstract()) && (crtcls != cls)) {
                    cl.add(crtcls);
                    count++;
                }
            }
        }

        return new NumericalResult(cls, count);
    }
}
