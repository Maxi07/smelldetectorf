package lrg.metrics.attributes;

import lrg.metrics.CollectionResult;
import lrg.metrics.Result;
import lrg.metrics.classes.ClassMeasure;

import java.util.ArrayList;

public abstract class AttributeMeasure extends ClassMeasure {

    public AttributeMeasure() {
        m_kind = "attributes";
    }

    public abstract Result measure(lrg.memoria.core.Attribute att);

    /**
     * Used as a filter of attributes. (i.e. this offers the possibility select only
     * attributes we are interested in)
     */
    protected boolean filter(lrg.memoria.core.Attribute att, lrg.memoria.core.Class cls) {
        //by default we count only attributes that are implemented in this class
        return (att.getScope() == cls);
    }

    public Result measure(lrg.memoria.core.Class cls) {
        int i, size;
        CollectionResult cr = new CollectionResult();
        ArrayList al = cls.getAttributeList();
        lrg.memoria.core.Attribute act_attribute;
        size = al.size();
        for (i = 0; i < size; i++) {
            act_attribute = (lrg.memoria.core.Attribute) al.get(i);
            if (filter(act_attribute, cls))
                cr.add(measure(act_attribute));
        }
        return cr;
    }
}
