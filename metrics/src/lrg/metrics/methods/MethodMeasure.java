package lrg.metrics.methods;

import lrg.metrics.CollectionResult;
import lrg.metrics.Result;
import lrg.metrics.classes.ClassMeasure;

import java.util.ArrayList;

public abstract class MethodMeasure extends ClassMeasure {

    public MethodMeasure() {
        m_kind = "methods";
    }

    /**
     * Used as a filter of methods. (i.e. this offers the possibility select only
     * methods we are interested in)
     */
    protected boolean filter(lrg.memoria.core.Method m, lrg.memoria.core.Class cls) {
        //by default we consider only methods that are implemented in this class
        return (m.getScope() == cls);
    }

    public abstract Result measure(lrg.memoria.core.Method meth);

    public Result measure(lrg.memoria.core.Class cls) {
        int i, size;
        CollectionResult cr = new CollectionResult();
        ArrayList ml = cls.getMethodList();
        lrg.memoria.core.Method act_method;
        size = ml.size();
        for (i = 0; i < size; i++) {
            act_method = (lrg.memoria.core.Method) ml.get(i);
            if (filter(act_method, cls))
                cr.add(measure(act_method));
        }
        return cr;
    }
}
