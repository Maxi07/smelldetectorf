package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.memoria.core.Call;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Exterior Calls.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_calle
 * <br>
 * <b>Description:</b> Number of calls to functions defined outside this class.
 * <br>
 * <b>Source:</b> M. Lorenz, J. Kidd -- "Object-Oriented Software Metrics",
 * Prentice Hall, 1994.
 */
public class ExteriorCalls extends MethodMeasure {

    public ExteriorCalls() {
        m_name = "ExteriorCalls";
        m_fullName = "Number Of Exterior Calls";
    }

    /**
     * This metric returns the number of calls to outside defined methods.
     * <br>
     * Obs.: All call occurences are counted.
     */
    public Result measure(lrg.memoria.core.Method actMethod) {
        int count;
        String calledClassName, actClassName;
        lrg.memoria.core.Method calledMethod;
        lrg.memoria.core.FunctionBody mb = actMethod.getBody();
        ArrayList cl;
        count = 0;
        Call currentCall;
        if (mb != null) {
            cl = mb.getCallList();
            for (int i = 0; i < cl.size(); i++) {
                currentCall = (lrg.memoria.core.Call) cl.get(i);
                calledMethod = currentCall.getMethod();
                calledClassName = calledMethod.getScope().getFullName();
                actClassName = actMethod.getScope().getFullName();
                if (!calledClassName.equals(actClassName))
                    count += currentCall.getCount();
            }
        }
        return new NumericalResult(actMethod, count);
    }
}
