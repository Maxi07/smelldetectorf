package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

/**
 * <b>Name:</b> Number Of Parameters.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ic_param
 * <br>
 * <b>Description:</b> The number of a method's parameters.
 * <br>
 * <b>Source:</b> t?
 */
public class NumberOfParameters extends MethodMeasure {

    public NumberOfParameters() {
        m_name = "NumberOfParameters";
        m_fullName = "Number Of Parameters";
    }

    /**
     * Measures the number of a method's parameters.
     */
    public Result measure(lrg.memoria.core.Method m) {
        return new NumericalResult(m, m.getParameterList().size());
    }
}
