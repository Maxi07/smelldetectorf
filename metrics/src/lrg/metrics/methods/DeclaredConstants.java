package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Declared Constants.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_consts
 * <br>
 * <b>Description:</b> Measure the number of constants declared in a method.
 * <br>
 * <b>Source:</b> t?
 */
public class DeclaredConstants extends MethodMeasure {

    public DeclaredConstants() {
        m_name = "DeclaredConstants";
        m_fullName = "Number Of Declared Constants";
    }

    /**
     * Measure the number of constants declared in a method.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count, i, size;
        ArrayList local_vars;
        lrg.memoria.core.LocalVariable lv;
        count = 0;
        //count local variables which are constants
        lrg.memoria.core.FunctionBody mb = m.getBody();
        if (mb != null)
            if ((local_vars = mb.getLocalVarList()) != null) {
                size = local_vars.size();
                for (i = 0; i < size; i++) {
                    lv = (lrg.memoria.core.LocalVariable) local_vars.get(i);
                    if (lv.isFinal())
                        count++;
                }
            }
        return new NumericalResult(m, count);
    }
}

