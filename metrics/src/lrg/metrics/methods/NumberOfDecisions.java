package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

/**
 * <b>Name:</b> Number Of Decisions.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ct_decis
 * <br>
 * <b>Description:</b> Number of instructions in a function containing a conditional
 * expression.(i.e. if, for, while, do while, switch)
 * <br>
 * <b>Source:</b> t?
 */
public class NumberOfDecisions extends MethodMeasure {

    public NumberOfDecisions() {
        m_name = "NumberOfDecisions";
        m_fullName = "Number Of Decisions";
    }

    /**
     * Returns the number of instructions with contain a decision (i.e. if, for, while, do while, switch).
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count = 0;
        lrg.memoria.core.FunctionBody mb = m.getBody();
        if (mb != null)
            count = mb.getNumberOfDecisions();
        return new NumericalResult(m, count);
    }
}
