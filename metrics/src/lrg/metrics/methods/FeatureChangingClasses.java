package lrg.metrics.methods;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Feature Changing Classes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> FCC
 * <br>
 * <b>Description:</b> Measures the dispersion of the changes, i.e. in how many classes are spread
 * the methods that are potentially affected by a change of the given feature.
 * <br>
 * A feature might be an attribute or a method.
 * A method depends on a feature if:
 * - it invokes that feature, or
 * - it redefines that feature (a method) from a superclass
 * Constructors and destructors are not considered as methods.
 * <br>
 * <b>Source:</b>
 */

public class FeatureChangingClasses extends MethodMeasure {

    public FeatureChangingClasses() {
        m_name = "FeatureChangingClasses";
        m_fullName = "Feature Changing Classes";
    }

    /**
     * Measures the dispersion of the changes, i.e. in how many classes are spread
     * the methods that are potentially affected by a change of the given feature.
     * <br>
     * A feature might be an attribute or a method.
     * A method depends on a feature if:
     * - it invokes that feature, or
     * - it redefines that feature (a method) from a superclass
     * Constructors and destructors are not considered as methods.
     */
    public Result measure(lrg.memoria.core.Method rewmeth) {
        int count = 0, i, j, k, l;
        lrg.memoria.core.Class cls = (lrg.memoria.core.Class) rewmeth.getScope();
        boolean ok;
        ArrayList calls, ml = new ArrayList(), descs, descmeth;
        lrg.memoria.core.Class crtcls;
        lrg.memoria.core.Method crtmeth;
        HashSet cl = new HashSet();
        ArrayList crtpars, rewpars;

        calls = rewmeth.getCallList();
        for (j = 0; j < calls.size(); j++) {
            try {
                crtmeth = (lrg.memoria.core.Method)
                          ((lrg.memoria.core.Call) calls.get(j)).getScope().getScope();
            } catch(ClassCastException e) {
                continue;
            }
            ml.add(crtmeth);
        }
        descs = cls.getDescendants();
        for (j = 0; j < descs.size(); j++) {
            try {
                crtcls = (lrg.memoria.core.Class) descs.get(j);
            } catch (ClassCastException e) {
                continue;
            }
            descmeth = crtcls.getMethodList();
            for (k = 0; k < descmeth.size(); k++) {
                crtmeth = (lrg.memoria.core.Method) descmeth.get(k);
                crtpars = crtmeth.getParameterList();
                rewpars = rewmeth.getParameterList();
                if (crtpars.size() == rewpars.size())
                    ok = true;
                else
                    ok = false;
                for (l = 0; l < crtpars.size(); l++)
                    if (((lrg.memoria.core.Parameter) crtpars.get(l)).getType() !=
                        ((lrg.memoria.core.Parameter) rewpars.get(l)).getType())
                        ok = false;
                if ((crtmeth.getName() == rewmeth.getName()) && ok)
                    ml.add(crtmeth);
            }
        }

        for (i = 0; i < ml.size(); i++) {
            if (!((lrg.memoria.core.Method) ml.get(i)).isConstructor()) {
                try {
                    crtcls = (lrg.memoria.core.Class) ((lrg.memoria.core.Method) ml.get(i)).getScope();
                } catch(ClassCastException e) {
                    continue;
                }
                if (!cl.contains(crtcls) && (!crtcls.isAbstract()) && (crtcls != cls)) {
                    cl.add(crtcls);
                    count++;
                }
            }
        }

        return new NumericalResult(rewmeth, count);
    }
}
