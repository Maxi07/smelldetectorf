package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.memoria.core.DataAbstraction;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Other Class Type Local Variables.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_other_clas_var
 * <br>
 * <b>Description:</b> Measure the number of class type variables
 * that are local to a function and their type is different from the
 * current class.
 * <br>
 * <b>Source:</b> t?
 */
public class OtherClassTypeVariables extends MethodMeasure {

    public OtherClassTypeVariables() {
        m_name = "OtherClassTypeVariables";
        m_fullName = "Number Of Other Class Type Variables";
    }

    /**
     * Measure the number of class type variables that are local to
     * a function and their type is different from the current class.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int size, count, i;
        ArrayList local_vars;
        lrg.memoria.core.FunctionBody mb = m.getBody();
        lrg.memoria.core.Variable var;
        lrg.memoria.core.Type actType;
        lrg.memoria.core.DataAbstraction actScope = (DataAbstraction)m.getScope();
        count = 0;
        if (mb != null) {
            local_vars = mb.getLocalVarList();
            size = local_vars.size();
            for (i = 0; i < size; i++) {
                var = (lrg.memoria.core.Variable) local_vars.get(i);
                actType = var.getType();
                if ((actType instanceof lrg.memoria.core.Class) && actType != actScope)
                    count++;
            }
        }
        return new NumericalResult(m, count);
    }
}
