package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.memoria.core.Call;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Interior Calls.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_calli
 * <br>
 * <b>Description:</b> Number of calls to functions defined in the class.
 * <br>
 * Obs.: All call occurences are counted.
 * <br>
 * <b>Source:</b> t?
 */
public class InteriorCalls extends MethodMeasure {

    public InteriorCalls() {
        m_name = "InteriorCalls";
        m_fullName = "Number of Interior Calls";
    }

    /**
     * Number of calls to functions defined in the class.
     * <br>
     * Obs.: All call occurences are counted.
     */
    public Result measure(lrg.memoria.core.Method actMethod) {
        int count, size;
        String calledClassName, actClassName;
        lrg.memoria.core.Method calledMethod;
        lrg.memoria.core.FunctionBody mb = actMethod.getBody();
        ArrayList cl;
        Call currentCall;
        count = 0;
        if (mb != null) {
            cl = mb.getCallList();
            size = cl.size();
            for (int i = 0; i < size; i++) {
                currentCall = (lrg.memoria.core.Call) cl.get(i);
                calledMethod = currentCall.getMethod();
                calledClassName = calledMethod.getScope().getFullName();
                actClassName = actMethod.getScope().getFullName();
                if (calledClassName.equals(actClassName))
                    count += currentCall.getCount();
            }
        }
        return new NumericalResult(actMethod, count);
    }
}
