package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Class Type Parameters.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ic_parcl
 * <br>
 * <b>Description:</b> Number of class-type parameters of a function. If a function returns
 * a class-type value then the returned value is considered as a class-type parameter.
 * This metric shows a specific type of coupling between classes.
 * <br>
 * <b>Source:</b> t?
 */
public class ClassTypeParameters extends MethodMeasure {

    public ClassTypeParameters() {
        m_name = "ClassTypeParameters";
        m_fullName = "Number Of Class Type Parameters";
    }

    /**
     * Measures the number of a method's "class parameters". If the method returns
     * a class-type value then the returned value is considered as a class-type parameter.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count = 0, i;
        ArrayList pl = m.getParameterList();
        lrg.memoria.core.Parameter p;
        if (pl != null) {
            int size = pl.size();
            for (i = 0; i < size; i++) {
                p = (lrg.memoria.core.Parameter) pl.get(i);
                if (p.getType() instanceof lrg.memoria.core.Class)
                    count++;
            }
        }
        if (m.getReturnType() instanceof lrg.memoria.core.Class)
            count++;
        return new NumericalResult(m, count);
    }
}
