package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.memoria.core.DataAbstraction;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Number Of Distinct Calls To Functions Defined Outside The Class.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_callpe
 * <br>
 * <b>Description:</b> Number of "outside calls". An "outside call" is a call to
 * an "outside defined" function. A function is "defined outside" if its definition
 * doesn't belong to the same class as the function being curently analized.
 * <br>
 * Obs.: Different calls to the same function count for one call.
 * <br>
 * <b>Source:</b> t?
 */
public class ExteriorCallPaths extends MethodMeasure {

    public ExteriorCallPaths() {
        m_name = "ExteriorCallPaths";
        m_fullName = "Number Of Exterior Call Paths";
    }

    /**
     * This metric returns the number of "outside calls". An "outside call" is a call
     * to an "outside defined" function. A function is "defined outside" if its
     * definition doesn't belong to the same class as the function being curently analized.
     * <br>
     * Obs.: Different calls to the same function count for one call.
     */

    public Result measure(lrg.memoria.core.Method actMethod) {
        int count;
        lrg.memoria.core.DataAbstraction calledClass, actClass;
        lrg.memoria.core.Method calledMethod;
        lrg.memoria.core.FunctionBody mb = actMethod.getBody();
        ArrayList cl;
        HashSet hs = new HashSet();
        count = 0;
        if (mb != null) {
            cl = mb.getCallList();
            for (int i = 0; i < cl.size(); i++) {
                calledMethod = ((lrg.memoria.core.Call) cl.get(i)).getMethod();
                calledClass = (DataAbstraction)calledMethod.getScope();
                actClass = (DataAbstraction)actMethod.getScope();
                if (calledClass != actClass) {
                    if (!hs.contains(calledMethod)) {
                        hs.add(calledMethod);
                        count++;
                    }
                }
            }
        }
        return new NumericalResult(actMethod, count);
    }
}
