package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.memoria.core.Call;
import lrg.memoria.core.Method;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Static Calls.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_stat_call
 * <br>
 * <b>Description:</b> Number of calls to static methods in a method.
 * <br>
 * Obs.: All calls are counted.
 * <br>
 * <b>Source:</b> t?
 */
public class NumberOfStaticCalls extends MethodMeasure {

    public NumberOfStaticCalls() {
        m_name = "NumberOfStaticCalls";
        m_fullName = "Number Of Static Calls";
    }

    /**
     * Measures the number of calls to static member methods in a method.
     * <br>
     * Obs.: All calls are counted.
     */
    public Result measure(lrg.memoria.core.Method actMethod) {
        int count, i, size;
        lrg.memoria.core.FunctionBody mb = actMethod.getBody();
        ArrayList cl;
        Call currentCall;
        lrg.memoria.core.Function calledMethod;
        count = 0;
        if (mb != null) {
            cl = mb.getCallList();
            size = cl.size();
            for (i = 0; i < size; i++) {
                currentCall = (lrg.memoria.core.Call) cl.get(i);
                calledMethod = currentCall.getMethod();
                if (calledMethod instanceof Method && ((Method)calledMethod).isStatic())
                    count += currentCall.getCount();
            }
        }
        return new NumericalResult(actMethod, count);
    }
}

