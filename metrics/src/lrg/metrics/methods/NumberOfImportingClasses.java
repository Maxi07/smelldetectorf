package lrg.metrics.methods;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Number Of Importing Classes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NIC
 * <br>
 * <b>Description:</b> Counts the number of distinct external classes,
 * not related by inheritance relations, from which the method accesses data.
 * <br>
 * Only the user-defined classes are considered.
 * <br>
 * <b>Source:</b> t?
 */

public class NumberOfImportingClasses extends MethodMeasure {

    public NumberOfImportingClasses() {
        m_name = "NumberOfImportingClasses";
        m_fullName = "Number Of Importing Classes";
    }

    /**
     * Counts the number of distinct external classes,
     * not related by inheritance relations, from which the method accesses data.
     * <br>
     * Only the user-defined classes are considered.
     */

    public Result measure(lrg.memoria.core.Method m) {
        int count = 0;
        ArrayList uses = m.getBody().getAccessList();
        HashSet classl = new HashSet();
        lrg.memoria.core.Class crtcls;
        lrg.memoria.core.Attribute crtatt;

        for (int i = 0; i < uses.size(); i++) {
            try {
                crtatt = (lrg.memoria.core.Attribute)
                         ((lrg.memoria.core.Access) uses.get(i)).getVariable();
            } catch (ClassCastException e) {
                continue;
            }
            crtcls = (lrg.memoria.core.Class) crtatt.getScope();
            if ((crtcls.getStatute() != lrg.memoria.core.Statute.LIBRARY) &&
                (crtcls != (lrg.memoria.core.Class) m.getScope()) &&
                (!((lrg.memoria.core.Class) m.getScope()).
                    getAncestorsList().contains(crtcls)) &&
                (!classl.contains(crtcls))) {
                classl.add(crtcls);
                count++;
            }
        }
        return new NumericalResult(m, count);
    }
}
