package lrg.metrics.methods;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Feature Changing Methods.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> FCM
 * <br>
 * <b>Description:</b> Counts the number of methods from other classes that depend
 * on a given feature.
 * <br>
 * A feature might be an attribute or a method.
 * A method depends on a feature if:
 * - it invokes that feature, or
 * - it redefines that feature (a method) from a superclass
 * Constructors and destructors are not considered as methods.
 * <br>
 * <b>Source:</b>
 */

public class FeatureChangingMethods extends MethodMeasure {

    public FeatureChangingMethods() {
        m_name = "FeatureChangingMethods";
        m_fullName = "Feature Changing Methods";
    }

    /**
     * Counts the number of methods from other classes that depend
     * on a given feature.
     * <br>
     * A feature might be an attribute or a method.
     * A method depends on a feature if:
     * - it invokes that feature, or
     * - it redefines that feature (a method) from a superclass
     * Constructors and destructors are not considered as methods.
     */
    public Result measure(lrg.memoria.core.Method rewmeth) {
        int count = 0, i, j, k, l;
        boolean ok;
        ArrayList calls, ml = new ArrayList(), descs, descmeth;
        lrg.memoria.core.Class crtcls, cls;
        lrg.memoria.core.Method crtmeth;
        HashSet cl = new HashSet();
        ArrayList crtpars, rewpars;

        cls = (lrg.memoria.core.Class) rewmeth.getScope();

        calls = rewmeth.getCallList();
        for (j = 0; j < calls.size(); j++) {
            try {
                crtmeth = (lrg.memoria.core.Method)
                          ((lrg.memoria.core.Call) calls.get(j)).getScope().getScope();
            } catch(ClassCastException e) {
                continue;
            }
            ml.add(crtmeth);
        }
        descs = cls.getDescendants();
        for (j = 0; j < descs.size(); j++) {
            try {
                crtcls = (lrg.memoria.core.Class) descs.get(j);
            } catch(ClassCastException e) {
                continue;
            }
            descmeth = crtcls.getMethodList();
            for (k = 0; k < descmeth.size(); k++) {
                crtmeth = (lrg.memoria.core.Method) descmeth.get(k);
                crtpars = crtmeth.getParameterList();
                rewpars = rewmeth.getParameterList();
                if (crtpars.size() == rewpars.size())
                    ok = true;
                else
                    ok = false;
                for (l = 0; l < crtpars.size(); l++)
                    if (((lrg.memoria.core.Parameter) crtpars.get(l)).getType() !=
                       ((lrg.memoria.core.Parameter) rewpars.get(l)).getType())
                        ok = false;
                if ((crtmeth.getName() == rewmeth.getName()) && ok)
                    ml.add(crtmeth);
            }
        }

        for (i = 0; i < ml.size(); i++) {
            if (!((lrg.memoria.core.Method) ml.get(i)).isConstructor()) {
                crtmeth = (lrg.memoria.core.Method) ml.get(i);
                crtcls = (lrg.memoria.core.Class) crtmeth.getScope();
                if (!cl.contains(crtmeth) && (!crtcls.isAbstract())) {
                    cl.add(crtmeth);
                    count++;
                }
            }
        }
        return new NumericalResult(rewmeth, count);
    }
}
