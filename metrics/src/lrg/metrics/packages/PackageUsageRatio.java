package lrg.metrics.packages;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;
import lrg.metrics.classes.ChangingClasses;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Package Usage Ratio.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> PUR
 * <br>
 * <b>Description:</b> The PUR metric is defined as the relative number of classes
 * from the measured package that are used from outside the package.
 * <br>
 * A class use a package if it calls methods, accesses attributes or
 * extends a class defined in that package.
 * Accesses of constructors and destructors are not considered.
 * The number of used classes will be diveded by the total number of classes
 * in the package. (PUR = PIS / PS)
 * <br>
 * <b>Source:</b>
 */

public class PackageUsageRatio extends PackageMeasure{

    public PackageUsageRatio() {
        m_name = "PackageUsageRatio";
        m_fullName = "Package Usage Ratio";
    }

    /**
     * The PUR metric is defined as the relative number of classes
     * from the measured package that are used from outside the package.
     * <br>
     * A class use a package if it calls methods, accesses attributes or
     * extends a class defined in that package.
     * Accesses of constructors and destructors are not considered.
     * The number of used classes will be diveded by the total number of classes
     * in the package. (PUR = PIS / PS)
     */

    public Result measure(lrg.memoria.core.Package pck) {
        double temp, pis, ps;
        PackageSize psMetric = new PackageSize();
        PackageInterfaceSize pisMetric = new PackageInterfaceSize();

        pis = ((NumericalResult) pisMetric.measure(pck)).getValue();
        ps = ((NumericalResult) psMetric.measure(pck)).getValue();
        if (pis == 0) temp = 0;
        else temp = pis / ps;
        return new NumericalResult(pck, temp);
    }
}
