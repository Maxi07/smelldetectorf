package lrg.metrics.packages;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Number of Client Packages.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOCP
 * <br>
 * <b>Description:</b> This metric counts the number of other packages that use classes from a
 * given package. This metric helps out to see the dispersion of the clientclasses
 * among packages.
 * <b>Source:</b>
 */

public class NumberOfClientPackages extends PackageMeasure{

    public NumberOfClientPackages() {
        m_name = "NumberOfClientPackages";
        m_fullName = "Number of Client Packages";
    }

    /**
     * This metric counts the number of other packages that use classes from a
     * given package. This metric helps out to see the dispersion of the clientclasses
     * among packages.
     */

    public Result measure(lrg.memoria.core.Package pck) {
        ArrayList pel = pck.getScopedElements(), cll = new ArrayList(), attl, ml, accl, calll;
        int i, j, k;
        lrg.memoria.core.Class crtcl;
        lrg.memoria.core.Package crtp;
        HashSet hs = new HashSet();

        for (i = 0; i < pel.size(); i++) {
            try {
                crtcl = (lrg.memoria.core.Class) pel.get(i);
            } catch (ClassCastException e) {
                continue;
            }
            cll.add(crtcl);
        }

        for (i = 0; i < cll.size(); i++) {
            crtcl = (lrg.memoria.core.Class) cll.get(i);

            attl = crtcl.getAttributeList();
            for (j = 0; j < attl.size(); j++) {
                accl = ((lrg.memoria.core.Attribute) attl.get(j)).getAccessList();
                for (k = 0; k < accl.size(); k++) {
                    try {
                        crtp = (lrg.memoria.core.Package)
                               ((lrg.memoria.core.Access) accl.get(k)).getScope().getScope().getScope().getScope();
                    } catch (ClassCastException e) {
                        continue;
                    }
                    if (crtp != pck) hs.add(crtp);
                }
            }

            ml = crtcl.getMethodList();
            for (j = 0; j < ml.size(); j++) {
                calll = ((lrg.memoria.core.Method) ml.get(j)).getCallList();
                for (k = 0; k < calll.size(); k++) {
                    try {
                        crtp = (lrg.memoria.core.Package)
                               ((lrg.memoria.core.Call) calll.get(k)).getScope().getScope().getScope().getScope();
                    } catch (ClassCastException e) {
                        continue;
                    }
                    if (crtp != pck) hs.add(crtp);
                }
            }
        }

        return new NumericalResult(pck, hs.size());
    }
}