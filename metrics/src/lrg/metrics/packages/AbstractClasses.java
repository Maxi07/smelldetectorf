package lrg.metrics.packages;


/**
   <b>Name:</b> Number Of Abstract Classes.
   <br>
   <b>Alternative:</b> 
   <br>
   <b>Acronym:</b> 
   <br>
   <b>Description:</b> Measure the number of classes defined using "abstract" 
   keyword from a package.
   <br>
   <b>Source:</b> M. Lorenz, J. Kidd -- "Object-Oriented Software Metrics",
   Prentice Hall, 1994
 */
public class AbstractClasses extends ClassIterator {

    public AbstractClasses() {
	m_name = "AbstractClasses";
	m_fullName = "Number Of Abstract Classes";
    }

    /**
       Return the number of abstract classes from a package.(i.e. classes defined 
       using "abstract" keyword)
     */
    protected boolean check(lrg.memoria.core.Class cls) {
	return !cls.isInterface() && cls.isAbstract();
    }
}
