package lrg.metrics.packages;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;
import lrg.metrics.classes.ChangingClasses;
import lrg.memoria.core.DataAbstraction;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Package Interface Size.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> PIS
 * <br>
 * <b>Description:</b> Is the number of classes of a package used from outside the package.
 * <br>
 * A class use a package if it calls methods, accesses attributes or extends a class
 * defined in that package.
 * Accesses of constructors and destructors are not considered.
 * <br> The implementation includes now the classes that are used only by extension.
 * <b>Source:</b>
 */

public class PackageInterfaceSize extends PackageMeasure{

    public PackageInterfaceSize() {
        m_name = "PackageInterfaceSize";
        m_fullName = "Package Interface Size";
    }

    /**
     * Is the number of classes of a package used from outside the package.
     * <br>
     * A class use a package if it calls methods, accesses attributes or extends a class
     * defined in that package.
     * Accesses of constructors and destructors are not considered.
     */

    public Result measure(lrg.memoria.core.Package pck) {
        int i, j, f;
        ArrayList pel = pck.getScopedElements(), descl;
        lrg.memoria.core.Class crtcl;
        HashSet hs = new HashSet();

        for (f = 0; f < pel.size(); f++) {
            try {
                crtcl = (lrg.memoria.core.Class) pel.get(f);
            } catch (ClassCastException e) {
                continue;
            }
            int k, l;
            boolean ok;
            ArrayList calls, accesses, ml = new ArrayList(), descs, descmeth;
            ArrayList methods = crtcl.getMethodList(), attribs = crtcl.getAttributeList();
            lrg.memoria.core.Class crtcls;
            lrg.memoria.core.Package crtpack;
            lrg.memoria.core.Method crtmeth, rewmeth;
            HashSet cl = new HashSet();
            ArrayList crtpars, rewpars;

            for (i = 0; i < attribs.size(); i++) {
                accesses = ((lrg.memoria.core.Attribute) attribs.get(i)).getAccessList();
                for (j = 0; j < accesses.size(); j++) {
                    try {
                        crtmeth = (lrg.memoria.core.Method)
                                  ((lrg.memoria.core.Access) accesses.get(j)).getScope().getScope();
                    } catch(ClassCastException e) {
                        continue;
                    }
                    ml.add(crtmeth);
                }
            }
            for (i = 0; i < methods.size(); i++) {
                rewmeth = (lrg.memoria.core.Method) methods.get(i);
                calls = rewmeth.getCallList();
                for (j = 0; j < calls.size(); j++) {
                    try {
                        crtmeth = (lrg.memoria.core.Method)
                                  ((lrg.memoria.core.Call) calls.get(j)).getScope().getScope();
                    } catch(ClassCastException e) {
                        continue;
                    }
                    ml.add(crtmeth);
                }
                descs = crtcl.getDescendants();
                for (j = 0; j < descs.size(); j++) {
                    try {
                        crtcls = (lrg.memoria.core.Class) descs.get(j);
                    } catch(ClassCastException e) {
                        continue;
                    }
                    descmeth = crtcls.getMethodList();
                    for (k = 0; k < descmeth.size(); k++) {
                        crtmeth = (lrg.memoria.core.Method) descmeth.get(k);
                        crtpars = crtmeth.getParameterList();
                        rewpars = rewmeth.getParameterList();
                        if (crtpars.size() == rewpars.size())
                            ok = true;
                        else
                            ok = false;
                        for (l = 0; l < crtpars.size(); l++)
                            if (((lrg.memoria.core.Parameter) crtpars.get(i)).getType() !=
                                ((lrg.memoria.core.Parameter) rewpars.get(i)).getType())
                                ok = false;
                        if ((crtmeth.getName() == rewmeth.getName()) && ok)
                            ml.add(crtmeth);
                    }
                }
            }

            for (i = 0; i < ml.size(); i++) {
                if (!((lrg.memoria.core.Method) ml.get(i)).isConstructor()) {
                    try {
                        crtcls = (lrg.memoria.core.Class) ((lrg.memoria.core.Method) ml.get(i)).getScope();
                    } catch(ClassCastException e) {
                        continue;
                    }
                    if (!cl.contains(crtcls) && (!crtcls.isAbstract())) {
                        cl.add(crtcls);
                        crtpack = crtcls.getPackage();
                        if (crtpack != pck)
                            hs.add(crtcl);
                    }
                }
            }
        }

        for (i = 0; i < pel.size(); i++) {
            try {
                crtcl = (lrg.memoria.core.Class) pel.get(i);
            } catch (ClassCastException e) {
                continue;
            }
            descl = crtcl.getDescendants();
            for (j = 0; j < descl.size(); j++)
                try {
                    if (((lrg.memoria.core.Class) descl.get(j)).getScope() != pck)
                        hs.add(crtcl);
                } catch(ClassCastException e) {
                    continue;
                }            
        }
        return new NumericalResult(pck, hs.size());
    }
}
