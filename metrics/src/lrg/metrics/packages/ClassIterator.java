package lrg.metrics.packages;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
   This class is an iterator over the classes of a specific package.
 */
public abstract class ClassIterator extends PackageMeasure {

    //function used in children of this class to perform a specific check
    protected abstract boolean check(lrg.memoria.core.Class cls);

    /**
       Return the number of classes which satisfy a specific condition 
       from a package. The condition is defined using "check" method.
     */
    public Result measure(lrg.memoria.core.Package pack) {
	int i, size, count;
	ArrayList cl = pack.getAllTypesList();
	size = cl.size();
	count = 0;
	for (i = 0; i < size; i++)
	    if (check((lrg.memoria.core.Class)cl.get(i)))
		count++;
	return new NumericalResult(pack, count);
    }
}
