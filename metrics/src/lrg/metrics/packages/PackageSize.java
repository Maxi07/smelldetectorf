package lrg.metrics.packages;

/**
 * <b>Name:</b> Package Size.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> PS
 * <br>
 * <b>Description:</b> Is the number of classes which are defined in the measured package..
 * <br>
 * <b>Source:</b>
 */

public class PackageSize extends ClassIterator {

    public PackageSize() {
	m_name = "PackageSize";
	m_fullName = "Package Size";
    }

    /**
     * Return the number of classes from a package.
     */
    protected boolean check(lrg.memoria.core.Class cls) {
	    return !cls.isInterface();
    }
}
