package lrg.metrics;

import java.util.SortedSet;
import java.util.TreeSet;

public abstract class Result {

    public void add(Result res) {
	//doesn't do anything
    }

    public void saveResults() {
	java.lang.System.out.println("ERROR: only instances of \"CollectionResult\" can be saved");
    }

    protected SortedSet getResultsSet() {
	//by default we have an empty set
	return new TreeSet();
    }
}

