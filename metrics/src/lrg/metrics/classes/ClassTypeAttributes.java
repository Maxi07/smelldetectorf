package lrg.metrics.classes;


/**
 * <b>Name:</b> Sum Of Class Type Attributes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data_class
 * <br>
 * <b>Description:</b> Number of class-type attributes for a class.
 * <br>
 * <b>Source:</b> t?
 */
public class ClassTypeAttributes extends AttributeIterator {

    public ClassTypeAttributes() {
        m_name = "ClassTypeAttributes";
        m_fullName = "Sum Of Class Type Attributes";
    }

    /**
     * Check if the current attribute has the "Class" type.
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        if (act_attr.getType() instanceof lrg.memoria.core.Class)
            return true;
        else
            return false;
    }
}
