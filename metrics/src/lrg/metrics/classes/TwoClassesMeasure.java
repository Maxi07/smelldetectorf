package lrg.metrics.classes;

import lrg.metrics.packages.PackageMeasure;
import lrg.metrics.Result;
import lrg.metrics.CollectionResult;

import java.util.ArrayList;

public abstract class TwoClassesMeasure extends PackageMeasure {

    public TwoClassesMeasure() {
        m_kind = "classes";
    }

    public abstract Result measure(lrg.memoria.core.Class cls1, lrg.memoria.core.Class cls2);

    /**
     * Used as a filter of classes. (i.e. this offers the possibility select only
     * classes we are interested in)
     */
    protected boolean filter(lrg.memoria.core.Class cls1, lrg.memoria.core.Class cls2) {
        //by default we count only "normal" classes
        return ((cls1.getStatute() == lrg.memoria.core.Statute.NORMAL) &&
                (cls2.getStatute() == lrg.memoria.core.Statute.NORMAL));
    }

    public Result measure(lrg.memoria.core.Package pack) {
        int i, j, size;
        ArrayList cl = pack.getAllTypesList();
        CollectionResult cr = new CollectionResult();
        lrg.memoria.core.Class act1_class, act2_class;

        size = cl.size();
        for (i = 0; i < size - 1; i++)
            for (j = i + 1; i < size; j++) {
                act1_class = (lrg.memoria.core.Class) cl.get(i);
                act2_class = (lrg.memoria.core.Class) cl.get(j);
                if (filter(act1_class, act2_class))
                    cr.add(measure(act1_class, act2_class));
            }
        return cr;
    }

}
