package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Usage Ratio Implementation.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> URI
 * <br>
 * <b>Description:</b> It counts the relative number of members that a class reuses from a
 * given ancestor. We consider that a derived class reuses something from its ancestor if:
 * - it accesses a protected attribute defined in the superclass
 * - it calls a protected method defined in the superclass.
 * <br>
 * Accesses of constructors and destructors are not counted.
 * This implementation detects the relation between a base class and a
 * subclass, where the subclass doesn't reuse anything.
 * <br>
 * <b>Source:</b>
 */

public class UsageRatioImplementation extends TwoClassesMeasure {

    public UsageRatioImplementation() {
        m_name = "UsageRatioImplementation";
        m_fullName = "Usage Ratio Implementation";
    }

    /**
     * It counts the relative number of members that a class reuses from a
     * given ancestor. We consider that a derived class reuses something from its ancestor if:
     * - it accesses a protected attribute defined in the superclass
     * - it calls a protected method defined in the superclass.
     * <br>
     * Accesses of constructors and destructors are not counted.
     * This implementation detects the relation between a base class and a
     * subclass, where the subclass doesn't reuse anything.
     */

    protected boolean filter(lrg.memoria.core.Class cls1, lrg.memoria.core.Class cls2) {
        //by default we count only "normal" classes
        //but for this metric we need to check if they are in an inheritance relation
        return ((cls1.getStatute() == lrg.memoria.core.Statute.NORMAL) &&
                (cls2.getStatute() == lrg.memoria.core.Statute.NORMAL) &&
                (cls1.getAncestorsList().contains(cls2)));
    }

    public Result measure(lrg.memoria.core.Class child, lrg.memoria.core.Class ancestor) {
        double temp, na, nd = 0;
        int i, j;
        ArrayList ml = child.getMethodList(), accl, calll;
        HashSet ua = new HashSet(), um = new HashSet();
        lrg.memoria.core.Method crtm, crtcm;
        lrg.memoria.core.Attribute crta;

        for (i = 0; i < ml.size(); i++) {
            crtm = (lrg.memoria.core.Method) ml.get(i);
            if (crtm.isConstructor()) continue;
            accl = crtm.getBody().getAccessList();
            for (j = 0; j < accl.size(); j++) {
                try {
                    crta = (lrg.memoria.core.Attribute)
                           ((lrg.memoria.core.Access) accl.get(j)).getVariable();
                } catch (ClassCastException e) {
                    continue;
                }
                try {
                    if ((((lrg.memoria.core.Class) crta.getScope()) == ancestor) &&
                        (crta.isProtected())) ua.add(crta);
                } catch(ClassCastException e) {
                    continue;
                }
            }

            calll = crtm.getBody().getCallList();
            for (j = 0; j < calll.size(); j++) {
                try {
                    crtcm = (lrg.memoria.core.Method)
                            ((lrg.memoria.core.Call) calll.get(j)).getFunction();
                } catch (ClassCastException e) {
                    continue;
                }
                try {
                    if ((((lrg.memoria.core.Class) crtcm.getScope()) == ancestor) &&
                        (crtcm.isProtected()) && (!crtcm.isConstructor())) um.add(crtcm);
                } catch(ClassCastException e) {
                    continue;
                }
            }
        }

        na = ua.size() + um.size();
        for (i = 0; i < ancestor.getAttributeList().size(); i++) {
            crta = (lrg.memoria.core.Attribute) ancestor.getAttributeList().get(i);
            if (crta.isProtected()) nd++;
        }
        for (i = 0; i < ancestor.getMethodList().size(); i++) {
            crtcm = (lrg.memoria.core.Method) ancestor.getMethodList().get(i);
            if (crtcm.isProtected()) nd++;
        }

        if (na == 0) temp = 0;
        else if (nd == 0) temp = Integer.MAX_VALUE;
             else temp = na / nd;

        return new NumericalResult(child, temp);
    }

}
