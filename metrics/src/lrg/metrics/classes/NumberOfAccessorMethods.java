package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of ccessor Methods.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOAM
 * <br>
 * <b>Description:</b> It is defined as the number of non-inherited accessor methods
 * that belong to the interface of a class.
 * <br>
 * The big problem is how to identify the accessor methods? We used the following "pattern":
 * accesor methods are small-methods ( < 10 Loc) , with a unitary cyclomatic complexity,
 * and we rely on the name convention, stating that the names of accesor methods
 * are prefixed with the get (Get) and set (Set) prefix.
 * <br>
 * <b>Source:</b>
 */

public class NumberOfAccessorMethods extends ClassMeasure {

    public NumberOfAccessorMethods() {
        m_name = "NumberOfAccessorMethods";
        m_fullName = "Number Of Accessor Methods";
    }

    /**
     * It is defined as the number of non-inherited accessor methods
     * that belong to the interface of a class.
     * <br>
     * The big problem is how to identify the accessor methods? We used the following "pattern":
     * accesor methods are small-methods ( < 10 Loc) , with a unitary cyclomatic complexity,
     * and we rely on the name convention, stating that the names of accesor methods
     * are prefixed with the get (Get) and set (Set) prefix.
     * <br>
     */

    public Result measure(lrg.memoria.core.Class c) {
        int count = 0;
        ArrayList methodl = c.getMethodList();

        for (int i = 0; i < methodl.size(); i++) {
            if ((((lrg.memoria.core.Method) methodl.get(i)).isAccessor()) &&
                (((lrg.memoria.core.Method) methodl.get(i)).isPublic()))
                count++;
        }

        return new NumericalResult(c, count);
    }
}
