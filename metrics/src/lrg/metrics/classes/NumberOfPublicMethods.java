package lrg.metrics.classes;

/**
 * <b>Name:</b> Number Of Public Methods.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NPubM
 * <br>
 * <b>Description:</b> Is the number of public methods in the measured class.
 * <br>
 * Constructors and Destructors are excluded.
 * <br>
 * <b>Source:</b>
 */

public class NumberOfPublicMethods extends MethodIterator {

    public NumberOfPublicMethods() {
        m_name = "NumberOfPublicMethods";
        m_fullName = "Number of Public Methods";
    }

    //returns "true" if the actual method is public and is not constructor
    protected boolean check(lrg.memoria.core.Method m) {
        return (m.isPublic() && !m.isConstructor());
    }

}
