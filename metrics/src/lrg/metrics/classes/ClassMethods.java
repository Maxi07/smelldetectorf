package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Class Methods In A Class.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NCM
 * <br>
 * <b>Description:</b> Measures the number of methods that belong to the class as a whole
 * rather than to instances of the class. (i.e. the number of static methods)
 * <br>
 * <b>Source:</b> M. Lorenz, J. Kidd -- "Object-Oriented Software Metrics",
 * Prentice Hall, 1994
 */
public class ClassMethods extends MethodIterator {

    public ClassMethods() {
        m_name = "ClassMethods";
        m_fullName = "Number Of Class Methods In A Class";
    }

    /**
     * Returns true if the method is defined using the "static" keyword.
     */
    protected boolean check(lrg.memoria.core.Method m) {
        return m.isStatic();
    }
}
