package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number of class properties.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> SIZE2
 * <br>
 * <b>Description:</b> The second size metric is the number of properties.
 * (i.e. attributes and methods defined in a class).
 * <br>
 * <b>Source:</b> W. Li, S. Henry. Maintenance Metrics for the Object Oriented Paradigm.
 * IEEE Proc. First International Software Metrics Symp., pp. 52-60, May 21-22, 1993.
 */
public class ClassProperties extends ClassMeasure {

    public ClassProperties() {
        m_name = "ClassProperties";
        m_fullName = "Number Of Class Properties";
    }

    /**
     * Measures the number of properties. (i.e. the attributes and
     * methods defined in a class)
     */
    public Result measure(lrg.memoria.core.Class act_class) {
        int count = 0;
        ArrayList attr_list, methods_list;
        attr_list = act_class.getAttributeList();
        methods_list = act_class.getMethodList();
        count = attr_list.size() + methods_list.size();
        return new NumericalResult(act_class, count);
    }
}




