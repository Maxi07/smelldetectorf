package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Average Number Of Instance Variables Per Class.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b>
 * <br>
 * <b>Description:</b> Measure the average number of "instance variable" that a class
 * has according to the following expression <br>
 * (total number of instance variables) / (total number of methods).
 * <br>
 * <b>Source:</b> M. Lorentz, J. Kidd -- "Object-Oriented Software Metrics",
 * Prentice Hall, 1994
 */
public class AverageInstanceVariables extends ClassMeasure {

    public AverageInstanceVariables() {
        m_name = "AverageInstanceVariables";
        m_fullName = "Average Number Of Instance Variables Per Class";
    }

    /**
     * Returns the average number of "instance variable" that a class
     * has according to the following expression <br>
     * (total number of instance variables) / (total number of methods).
     * <br> We are reffering to "total number of instance variables" and
     * "total number of methods" of a specific class.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        double temp, var_number, method_number = 0;
        int size, i;
        ArrayList method_list = cls.getMethodList();
        size = method_list.size();
        for (i = 0; i < size; i++)
            if (((lrg.memoria.core.Method) method_list.get(i)).getScope() == cls)
                method_number++;
        InstanceVariables iv = new InstanceVariables();
        NumericalResult n_result = (NumericalResult) iv.measure(cls);
        var_number = n_result.getValue();
        if (method_number != 0)
            temp = var_number / method_number;
        else
            if (var_number == 0)
                temp = 0;
            else
                temp = Integer.MAX_VALUE;   //if we have no methods, we return an Integer.MAX_VALUE
        return new NumericalResult(cls, temp);
    }

}
