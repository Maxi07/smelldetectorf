package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Changing Classes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> CC
 * <br>
 * <b>Description:</b> Measures the dispersion of the changes, i.e. in how many classes are spread
 * the methods that are potentially affected by a change in the given class.
 * <br>
 * A method depends on a class if
 *  - it invokes methods defined by the class, or
 *  - it accesses attributes of the class, or
 *  - it redefines a method from a superclass
 * Constructors and destructors are not considered as accessing points.
 * CC is not computed for abstract classes, and abstract classes are not counted as CC.
 * Also, special classes (is_special = 2) are not analized.
 * <br>
 * <b>Source:</b>
 */

public class ChangingClasses extends ClassMeasure {

    public ChangingClasses() {
        m_name = "ChangingClasses";
        m_fullName = "Changing Classes";
    }

    /**
     * Measures the dispersion of the changes, i.e. in how many classes are spread
     * the methods that are potentially affected by a change in the given class.
     * <br>
     * A method depends on a class if
     * - it invokes methods defined by the class, or
     * - it accesses attributes of the class, or
     * - it redefines a method from a superclass
     * Constructors and destructors are not considered as accessing points.
     * CC is not computed for abstract classes, and abstract classes are not counted as CC.
     * Also, special classes (is_special = 2) are not analized.
     */

    public Result measure(lrg.memoria.core.Class cls) {
        int count = 0, i, j, k, l;
        boolean ok;
        ArrayList calls, accesses, ml = new ArrayList(), descs, descmeth;
        ArrayList methods = cls.getMethodList(), attribs = cls.getAttributeList();
        lrg.memoria.core.Class crtcls;
        lrg.memoria.core.Method crtmeth, rewmeth;
        HashSet cl = new HashSet();
        ArrayList crtpars, rewpars;

        for (i = 0; i < attribs.size(); i++) {
            accesses = ((lrg.memoria.core.Attribute) attribs.get(i)).getAccessList();
            for (j = 0; j < accesses.size(); j++) {
                try {
                    crtmeth = (lrg.memoria.core.Method)
                              ((lrg.memoria.core.Access) accesses.get(j)).getScope().getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                ml.add(crtmeth);
            }
        }
        for (i = 0; i < methods.size(); i++) {
            rewmeth = (lrg.memoria.core.Method) methods.get(i);
            calls = rewmeth.getCallList();
            for (j = 0; j < calls.size(); j++) {
                try {
                    crtmeth = (lrg.memoria.core.Method)
                              ((lrg.memoria.core.Call) calls.get(j)).getScope().getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                ml.add(crtmeth);
            }
            descs = cls.getDescendants();
            for (j = 0; j < descs.size(); j++) {
                try {
                    crtcls = (lrg.memoria.core.Class) descs.get(j);
                } catch (ClassCastException e) {
                    continue;
                }
                descmeth = crtcls.getMethodList();
                for (k = 0; k < descmeth.size(); k++) {
                    crtmeth = (lrg.memoria.core.Method) descmeth.get(k);
                    crtpars = crtmeth.getParameterList();
                    rewpars = rewmeth.getParameterList();
                    if (crtpars.size() == rewpars.size()) ok = true;
                    else ok = false;
                    for (l = 0; l < crtpars.size(); l++)
                        if (((lrg.memoria.core.Parameter) crtpars.get(l)).getType() !=
                            ((lrg.memoria.core.Parameter) rewpars.get(l)).getType())
                            ok = false;
                    if ((crtmeth.getName() == rewmeth.getName()) && ok)
                        ml.add(crtmeth);
                }
            }
        }

        for (i = 0; i < ml.size(); i++) {
            if (!((lrg.memoria.core.Method) ml.get(i)).isConstructor()) {
                try {
                    crtcls = (lrg.memoria.core.Class) ((lrg.memoria.core.Method) ml.get(i)).getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                if (!cl.contains(crtcls) && (!crtcls.isAbstract()) && (crtcls != cls)) {
                    cl.add(crtcls);
                    count++;
                }
            }
        }

        return new NumericalResult(cls, count);
    }
}
