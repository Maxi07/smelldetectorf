package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Instance Methods In A Class.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NIM
 * <br>
 * <b>Description:</b> Measures the number of methods in a class defined without using
 * the "static" keyword.
 * <br>
 * <b>Source:</b> M. Lorentz & J. Kidd -- "Object Oriented Software Metrics",
 * Prentice Hall, 1994
 */
public class InstanceMethods extends MethodIterator {

    public InstanceMethods() {
        m_name = "InstanceMethods";
        m_fullName = "Number of Instance Methods in a Class";
    }

    //returns "true" if the actual method isn't static
    protected boolean check(lrg.memoria.core.Method m) {
        return !m.isStatic();
    }

}
