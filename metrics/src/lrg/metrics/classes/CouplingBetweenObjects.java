package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Coupling Between Objects.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> CBO
 * <br>
 * <b>Description:</b> The Coupling Between Objects (CBO) for a class is
 * the number of other classes to which it is coupled.
 * <br>
 * <b>Source:</b> S.R. Chidamber, C.F. Kemerer. A metrics Suite for Object Oriented Design.
 * IEEE Transactions on Softw. Engineering, Vol.20, No.6, June 1994.
 */

public class CouplingBetweenObjects extends ClassMeasure {

    private HashSet used_classes;

    public CouplingBetweenObjects() {
        m_name = "CouplingBetweenObjects";
        m_fullName = "Coupling Between Objects";
    }

    /**
     * Count the number of types which appear in the "type of the method" (i.e. number of types
     * from parameters and the return type)
     */
    //It doesn't count parameters, local vars or attributes that are not used in the current class
    private int para_retTypesCount(lrg.memoria.core.Function act_method) {
        int size, j, types_count = 0;
        ArrayList parameter_list;
        lrg.memoria.core.Parameter act_param;
        lrg.memoria.core.Type act_type;
        parameter_list = act_method.getParameterList();
        size = parameter_list.size();
        for (j = 0; j < size; j++) {
            act_param = (lrg.memoria.core.Parameter) parameter_list.get(j);
            act_type = act_param.getType();
            if (act_type instanceof lrg.memoria.core.Class && !used_classes.contains(act_type) &&
                    act_param.getAccessList().contains(act_method.getScope())) {
                used_classes.add(act_type);
                types_count++;
            }
        }
        act_type = act_method.getReturnType();
        if (act_type instanceof lrg.memoria.core.Class && !used_classes.contains(act_type)) {
            used_classes.add(act_type);
            types_count++;
        }
        return types_count;
    }

    //count the number of types of variables accessed from the method's body
    private int accessTypesCount(lrg.memoria.core.FunctionBody act_body) {
        int j, size, types_count = 0;
        ArrayList access_list = act_body.getAccessList();
        lrg.memoria.core.Access act_access;
        lrg.memoria.core.Type act_type;
        size = access_list.size();
        for (j = 0; j < size; j++) {
            act_access = (lrg.memoria.core.Access) access_list.get(j);
            act_type = act_access.getVariable().getType();
            if (act_type instanceof lrg.memoria.core.Class && !used_classes.contains(act_type)) {
                used_classes.add(act_type);
                types_count++;
            }
        }
        return types_count;
    }

    //count the number of distinct types that are used in calls
    private int callTypesCount(lrg.memoria.core.FunctionBody act_body) {
        int size, types_count = 0, j;
        ArrayList calls_list = act_body.getCallList();
        lrg.memoria.core.Call act_call;
        lrg.memoria.core.Function act_called_method;
        size = calls_list.size();
        for (j = 0; j < size; j++) {
            act_call = (lrg.memoria.core.Call) calls_list.get(j);
            act_called_method = act_call.getMethod();
            types_count += para_retTypesCount(act_called_method);
        }
        return types_count;
    }

    /**
     * Returns the number of other classes to which this class is coupled.
     */
    public Result measure(lrg.memoria.core.Class act_class) {
        int size, i, count = 0;
        ArrayList method_list;
        lrg.memoria.core.Method act_method;
        lrg.memoria.core.FunctionBody act_body;
        used_classes = new HashSet();
        method_list = act_class.getMethodList();
        size = method_list.size();
        for (i = 0; i < size; i++) {
            act_method = (lrg.memoria.core.Method) method_list.get(i);
            if (act_method.getScope() == act_class) {
                count += para_retTypesCount(act_method);
                act_body = act_method.getBody();
                if (act_body != null) {
                    count += accessTypesCount(act_body);
                    count += callTypesCount(act_body);
                }
            }
        }
        return new NumericalResult(act_class, count);

    }
}
