package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Weighted Changing Methods.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> WCM
 * <br>
 * <b>Description:</b> In the CM metric, if a method was using at least something
 * from a provider-class we added 1 to the metric.
 * For WCM, we count the number of "usages" in each method.
 * <br>
 * "Usages in a method" means the number of distinct fields and methods of the measured class,
 * accesed from the method. We add the number of method from the measured class
 * that are overrided by the method.
 * Constructors and destructors are not considered.
 * The class must be a concrete class, i.e. interfaces should not be counted.
 * <br>
 * <b>Source:</b>
 */


public class WeightedChangingMethods extends ClassMeasure {

    public WeightedChangingMethods() {
        m_name = "WeightedChangingMethods";
        m_fullName = "Weighted Changing Methods";
    }

    /**
     * In the CM metric, if a method was using at least something
     * from a provider-class we added 1 to the metric.
     * For WCM, we count the number of "usages" in each method.
     * <br>
     * "Usages in a method" means the number of distinct fields and methods of the measured class,
     * accesed from the method. We add the number of method from the measured class
     * that are overrided by the method.
     * Constructors and destructors are not considered.
     * The class must be a concrete class, i.e. interfaces should not be counted.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        int count = 0, i, j, k, l, accs;
        boolean ok;
        ArrayList calls, accesses, ml = new ArrayList(), descs, descmeth;
        ArrayList methods = cls.getMethodList(), attribs = cls.getAttributeList();
        lrg.memoria.core.Class crtcls;
        lrg.memoria.core.Variable crtvar;
        lrg.memoria.core.Function crtfcn;
        lrg.memoria.core.Method crtmeth, rewmeth;
        ArrayList crtpars, rewpars;
        HashSet hs = new HashSet();
        HashSet ms = new HashSet();

        for (i = 0; i < attribs.size(); i++) {
            accesses = ((lrg.memoria.core.Attribute) attribs.get(i)).getAccessList();
            for (j = 0; j < accesses.size(); j++) {
                try {
                    crtmeth = (lrg.memoria.core.Method)
                              ((lrg.memoria.core.Access) accesses.get(j)).getScope().getScope();
                } catch(ClassCastException e) {
                    continue;
                }
                if (!ms.contains(crtmeth)) {
                    ms.add(crtmeth);
                    ml.add(crtmeth);
                }
            }
        }
        for (i = 0; i < methods.size(); i++) {
            rewmeth = (lrg.memoria.core.Method) methods.get(i);
            calls = rewmeth.getCallList();
            for (j = 0; j < calls.size(); j++) {
                try {
                    crtmeth = (lrg.memoria.core.Method)
                              ((lrg.memoria.core.Call) calls.get(j)).getScope().getScope();
                } catch(ClassCastException e) {
                    continue;
                }
                if (!ms.contains(crtmeth)) {
                    ms.add(crtmeth);
                    ml.add(crtmeth);
                }
            }
            descs = cls.getDescendants();
            for (j = 0; j < descs.size(); j++) {
                try {
                    crtcls = (lrg.memoria.core.Class) descs.get(j);
                } catch(ClassCastException e) {
                    continue;
                }
                descmeth = crtcls.getMethodList();
                for (k = 0; k < descmeth.size(); k++) {
                    crtmeth = (lrg.memoria.core.Method) descmeth.get(k);
                    crtpars = crtmeth.getParameterList();
                    rewpars = rewmeth.getParameterList();
                    if (crtpars.size() == rewpars.size()) ok = true;
                    else ok = false;
                    for (l = 0; l < crtpars.size(); l++)
                        if (((lrg.memoria.core.Parameter) crtpars.get(l)).getType() !=
                            ((lrg.memoria.core.Parameter) rewpars.get(l)).getType())
                            ok = false;
                    if (((crtmeth.getName() == rewmeth.getName()) && ok) &&
                        (!ms.contains(crtmeth))) {
                        ms.add(crtmeth);
                        count++;
                    }
                }
            }
        }

        for (i = 0; i < ml.size(); i++) {
            crtmeth = (lrg.memoria.core.Method) ml.get(i);
            accs = 0;
            if (!crtmeth.isConstructor()) {
                try {
                    crtcls = (lrg.memoria.core.Class) crtmeth.getScope();
                } catch(ClassCastException e) {
                    continue;
                }
                if (!crtcls.isAbstract()) {
                    accesses = crtmeth.getBody().getAccessList();
                    hs.clear();
                    for (j = 0; j < accesses.size(); j++) {
                        crtvar = ((lrg.memoria.core.Access) accesses.get(j)).getVariable();
                        if ((crtvar.getScope() == cls) && (!hs.contains(crtvar))) {
                            hs.add(crtvar);
                            accs++;
                        }
                    }
                    calls = crtmeth.getBody().getCallList();
                    hs.clear();
                    for (j = 0; j < calls.size(); j++) {
                        crtfcn = ((lrg.memoria.core.Call) calls.get(j)).getFunction();
                        if ((crtfcn.getScope() == cls) && (!hs.contains(crtfcn))) {
                            hs.add(crtfcn);
                            accs++;
                        }
                    }
                    count += accs;
                }
            }
        }

        return new NumericalResult(cls, count);
    }
}
