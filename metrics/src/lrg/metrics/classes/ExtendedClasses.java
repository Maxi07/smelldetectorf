package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Extended Classes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_extend
 * <br>
 * <b>Description:</b> Equals 1 if the class extends another one and 0 otherwise.
 * <br>
 * <b>Source:</b> t?
 */
public class ExtendedClasses extends ClassMeasure {

    public ExtendedClasses() {
        m_name = "ExtendedClasses";
        m_fullName = "Number Of Extended Classes";
    }

    /**
     * Measures the number of extended classes by a class.
     */
    public Result measure(lrg.memoria.core.Class c) {
        int count = 0;
        if (c.isInterface()) {
            //for interfaces it counts the numbar of their extends
            ArrayList extendedInterfaces = c.getAncestorsList();
            count = extendedInterfaces.size();
        }
        else {
            //for classes implemented interfaces are not considered
            lrg.memoria.core.DataAbstraction ancestor = c.getFirstAncestor();
            if (ancestor != null && !ancestor.getName().equals("Object"))
                count = 1;
        }
        return new NumericalResult(c, count);
    }
}
