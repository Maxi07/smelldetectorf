package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

/**
 * <b>Name:</b> ClientInterfaceWidth.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> CIW
 * <br>
 * <b>Description:</b> This metric measures the "width" of the interface of a class.
 * <br>
 * The metric counts public methods and public fields.Constructors and
 * destructors are not counted.
 * <br>
 * <b>Source:</b>
 */

public class ClientInterfaceWidth extends ClassMeasure {

    public ClientInterfaceWidth() {
        m_name = "ClientInterfaceWidth";
        m_fullName = "Client Interface Width";
    }

    /**
     * This metric measures the "width" of the interface of a class.
     * <br>
     * The metric counts public methods and public fields.Constructors and
     * destructors are not counted.
     */

    public Result measure(lrg.memoria.core.Class c) {
        NumberOfPublicMethods nPubMMetric = new NumberOfPublicMethods();
        PublicAttributes paMetric = new PublicAttributes();
        double npubm, pa, temp;

        npubm = ((NumericalResult) nPubMMetric.measure(c)).getValue();
        pa = ((NumericalResult) paMetric.measure(c)).getValue();

        temp = npubm + pa;

        return new NumericalResult(c, temp);
    }
}
