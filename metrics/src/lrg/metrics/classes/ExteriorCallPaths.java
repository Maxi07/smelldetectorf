package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Exterior Call Paths.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_func_calle
 * <br>
 * <b>Description:</b> Number of calls from the current class's methods to functions
 * defined outside of the this class.
 * <br>
 * Obs.: Different calls to a method (from a specific method) count for one call.
 * <br>
 * <b>Source:</b> t?
 */
public class ExteriorCallPaths extends ClassMeasure {

    public ExteriorCallPaths() {
        m_name = "ExteriorCallPaths";
        m_fullName = "Number Of Exterior Call Paths";
    }

    /**
     * Measures the number of calls from the current class's methods to functions
     * defined outside of the this class.
     * <br>
     * Obs.: Different calls to a method (from a specific method) count for one call.
     */
    public Result measure(lrg.memoria.core.Class c) {
        int size, i;
        double count = 0;
        ArrayList method_list = c.getMethodList();
        lrg.memoria.core.Method act_method;
        lrg.metrics.methods.ExteriorCallPaths mpe = new lrg.metrics.methods.ExteriorCallPaths();
        NumericalResult temp_result;
        size = method_list.size();
        for (i = 0; i < size; i++) {
            act_method = (lrg.memoria.core.Method) method_list.get(i);
            if (act_method.getScope() == c) {
                temp_result = (NumericalResult) mpe.measure(act_method);
                count += temp_result.getValue();
            }
        }
        return new NumericalResult(c, count);
    }
}
