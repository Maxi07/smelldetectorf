package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Changing Methods.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> CM
 * <br>
 * <b>Description:</b> Counts the number of methods from other classes
 * that depend on a given class.
 * <br>
 * A method depends on a class if
 *  - it invokes methods defined by the class, or
 *  - it accesses attributes of the class, or
 *  - it redefines a method from a superclass
 * The class must be a concrete class, i.e. interfaces should not be counted.
 * Constructors and destructors accesses are not considered.
 * <br>
 * <b>Source:</b>
 */

public class ChangingMethods extends ClassMeasure {

    public ChangingMethods() {
        m_name = "ChangingMethods";
        m_fullName = "Changing Methods";
    }

    /**
     * Counts the number of methods from other classes
     * that depend on a given class.
     * <br>
     * A method depends on a class if
     * - it invokes methods defined by the class, or
     * - it accesses attributes of the class, or
     * - it redefines a method from a superclass
     * The class must be a concrete class, i.e. interfaces should not be counted.
     * Constructors and destructors accesses are not considered.
     */

    public Result measure(lrg.memoria.core.Class cls) {
        int count = 0, i, j, k, l;
        boolean ok;
        ArrayList calls, accesses, ml = new ArrayList(), descs, descmeth;
        ArrayList methods = cls.getMethodList(), attribs = cls.getAttributeList();
        lrg.memoria.core.Class crtcls;
        lrg.memoria.core.Method crtmeth, rewmeth;
        HashSet cl = new HashSet();
        ArrayList crtpars, rewpars;

        for (i = 0; i < attribs.size(); i++) {
            accesses = ((lrg.memoria.core.Attribute) attribs.get(i)).getAccessList();
            for (j = 0; j < accesses.size(); j++) {
                try {
                    crtmeth = (lrg.memoria.core.Method)
                              ((lrg.memoria.core.Access) accesses.get(j)).getScope().getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                ml.add(crtmeth);
            }
        }
        for (i = 0; i < methods.size(); i++) {
            rewmeth = (lrg.memoria.core.Method) methods.get(i);

            calls = rewmeth.getCallList();
            for (j = 0; j < calls.size(); j++) {
                try {
                    crtmeth = (lrg.memoria.core.Method)
                              ((lrg.memoria.core.Call) calls.get(j)).getScope().getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                ml.add(crtmeth);
            }
            descs = cls.getDescendants();
            for (j = 0; j < descs.size(); j++) {
                try {
                    crtcls = (lrg.memoria.core.Class) descs.get(j);
                } catch (ClassCastException e) {
                    continue;
                }
                descmeth = crtcls.getMethodList();
                for (k = 0; k < descmeth.size(); k++) {
                    crtmeth = (lrg.memoria.core.Method) descmeth.get(k);
                    crtpars = crtmeth.getParameterList();
                    rewpars = rewmeth.getParameterList();
                    if (crtpars.size() == rewpars.size()) ok = true;
                    else ok = false;
                    for (l = 0; l < crtpars.size(); l++)
                        if (((lrg.memoria.core.Parameter) crtpars.get(l)).getType() !=
                            ((lrg.memoria.core.Parameter) rewpars.get(l)).getType())
                            ok = false;
                    if ((crtmeth.getName() == rewmeth.getName()) && ok)
                        ml.add(crtmeth);
                }
            }
        }

        for (i = 0; i < ml.size(); i++) {
            if (!((lrg.memoria.core.Method) ml.get(i)).isConstructor()) {
                crtmeth = (lrg.memoria.core.Method) ml.get(i);
                try {
                    crtcls = (lrg.memoria.core.Class) crtmeth.getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                if (!cl.contains(crtmeth) && (!crtcls.isAbstract()) && (crtcls != cls)) {
                    cl.add(crtmeth);
                    count++;
                }
            }
        }

        return new NumericalResult(cls, count);
    }
}
