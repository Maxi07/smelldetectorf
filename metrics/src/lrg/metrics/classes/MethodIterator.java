package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * An abstract class used to iterate through methods.
 */
public abstract class MethodIterator extends ClassMeasure {

    //abstract method which returns the result of a specific test.
    protected abstract boolean check(lrg.memoria.core.Method m);

    /**
     * This method iterates through methods and counts the number of times
     * that the methods pass the specific test.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        int i, size, count;
        ArrayList method_list = cls.getMethodList();
        lrg.memoria.core.Method act_method;
        size = method_list.size();
        count = 0;
        for (i = 0; i < size; i++) {
            act_method = (lrg.memoria.core.Method) method_list.get(i);
            if (check(act_method))
                count++;
        }
        return new NumericalResult(cls, count);
    }

}
