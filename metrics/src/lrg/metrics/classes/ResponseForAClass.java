package lrg.metrics.classes;

import lrg.memoria.core.Call;
import lrg.memoria.core.Statute;
import lrg.memoria.core.Function;
import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * <b>Name:</B> Response For A Class.
 * <br>
 * <b>Alternative:</b>
 * <br>C
 * <b>Acronym:</b>RFC
 * <br>
 * <b>Description:</b> The Response Set for a Class (RS) is a set of methods
 * that can be potentially executed in response to a message received by an
 * object of that class. The Response for a Class (RFC) is the cardinality of
 * the response set for that class.
 * <br>
 * <b>Source:</b> S.R. Chidamber, C.F. Kemerer. A metrics Suite for Object Oriented Design.
 * IEEE Transactions on Softw. Engineering, Vol.20, No.6, June 1994.
 */
public class ResponseForAClass extends ClassMeasure {

    public ResponseForAClass() {
        m_name = "ResponseForAClass";
        m_fullName = "Response For A Class";
    }

    /**
     * The Response Set for a Class (RS) is a set of methods
     * that can be potentially executed in response to a message received by an
     * object of that class. This method measure  the cardinality of
     * the response set for that class.
     */
    public Result measure(lrg.memoria.core.Class act_class) {
        int size, i;
        ArrayList method_list = act_class.getMethodList();
        lrg.memoria.core.Function act_method;
        lrg.memoria.core.FunctionBody act_body;
        TreeSet calledMethodNamesSet = new TreeSet();

        //add the number of methods of the current class
        Function currentMethod;
        for (int j = 0; j < method_list.size(); j++) {
            currentMethod = (Function) method_list.get(j);
            if (currentMethod.getStatute() == Statute.NORMAL)
                calledMethodNamesSet.add(currentMethod.getFullName());
        }

        size = method_list.size();
        for (i = 0; i < size; i++) {
            act_method = (lrg.memoria.core.Function) method_list.get(i);
            act_body = act_method.getBody();
            //add the Ri (the number of methods called by the method i)
            if (act_body != null) {
                ArrayList calls = act_body.getCallList();
                Call currentCall;
                Function calledMethod;
                for (int j = 0; j < calls.size(); j++) {
                    currentCall = (Call) calls.get(j);
                    calledMethod = currentCall.getMethod();
//                    if (calledMethod.getStatute() == Statute.NORMAL)
                    calledMethodNamesSet.add(calledMethod.getFullName());
                }
            }
        }
        return new NumericalResult(act_class, calledMethodNamesSet.size());
    }
}
