package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Package Data Members.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data_package
 * <br>
 * <b>Description:</b> Number of package attributes for a class. (i.e. data members
 * declared in the class declaration without any of the the following keywords private,
 * protected, public)
 * <br>
 * <b>Source:</b> t?
 */
public class PackageAttributes extends AttributeIterator {

    public PackageAttributes() {
        m_name = "PackageAttributes";
        m_fullName = "Number of Package Data Members";
    }

    /**
     * Check if the current attribute has "package vizibility".
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return act_attr.isPackage();
    }
}
