package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Number of Client Classes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> COC
 * <br>
 * <b>Description:</b> It is defined as the number of other classes that access the interface of
 * the measured class.
 * <br>
 * <b>Source:</b>
 */

public class NumberOfClientClasses extends ClassMeasure {

    public NumberOfClientClasses() {
        m_name = "NumberOfClientClasses";
        m_fullName = "Number of Client Classes";
    }

    /**
     * It is defined as the number of other classes that access the interface of
     * the measured class.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        ArrayList attl, ml, accl, calll;
        int j, k;
        lrg.memoria.core.Class crtc;
        HashSet hs = new HashSet();

        attl = cls.getAttributeList();
        for (j = 0; j < attl.size(); j++) {
            accl = ((lrg.memoria.core.Attribute) attl.get(j)).getAccessList();
            for (k = 0; k < accl.size(); k++) {
                try {
                    crtc = (lrg.memoria.core.Class)
                           ((lrg.memoria.core.Access) accl.get(k)).getScope().getScope().getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                if (crtc != cls) hs.add(crtc);
            }
        }

        ml = cls.getMethodList();
        for (j = 0; j < ml.size(); j++) {
            calll = ((lrg.memoria.core.Method) ml.get(j)).getCallList();
            for (k = 0; k < calll.size(); k++) {
                try {
                    crtc = (lrg.memoria.core.Class)
                           ((lrg.memoria.core.Call) calll.get(k)).getScope().getScope().getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                if (crtc != cls) hs.add(crtc);
            }
        }

        return new NumericalResult(cls, hs.size());
    }
}
