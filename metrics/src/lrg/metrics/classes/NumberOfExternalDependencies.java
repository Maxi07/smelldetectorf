package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Number of External Dependencies.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOED
 * <br>
 * <b>Description:</b> Counts thed classes in other packages on which the given class depends on.
 * Accesses to other packages' attribs and methods are counted.
 * <br>
 * <b>Source:</b>
 */

public class NumberOfExternalDependencies extends ClassMeasure {

    public NumberOfExternalDependencies() {
        m_name = "NumberOfExternalDependencies";
        m_fullName = "Number of External Dependencies";
    }

    /**
     * Counts thed classes in other packages on which the given class depends on.
     * Accesses to other packages' attribs and methods are counted.
     */
    public Result measure(lrg.memoria.core.Class act_class) {
        double nd = 0;
        int i, j;
        ArrayList ml = act_class.getMethodList(), accl, calll;
        HashSet ua = new HashSet(), um = new HashSet();
        lrg.memoria.core.Method crtm, crtcm;
        lrg.memoria.core.Attribute crta;

        for (i = 0; i < ml.size(); i++) {
            crtm = (lrg.memoria.core.Method) ml.get(i);
            if (crtm.isConstructor()) continue;
            accl = crtm.getBody().getAccessList();
            for (j = 0; j < accl.size(); j++) {
                try {
                    crta = (lrg.memoria.core.Attribute)
                           ((lrg.memoria.core.Access) accl.get(j)).getVariable();
                } catch (ClassCastException e) {
                    continue;
                }
                if ((!ua.contains(crta)) && (crta.getScope().getScope() != act_class.getScope())){
                    ua.add(crta);
                    nd++;
                }
            }

            calll = crtm.getBody().getCallList();
            for (j = 0; j < calll.size(); j++) {
                try {
                    crtcm = (lrg.memoria.core.Method)
                            ((lrg.memoria.core.Call) calll.get(j)).getFunction();
                } catch (ClassCastException e) {
                    continue;
                }
                if ((!crtcm.isConstructor()) && (!um.contains(crtcm)) &&
                    (crtcm.getScope().getScope() != act_class.getScope())) {
                    um.add(crtcm);
                    nd++;
                }
            }
        }

        return new NumericalResult(act_class, nd);
    }
}
