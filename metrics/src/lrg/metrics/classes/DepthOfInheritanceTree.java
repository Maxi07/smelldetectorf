package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.memoria.core.DataAbstraction;

/**
 * <b>Name:</b> Depth Of Inheritance Tree.
 * <br>
 * <b>Alternative:</b> Class Hierarchy Nesting Level.
 * <br>
 * <b>Acronym:</b> DIT
 * <br>
 * <b>Description:</b> Depth of Inheritance Tree (DIT) represents the
 * length of the tree from that class to the root class of the inheritanceCoupling tree.
 * Interfaces are not considered.
 * <br>
 * Obs.: We considered that the level of hierarcy root is 0.
 * <br>
 * <b>Source:</b> S.R. Chidamber, C.F. Kemerer. A metrics Suite for Object Oriented Design.
 * IEEE Transactions on Software Engineering, Vol.20, No.6, June 1994.
 */
public class DepthOfInheritanceTree extends ClassMeasure {

    public DepthOfInheritanceTree() {
        m_name = "DepthOfInheritanceTree";
        m_fullName = "Depth Of Inheritance Tree";
    }

    //tune the check condition according to the needs of this metric
    //protected boolean filter(lrg.memoria.lrg.insider.lrg.insider.core.Class cls) {
    //filter only "Normal Classes" and skip interfaces
    //   return (cls.getStatute() == lrg.memoria.lrg.insider.lrg.insider.core.Statute.NORMAL && !cls.isInterface());
    //}

    /**
     * Returns the depth of the tree from that class to the root class of
     * the inheritanceCoupling tree. When counted for a class, interfaces are not
     * considered.
     * <br>
     * Obs.: We considered that the level of hierarcy root is 0.
     */
    public Result measure(lrg.memoria.core.Class act_class) {
        int count = 0;

        if (!act_class.isInterface()) {
            DataAbstraction c;
            for (c = act_class; c != null && !c.getName().equals("Object"); c = c.getFirstAncestor())
                count++;
            //we consider that the level of the hierarchy root is 0
            count--;
        }
        return new NumericalResult(act_class, count);
    }
}
