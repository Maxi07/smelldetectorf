package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Class Variables.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data_static
 * <br>
 * <b>Description:</b> Number attributes which are class variable.
 * (i.e. data members declared using "static" attribute and without
 * using "final" attribute)
 * <br>
 * <b>Source:</b> t?
 */
public class ClassVariables extends AttributeIterator {

    public ClassVariables() {
        m_name = "ClassVariables";
        m_fullName = "Number Of Class Variables";
    }

    /**
     * Check if the current attribute is a class variable.
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return (!act_attr.isFinal() && act_attr.isStatic());
    }
}
