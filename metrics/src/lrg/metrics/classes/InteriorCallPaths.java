package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Calls To Functions Defined In The Class.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_callpi
 * <br>
 * <b>Description:</b> This metric represents the number of calls from the
 * current class's methods to functions defined inside of this class.
 * <br>
 * Obs.: Different calls to a method (from a method) count for one call.
 * <br>
 * <b>Source:</b> t?
 */
public class InteriorCallPaths extends ClassMeasure {

    public InteriorCallPaths() {
        m_name = "InteriorCallPaths";
        m_fullName = "Number Of Calls To Interior Methods";
    }

    /**
     * Measures the number of calls from the current class's methods to
     * functions defined inside of this class.
     * <br>
     * Obs.: Different calls to a method (from a specific method)
     * count for one call.
     */
    public Result measure(lrg.memoria.core.Class c) {
        int size, i;
        double count = 0;
        ArrayList method_list = c.getMethodList();
        lrg.memoria.core.Method act_method;
        lrg.metrics.methods.InteriorCallPaths mpi = new lrg.metrics.methods.InteriorCallPaths();
        NumericalResult temp_result;
        size = method_list.size();
        for (i = 0; i < size; i++) {
            act_method = (lrg.memoria.core.Method) method_list.get(i);
            if (act_method.getScope() == c) {
                temp_result = (NumericalResult) mpi.measure(act_method);
                count += temp_result.getValue();
            }
        }
        return new NumericalResult(c, count);
    }

}
