package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Tight Class Cohesion.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> TCC
 * <br>
 * <b>Description:</b> It is defined as the relative number of directly connected methods.
 * Two methods are directly connected if they access a common instance variable of the class.
 * <br>
 * Constructors and destructors are not considered as methods.
 * <br>
 * <b>Source:</b>
 */

public class TightClassCohesion extends ClassMeasure {

    public TightClassCohesion() {
        m_name = "TightClassCohesion";
        m_fullName = "Tight Class Cohesion";
    }

    /**
     * It is defined as the relative number of directly connected methods.
     * Two methods are directly connected if they access a common instance variable of the class.
     * <br>
     * Constructors and destructors are not considered as methods.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        double temp, den = 0, nom = 0;
        int i, j, nm, k;
        ArrayList ml = cls.getMethodList();
        ArrayList al2;
        HashSet al1 = new HashSet();
        lrg.memoria.core.Attribute crtatt;
        lrg.memoria.core.Method m1, m2;

        nm = ml.size();
        for (i = 0; i < nm - 1; i++)
            for (j = i + 1; j < nm; j++) {
                m1 = (lrg.memoria.core.Method)ml.get(i);
                m2 = (lrg.memoria.core.Method)ml.get(j);
                if (m1.isConstructor() || m2.isConstructor())
                    continue;
                for (k = 0; k < m1.getBody().getAccessList().size(); k++)
                    al1.add(((lrg.memoria.core.Access) m1.getBody().getAccessList().get(k)).getVariable());
                al2 = m2.getBody().getAccessList();
                for (k = 0; k < al2.size(); k++) {
                    try {
                        crtatt = (lrg.memoria.core.Attribute)
                                 ((lrg.memoria.core.Access) al2.get(k)).getVariable();
                    } catch (ClassCastException e) {
                        continue;
                    }
                    if (al1.contains(((lrg.memoria.core.Access) al2.get(k)).getVariable()))
                        if ((crtatt.getScope() == cls) && (!crtatt.isStatic())) {
                            den++;
                            break;
                        }
                }
                nom++;
            }

        if (den == 0) temp = 0;
        else temp = den / nom;

        return new NumericalResult(cls, temp);
    }
}
