package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Private Data Members.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data_priv
 * <br>
 * <b>Description:</b> Number of private attributes for a class.
 * <br>
 * <b>Source:</b> t?
 */
public class PrivateAttributes extends AttributeIterator {

    public PrivateAttributes() {
        m_name = "PrivateAttributes";
        m_fullName = "Number of Private Data Members";
    }

    /**
     * Check if the current attribute is private.
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return act_attr.isPrivate();
    }
}
