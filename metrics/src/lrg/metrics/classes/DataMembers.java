package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Data Members.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data
 * <br>
 * <b>Description:</b> This metric represents the total number of attributes
 * that a class has.
 * <br>
 * <b>Source:</b> t?
 */
public class DataMembers extends ClassMeasure {

    public DataMembers() {
        m_name = "DataMembers";
        m_fullName = "Number Of Data Members";
    }

    /**
     * Measures the total number of attributes that a class has.
     */
    public Result measure(lrg.memoria.core.Class c) {
        ArrayList attr_list = c.getAttributeList();
        return new NumericalResult(c, attr_list.size());
    }
}
