package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Public Data Members.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data_public
 * <br>
 * <b>Description:</b> Number of public attributes for a class.
 * <br>
 * <b>Source:</b> t?
 */
public class PublicAttributes extends AttributeIterator {

    public PublicAttributes() {
        m_name = "PublicAttributes";
        m_fullName = "Number of Public Data Members";
    }

    /**
     * Check if the current attribute is public.
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return act_attr.isPublic();
    }
}
