package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Data Abstraction Coupling 2.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> DAC2
 * <br>
 * <b>Description:</b> The number of different classes that are used as types in a class.
 * We are counting here not only the number of types of attributes (as in "Data Abstraction
 * Coupling" metric) but the total number of class types that appear inside the class.
 * (i.e. we consider attributes, local variables and parameters)
 * <br>
 * Different occurences of a class type is counted only once.
 * <br>
 * <b>Source:</b> ??
 */

public class DataAbstractionCoupling_2 extends ClassMeasure {

    private HashSet used_class_types;
    private int count;

    public DataAbstractionCoupling_2() {
        m_name = "DataAbstractionCoupling_2";
        m_fullName = "Data Abstraction Coupling 2";
    }

    /*
      Check if the current type is class type and if it wasn't already considered.
      If the above condition is true then count it (i.e. the type).
     */
    private void add(lrg.memoria.core.Type type) {
        if (type instanceof lrg.memoria.core.Class && !used_class_types.contains(type)) {
            used_class_types.add(type);
            count++;
        }
    }

    /**
     * Measures The number of different classes that are used as types in a class.
     * We are counting here not only the number of types of attributes (as in "Data Abstraction
     * Coupling" metric) but the total number of class types that appear inside the class.
     * (i.e. we consider attributes, local variables and parameters)
     * <br>
     * Different occurences of a class type is counted only once.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        int size, i;
        lrg.memoria.core.Method act_method;
        lrg.memoria.core.FunctionBody act_methodBody;
        lrg.memoria.core.InitializerBody act_initBody;
        ArrayList method_list, initializer_list;
        count = 0;
        used_class_types = new HashSet();

        used_class_types.add(cls);

        //add attributes
        addAttributes(cls);
        //add the number of class types that appear in "initialization blocks"
        initializer_list = cls.getInitializerList();
        size = initializer_list.size();
        for (i = 0; i < size; i++) {
            act_initBody = (lrg.memoria.core.InitializerBody) initializer_list.get(i);
            addLocalVars(act_initBody);
        }
        //add the number of class type parameters and local variables from methods
        method_list = cls.getMethodList();
        size = method_list.size();
        for (i = 0; i < size; i++) {
            act_method = (lrg.memoria.core.Method) method_list.get(i);
            if (act_method.getScope() == cls) {
                addParameters(act_method);
                act_methodBody = act_method.getBody();
                addLocalVars(act_methodBody);

            }
        }
        return new NumericalResult(cls, count);
    }

    //count the number of class type attributes of this class
    private void addAttributes(lrg.memoria.core.Class cls) {
        ArrayList attr_list;
        int size, i;
        lrg.memoria.core.Attribute act_attr;
        attr_list = cls.getAttributeList();
        size = attr_list.size();
        for (i = 0; i < size; i++) {
            act_attr = (lrg.memoria.core.Attribute) attr_list.get(i);
//      if (act_attr.getClassScope() == cls)
            add(act_attr.getType());
        }
    }

    //count parameters types    
    private void addParameters(lrg.memoria.core.Function act_method) {
        ArrayList param_list;
        lrg.memoria.core.Type act_type;
        int size, j;
        param_list = act_method.getParameterList();
        size = param_list.size();
        for (j = 0; j < size; j++) {
            act_type = ((lrg.memoria.core.Parameter) param_list.get(j)).getType();
            add(act_type);
        }
    }

    //count local-vars types (defined in methods)
    private void addLocalVars(lrg.memoria.core.Body act_body) {
        ArrayList var_list;
        lrg.memoria.core.Type act_type;
        int size, j;
        if (act_body != null) {
            var_list = act_body.getLocalVarList();
            size = var_list.size();
            for (j = 0; j < size; j++) {
                act_type = ((lrg.memoria.core.LocalVariable) var_list.get(j)).getType();
                add(act_type);
            }
        }
    }

}
