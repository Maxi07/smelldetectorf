package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

/**
 * <b>Name:</b> Number Of Public Base Classes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_base_publ
 * <br>
 * <b>Description:</b> Number of public base classes which are extended
 * by a class. The result is 0 if the base class isn't public and 1
 * otherwise.
 * <br>
 * <b>Source:</b> t?
 */
public class PublicBaseClasses extends ClassMeasure {

    public PublicBaseClasses() {
        m_name = "PublicBaseClasses";
        m_fullName = "Number Of Public Base Classes";
    }

    /**
     * Measures the number of public base classes which are extended
     * by a class. The result is 0 if the base class isn't public and 1
     * otherwise.
     */
    public Result measure(lrg.memoria.core.Class c) {
        int count = 0;
        lrg.memoria.core.Class currentAncestor;
        lrg.memoria.core.ModelElementList ancestors = c.getAncestorsList();
        for (int i = 0; i < ancestors.size(); i++) {
            currentAncestor = (lrg.memoria.core.Class)ancestors.get(i);
            if (currentAncestor != null && !currentAncestor.getName().equals("Object") &&
                !currentAncestor.isInterface() && currentAncestor.isPublic())
                count ++;
        }
        return new NumericalResult(c, count);
    }
}


