package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Data Abstraction Coupling.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> DAC
 * <br>
 * <b>Description:</b> The number of different classes that are used as types of
 * attributes in a class.
 * <br>
 * <b>Source:</b> W. Li, S. Henry. Maintenance Metrics for the Object Oriented Paradigm.
 * IEEE Proc. First International Software Metrics Symp., pp. 52-60, May 21-22, 1993.
 */

public class DataAbstractionCoupling extends ClassMeasure {

    public DataAbstractionCoupling() {
        m_name = "DataAbstractionCoupling";
        m_fullName = "Data Abstraction Coupling";
    }

    /**
     * Measures the number of different classes that are used as types of attributes in a class.
     */
    public Result measure(lrg.memoria.core.Class act_class) {
        int count, size, i;
        lrg.memoria.core.Type act_type;
        lrg.memoria.core.Attribute act_attr;
        ArrayList attr_list = act_class.getAttributeList();
        HashSet used_class_types = new HashSet();
        size = attr_list.size();
        count = 0;
        for (i = 0; i < size; i++) {
            act_attr = (lrg.memoria.core.Attribute) attr_list.get(i);
            if (act_attr.getScope() == act_class) {
                act_type = act_attr.getType();
                if (act_type instanceof lrg.memoria.core.Class && !used_class_types.contains(act_type)) {
                    used_class_types.add(act_type);
                    count++;
                }
            }
        }
        return new NumericalResult(act_class, count);
    }
}


