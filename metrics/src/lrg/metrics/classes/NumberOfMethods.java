package lrg.metrics.classes;

/**
 * <b>Name:</b> Number Of Methods.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOM
 * <br>
 * <b>Description:</b> It is the number of methods defined (not-inherited) in the measured class.
 * <br>
 * Constructors and Destructors are excluded.
 * <br>
 * <b>Source:</b>
 */

public class NumberOfMethods extends MethodIterator {

    public NumberOfMethods() {
        m_name = "NumberOfMethods";
        m_fullName = "Number Of Methods";
    }

    /**
     * Returns true if the method is not constructor.
     */
    protected boolean check(lrg.memoria.core.Method m) {
        return !m.isConstructor();
    }
}
