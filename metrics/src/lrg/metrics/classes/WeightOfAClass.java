package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;
import java.util.ArrayList;

/**
 * <b>Name:</b> Weight of a Class.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> WOC
 * <br>
 * <b>Description:</b> Is the number of non-accessor methods in the interface of the class
 * divided by the total number of interface members.
 * <br>
 * Inherited members are not counted. The members that belongs to
 * the interface of the class are public methods and public fields.
 * Constructors and destructors are eliminated.
 * <br>
 * <b>Source:</b>
 */

public class WeightOfAClass extends ClassMeasure {

    public WeightOfAClass() {
        m_name = "WeightOfAClass";
        m_fullName = "Weight of a Class";
    }

    /**
     * Is the number of non-accessor methods in the interface of the class
     * divided by the total number of interface members.
     * <br>
     * Inherited members are not counted. The members that belongs to
     * the interface of the class are public methods and public fields.
     * Constructors and destructors are eliminated.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        double temp, den = 0, nom =0;
        int i;
        ArrayList methl = cls.getMethodList(), attl = cls.getAttributeList();
        lrg.memoria.core.Method crtmeth;

        for (i = 0; i < methl.size(); i++) {
            crtmeth = (lrg.memoria.core.Method) methl.get(i);
            if (crtmeth.isPublic() && (!crtmeth.isConstructor())) {
                nom++;
                if (!crtmeth.isAccessor())
                    den++;
            }
        }

        for (i = 0; i < attl.size(); i++) {
            if (((lrg.memoria.core.Attribute) attl.get(i)).isPublic())
                nom++;
        }

        if (nom != 0)
                    temp = den / nom;
                else
                    if (den == 0)
                        temp = 0;
                    else //if we have no interface methods, we return an Integer.MAX_VALUE
                        temp = Integer.MAX_VALUE;

        return new NumericalResult(cls, temp);
    }
}
