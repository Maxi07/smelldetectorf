package lrg.memoria.core;

public interface TypeDecorator extends Type {
    Type getRootType();

    Type getDecoratedType();

    Scope getScope();

    boolean isPointer();

    boolean isArray();

    boolean isTypedefAlias();

    boolean isReference();
}
