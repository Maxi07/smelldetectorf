package lrg.memoria.core;

public class AnnotationPropertyValuePair extends ModelElement{
	private AnnotationProperty ap;
	private String value;
	
	public AnnotationPropertyValuePair(){
	}
	
	public void accept(ModelVisitor v) {
		// TODO Auto-generated method stub	
	}

	public AnnotationProperty getAp() {
		return ap;
	}

	public void setAp(AnnotationProperty ap) {
		this.ap = ap;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
