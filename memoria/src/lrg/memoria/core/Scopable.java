package lrg.memoria.core;

/**
 * Represent the model entities which can be put in a scope.
 */
public interface Scopable {
    public Scope getScope();    
}
