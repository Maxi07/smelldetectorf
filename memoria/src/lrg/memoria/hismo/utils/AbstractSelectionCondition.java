package lrg.memoria.hismo.utils;



/**
 * Created by IntelliJ IDEA.
 * User: ratiud
 * Date: Jan 30, 2004
 * Time: 12:00:18 PM
 * To change this template use Options | File Templates.
 */
public interface AbstractSelectionCondition {
    public boolean isSatisfiedBy(lrg.memoria.hismo.core.AbstractHistory aHistory);
}
