package lrg.memoria.hismo.core;

public class GlobalVariableHistoryGroup extends AbstractHistoryGroup {

    public AbstractHistory createHistory(VersionsList versionsList) {
        return new GlobalVariableHistory(versionsList);
    }

    public AbstractHistoryGroup createHistoryGroup() {
        return new GlobalVariableHistoryGroup();
    }
}
