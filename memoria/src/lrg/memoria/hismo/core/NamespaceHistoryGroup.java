package lrg.memoria.hismo.core;

public class NamespaceHistoryGroup extends AbstractHistoryGroup {

    public AbstractHistory createHistory(VersionsList versionsList) {
        return new NamespaceHistory(versionsList);
    }

    public AbstractHistoryGroup createHistoryGroup() {
        return new NamespaceHistoryGroup();
    }
}

