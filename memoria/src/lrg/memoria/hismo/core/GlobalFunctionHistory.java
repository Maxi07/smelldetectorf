package lrg.memoria.hismo.core;

public class GlobalFunctionHistory extends FunctionHistory {

    private NamespaceHistory namespaceHistory;

    public GlobalFunctionHistory(NamespaceHistory nsh) {
        namespaceHistory = nsh;
    }

    public NamespaceHistory getNamespaceHistory() {
        return namespaceHistory;
    }

    public GlobalFunctionHistory(VersionsList versions) {
        super(versions);
    }    
}