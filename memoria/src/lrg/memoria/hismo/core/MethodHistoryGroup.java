package lrg.memoria.hismo.core;


/**
 * Created by IntelliJ IDEA.
 * User: ratiud
 * Date: Jan 31, 2004
 * Time: 10:39:35 AM
 * To change this template use Options | File Templates.
 */
public class MethodHistoryGroup extends AbstractHistoryGroup {

    public AbstractHistory createHistory(VersionsList versionsList) {
        return new MethodHistory(versionsList);
    }

    public AbstractHistoryGroup createHistoryGroup() {
        return new MethodHistoryGroup();
    }
}
