package lrg.memoria.importer.recoder;

import recoder.java.ProgramElement;

interface Listener {
    void enterModelComponent(ProgramElement pe);

    void leaveModelComponent(ProgramElement pe);
}