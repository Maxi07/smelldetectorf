package lrg.memoria.importer.recoder;

public class ConstructorDeclarationListener extends MethodDeclarationListener {
    static {
        ModelConstructor.addFactory("lrg.memoria.importer.recoder.ConstructorDeclarationListener", new MethodDeclarationListener.Factory());
    }

    private ConstructorDeclarationListener() {
    }
}
