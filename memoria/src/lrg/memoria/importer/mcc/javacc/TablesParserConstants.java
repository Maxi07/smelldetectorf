/* Generated By:JavaCC: Do not edit this line. TablesParserConstants.java */
package lrg.memoria.importer.mcc.javacc;

public interface TablesParserConstants {

  int EOF = 0;
  int EOLN = 4;
  int ERROR = 5;
  int ONLY_DECLARED = 6;
  int NO_ONE = 7;
  int NULL = 8;
  int NOT_INIT = 9;
  int NO_NAME = 10;
  int INIT_NULL_BODY = 11;
  int UNKNOWN = 12;
  int INTEGER_NUMBER = 13;
  int STRING = 14;

  int DEFAULT = 0;

  String[] tokenImage = {
    "<EOF>",
    "\"\\\"\"",
    "\"\\t\"",
    "\"\\r\"",
    "\"\\n\"",
    "\"<ERROR>\"",
    "\"<ONLY_DECLARED>\"",
    "\"<NO_ONE>\"",
    "\"NULL\"",
    "\"<NOT_INIT>\"",
    "\"<NO_NAME>\"",
    "\"<INIT_NULL_BODY>\"",
    "\"<UNKNOWN>\"",
    "<INTEGER_NUMBER>",
    "<STRING>",
  };

}
