package lrg.memoria.importer.mcc.loader;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 5, 2004
 * Time: 1:26:06 PM
 * To change this template use Options | File Templates.
 */
public interface TypeVisitor
{
    void setId(Integer id);
    void setLocation(String fileName, String startPosition, String stopPosition);
    void setName(String name);
    void setKind(String kind);
    void setPackageId(String packageId);
    void setNamespaceId(String namespaceId);
    void setIsAbstract(String isAbstract);
    void setIsInterface(String isInterface);
    void setIsGeneric(String isGeneric);
    void setScopeId(String scopeId);
    void setDecoratedType(String decoratedType);
    void addType();
    void typesEOF();    
}
