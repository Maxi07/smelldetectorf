package lrg.memoria.importer.mcc.loader;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 10, 2004
 * Time: 10:45:49 AM
 * To change this template use Options | File Templates.
 */
public interface NamespaceVisitor {
    void setId(Integer id);
    void setName(String name);
    void addNamespace();
}
