package lrg.memoria.importer.mcc.loader;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 10, 2004
 * Time: 11:31:57 AM
 * To change this template use Options | File Templates.
 */
public interface VariableVisitor {
    void setId(Integer id);
    void setFileName(String fileName);
    void setDeclarationLine(Integer declarationLine);
    void setVariableName(String variableName);
    void setKindOf(String kindOf);
    void setTypeId(String typeId);
    void setAccess(String access);
    void setClassId(String classId);
    void setBodyId(String bodyId);
    void setPackageId(String packageId);
    void setNamespaceId(String namespaceId);
    void setIsStatic(Integer isStatic);
    void setIsConst(Integer isConst);
    void setRefersTo(String refersTo);
    void addVariable();
    void variablesEOF();        
}
