package lrg.memoria.importer.mcc.loader;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 7, 2004
 * Time: 10:33:14 AM
 * To change this template use Options | File Templates.
 */
public interface BodyVisitor {
    void setId(Integer id);
    void setLocation(String fileName, Integer startPosition, Integer stopPosition);
    void setPackageId(Integer packageId);
    void setNoDecisions(Integer noDecisions);
    void setNoAnd(Integer noAnd);
    void setNoOr(Integer noOr);
    void setNoReturn(Integer noReturn);
    void setNoCatch(Integer noCatch);
    void setNoLoops(Integer noLoops);
    void setMaxNesting(Integer noMaxNesting);
    void setNoStatements(Integer noStatements);
    void setNoCodeLine(Integer noCodeLine);
    void setCyclomaticNumber(Integer cyclo);    
    void addBody();
}
