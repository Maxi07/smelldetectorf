package lrg.memoria.importer.mcc.loader;

public interface ChainVisitor {
    void setId(Integer id);
    void setT2TRelationId(String T2TRelationId);
    void setTemplateInstanceId(String templateInstanceId);
    void addChain();
}
