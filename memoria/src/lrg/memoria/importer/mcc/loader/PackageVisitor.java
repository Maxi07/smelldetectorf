package lrg.memoria.importer.mcc.loader;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 10, 2004
 * Time: 11:09:12 AM
 * To change this template use Options | File Templates.
 */
public interface PackageVisitor {
    void setId(Integer id);
    void setName(String name);
    void addPackage();
}
