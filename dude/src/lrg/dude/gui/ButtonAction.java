package lrg.dude.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 08.05.2004
 * Time: 19:33:50
 * To change this template use File | Settings | File Templates.
 */
public class ButtonAction extends AbstractAction {
    private GUI parent;

    public final static String PATH   = "path";
    public final static String SAVE  = "save";
    public final static String COMPUTE = "compute";
    public final static String STATISTICS = "statistics";
    public final static String HELP   = "help";
    public final static String ABOUT  = "about";

    public ButtonAction(GUI parent){
        this.parent = parent;
    }

    public void actionPerformed(ActionEvent ae) {
        String command = ae.getActionCommand( );
        if(command == PATH){
            parent.setPathAction();
        }
        else if(command == COMPUTE){
            parent.computeAction();
        }
        else if(command == STATISTICS){
            parent.showStatisticsAction();
        }
        else if(command == SAVE){
            parent.saveAction();
        }
        else if(command == HELP){
            parent.helpAction();
        }
        else if(command == ABOUT){
            parent.aboutAction();
        }
    }
}