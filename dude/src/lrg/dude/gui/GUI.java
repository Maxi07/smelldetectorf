package lrg.dude.gui;

import lrg.dude.duplication.*;
import lrg.common.utils.ProgressObserver;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;


/**
 * Created by IntelliJ IDEA.
 * User: Richard Wettel
 * Date: 06.05.2004
 */
public class GUI extends JPanel implements Observer, ProgressObserver {
    private String startingPath = null;
    private Processor processor = null;
    private Parameters params;
    private StringCompareStrategy compareStrategy;
    private Duplication[] duplicationFound = null;
    private long startTime;
    private long lastElapsedTime;

    private JSpinner spinnerMinL, spinnerMaxLB, spinnerMinEC;
    private JComboBox comparisonStrategyCB;
    private JSpinner similarityThreshold;
    private JCheckBox cboxIgnoreComments;
    private JButton pathButton, saveButton, computeButton,
    statsButton, helpButton, aboutButton;

    private JTable table;
    private TableSorter sorter;
    private JScrollPane tablePane;


    private FileViewer fileViewer1, fileViewer2;

    private JLabel statusLabel;

    private JProgressBar progressBar;
    private int progressValue;


    public GUI() {
        //Do the layout.
        //setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        setLayout(new BorderLayout(10, 10));
        JPanel upperPanel = new JPanel();
        upperPanel.setLayout(new BoxLayout(upperPanel, BoxLayout.LINE_AXIS));
        JPanel middlePanel = new JPanel(new GridLayout(1, 1));
        middlePanel.setBorder(BorderFactory.createTitledBorder("Results"));
        JPanel lowerPanel = new JPanel(new GridLayout(1, 2));


        //controls
        ButtonAction buttonAction = new ButtonAction(this);
        pathButton = new JButton("Set path", new ImageIcon("res/graphics/Open.png"));
        pathButton.setActionCommand(ButtonAction.PATH);
        pathButton.addActionListener(buttonAction);
        pathButton.setToolTipText("Sets the starting path");

        saveButton = new JButton("Save results", new ImageIcon("res/graphics/Save.png"));
        saveButton.setActionCommand(ButtonAction.SAVE);
        saveButton.addActionListener(buttonAction);
        saveButton.setToolTipText("Save the results list (in the sorted order) to a file");
        saveButton.setEnabled(false);

        computeButton = new JButton("Find duplicates", new ImageIcon("res/graphics/Find.png"));
        computeButton.setActionCommand(ButtonAction.COMPUTE);
        computeButton.addActionListener(buttonAction);
        computeButton.setToolTipText("Search for duplication chains");
        computeButton.setEnabled(false);

        statsButton = new JButton("Statistics", new ImageIcon("res/graphics/Statistics.png"));
        statsButton.setActionCommand(ButtonAction.STATISTICS);
        statsButton.addActionListener(buttonAction);
        statsButton.setToolTipText("Show statistics on the last search");
        statsButton.setEnabled(false);

        helpButton = new JButton("Help", new ImageIcon("res/graphics/Help.png"));
        helpButton.setActionCommand(ButtonAction.HELP);
        helpButton.addActionListener(buttonAction);
        helpButton.setToolTipText("Help documentation");

        aboutButton = new JButton("About", new ImageIcon("res/graphics/About.png"));
        aboutButton.setActionCommand(ButtonAction.ABOUT);
        aboutButton.addActionListener(buttonAction);
        aboutButton.setToolTipText("Info about this program");

        JPanel controlPanel = new JPanel(new GridLayout(2, 3));
        controlPanel.add(pathButton);
        controlPanel.add(computeButton);
        controlPanel.add(helpButton);
        controlPanel.add(saveButton);
        controlPanel.add(statsButton);
        controlPanel.add(aboutButton);

        controlPanel.setBorder(BorderFactory.createTitledBorder("Controls"));



        //parameters
        spinnerMinL = new JSpinner(new SpinnerNumberModel(7, 1, 999, 1));
        spinnerMaxLB = new JSpinner(new SpinnerNumberModel(2, 0, 10, 1));
        spinnerMinEC = new JSpinner(new SpinnerNumberModel(2, 1, 10, 1));
        //nou
        cboxIgnoreComments = new JCheckBox("Ignore Comments", true);
        comparisonStrategyCB = new JComboBox(new String[]{"compareTo", "Levenshtein Distance"});
        similarityThreshold = new JSpinner(new SpinnerNumberModel(0.2, 0.0, 1.0, 0.05));

        //panoul de parametri
        JPanel parametersPanel = new JPanel(new GridLayout(0, 2));
        parametersPanel.add(new JLabel("MinLength"));
        parametersPanel.add(spinnerMinL);
        parametersPanel.add(new JLabel("MaxLineBias"));
        parametersPanel.add(spinnerMaxLB);
        parametersPanel.add(new JLabel("MinExactChunk"));
        parametersPanel.add(spinnerMinEC);
        parametersPanel.add(cboxIgnoreComments);
        parametersPanel.add(new JLabel());  //just for layout purposes
        parametersPanel.add(new JLabel("Comparison Strategy"));
        parametersPanel.add(comparisonStrategyCB);
        parametersPanel.add(new JLabel("LD/length Threshold"));
        parametersPanel.add(similarityThreshold);
        parametersPanel.setBorder(BorderFactory.createTitledBorder("Parameters"));

        //partea de sus finala
        upperPanel.add(controlPanel);
        upperPanel.add(Box.createHorizontalGlue());
        upperPanel.add(parametersPanel);


        sorter = new TableSorter(new MyTableModel()); //ADDED THIS
        table = new JTable(sorter);             //NEW
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        sorter.setTableHeader(table.getTableHeader()); //ADDED THIS


        //Set up tool tips for column headers.
        table.getTableHeader().setToolTipText("Click to specify sorting; Control-Click to specify secondary sorting");
        table.getTableHeader().setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        table.getTableHeader().setVisible(false);



        //establish the selection of rows
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        //Ask to be notified of selection changes.
        ListSelectionModel rowSM = table.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                //Ignore extra messages.
                if (e.getValueIsAdjusting()) return;
                ListSelectionModel lsm = (ListSelectionModel) e.getSource();
                if (!lsm.isSelectionEmpty()) {
                    int realSelectedRow = sorter.modelIndex(lsm.getMinSelectionIndex());
                    viewDuplicate(realSelectedRow);
                }
            }
        });

        tablePane = new JScrollPane(table);
        tablePane.setBorder(BorderFactory.createTitledBorder("Duplicates found"));



        //file1 contents
        fileViewer1 = new FileViewer();
        fileViewer2 = new FileViewer();

        JPanel visualizationContainer = new JPanel(new GridLayout(1, 2));
        visualizationContainer.add(fileViewer1);
        visualizationContainer.add(fileViewer2);
        visualizationContainer.setBorder(BorderFactory.createTitledBorder("Duplicate viewer"));

        JSplitPane resultsPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                tablePane, visualizationContainer);

        middlePanel.add(resultsPane);

        statusLabel = new JLabel("<status bar>");

        lowerPanel.add(statusLabel);
        progressBar = new JProgressBar(0, 100); //for now 100, don't know the real value yet
        progressBar.setStringPainted(true); //get space for the string
        progressBar.setBorderPainted(true);
        progressBar.setVisible(false);
        lowerPanel.add(progressBar);

        add(upperPanel, BorderLayout.PAGE_START);
        add(middlePanel, BorderLayout.CENTER);
        add(lowerPanel, BorderLayout.PAGE_END);

    }

    public void viewDuplicate(int index) {
        CodeFragment code1 = duplicationFound[index].getReferenceCode();
        CodeFragment code2 = duplicationFound[index].getDuplicateCode();
        fileViewer1.viewFile(startingPath, code1.getEntityName(), duplicationFound[index].getReferenceCode());
        fileViewer2.viewFile(startingPath, code2.getEntityName(), duplicationFound[index].getDuplicateCode());
    }

    public void setPathAction() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setCurrentDirectory(new File("."));
        int returnVal = chooser.showDialog(this, "Set path");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            startingPath = chooser.getSelectedFile().getAbsolutePath();
            statusLabel.setText("Starting path set to: " + startingPath);
            computeButton.setEnabled(true);
        } else
            statusLabel.setText("Path setting operation cancelled");
    }

    public void computeAction() {
        computeButton.setEnabled(false);
        pathButton.setEnabled(false);
        saveButton.setEnabled(false);
        statsButton.setEnabled(false);
        progressBar.setString("Reading files...");          //but don't paint it
        progressBar.setIndeterminate(true);
        progressBar.setVisible(true);

        if (startingPath != null) {
            switch (comparisonStrategyCB.getSelectedIndex()) {
                case 0:
                    processor = new SuffixTreeProcessor(startingPath, this,
                	// processor = new Processor(startingPath, this,
                    		new IdenticalCompareStrategy());
                    break;
                case 1:
                    processor = new Processor(startingPath, this,
                            new LevenshteinDistanceStrategy(((Double) similarityThreshold.getValue()).doubleValue()));
                    break;
                default:
                    System.out.println("No such comparison strategy");
            }
            params = new Parameters(((Integer) spinnerMinL.getValue()).intValue(),
                    ((Integer) spinnerMaxLB.getValue()).intValue(),
                    ((Integer) spinnerMinEC.getValue()).intValue(),
                    !cboxIgnoreComments.isSelected());
            processor.setParams(params);

            processor.attach(this);
            statusLabel.setText("Please wait...");
            startTime = System.currentTimeMillis();
            processor.start();
        }
    }

    public void saveAction() {
        if (duplicationFound.length > 0) {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("."));
            int returnVal = chooser.showSaveDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = chooser.getSelectedFile();
                if (file.exists()) {
                    Object[] options = {"OK", "CANCEL"};
                    int n = JOptionPane.showOptionDialog(this,
                            "The file already exists. Overwrite?",
                            "Overwrite existing file?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            options,
                            options[0]);
                    if (n == 1) {
                        statusLabel.setText("Save operation cancelled");
                        return;
                    }
                }
                statusLabel.setText("Saving to file: " + file.getName());
                saveFormattedToFile(file);
                statusLabel.setText("Results saved to file: " + file.getName());
            } else
                statusLabel.setText("Save operation cancelled");

        }
    }


    private void saveFormattedToFile(File file) {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        } catch (FileNotFoundException fnfe) {
            statusLabel.setText("ERROR: no such file " + file.getName());
            return;
        }
        try {
            out.write("************** Starting path & Parameters ***************");
            out.newLine();
            out.write("Date: " + new Date(System.currentTimeMillis()));
            out.newLine();
            out.write("Starting path: " + startingPath);
            out.newLine();
            out.write("Search parameters: ");
            out.newLine();
            out.write("\tMinimum length: " + params.getMinLength());
            out.newLine();
            out.write("\tMaximum line bias: " + params.getMaxLineBias());
            out.newLine();
            out.write("\tMinimum exact chunk: " + params.getMinExactChunk());
            out.newLine();
            out.write("\tIgnore comments: " + !params.isConsiderComments());
            out.newLine();
            out.newLine();
            for (int i = 0; i < duplicationFound.length; i++) {
                for (int j = 0; j < sorter.getColumnCount(); j++) {
                    switch (j) {
                        case 0:
                            out.newLine();
                            out.write("************** " + (i + 1) + " ***************");
                            out.newLine();
                            out.newLine();
                            out.write("file: ");
                            break;
                        case 1:
                            out.write(", start line: ");
                            break;
                        case 2:
                            out.write(", end line: ");
                            break;
                        case 3:
                            out.newLine();
                            out.write("file: ");
                            break;
                        case 4:
                            out.write(", start line: ");
                            break;
                        case 5:
                            out.write(", end line: ");
                            break;
                        case 6:
                            out.newLine();
                            out.write("Lines of copied code: ");
                            break;
                        case 7:
                            out.newLine();
                            out.write("Lines in file: ");
                            break;
                        case 8:
                            out.newLine();
                            out.write("Duplication chain type: ");
                            break;
                        case 9:
                            out.newLine();
                            out.write("Duplication chain Signature: ");
                            break;
                    }
                    out.write((sorter.getValueAt(i, j)).toString());
                }
                for (int k = 0; k < 3; k++)
                    out.newLine();
            }
        } catch (IOException ioe) {
            statusLabel.setText("ERROR: could not write to file " + file.getName());
            return;
        }
        try {
            out.close();
        } catch (IOException ioe) {
            statusLabel.setText("ERROR: could not close the file " + file.getName());
            return;
        }

    }

    public void helpAction() {
        JEditorPane editorPane = new JEditorPane();
        editorPane.setEditable(false);
        String page = "res/helpfiles/help.html";
        File localFile = new File(page);
        page = "file:///" + localFile.getAbsolutePath();
        URL helpURL = null;
        try {
            helpURL = new URL(page);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (helpURL != null) {
            try {
                editorPane.setPage(helpURL);
            } catch (IOException e) {
                System.err.println("Attempted to read a bad URL: " + helpURL);
            }
        } else {
            System.err.println("Couldn't find file: " + page);
        }

        //Put the editor pane in a scroll pane.
        JScrollPane editorScrollPane = new JScrollPane(editorPane);
        editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        editorScrollPane.setPreferredSize(new Dimension(800, 600));
        editorScrollPane.setMinimumSize(new Dimension(640, 480));

        JFrame helpFrame = new JFrame("Help on DuDe");
        helpFrame.setContentPane(editorScrollPane);
        //Display the window.
        helpFrame.pack();
        helpFrame.setVisible(true);
    }

    public void aboutAction() {
        StringBuffer about = new StringBuffer();
        about.append("DuDe, build 0.83\n");
        about.append("Author: Richard Wettel\n");
        about.append("wettel@cs.utt.ro\n");
        about.append("LOOSE Research Group\n");
        about.append("http://loose.utt.ro\n");
        about.append("All rights reserved, 2004\n");
        JOptionPane.showConfirmDialog(this, about.toString(), "About Dude",
                JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE, new ImageIcon("res/graphics/Icon.gif"));
    }

    public void showStatisticsAction() {
        StringBuffer stats = new StringBuffer("Statistical data:\n\n");
        if (processor != null) {
            long noOfRelevantLines = processor.getMatrixLinesLength();
            long noOfDuplicatedLines = processor.getNumberOfDuplicatedLines();
            double percentage = noOfDuplicatedLines * 100.0 / noOfRelevantLines;
            stats.append("Starting path: " + startingPath + "\n");
            stats.append("Number of analyzed files: " + processor.getNumberOfEntities() + "\n");
            stats.append("Total number of lines: " + processor.getNumberOfRawLines() + "\n");
            stats.append("Number of relevant lines: " + noOfRelevantLines + "\n");
            stats.append("Number of duplicated lines: " + noOfDuplicatedLines +
                    " (" + Math.round(percentage) + "%)\n");
            stats.append("Number of matrix cells: " + processor.getNumberOfDots() + "\n");
            stats.append("Number of duplication chains: " + duplicationFound.length + "\n");
            stats.append("Elapsed time: " + TimeMeasurer.convertTimeToString(lastElapsedTime) + "\n");
        } else {
            stats.append("No statistics data available");
        }
        JOptionPane.showConfirmDialog(this, stats.toString(), "Statistics",
                JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE, new ImageIcon("res/graphics/Statistics.png"));
    }

    public void getDuplication(Subject source) {
        lastElapsedTime = System.currentTimeMillis() - startTime;
        duplicationFound = ((Processor) source).getSearchResults();
        updateResultsTable();
        Toolkit.getDefaultToolkit().beep();
        computeButton.setEnabled(true);
        progressBar.setValue(progressBar.getMinimum());
        progressBar.setString(""); //hide % string
    }

    private void updateResultsTable() {
        statsButton.setEnabled(true);
        pathButton.setEnabled(true);

        fileViewer1.clear();
        fileViewer2.clear();

        int numberOfDuplicationChains = duplicationFound.length;

        if (duplicationFound != null && duplicationFound.length > 0) {
            //dimensiunile coloanelor de tabel
            Object[][] data = new Object[numberOfDuplicationChains][10];
            Duplication currentDuplication = null;
            for (int i = 0; i < numberOfDuplicationChains; i++) {
                currentDuplication = duplicationFound[i];
                data[i][0] = currentDuplication.getReferenceCode().getEntityName();
                data[i][1] = new Integer((int) currentDuplication.getReferenceCode().getBeginLine());
                data[i][2] = new Integer((int) currentDuplication.getReferenceCode().getEndLine());
                data[i][3] = currentDuplication.getDuplicateCode().getEntityName();
                data[i][4] = new Integer((int) currentDuplication.getDuplicateCode().getBeginLine());
                data[i][5] = new Integer((int) currentDuplication.getDuplicateCode().getEndLine());
                data[i][6] = new Integer((int) (currentDuplication.copiedLength()));
                data[i][7] = new Integer((int) (currentDuplication.realLength()));
                data[i][8] = currentDuplication.getType();
                data[i][9] = currentDuplication.getSignature();
            }


            table.getTableHeader().setVisible(true);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
            sorter.setTableModel(new MyTableModel(data));

            TableColumn column = null;
            for (int i = 0; i < 6; i++) {
                column = table.getColumnModel().getColumn(i);
                switch (i) {
                    case 0:
                    case 3:
                        column.setPreferredWidth(200);
                        break;
                    case 1:
                    case 2:
                    case 4:
                    case 5:
                        column.setPreferredWidth(30);
                        break;
                    case 6:
                    case 7:
                        column.setPreferredWidth(40);
                        break;
                    case 8:
                        column.setPreferredWidth(50);
                        break;
                    case 9:
                        column.setPreferredWidth(200);
                        break;
                }
            }
            saveButton.setEnabled(true);
        } else {
            sorter.setTableModel(new MyTableModel());
            table.getTableHeader().setVisible(false);
            saveButton.setEnabled(false);
        }
        progressBar.setVisible(false);
        statusLabel.setText("Found " + numberOfDuplicationChains + " duplication chain" +
                (numberOfDuplicationChains == 1 ? "" : "s") + " in " +
                TimeMeasurer.convertTimeToString(lastElapsedTime));
    }

    public void setMaxValue(int i) {
        progressBar.setMaximum(i);
        progressValue = 0;
        progressBar.setIndeterminate(false);
        progressBar.setString(null); //display % string
    }

    public void increment() {
        progressBar.setValue(++progressValue);
    }
}
