package lrg.dude.duplication;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: LrgMember
 * Date: Apr 17, 2004
 * Time: 3:39:40 PM
 * To change this template use File | Settings | File Templates.
 */

//this cleaner should be put to work after a whitespace cleaner did its job
public class NoiseCleaner extends CleaningDecorator {
    private final String noiseFile = "noise.dat";

    public NoiseCleaner(CleaningDecorator next) {
        super(next);
    }

    protected StringList specificClean(StringList text) {
        String[] noise = getNoise();
        for (int i = 0; i < text.size(); i++)
            for (int n = 0; n < noise.length; n++)
                if (text.get(i).equals(noise[n]))
                    text.set(i, "");
        return text;
    }

    private String[] getNoise() {
        ArrayList lines = new ArrayList();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(noiseFile)));
            String linie = null;
            while ((linie = in.readLine()) != null) {
                lines.add(linie);
            }
            in.close();
        } catch (FileNotFoundException fe) {
            System.out.println("Nu exista fisierul " + noiseFile + ": " + fe);
        } catch (IOException ioe) {
            System.out.println("Eroare citire fisier : " + ioe);
        }
        String[] sintacticElements = new String[lines.size()];
        for (int i = 0; i < sintacticElements.length; i++)
            sintacticElements[i] = (String) lines.get(i);
        return sintacticElements;
    }
}
