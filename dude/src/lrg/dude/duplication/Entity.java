package lrg.dude.duplication;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 25.02.2004
 * Time: 18:34:44
 * To change this template use duplication.File | Settings | duplication.File Templates.
 */

public interface Entity {
    public String getName();

    public StringList getCode();

    public int getNoOfRelevantLines(); //for clustering reasons

    public void setNoOfRelevantLines(int norl); 
}
