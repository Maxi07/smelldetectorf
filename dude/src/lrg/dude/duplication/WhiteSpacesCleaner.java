package lrg.dude.duplication;

/**
 * Created by IntelliJ IDEA.
 * User: LrgMember
 * Date: Apr 17, 2004
 * Time: 2:52:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class WhiteSpacesCleaner extends CleaningDecorator {

    public WhiteSpacesCleaner(CleaningDecorator next) {
        super(next);
    }

    public StringList specificClean(StringList text) {
        String[] whitespace = getWhiteSpace();
        String currentLine = null;
        for (int i = 0; i < text.size(); i++) {
            for (int w = 0; w < whitespace.length; w++) {
                text.set(i, text.get(i).replaceAll(whitespace[w], ""));
            }
        }
        return text;
    }

    /**
     * Reads the Strings considered as whitespace
     *
     * @return array of whitespace Strings
     */
    private String[] getWhiteSpace() {
        String[] whitespace;

        whitespace = new String[3];
        whitespace[0] = " ";
        whitespace[1] = "\t";
        whitespace[2] = "\n";

        return whitespace;
    }

}
