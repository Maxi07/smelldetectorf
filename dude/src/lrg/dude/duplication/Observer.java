package lrg.dude.duplication;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 13.05.2004
 * Time: 23:06:26
 * To change this template use File | Settings | File Templates.
 */
public interface Observer {
    public void getDuplication(Subject source);
}
