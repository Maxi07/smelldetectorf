package lrg.dude.duplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

public class VirtualColumnMatrix {

    public VirtualColumnMatrix(int size, int startColumnIndex) {
    	this.startColumnIndex = startColumnIndex;
        list = new ArrayList();
        for (int i = 0; i < size; i++)
            list.add(new HashMap());

    }
    
    public void removeAll() {
    	freeColumns(startColumnIndex, startColumnIndex + list.size());
    	list.removeAll(list);
    }

    public Boolean get(int x, int y) {
        HashMap map = (HashMap) list.get(y - startColumnIndex);
        return (Boolean) map.get(new Integer(x));
    }

    public void set(int x, int y, Boolean element) {
        HashMap map = (HashMap) list.get(y - startColumnIndex);
        map.put(new Integer(x), element);
    }

    public Iterator iterator(int i) {
        HashMap map = (HashMap) list.get(i - startColumnIndex);
        TreeSet set = new TreeSet(map.keySet());
        return set.iterator();
    }

    public void freeColumns(int start, int end) {
        for (int i = start - startColumnIndex; i < end - startColumnIndex; i++)
            ((HashMap) list.get(i)).clear();

    }

    private ArrayList list;
    private int startColumnIndex;
}