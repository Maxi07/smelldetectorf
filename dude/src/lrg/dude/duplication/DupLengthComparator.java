package lrg.dude.duplication;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 14.03.2004
 * Time: 21:39:57
 * To change this template use Options | File Templates.
 */
public class DupLengthComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        Duplication d1 = (Duplication) o1;
        Duplication d2 = (Duplication) o2;
        if (d1.copiedLength() == d2.copiedLength())
            return 0;
        else if (d1.copiedLength() > d2.copiedLength())
            return 1;
        else
            return -1;
    }
}
